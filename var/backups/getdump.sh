#!/bin/bash
#
# Put this file into /var/backups directory
#
#
#

# Remote Server Data
DBNAME=eastwestfashion
DBUSER=gendalf
DBPASW=QM45TXfB5HWbdW9A
DBHOST=eastwestfashion.ru
SSHAUTH=ewf@eastwestfashion.ru

# 
SOURCE="${BASH_SOURCE[0]}"
SQL_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $SQL_DIR
SQL_FILE=$SQL_DIR/$DBNAME.sql
BZIP_FILE=$SQL_FILE.bz2

if [ -f $BZIP_FILE ];
then
	echo "Delete previous bz2 file if exist..."
	rm -f $BZIP_FILE
fi

if [ -f $SQL_FILE ];
then
	echo "Backup previous SQL file if exist..."
	mv -f $SQL_FILE $SQL_FILE.$(date +%Y-%m-%d_%H.%M).bkp.sql
fi

echo "Download MySQL Database: $DBNAME"
ssh $SSHAUTH "mysqldump -p$DBPASW -u $DBUSER --opt $DBNAME | bzip2 -9" > $BZIP_FILE

##### Alternatieve
#mysqldump -p$DBPASW -u $DBUSER --opt $DBNAME -h $DBHOST > $SQL_FILE

if [ -f $SQL_FILE ];
then
    echo "Unzip file"
    bunzip2 $BZIP_FILE
    echo "Restore database locally."
    mysql -f -u $DBUSER -p$DBPASW $DBNAME < $SQL_FILE
    echo "Re-configure database for local usage."
    mysql -f -u $DBUSER -p$DBPASW $DBNAME < $SQL_DIR/configure.prj.sql
else
    echo "No downloaded archive file: $BZIP_FILE"
fi

echo "Done "$(date "+%Y-%m-%d %H:%M")
