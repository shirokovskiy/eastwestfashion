#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
SQL_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
SQL_FILE=$SQL_DIR/ewf.sql
cd $SQL_DIR

if [ -f $SQL_FILE ];
then
    mysql --defaults-extra-file=mydump.cnf eastwestfashion < ewf.sql
    mysql --defaults-extra-file=mydump.cnf eastwestfashion < configure.prj.sql

    rm -rf var/cache/*
    rm -rf var/session/*

    7z a ewf.7z ewf.sql
    rm ewf.sql
else
    echo "NO file $SQL_FILE"
    scp dbewf:~/ewf.7z .
    7z e ewf.7z
fi

