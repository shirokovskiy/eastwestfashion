UPDATE `core_config_data` SET `value` = 'http://www.eastwestfashion.du/' WHERE `config_id` IN (5,6) AND `value` LIKE 'http://www.eastwestfashion.ru/';
TRUNCATE TABLE `log_url`;
TRUNCATE TABLE `log_url_info`;
TRUNCATE TABLE `log_visitor`;
TRUNCATE TABLE `log_visitor_info`;
TRUNCATE TABLE `log_visitor_online`;
-- UPDATE `catalog_product_entity_int` SET `value` = 2 WHERE `attribute_id` = 96 AND `value` = 1;
TRUNCATE `core_url_rewrite`;
