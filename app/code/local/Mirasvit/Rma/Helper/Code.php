<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   RMA
 * @version   1.0.5
 * @revision  469
 * @copyright Copyright (C) 2014 Mirasvit (http://mirasvit.com/)
 */


class Mirasvit_Rma_Helper_Code extends Mirasvit_MstCore_Helper_Code
{
    protected $k = "R3LA8HXYZF";
    protected $s = "RMA";
    protected $o = "5620";
    protected $v = "1.0.5";
    protected $r = "469";
    protected $p = "MCore/1.0.7-123|Rma/1.0.5-346";
}
