<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   RMA
 * @version   1.0.5
 * @revision  469
 * @copyright Copyright (C) 2014 Mirasvit (http://mirasvit.com/)
 */


class Mirasvit_Rma_Helper_Help extends Mirasvit_MstCore_Helper_Help
{
    protected $_help = array(
        'system' => array(
        	'general_return_address' => '',
        	'general_default_status' => '',
        	'general_default_user' => '',
        	'general_return_period' => '',
        	'general_is_require_shipping_confirmation' => '',
        	'general_shipping_confirmation_text' => '',
        	'general_is_gift_active' => '',
        	'general_is_helpdesk_active' => '',
        	'general_is_manual_creditmemo' => '',
        	'general_credit_memo_adjustment_fee' => '',
        	'general_is_refund_shipping_fee' => '',
        	'general_is_online_refund' => '',
        	'general_is_send_exchange_order_confirmation' => '',
        	'general_brand_attribute' => '',
        	'frontend_is_active' => '',
        	'policy_is_active' => '',
        	'policy_policy_block' => '',
        	'number_format' => '',
        	'number_counter_start' => '',
        	'number_counter_step' => '',
        	'number_counter_length' => '',
        	'notification_sender_email' => '',
        	'notification_customer_email_template' => '',
        	'notification_admin_email_template' => '',
        ),
    );

    /************************/

}