<?php

class Hardik_Ajaxcart_Model_Observer {

    public function addToCartEvent($observer) {

        $request = Mage::app()->getFrontController()->getRequest();

        if (!$request->getParam('in_cart') && !$request->getParam('is_checkout')) {

            Mage::getSingleton('checkout/session')->setNoCartRedirect(true);
			
			$_product = $observer->getProduct();
			$atr = array();
			if (is_array($request->getParam('super_attribute', false))) {
				$_attributes = $_product->getTypeInstance(true)->getConfigurableAttributesAsArray($_product);
				foreach($_attributes as $_attribute){
					foreach($request->getParam('super_attribute', false) as $index=>$value) {
						if ($_attribute['attribute_id']==$index) {
							foreach($_attribute['values'] as $value2) {
								if ($value == $value2['value_index']) {
									$atr[$_attribute['attribute_code']] = array(0 => $_attribute['store_label'], 1 => $value2['label']);
								}
							}
						}
					}
				}
			}
			$qty = $request->getParam('qty');
			if (!$qty) {
				$qty = 1;
			}
            $_response = Mage::getModel('ajaxcart/response')
					->setProductManufacturer($observer->getProduct()->getAttributeText('manufacturer'))
                    ->setProductName($observer->getProduct()->getName())
					->setProductQty($qty)
					->setAtr($atr)
                    ->setMessage(Mage::helper('checkout')->__('%s was added into cart.', $observer->getProduct()->getName()));

            //append updated blocks
            $_response->addUpdatedBlocks($_response);

            $_response->send();
        }
        if ($request->getParam('is_checkout')) {

            Mage::getSingleton('checkout/session')->setNoCartRedirect(true);

            $_response = Mage::getModel('ajaxcart/response')
                    ->setProductName($observer->getProduct()->getName())
                    ->setMessage(Mage::helper('checkout')->__('%s was added into cart.', $observer->getProduct()->getName()));
            $_response->send();
        }
    }

    public function updateItemEvent($observer) {

        $request = Mage::app()->getFrontController()->getRequest();

        if (!$request->getParam('in_cart') && !$request->getParam('is_checkout')) {

            Mage::getSingleton('checkout/session')->setNoCartRedirect(true);

            $_response = Mage::getModel('ajaxcart/response')
                    ->setMessage(Mage::helper('checkout')->__('Item was updated.'));

            //append updated blocks
            $_response->addUpdatedBlocks($_response);

            $_response->send();
        }
        if ($request->getParam('is_checkout')) {

            Mage::getSingleton('checkout/session')->setNoCartRedirect(true);

            $_response = Mage::getModel('ajaxcart/response')
                    ->setMessage(Mage::helper('checkout')->__('Item was updated.'));
            $_response->send();
        }
    }

    public function getConfigurableOptions($observer) {
        $is_ajax = Mage::app()->getFrontController()->getRequest()->getParam('ajax');

        if($is_ajax) {
            $_response = Mage::getModel('ajaxcart/response');

            $product = Mage::registry('current_product');
            if (!$product->isConfigurable() && !$product->getTypeId() == 'bundle'){return false;exit;}

            //append configurable options block
            $_response->addConfigurableOptionsBlock($_response);
            $_response->send();
        }
        return;
    }

    public function getGroupProductOptions() {
        $id = Mage::app()->getFrontController()->getRequest()->getParam('product');
        $options = Mage::app()->getFrontController()->getRequest()->getParam('super_group');

        if($id) {
            $product = Mage::getModel('catalog/product')->load($id);
            if($product->getData()) {
                if($product->getTypeId() == 'grouped' && !$options) {
                    $_response = Mage::getModel('ajaxcart/response');
                    Mage::register('product', $product);
                    Mage::register('current_product', $product);

                    //add group product's items block
                    $_response->addGroupProductItemsBlock($_response);
                    $_response->send();
                }
            }
        }
    }

}