<?php
class Df_Forum2_Admin_IndexController extends Df_Core_Controller_Admin {
	/** @return void */
	public function indexAction() {
		try {
			$this
				->loadLayout()
				->_setActiveMenu('rm/forum2')
				->renderLayout()
			;
		}
		catch(Exception $e) {
			df_handle_entry_point_exception($e);
		}
	}

	/**
	 * @override
	 * @return bool
	 */
	protected function _isAllowed() {
		/** @var bool $result */
		$result =
				parent::_isAllowed()
			&&
				df_enabled(Df_Core_Feature::FORUM2)
			&&
				df_mage()->admin()->session()->isAllowed('admin/rm/forum2')
		;
		return $result;
	}
}