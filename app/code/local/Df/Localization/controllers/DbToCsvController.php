<?php
class Df_Localization_DbToCsvController extends Mage_Adminhtml_Controller_Action {
	/** @return void */
	public function indexAction() {
		$this
			->_title($this->__('System'))
			->_title($this->__('Локализация'))
			->_title('Запись переводов из БД в CSV')
			->loadLayout()
		;
		$this
			->_setActiveMenu('system/df_localization')
			->renderLayout()
		;
	}

	/** @return void */
	public function exportAction() {
		Df_Localization_Model_Exporter::i()->process();
		$this->_redirect('*/*/*');
	}

	/** @return bool */
	protected function _isAllowed() {
		return
				df_enabled(Df_Core_Feature::LOCALIZATION)
			&&
				df_mage()->admin()->session()->isAllowed('system/df_localization/dbToCsv')
		;
	}
}