<?php
class Df_Localization_Model_Translator_Dictionary
	extends Df_Core_Model_SimpleXml_Parser_Entity_Singleton {
	/**
	 * @override
	 * @return Df_Varien_Simplexml_Element
	 */
	public function getSimpleXmlElement() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_Varien_Simplexml_Element $document */
			$document = @simplexml_load_file($this->getPathFull(), Df_Varien_Simplexml_Element::_CLASS);
			df_assert($document);
			$this->{__METHOD__} = df_array_first($document->xpath('/dictionary'));
			df_assert($this->{__METHOD__});
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param string $text
	 * @param string $code
	 * @return string|null
	 * @throws Exception
	 */
	public function translate($text, $code) {
		/** @var bool $isProcessing */
		/**
		 * Не допускаем рекурсивность данного метода,
		 * потому что она может привести к зависанию системы.
		 *
		 * Рекурсивность может возникнуть
		 * из-за вызова метода Mage_Core_Block_Template::getTemplate ниже.
		 *
		 * @var bool $isProcessing
		 */
		static $isProcessing = false;
		/** @var string|null $result */
		$result = null;
		if ((false === $isProcessing) && $this->hasEntry($text)) {
			$isProcessing = true;
			try {
				if (!df()->state()->hasBlocksBeenGenerated()) {
					if (!Mage::app()->getRequest()->isXmlHttpRequest()) {
						/**
						 * Вызов из макета.
						 * Пока никак не обрабатываем.
						 * Не помню, почему.
						 * Надо выяснить и изложить причину в комментарии.
						 */
						Df_Localization_Model_Translator::s()->log('блоки ещё не созданы');
					}
					else {
						/**
						 * Вызов из контроллера, обработка асинхронного запроса
						 */
						Df_Localization_Model_Translator::s()->log(
							'вызов из контроллера, обработка асинхронного запроса'
						);
						$result = $this->handleTranslateForController($text, $code);
					}
				}
				else {
					if (!df()->state()->hasLayoutRenderingBeenStarted()) {
						/**
						 * Вызов из контроллера.
						 */
						Df_Localization_Model_Translator::s()->log('рисование не началось');
						$result = $this->handleTranslateForController($text, $code);
					}
					else {
						/**
						 * Вызов из шаблона.
						 */
						Df_Localization_Model_Translator::s()->log('рисование шаблона');
						$result = $this->handleTranslateForTemplate($text, $code);
					}
				}
				$isProcessing = false;
			}
			catch(Exception $e) {
				$isProcessing = false;
				throw $e;
			}
		}
		return $result;
	}

	/**
	 * @param string $a
	 * @param string $b
	 * @return bool
	 */
	private function _continue($a, $b) {return $a && !in_array($a, array('*', $b));}

	/**
	 * @param string $code
	 * @return string
	 */
	private function getModuleNameFromCode($code) {
		/** @var string $result */
		$result = '';
		/** @var array $codeParts */
		$codeParts =
			explode(
				Mage_Core_Model_Translate::SCOPE_SEPARATOR
				,$code
			)
		;
		if (1 < count($codeParts)) {
			$result = df_array_first($codeParts);
		}
		return $result;
	}

	/** @return Df_Localization_Model_Translator_Dictionary_Modules */
	private function getModules() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = Df_Localization_Model_Translator_Dictionary_Modules::i($this->e());
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param string $text
	 * @param string $code
	 * @return string|null
	 * @throws Exception
	 */
	private function handleTranslateForController($text, $code) {
		/** @var string $result */
		$result = null;
		/** @var Mage_Core_Controller_Varien_Action|null $currentController */
		$currentController = df()->state()->getController();
		if (!is_null($currentController)) {
			/** @var string $currentControllerClass */
			$currentControllerClass = get_class($currentController);
			/** @var string|null $currentModuleName */
			$currentModuleName =
				/**
				 * Приоритет должен отдаваться имени модуля, указанному в коде,
				 * потому что вызов метода __ необязательно был произведён в контексте $this
				 */
				$this->getModuleNameFromCode($code)
			;
			if (!$currentModuleName) {
				$currentModuleName =
					df()->reflection()->getModuleNameByControllerClassName($currentControllerClass)
				;
			}
			df_assert_string_not_empty($currentModuleName);
			/** @var Df_Localization_Model_Translator_Dictionary_Module|null $currentModule */
			$currentModule = $this->getModules()->findById($currentModuleName);
			if (!is_null($currentModule)) {
				/**
				 * например: «checkout_onepage_index»
				 * @var string $currentControllerAction
				 */
				$currentControllerAction =
					$currentController
						->getFullActionName(
							$delimiter = '_'
						)
				;
				foreach ($currentModule->getControllers() as $controller) {
					/** @var Df_Localization_Model_Translator_Dictionary_ModulePart_Controller $controller */
					if (
							$this->_continue($controller->getAction(), $currentControllerAction)
						||
							$this->_continue($controller->getControllerClass(), $currentControllerClass)
					) {
						continue;
					}
					/** @var Df_Localization_Model_Translator_Dictionary_ModulePart_Terms $terms */
					$terms = $controller->getTerms();
					/** @var Df_Localization_Model_Translator_Dictionary_ModulePart_Term|null $term */
					$term = $terms->findById($text);
					if (!is_null($term)) {
						$result = $term->getTextTranslated();
						break;
					}
				}
			}
		}
		return $result;
	}

	/**
	 * @param string $text
	 * @param string $code
	 * @return string|null
	 * @throws Exception
	 */
	private function handleTranslateForTemplate($text, $code) {
		/** @var string $result */
		$result = null;
		/** @var Mage_Core_Block_Abstract|null $currentBlock */
		$currentBlock = df()->state()->getCurrentBlock();
		/** @var string|null $currentModuleName */
		$currentModuleName =
			/**
			 * Приоритет должен отдаваться имени модуля, указанному в коде,
			 * потому что вызов метода __ необязательно был произведён в контексте $this
			 */
			$this->getModuleNameFromCode($code)
		;
		if (!$currentModuleName && !is_null($currentBlock)) {
			$currentModuleName = $currentBlock->getModuleName();
		}
		if ($currentModuleName) {
			/** @var Df_Localization_Model_Translator_Dictionary_Module|null $currentModule */
			$currentModule = $this->getModules()->findById($currentModuleName);
			if (!is_null($currentModule)) {
				/** @var string $currentTemplate */
				$currentTemplate = null;
				if ($currentBlock && ($currentBlock instanceof Mage_Core_Block_Template)) {
					/** @var Mage_Core_Block_Template $currentBlockTemplated */
					$currentBlockTemplated = $currentBlock;
					$currentTemplate = $currentBlockTemplated->getTemplate();
				}
				foreach ($currentModule->getBlocks() as $block) {
					/** @var Df_Localization_Model_Translator_Dictionary_ModulePart_Block $block */
					if (
							$this->_continue($block->getTemplate(), $currentTemplate)
						||
							$this->_continue(
								$block->getBlockClass(), $currentBlock ? get_class($currentBlock) : ''
							)
						||
							$this->_continue(
								$block->getName(), $currentBlock ? $currentBlock->getNameInLayout() : ''
							)
					) {
						continue;
					}
					/** @var Df_Localization_Model_Translator_Dictionary_ModulePart_Terms $terms */
					$terms = $block->getTerms();
					/** @var Df_Localization_Model_Translator_Dictionary_ModulePart_Term|null $term */
					$term = $terms->findById($text);
					if (!is_null($term)) {
						$result = $term->getTextTranslated();
						break;
					}
				}
			}
		}
		return $result;
	}

	/**
	 * Этот метод используется только для быстрой проверки
	 * наличия в словаре перевода конкретного текста.
	 * @see Df_Localization_Model_Translator_Dictionary::translate()
	 * @param string $text
	 * @return bool
	 */
	private function hasEntry($text) {
		/**
		 * Метод реализован именно таким способом ради ускорения.
		 * Данная реализация работает быстрее, нежели использование @see in_array
		 * @link http://stackoverflow.com/a/5036972/254475
		 */
		if (!isset($this->_entries)) {
			/** @var bool $canUseCache */
			$canUseCache = Mage::app()->useCache('translate');
			/** @var string $cacheId */
			$cacheId = __METHOD__;
			if ($canUseCache) {
				$this->_entries = @unserialize(Mage::app()->loadCache($cacheId));
			}
			if (!isset($this->_entries) || !is_array($this->_entries)) {
				foreach ($this->e()->xpath('//en_US') as $entry) {
					/** @var Df_Varien_Simplexml_Element $entry */
					$this->_entries[(string)$entry] = true;
				}
				if ($canUseCache) {
					Mage::app()->saveCache(
						serialize($this->_entries)
						, $cacheId
						, array(Mage_Core_Model_Translate::CACHE_TAG)
						, null
					);
				}
			}
		}
		return isset($this->_entries[$text]);
	}
	/** @var array(string => bool) */
	private $_entries;

	/** @return string */
	private function getPathFull() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				df_concat_path(
					Mage::getConfig()->getModuleDir('etc', 'Df_Localization')
					,'rm'
					,$this->getPathLocal()
				)
			;
			if (!file_exists($this->{__METHOD__})) {
				df_error('Не найден требуемый файл «%s».', $this->{__METHOD__});
			}
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	private function getPathLocal() {return $this->cfg(self::P__PATH_LOCAL);}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_prop(self::P__PATH_LOCAL, self::V_STRING_NE);
	}
	const _CLASS = __CLASS__;
	const P__PATH_LOCAL = 'path_local';
	/**
	 * @param string $pathLocal
	 * @return Df_Localization_Model_Translator_Dictionary
	 */
	public static function i($pathLocal) {return new self(array(self::P__PATH_LOCAL => $pathLocal));}
}