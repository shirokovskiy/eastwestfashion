<?php
class Df_Localization_Model_Translator_Dictionary_ModulePart_Block
	extends Df_Core_Model_SimpleXml_Parser_Entity {
	/** @return string|null */
	public function getBlockClass() {
		return $this->getAttribute('class');
	}

	/**
	 * @override
	 * @return string
	 */
	public function getId() {
		return
			$this->getName()
			? $this->getName()
			: implode('::', array($this->getBlockClass(), $this->getTemplate()))
		;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getName() {
		return
			df_nts(
				$this->getAttribute('name')
			)
		;
	}

	/** @return string|null */
	public function getTemplate() {
		return $this->getAttribute('template');
	}
	
	/** @return Df_Localization_Model_Translator_Dictionary_ModulePart_Terms */
	public function getTerms() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				Df_Localization_Model_Translator_Dictionary_ModulePart_Terms::i($this->e())
			;
		}
		return $this->{__METHOD__};
	}

	/** Используется из @see Df_Localization_Model_Translator_Dictionary_ModulePart_Blocks::getItemClass() */
	const _CLASS = __CLASS__;
}