<?php
class Df_Localization_Model_Morpher extends Df_Core_Model_Abstract {
	/**
	 * @param string $word
	 * @return Df_Localization_Model_Morpher_Response
	 */
	public function getResponse($word) {
		if (!isset($this->{__METHOD__}[$word])) {
			df_param_string_not_empty($word, 0);
			/** @var Df_Localization_Model_Morpher_Response $result */
			$result = Df_Localization_Model_Morpher_Response::i($word, $this->getResponseAsText($word));
			if (!$result->isValid()) {
				df_error(
					'При вычислении склонений слова «%s» произошёл сбой: «%s».'
					,$word
					,$result->getErrorMessage()
				);
			}
			$this->{__METHOD__}[$word] = $result;
		}
		return $this->{__METHOD__}[$word];
	}

	/**
	 * @param string $word
	 * @return Df_Localization_Model_Morpher_Response|null
	 */
	public function getResponseSilent($word) {
		/** @var Df_Localization_Model_Morpher_Response|null $result */
		$result = null;
		try {
			$result = $this->getResponse($word);
		}
		catch (Exception $e) {}
		return $result;
	}

	/** @return Mage_Core_Model_Cache */
	private function getCache() {return Mage::app()->getCacheInstance();}

	/**
	 * @param string $word
	 * @return string
	 */
	private function getResponseAsText($word) {
		if (!isset($this->{__METHOD__}[$word])) {
			df_param_string_not_empty($word, 0);
			/**
			 * Обратите внимание, что ключ кэширования не должен содержать русские буквы,
			 * потому что когда кэш хранится в файлах, то русские буквы будут заменены на символ «_»,
			 * и имя файла будет выглядеть как «mage---b26_DF_LOCALIZATION_MODEL_MORPHER________».
			 * Чтобы избавиться от русских букв при сохранении уникальности ключа, испольузем функцию md5.
			 * @var string $cacheKey
			 */
			$cacheKey = implode('::', array(get_class($this), md5($word)));
			/** @var string $result */
			$result = $this->getCache()->load($cacheKey);
			if (false === $result) {
				$result = Df_Localization_Model_Morpher_Request::i($word)->getResponse();
				$this->getCache()->save($result, $cacheKey);
			}
			df_result_string_not_empty($result);
			$this->{__METHOD__}[$word] = $result;
		}
		return $this->{__METHOD__}[$word];
	}

	const _CLASS = __CLASS__;

	/** @return Df_Localization_Model_Morpher */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}