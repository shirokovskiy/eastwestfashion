<?php
class Df_Sales_Block_Order_View extends Mage_Sales_Block_Order_View {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * Escape html entities
	 * @override
	 * @param mixed $data
	 * @param array $allowedTags[optional]
	 * @return string
	 */
	public function escapeHtml($data, $allowedTags = null) {
		if (
				df_enabled(Df_Core_Feature::SALES)
			&&
				df_cfg()->sales()->orderComments()->preserveLineBreaksInCustomerAccount()
		) {
			if (is_null($allowedTags)) {
				$allowedTags = array();
			}

			$allowedTags =
				rm_array_unique_fast(
					array_merge(
						$allowedTags
						,array('br')
					)
				)
			;
			$data = nl2br($data);
		}


		/** @var string $result */
		$result =
			df_text()->escapeHtml($data, $allowedTags)
		;
		df_result_string($result);
		return $result;
	}

}