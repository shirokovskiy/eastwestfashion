<?php
class Df_Sales_Model_Resource_Setup extends Df_Core_Model_Resource_Setup_Abstract {
	/** @return Df_Sales_Model_Resource_Setup */
	public function install_2_23_5() {
		Df_Sales_Model_Setup_2_23_5::s()->process();
		return $this;
	}
}