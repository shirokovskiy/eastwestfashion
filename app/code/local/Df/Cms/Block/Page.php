<?php
class Df_Cms_Block_Page extends Mage_Cms_Block_Page {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @return string[]
	 */
	public function getCacheKeyInfo() {
		/** @var string[] $result */
		$result = parent::getCacheKeyInfo();
		if (
				df_module_enabled(Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->cmsPage()
		) {
			$result =
				array_merge(
					$result
					,array(
						get_class($this)
						,$this->getPage()->getId()
						/**
						 * Здесь md5 не нужно,
						 * потому что @see Mage_Core_Block_Abstract::getCacheKey()
						 * использует аналогичную md5 функцию sha1
						 */
						,$this->getMessagesBlock()->toHtml()
					)
				)
			;
		}
		return $result;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		if (
				df_module_enabled(Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->cmsPage()
		) {
			$this->setData('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
		}
	}

}