<?php
class Df_Cms_Block_Frontend_Menu_Contents extends Mage_Core_Block_Abstract {
	/** @return Df_Cms_Model_ContentsMenu */
	public function getMenu() {return $this->_getData(self::P__MENU);}

	/**
	 * @override
	 * @return string
	 */
	protected function _toHtml() {
		/** @var string $result */
		$result = '';
		if (
				df_cfg()->cms()->hierarchy()->isEnabled()
			&&
				df_enabled(Df_Core_Feature::CMS_2)
			&&
				count($this->getMenu()->getApplicators())
		) {
			/** @var array $renderedNodes */
			$renderedNodes = array();
			foreach (df_h()->cms()->getTree()->getTree()->getNodes() as $node) {
				/** @var Df_Cms_Varien_Data_Tree_Node $node */
				/** @var Df_Cms_Model_Hierarchy_Node $cmsNode */
				$cmsNode = $node->getCmsNode();
				if (
						!$cmsNode->getParentNodeId()
					&&
						in_array($cmsNode->getId(), $this->getMenu()->getRootNodeIds())
				) {
					$renderedNodes[]=
						$this->createHtmlListItem(
							$this->renderNode(
								$node
								,(0 === $cmsNode->getMenuLevelsDown())
								? null
								: $cmsNode->getMenuLevelsDown()
							)
							,df_clean(array(
								'class' => count($node->getChildren()) ? 'parent' : null
							))
						)					;
				}
			}
			if (count($renderedNodes)) {
				$result =
					Df_Core_Model_Format_Html_Tag::output(
						$this->createHtmlList(df_concat($renderedNodes), array('class' => 'cms-menu'))
						,'div'
						,array('class' => 'df-cms-menu-wrapper')
					)
				;
			}
		}
		df_result_string($result);
		return $result;
	}

	/**
	 * @param string $content
	 * @param array $attributes[optional]
	 * @return string
	 */
	private function createHtmlList($content, array $attributes = array()) {
		df_param_string($content, 0);
		/** @var string $result */
		$result =
			Df_Core_Model_Format_Html_Tag::output(
				$content
				,'ul'
				,$attributes
			)
		;
		df_result_string($result);
		return $result;
	}

	/**
	 * @param string $content
	 * @param array $attributes[optional]
	 * @return string
	 */
	private function createHtmlListItem($content, array $attributes = array()) {
		df_param_string($content, 0);
		/** @var string $result */
		$result =
			Df_Core_Model_Format_Html_Tag::output(
				$content
				,'li'
				,$attributes
			)
		;
		df_result_string($result);
		return $result;
	}

	/**
	 * @param Df_Cms_Varien_Data_Tree_Node $parent
	 * @param int|null $menuLevelsDown[optional]
	 * @return string
	 */
	private function renderChildren(Df_Cms_Varien_Data_Tree_Node $parent, $menuLevelsDown = null) {
		/** @var array $renderedNodes */
		$renderedNodes = array();
		if (is_null($menuLevelsDown) || (0 < $menuLevelsDown)) {
			foreach ($parent->getChildren() as $childNode) {
				/** @var Df_Cms_Varien_Data_Tree_Node $childNode */
				$renderedNodes[]=
					$this->createHtmlListItem(
						$this->renderNode($childNode, $menuLevelsDown)
						,df_clean(array(
							'class' => $childNode->getChildren() ? 'parent' : null
						))
					)
				;
			}
		}
		return !$renderedNodes ? '' : $this->createHtmlList(df_concat($renderedNodes));
	}

	/**
	 * @param Df_Cms_Varien_Data_Tree_Node $node
	 * @return string
	 */
	private function renderLabel(Df_Cms_Varien_Data_Tree_Node $node) {
		/** @var Df_Cms_Model_Hierarchy_Node $cmsNode */
		$cmsNode = $node->getCmsNode();
		return
					df_h()->cms()->getCurrentNode()
				&&
					($cmsNode->getId() === df_h()->cms()->getCurrentNode()->getId())
			?
				Df_Core_Model_Format_Html_Tag::output(
					Df_Core_Model_Format_Html_Tag::output($cmsNode->getLabel(), 'strong'), 'span'
				)
			:
				Df_Core_Model_Format_Html_Tag::output(
					$cmsNode->getLabel(), 'a', array('href' => $cmsNode->getUrl())
				)
		;
	}

	/**
	 * @param Df_Cms_Varien_Data_Tree_Node $node
	 * @param int|null $menuLevelsDown[optional]
	 * @return string
	 */
	private function renderNode(Df_Cms_Varien_Data_Tree_Node $node, $menuLevelsDown = null) {
		return
			df_concat(
				$this->renderLabel($node)
				,$this->renderChildren($node, is_null($menuLevelsDown) ? null : $menuLevelsDown - 1)
			)
		;
	}

	const _CLASS = __CLASS__;
	const P__MENU = 'menu';
	/**
	 * @param Df_Cms_Model_ContentsMenu $menu
	 * @return Df_Cms_Block_Frontend_Menu_Contents
	 */
	public static function i(Df_Cms_Model_ContentsMenu $menu) {
		return df_block(__CLASS__, null, array(self::P__MENU => $menu));
	}
}