<?php
class Df_Cms_Model_Resource_Page_Collection extends Mage_Cms_Model_Resource_Page_Collection {
	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Cms_Model_Page::mf(), Df_Cms_Model_Resource_Page::mf());
	}
	const _CLASS = __CLASS__;

	/** @return Df_Cms_Model_Resource_Page_Collection */
	public static function i() {return new self;}
}