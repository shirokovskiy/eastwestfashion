<?php
/**
 * @method Df_Cms_Model_Resource_Page getResource()
 */
class Df_Cms_Model_Page extends Mage_Cms_Model_Page {
	/**
	 * @return Df_Cms_Model_Page
	 * @throws Exception
	 */
	public function deleteRm() {
		/** @var Mage_Core_Model_Store $currentStore */
		$currentStore = Mage::app()->getStore();
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		/** @var bool $updateMode */
		$updateMode = Mage::app()->getUpdateMode();
		/**
		 * Очень важный момент!
		 * Если Magento находится в режиме обновления,
		 * то Mage_Core_Model_App::getStore()
		 * всегда будет возвращать Mage_Core_Model_App::getDefaultStore(),
		 * даже для такого кода: Mage_Core_Model_App::getStore(999).
		 * Это приводит к весьма некорректному поведению системы в некоторых ситцациях,
		 * когда мы обновляем товарные разделы своим установочным скриптом:
		 * @see Mage_Catalog_Model_Resource_Abstract::_saveAttributeValue():
		 * $storeId = (int)Mage::app()->getStore($object->getStoreId())->getId();
		 * Этот код заведомо вернёт неправильный результат!
		 */
		Mage::app()->setUpdateMode(false);
		try {
			$this->delete();
		}
		catch(Exception $e) {
			Mage::app()->setCurrentStore($currentStore);
			Mage::app()->setUpdateMode($updateMode);
			throw $e;
		}
		Mage::app()->setCurrentStore($currentStore);
		Mage::app()->setUpdateMode($updateMode);
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Cms_Model_Resource_Page::mf());
	}
	const _CLASS = __CLASS__;
	const P__IS_ACTIVE = 'is_active';
	const P__TITLE = 'title';

	/** @return Df_Cms_Model_Resource_Page_Collection */
	public static function c() {return self::s()->getCollection();}
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Cms_Model_Page
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/**
	 * @static
	 * @param int|string $id
	 * @param string|null $field [optional]
	 * @return Df_Cms_Model_Page
	 */
	public static function ld($id, $field = null) {return df_load(self::i(), $id, $field);}
	/**
	 * @see Df_Cms_Model_Resource_Page_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf(__CLASS__);}
	/** @return Df_Cms_Model_Page */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}