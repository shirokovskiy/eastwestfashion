<?php
class Df_Cms_Model_Tree extends Df_Core_Model_Abstract {
	/** @return Varien_Data_Tree */
	public function getTree() {
		if (!isset($this->{__METHOD__})) {
			/** @var Varien_Data_Tree $result */
			$result = new Varien_Data_Tree();
			$this->_nodesMap = array();
			foreach ($this->getCmsNodes() as $cmsNode) {
				/** @var Df_Cms_Model_Hierarchy_Node $cmsNode */
				/** @var Df_Cms_Varien_Data_Tree_Node $varienNode */
				$varienNode = $this->convertCmsNodeToVarienDataTreeNode($cmsNode, $result);
				if (0 === $cmsNode->getParentNodeId()) {
					/**
					 * Корневой узел
					 */
					$result->addNode($varienNode);
					$this->_nodesMap[rm_nat($cmsNode->getId())] = $varienNode;
				}
				else {
					/**
					 * Некорневой узел.
					 * Надо найти родителя данного узла, и связать данный узел с родителем.
					 * Обратите внимание, что благодаря вызову
					 * Df_Cms_Model_Resource_Hierarchy_Node_Collection::setTreeOrder
					 * родительский узел уже должен присутствовать в дереве.
					 */
					/** @var Df_Cms_Varien_Data_Tree_Node|null $parentNode */
					$parentNode = $this->getParentForCmsNodeInVarienDataTree($cmsNode);
					if (!is_null($parentNode)) {
						$parentNode->addChild($varienNode);
						$this->_nodesMap[rm_nat($cmsNode->getId())] = $varienNode;
					}
				}
			}
			/**
			 * Дублируем детей в поле «children_nodes»,
			 * потому что это поле использует метод
			 * Mage_Catalog_Block_Navigation::_renderCategoryMenuItemHtml
			 * в случае, если Mage::helper('catalog/category_flat')->isEnabled()
			 */
			foreach ($result->getNodes() as $node) {
				/** @var Df_Cms_Varien_Data_Tree_Node $node */
				df_assert($node instanceof Df_Cms_Varien_Data_Tree_Node);
				/** @var Varien_Data_Tree_Node_Collection $children */
				$children = $node->getChildren();
				$node->setData('children_nodes', $children->getNodes());
			}
			$this->{__METHOD__} = $result;
		}
		df_assert($this->{__METHOD__} instanceof Varien_Data_Tree);
		return $this->{__METHOD__};
	}

	/**
	 * @var Df_Cms_Varien_Data_Tree_Node[]
	 */
	private $_nodesMap = array();

	/**
	 * @param Df_Cms_Model_Hierarchy_Node $cmsNode
	 * @param Varien_Data_Tree $tree
	 * @return Df_Cms_Varien_Data_Tree_Node
	 */
	private function convertCmsNodeToVarienDataTreeNode(
		Df_Cms_Model_Hierarchy_Node $cmsNode, Varien_Data_Tree $tree
	) {
		/** @var bool $isItTemplateMela */
		static $isItTemplateMela;
		if (!isset($isItTemplateMela)) {
				$isItTemplateMela =
					/**
					 * TemplateMela использует старую схему работы с меню
					 * @link http://magento-forum.ru/forum/293/
					 * @link http://magento-forum.ru/forum/316/
					 *
					 * Обратите внимание, что алгоритм ниже чуть сложнее, но и точнее, чем
					 * 		'TM_CustomMenu_Block_Navigation'
					 * 	===
					 * 		Mage::getConfig()->getBlockClassName('catalog/navigation')
					 *  потому что TM_CustomMenu_Block_Navigation может иметь потомков
					 * (например, Df_Catalog_Block_Navigation extends TM_CustomMenu_Block_Navigation)
					 */
					(
							@class_exists('TM_CustomMenu_Block_Navigation')
						&&
								rm_layout()->getBlockSingleton('catalog/navigation')
							instanceof
								TM_CustomMenu_Block_Navigation
					)
			;
		}
		/** @var Df_Cms_Varien_Data_Tree_Node $result */
		$result =
			new Df_Cms_Varien_Data_Tree_Node(
				$data = array(
					'name' => $cmsNode->getLabel()
					,/**
					 * Обратите внимание, что Magento 1.7 RC1 трактует флаг is_active иначе,
				 	 * чем предыдущие версии.
					 * В предыдущих версиях is_active означает, что рубрика подлежит публикации.
					 * В Magento 1.7 is_active означает, что рубрика является текущей
					 */
					'is_active' => df_magento_version('1.7', '<') || $isItTemplateMela
					,'id' => 'df-cms-' . $cmsNode->getId()
					,'url' =>
						is_null($cmsNode->getPageIdentifier())
						? 'javascript:void(0);'
						: $cmsNode->getUrl()
					,/**
					 * привязываем узел CMS к созданному на его основе узлу Varien_Data_Tree_Node
					 */
					'cms_node' => $cmsNode
				)
				,$idField = 'id'
				,$tree
				,$parent = null
			)
		;
		return $result;
	}

	/** @return Df_Cms_Model_Resource_Hierarchy_Node_Collection */
	private function getCmsNodes() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_Cms_Model_Resource_Hierarchy_Node_Collection $result */
			$result = Df_Cms_Model_Hierarchy_Node::c();
			$result
				->addStoreFilter(
					Mage::app()->getStore()
					,false
				)
				->joinCmsPage()
				->joinMetaData()
				->filterExcludedPagesOut()
				->filterUnpublishedPagesOut()
				->setTreeOrder()
			;
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param Df_Cms_Model_Hierarchy_Node $cmsNode
	 * @return Df_Cms_Varien_Data_Tree_Node|null
	 */
	private function getParentForCmsNodeInVarienDataTree(Df_Cms_Model_Hierarchy_Node $cmsNode) {
		// Результат может быть равен null,
		// если родительская рубрика по каким-то причинам не должна отображаться в меню
		// (например, если так указано в настройках рубрики).
		return df_a($this->_nodesMap, $cmsNode->getParentNodeId());
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Cms_Model_Tree
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}