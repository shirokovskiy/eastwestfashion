<?php
class Df_Payment_Exception_Response extends Df_Payment_Exception_Client {
	/**
	 * @override
	 * @return string
	 */
	public function getMessageRm() {
		return $this->getResponse()->getReport();
	}

	/**
	 * @param Df_Payment_Model_Response $response
	 * @return Df_Payment_Exception_Response
	 */
	public function setResponse(Df_Payment_Model_Response $response) {
		$this->setData(self::P__RESPONSE, $response);
		return $this;
	}

	/** @return Df_Payment_Model_Response */
	private function getResponse() {
		return $this->cfg(self::P__RESPONSE);
	}
	const P__RESPONSE = 'response';
}