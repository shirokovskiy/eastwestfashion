<?php
class Df_Admin_Model_Resource_Setup extends Df_Core_Model_Resource_Setup_Abstract {
	/**
	 * @return Df_Admin_Model_Resource_Setup
	 * @return void
	 */
	public function install_2_23_7() {
		Df_Admin_Model_Setup_2_23_7::s()->process();
	}
	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @return string
	 */
	public static function mf() {
		return 'df_admin/setup';
	}
}