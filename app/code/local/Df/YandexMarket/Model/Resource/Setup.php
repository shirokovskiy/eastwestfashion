<?php
class Df_YandexMarket_Model_Resource_Setup extends Df_Core_Model_Resource_Setup_Abstract {
	/** @return Df_YandexMarket_Model_Resource_Setup */
	public function install_2_17_33() {
		Df_YandexMarket_Model_Setup_2_17_33::s()->process();
		return $this;
	}
	/** @return Df_YandexMarket_Model_Resource_Setup */
	public function upgrade_2_17_46() {
		Df_YandexMarket_Model_Setup_2_17_46::s()->process();
		return $this;
	}
	const _CLASS = __CLASS__;
	const ATTRIBUTE__YANDEX_MARKET_CATEGORY = 'rm_yandex_market_category';
}