<?php
class Df_YandexMarket_Model_Categories extends Df_Core_Model_Abstract {
	/** @return string */
	public function getNodesAsText() {
		return implode("\r\n", $this->getNodesAsTextArray());
	}

	/** @return string[] */
	public function getNodesAsTextArray() {
		if (!isset($this->{__METHOD__})) {
			/** @var string[] $result */
			$result = null;
			/** @var string $cacheKey */
			$cacheKey = implode('::', array(get_class($this), __FUNCTION__));
			/** @var string|bool $resultSerialized */
			$resultSerialized = $this->getCache()->load($cacheKey);
			if (false !== $resultSerialized) {
				$result = @unserialize($resultSerialized);
			}
			if (!is_array($result)) {
				$result = $this->getTree()->getNodesAsTextArray();
				$this->getCache()
					->save(
						$data = serialize($result)
						,$id = $cacheKey
						,$tags = array()
						,$lifeTime = 7 * 86400
					)
				;
			}
			df_result_array($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param string $path
	 * @return bool
	 */
	public function isPathValid($path) {return in_array($path, $this->getNodesAsTextArray(), $path);}

	/** @return Mage_Core_Model_Cache */
	private function getCache() {return Mage::app()->getCacheInstance();}

	/** @return Df_YandexMarket_Model_Category_Tree */
	public function getTree() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_YandexMarket_Model_Category_Tree $result */
			$result = new Df_YandexMarket_Model_Category_Tree();
			foreach (Df_YandexMarket_Model_Category_Excel_Document::s()->getRows() as $row) {
				Df_YandexMarket_Model_Category_Excel_Processor_Row
					::i(
						array(
							Df_YandexMarket_Model_Category_Excel_Processor_Row::P__TREE => $result
							,Df_YandexMarket_Model_Category_Excel_Processor_Row::P__ROW => $row
						)
					)->process()
				;
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_YandexMarket_Model_Categories
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/** @return Df_YandexMarket_Model_Categories */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}