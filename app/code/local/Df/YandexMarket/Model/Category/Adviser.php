<?php
class Df_YandexMarket_Model_Category_Adviser extends Df_Core_Model_Abstract {
	/**
	 * @param string $piece
	 * @return string[]
	 */
	public function getSuggestions($piece) {
		if (!isset($this->{__METHOD__}[$piece])) {
			$this->{__METHOD__}[$piece] =
				Df_YandexMarket_Model_Category_Adviser_Case::i($piece)->getSuggestions()
			;
		}
		return $this->{__METHOD__}[$piece];
	}

	/**
	 * @override
	 * @return int|null
	 */
	protected function getCacheLifetime() {return 86400 * 7;}

	/**
	 * @override
	 * @return string[]
	 */
	protected function getPropertiesToCache() {return self::m(__CLASS__, 'getSuggestions');}
	
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_YandexMarket_Model_Category_Adviser
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/** @return Df_YandexMarket_Model_Category_Adviser */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}

