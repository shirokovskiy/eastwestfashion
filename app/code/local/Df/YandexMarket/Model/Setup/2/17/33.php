<?php
/**
 * @method Df_YandexMarket_Model_Resource_Setup getSetup()
 */
class Df_YandexMarket_Model_Setup_2_17_33 extends Df_Core_Model_Setup {
	/**
	 * @override
	 * @return Df_YandexMarket_Model_Setup_2_17_33
	 */
	public function process() {
		Df_Catalog_Model_Resource_Installer_Attribute::s()->addAdministrativeAttribute(
			$entityType = 'catalog_category'
			,$attributeId = Df_YandexMarket_Model_Resource_Setup::ATTRIBUTE__YANDEX_MARKET_CATEGORY
			,$attributeLabel = 'Категория Яндекс.Маркета'
			,$groupName = 'General Information'
		);
		Df_Catalog_Model_Resource_Installer_Attribute::s()->addAdministrativeAttribute(
			$entityType = 'catalog_product'
			,$attributeId = Df_YandexMarket_Model_Resource_Setup::ATTRIBUTE__YANDEX_MARKET_CATEGORY
			,$attributeLabel = 'Категория Яндекс.Маркета'
		);
		/**
		 * Вот в таких ситуациях, когда у нас меняется структура прикладного типа товаров,
		 * нам нужно сбросить глобальный кэш EAV.
		 */
		rm_eav_reset();
		return $this;
	}

	const _CLASS = __CLASS__;
	/** @return Df_YandexMarket_Model_Setup_2_17_33 */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}