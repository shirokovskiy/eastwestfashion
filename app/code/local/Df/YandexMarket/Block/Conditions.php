<?php
/**
 * Обратите внимание, что Magento не создаёт отдельные экземпляры данного класса
 * для вывода каждого поля!
 * Magento использует ЕДИНСТВЕННЫЙ экземпляр данного класса для вывода всех полей!
 * Поэтому в объектах данного класса нельзя кешировать информацию,
 * которая индивидуальна для поля конкретного поля!
 */
class Df_YandexMarket_Block_Conditions extends Mage_Adminhtml_Block_System_Config_Form_Field {
	/**
	 * @override
	 * @param Varien_Data_Form_Element_Abstract $element
	 * @return string
	 */
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
		/** @var Df_YandexMarket_Model_Field_Conditions $field */
		$field =
			Df_YandexMarket_Model_Field_Conditions::i(
				array(
					Df_YandexMarket_Model_Field_Conditions::P__ELEMENT => $element
					,Df_YandexMarket_Model_Field_Conditions::P__BLOCK => $this
				)
			)
		;
		/** @var string $result */
		$result =
				$field->getHtml()
			.
				rm_sprintf(
					'<input type="hidden" value="0" name="%s"/>'
					,$element->getData('name')
				)
		;
		df_result_string($result);
		return $result;
	}

	const _CLASS = __CLASS__;
}