<?php
class Df_Client_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup {
	/**
	 * @throws Exception
	 * @return Df_Client_Model_Resource_Setup
	 */
	public function install_1_0_0() {
		Df_Client_Model_Resource_DelayedMessage::s()->createTable($this);
		/**
		 * Обратите внимание на архитектуру.
		 * Мы не оповещаем сервер Российской сборки Magento
		 * о факте установке Российской сборки Magento
		 * прямо сейчас, потому что иначе будет происходить зацикливание,
		 * ибо обращение к самому себе как к серверу запустит снова метод install_1_0_0,
		 * и так до бесконечности.
		 *
		 * Вместо этого мы устанавливаем в сессии флаг
		 * «Российская сборка Magento только что установлена»,
		 * и лишь затем, на событие controller_action_postdispatch, отсылаем серверу оповещение.
		 */
		/** @var Mage_Core_Model_Session $coreSession */
		$coreSession = rm_session_core();
		$coreSession->setData(self::FLAG__JUST_INSTALLED, true);
		return $this;
	}
	const FLAG__JUST_INSTALLED = 'df_client__just_installed';
}