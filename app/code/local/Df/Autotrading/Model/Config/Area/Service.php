<?php
class Df_Autotrading_Model_Config_Area_Service extends Df_Shipping_Model_Config_Area_Service {
	/** @return bool */
	public function checkCargoOnReceipt() {return $this->getVarFlag(self::KEY__VAR__CHECK_CARGO_ON_RECEIPT);}

	/** @return bool */
	public function needBagPacking() {return $this->getVarFlag(self::KEY__VAR__NEED_BAG_PACKING);}

	/** @return bool */
	public function needBox() {return $this->getVarFlag(self::KEY__VAR__NEED_BOX);}

	/** @return bool */
	public function needCollapsiblePalletBox() {
		return $this->getVarFlag(self::KEY__VAR__NEED_COLLAPSIBLE_PALLET_BOX);
	}

	/** @return bool */
	public function needOpenSlatCrate() {return $this->getVarFlag(self::KEY__VAR__NEED_OPEN_SLAT_CRATE);}

	/** @return bool */
	public function needTaping() {return $this->getVarFlag(self::KEY__VAR__NEED_TAPING);}

	/** @return bool */
	public function needTapingAdvanced() {return $this->getVarFlag(self::KEY__VAR__NEED_TAPING_ADVANCED);}

	/** @return bool */
	public function needPalletPacking() {return $this->getVarFlag(self::KEY__VAR__NEED_PALLET_PACKING);}

	/** @return bool */
	public function needPlywoodBox() {return $this->getVarFlag(self::KEY__VAR__NEED_PLYWOOD_BOX);}

	/** @return bool */
	public function notifySenderAboutDelivery() {
		return $this->getVarFlag(self::KEY__VAR__NOTIFY_SENDER_ABOUT_DELIVERY);
	}

	const _CLASS = __CLASS__;
	const KEY__VAR__CHECK_CARGO_ON_RECEIPT = 'check_cargo_on_receipt';
	const KEY__VAR__NEED_BAG_PACKING = 'need_bag_packing';
	const KEY__VAR__NEED_BOX = 'need_box';
	const KEY__VAR__NEED_COLLAPSIBLE_PALLET_BOX = 'need_collapsible_pallet_box';
	const KEY__VAR__NEED_INSURANCE = 'need_insurance';
	const KEY__VAR__NEED_OPEN_SLAT_CRATE = 'need_open_slat_crate';
	const KEY__VAR__NEED_TAPING = 'need_taping';
	const KEY__VAR__NEED_TAPING_ADVANCED = 'need_taping_advanced';
	const KEY__VAR__NEED_PALLET_PACKING = 'need_pallet_packing';
	const KEY__VAR__NEED_PLYWOOD_BOX = 'need_plywood_box';
	const KEY__VAR__NOTIFY_SENDER_ABOUT_DELIVERY = 'notify_sender_about_delivery';

}