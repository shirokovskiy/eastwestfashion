<?php
class Df_Autotrading_Model_Request_Locations extends Df_Shipping_Model_Request {
	/** @return string[][] */
	public function getLocations() {
		if (!isset($this->{__METHOD__})) {
			/** @var string[][] $result */
			$result = null;
			/**
			 * Обратите внимание, что не используем в качестве ключа __METHOD__,
			 * потому что данный метод может находиться
			 * в родительском по отношени к другим классе.
			 * @var string $cacheKey
			 */
			$cacheKey = implode('::', array(get_class($this), __FUNCTION__));
			/** @var string|bool $resultSerialized */
			$resultSerialized = $this->getCache()->load($cacheKey);
			if (false !== $resultSerialized) {
				$result = @unserialize($resultSerialized);
			}
			if (!is_array($result)) {
				$this->response()->json();
				/** @var string[][] $peripheralLocations */
				$peripheralLocations = array();
				$peripheralLocationsRaw = df_column($this->response()->json(), 'value');
				foreach ($peripheralLocationsRaw as $peripheralLocationRaw) {
					$peripheralLocations[]= $this->parseLocation($peripheralLocationRaw);
				}
				/** @var string[] $all */
				$all = array();
				/** @var mixed[][] $allMap */
				$allMap = array();
				foreach ($peripheralLocations as $peripheralLocation) {
					/** @var string $peripheralLocation */
					$all[]= df_a($peripheralLocation, self::KEY__LOCATION);
					$allMap[df_a($peripheralLocation, self::KEY__LOCATION)] =
						df_a($peripheralLocation, self::KEY__ORIGINAL_NAME)
					;
				}
				/**
				 * Обратите внимание,
				 * что запросом выше мы получили только провинциальные населенные пункты.
				 * Надо получить ещё региональные центры.
				 */
				/** @var string $regionalCentersResponseAsText */
				$regionalCentersResponseAsText =
					file_get_contents('http://www.ae5000.ru/api.php?metod=city&type=to')
				;
				df_assert_string_not_empty($regionalCentersResponseAsText);
				/**
				 * Почему-то Автотрейдинг возвращает JSON в кодировке Windows-1251
				 */
				$regionalCentersResponseAsText =
					df_text()->convertWindows1251ToUtf8($regionalCentersResponseAsText)
				;
				/** @var array(string => mixed) $regionalCentersResponse */
				$regionalCentersResponse = Zend_Json::decode($regionalCentersResponseAsText);
				df_assert_array($regionalCentersResponse);
				/** @var string $regionalCentersResponseStatus */
				$regionalCentersResponseStatus = df_a($regionalCentersResponse, 'stat');
				df_assert_eq('ok', $regionalCentersResponseStatus);
				/** @var array(array(string => string)) $regionalCenters */
				$regionalCenters = df_a($regionalCentersResponse, 'result');
				df_assert_array($regionalCenters);
				foreach ($regionalCenters as $regionalCenter) {
					/** @var array(string => string) $regionalCenter */
					/** @var string $regionalCenterName */
					$regionalCenterName = df_a($regionalCenter, 'name');
					df_assert_string_not_empty($regionalCenterName);
					$regionalCenterName = mb_strtoupper($regionalCenterName);
					$all[]= $regionalCenterName;
					$allMap[$regionalCenterName] = $regionalCenterName;
				}
				$all = rm_array_unique_fast($all);
				/** @var mixed[][] $grouped */
				$grouped = array();
				/**
				 * Группируем и индексируем данные по региональным центрам
				 */
				foreach ($peripheralLocations as $peripheralLocation) {
					/** @var string[] $peripheralLocation */
					/** @var string $regionalCenter */
					$regionalCenter = df_a($peripheralLocation, self::KEY__REGIONAL_CENTER);
					/** @var string[] $locationsForRegionalCenter */
					$locationsForRegionalCenter = df_a($grouped, $regionalCenter, array());
					$locationsForRegionalCenter[df_a($peripheralLocation, self::KEY__LOCATION)] =
						df_a($peripheralLocation, self::KEY__ORIGINAL_NAME)
					;
					$grouped[$regionalCenter] = $locationsForRegionalCenter;
				}
				/** @var array(string => string) $mapFromPeripheralLocationToRegionalCenter */
				$mapFromPeripheralLocationToRegionalCenter = array();
				foreach ($grouped as $regionalCenter => $peripheralLocations) {
					/** @var string $regionalCenter */
					/** @var array(string => string) $peripheralLocations */
					foreach ($peripheralLocations as $peripheralLocationName => $peripheralLocationOriginalName) {
						/** @var string $peripheralLocationName */
						/** @var string $peripheralLocationOriginalName */
						$mapFromPeripheralLocationToRegionalCenter[$peripheralLocationName] =
							array(
								'regionalCenter' => $regionalCenter
								,'peripheralLocationOriginalName' => $peripheralLocationOriginalName
							)
						;
					}
				}
				$result =
					array(
						self::LOCATIONS__ALL => $all
						,self::LOCATIONS__ALL_MAP => $allMap
						,self::LOCATIONS__GROUPED => $grouped
						,self::LOCATIONS__MAP_FROM_PERIPHERAL_TO_REGIONAL_CENTER =>
							$mapFromPeripheralLocationToRegionalCenter
					)
				;
				$this->getCache()->save(serialize($result), $cacheKey);
			}
			df_result_array($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return string[][] */
	public function getLocationsAll() {return df_a($this->getLocations(), self::LOCATIONS__ALL);}

	/** @return string[][] */
	public function getLocationsAllMap() {return df_a($this->getLocations(), self::LOCATIONS__ALL_MAP);}

	/** @return string[][] */
	public function getLocationsGrouped() {return df_a($this->getLocations(), self::LOCATIONS__GROUPED);}

	/**
	 * @param string $perigheralLocationName
	 * @return array(string => string)|null
	 */
	public function getPeripheralLocationInfo($perigheralLocationName) {
		return
			df_a(
				df_a($this->getLocations(), self::LOCATIONS__MAP_FROM_PERIPHERAL_TO_REGIONAL_CENTER)
				,$perigheralLocationName
			)
		;
	}

	/**
	 * @param string $location
	 * @return bool
	 */
	public function isLocationAllowed($location) {
		return in_array(mb_strtoupper(df_trim($location)), $this->getLocationsAll());
	}

	/**
	 * @param string $rawLocationName
	 * @return string[]
	 */
	public function parseLocation($rawLocationName) {
		df_param_string($rawLocationName, 0);
		/** @var string $originalName */
		$originalName = $this->removeRegionalCenterFromLocationName($rawLocationName);
		$replacementMap = array(
			' (порт восточный)' => ''
			,' (спец.тариф)' => ''
			,' (газо-конден. промысел)' => ''
			,'Княжпогост (Емва)' => 'Княжпогост'
			,'Курумоч (Береза)' => 'Курумоч'
			,'Матырский (ОЭЗ)' => 'Матырский'
			,'Новоникольское (МН Дружба, Траннефтепродукт)' => 'Новоникольское'
			,'Озерск (до поста ГАИ)' => 'Озерск'
			,'Озерск (только до поста ГАИ)' => 'Озерск'
			,'Снежинск (только до поста ГАИ)' => 'Снежинск'
			,'Трехгорный (до поста ГАИ)' => 'Трехгорный'
			,'Алексеевка (ближняя)' => 'Алексеевка'
			,'Петергоф (Петродворец)' => 'Петергоф'
			,'Пыть-Ях (г. Лянтор)' => 'Пыть-Ях'
			,'Ростилово (КС-17)' => 'Ростилово'
			,'Сосьва (ч/з Серов)' => 'Сосьва'
			,'Спасск (Беднодемьяновск)' => 'Спасск'
			,'Строитель (ДСУ-2)' => 'Строитель'
			,'Химки (Вашутинское шоссе)' => 'Химки'
			,'Хоста (с. «Калиновое озеро»)' => 'Хоста'
			,'Хоста (село «Каштаны»)' => 'Хоста'
			,'Ниж.Новгород' => 'Нижний Новгород'
			,'Ниж.Тагил' => 'Нижний Тагил'
			,'Наб.Челны' => 'Набережные Челны'
			,'Ал-Гай' => 'Александров Гай'
			,'Нов. Уренгой' => 'Новый Уренгой'
		);
		/** @var string $repeatedPart */
		$repeatedPart = strtr($rawLocationName, $replacementMap);
		/** @var string $regionalCenter */
		$regionalCenter = null;
		/** @var string $place */
		$place = null;
		if (!rm_contains($repeatedPart, '(')) {
			$regionalCenter = $repeatedPart;
			$place = $repeatedPart;
		}
		else {
			/** @var string[] $locationParts */
			$locationParts = df_trim(explode('(', $repeatedPart), ') ');
			$regionalCenter = df_a($locationParts, 1);
			df_assert($regionalCenter);
			/** @var string $placeWithSuffix */
			$placeWithSuffix = df_a($locationParts, 0);
			df_assert($placeWithSuffix);
			/** @var string $place */
			$place =
				preg_replace(
					'#(.+)\s+(р\.ц\.|г\.|рп|г\.|п\.|с\.|c\.|мкр|д\.|пгт|снп\.|снп|стц|нп|р\-н|пос\.|ст\-ца|ж\/д ст\.|ст\.|а\/п|кп\.|х\.)#u'
					,'$1'
					,$placeWithSuffix
				)
			;
			df_assert($place);
		}
		df_h()->directory()->normalizeLocationName($place);
		df_h()->directory()->normalizeLocationName($regionalCenter);
		/** @var string[] $result */
		$result =
			array(
				self::KEY__LOCATION => df_h()->directory()->normalizeLocationName($place)
				,self::KEY__REGIONAL_CENTER => df_h()->directory()->normalizeLocationName($regionalCenter)
				,self::KEY__ORIGINAL_NAME => $originalName
			)
		;
		return $result;
	}

	/**
	 * @param string
	 * @return string
	 */
	private function removeRegionalCenterFromLocationName($locationName) {
		df_param_string($locationName, 0);
		/** @var string $result */
		$result = null;
		/** @var int|bool $regionalCenterPosition */
		$regionalCenterPosition = mb_strrpos($locationName, '(');
		if (false === $regionalCenterPosition) {
			$result = $locationName;
		}
		else {
			$result = df_trim(mb_substr($locationName, 0 ,rm_nat($regionalCenterPosition)));
		}
		df_result_string_not_empty($result);
		return $result;
	}

	/**
	 * @override
	 * @return array(string => string)
	 */
	protected function getHeaders() {
		return array_merge(parent::getHeaders(), array(
			'Accept' => 'application/json, text/javascript, */*; q=0.01'
			,'Accept-Encoding' => 'gzip, deflate'
			,'Accept-Language' => 'en-us,en;q=0.5'
			,'Connection' => 'keep-alive'
			,'Host' => $this->getQueryHost()
			,'Referer' => 'http://www.ae5000.ru/rates/calculate/'
			,'User-Agent' => Df_Core_Const::FAKE_USER_AGENT
			,'X-Requested-With' => 'XMLHttpRequest'
		));
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getQueryHost() {return 'www.ae5000.ru';}

	/**
	 * @override
	 * @return array
	 */
	protected function getQueryParams() {return array('term' => ' ');}

	/**
	 * @override
	 * @return string
	 */
	protected function getQueryPath() {return '/site/autocomplete';}

	const _CLASS = __CLASS__;
	const KEY__LOCATION = 'location';
	const KEY__ORIGINAL_NAME = 'original_name';
	const KEY__REGIONAL_CENTER = 'regional_center';
	const LOCATIONS__ALL = 'all';
	const LOCATIONS__ALL_MAP = 'all_map';
	const LOCATIONS__GROUPED = 'grouped';
	const LOCATIONS__MAP_FROM_PERIPHERAL_TO_REGIONAL_CENTER = 'map_from_peripheral_to_regional_center';
	const P__REGIONAL_CENTER = 'regional_center';

	/** @return Df_Autotrading_Model_Request_Locations */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}