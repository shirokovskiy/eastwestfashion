<?php
class Df_Checkout2_Frontend_IndexController extends Mage_Core_Controller_Front_Action {
	/** @return void */
	public function indexAction() {
		try {
			$this
				->loadLayout()
				->renderLayout()
			;
		}
		catch(Exception $e) {
			df_handle_entry_point_exception($e);
		}
	}
}