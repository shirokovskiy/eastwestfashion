<?php
class Df_NightExpress_Model_Request_Locations extends Df_NightExpress_Model_Request {
	/** @return array(string => string) */
	public function getLocations() {
		if (!isset($this->{__METHOD__})) {
			/** @var array(string => string) $result */
			$result = null;
			/**
			 * Обратите внимание, что не используем в качестве ключа __METHOD__,
			 * потому что данный метод может находиться
			 * в родительском по отношени к другим классе.
			 * @var string $cacheKey
			 */
			$cacheKey = implode('::', array(get_class($this), __FUNCTION__));
			/** @var string|bool $resultSerialized */
			$resultSerialized = $this->getCache()->load($cacheKey);
			if (false !== $resultSerialized) {
				$result = @unserialize($resultSerialized);
			}
			if (!is_array($result)) {
				$result = $this->parseLocations();
				df_assert_array($result);
				$resultSerialized = serialize($result);
				$this->getCache()->save($resultSerialized, $cacheKey);
			}
			df_assert_array($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getQueryPath() {return '/calculator';}

	/** @return array(string => string) */
	private function parseLocations() {
		/** @var array(string => string) $result */
		$result = array();
		/** @var array(string => string) $options */
		$options = $this->response()->options('#city_in option', $idIsString = true);
		foreach ($options as $locationName => $locationId) {
			/** @var string $locationName */
			/** @var string $locationId */
			df_assert_string($locationId);
			if ('0' !== $locationId) {
				/** @var string $locationName */
				$locationName = df_trim($locationName, '[]-');
				$result[$locationName] = $locationId;
			}
		}
		return $result;
	}

	const _CLASS = __CLASS__;
	/** @return Df_NightExpress_Model_Request_Locations */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}