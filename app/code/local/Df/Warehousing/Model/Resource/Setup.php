<?php
class Df_Warehousing_Model_Resource_Setup extends Df_Core_Model_Resource_Setup_Abstract {
	/** @return Df_Warehousing_Model_Resource_Setup */
	public function install_1_0_0() {
		Df_Warehousing_Model_Resource_Warehouse::s()->tableCreate($this);
		return $this;
	}
}