<?php
class Df_Index_Helper_Data extends Mage_Core_Helper_Abstract {
	/** @return Df_Index_Helper_Data */
	public function reindexEverything() {
		foreach (df_mage()->index()->indexer()->getProcessesCollection() as $process) {
			/** @var Mage_Index_Model_Process $process */
			/**
			 * @todo Лучше перестраивать расчётные таблицы
			 * только для обрабатываемого магазина.
			 * Сейчас же мы перестраиваем расчётные таблицы для всех магазинов системы
			 */
			$process->reindexEverything();
		}
		return $this;
	}

	/** @return Df_Index_Helper_Data */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}