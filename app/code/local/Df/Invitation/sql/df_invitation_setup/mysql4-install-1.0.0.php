<?php
/* @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
/** @var string $tableInvitation */
$tableInvitation =
	$installer->getTable(
		Df_Invitation_Model_Resource_Invitation::TABLE_NAME
	)
;
/** @var string $tableInvitationHistory */
$tableInvitationHistory =
	$installer->getTable(
		Df_Invitation_Model_Resource_Invitation_History::TABLE_NAME
	)
;
$tableInvitationTrack = $installer->getTable('df_invitation/invitation_track');
$tableCustomer	  = $installer->getTable('customer/entity');
$tableCustomerGroup = $installer->getTable('customer/customer_group');
$installer->run("
DROP TABLE IF EXISTS `{$tableInvitation}`;
CREATE TABLE `{$tableInvitation}` (
	`invitation_id` INT UNSIGNED  NOT null AUTO_INCREMENT PRIMARY KEY ,`customer_id` INT( 10 ) UNSIGNED DEFAULT null ,`date` DATETIME NOT null ,`email` VARCHAR( 255 ) NOT null ,`referral_id` INT( 10 ) UNSIGNED DEFAULT null ,`protection_code` CHAR(32) NOT null,`signup_date` DATETIME DEFAULT null,`store_id` SMALLINT(5) UNSIGNED NOT null,`group_id` smallint(3) unsigned null DEFAULT null,`message` TEXT DEFAULT null,`status` enum('new','sent','accepted','canceled') NOT null DEFAULT 'new',INDEX `IDX_customer_id` (`customer_id`),INDEX `IDX_referral_id` (`referral_id`)

	,
  	CONSTRAINT `FK_INVITATION_STORE`
		FOREIGN KEY (`store_id`)
		REFERENCES `{$installer->getTable('core_store')}` (`store_id`)
		ON DELETE CASCADE
		ON UPDATE CASCADE


	,
  	CONSTRAINT `FK_INVITATION_CUSTOMER`
		FOREIGN KEY (`customer_id`)
		REFERENCES `{$tableCustomer}` (`entity_id`)
		ON DELETE SET null
		ON UPDATE CASCADE


	,
  	CONSTRAINT `FK_INVITATION_REFERRAL`
		FOREIGN KEY (`referral_id`)
		REFERENCES `{$tableCustomer}` (`entity_id`)
		ON DELETE SET null
		ON UPDATE CASCADE



	,
  	CONSTRAINT `FK_INVITATION_CUSTOMER_GROUP`
		FOREIGN KEY (`group_id`)
		REFERENCES `{$tableCustomerGroup}` (`customer_group_id`)
		ON DELETE SET null
		ON UPDATE CASCADE


) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
/** @var Varien_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();
$installer->run("
DROP TABLE IF EXISTS `{$tableInvitationHistory}`;
CREATE TABLE `{$tableInvitationHistory}` (
	`history_id` INT UNSIGNED NOT null AUTO_INCREMENT PRIMARY KEY,`invitation_id` INT UNSIGNED NOT null,`date` DATETIME NOT null,`status` enum('new','sent','accepted','canceled') NOT null DEFAULT 'new',INDEX `IDX_invitation_id` (`invitation_id`)

	,
  	CONSTRAINT `FK_INVITATION_HISTORY_INVITATION`
		FOREIGN KEY (`invitation_id`)
		REFERENCES `{$tableInvitation}` (`invitation_id`)
		ON DELETE CASCADE
		ON UPDATE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->run("
CREATE TABLE `{$tableInvitationTrack}` (
	`track_id` int(10) unsigned NOT null AUTO_INCREMENT,`inviter_id` int(10) unsigned NOT null DEFAULT 0,`referral_id` int(10) unsigned NOT null DEFAULT 0,PRIMARY KEY (`track_id`),UNIQUE KEY `UNQ_INVITATION_TRACK_IDS` (`inviter_id`,`referral_id`),KEY `FK_INVITATION_TRACK_REFERRAL` (`referral_id`),CONSTRAINT `FK_INVITATION_TRACK_INVITER`
		FOREIGN KEY (`inviter_id`)
		REFERENCES `{$tableCustomer}` (`entity_id`)
		ON DELETE CASCADE
		ON UPDATE CASCADE

	,CONSTRAINT `FK_INVITATION_TRACK_REFERRAL`
		FOREIGN KEY (`referral_id`)
		REFERENCES `{$tableCustomer}` (`entity_id`)
		ON DELETE CASCADE
		ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();
