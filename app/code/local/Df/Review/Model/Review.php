<?php
class Df_Review_Model_Review extends Mage_Review_Model_Review {
	/**
	 * @param int|null $value [optional]
	 * @return Df_Review_Model_Review
	 */
	public function setCustomerId($value = null) {
		if (!is_null($value)) {
			df_param_integer($value, 0);
		}
		$this->setData(self::P__CUSTOMER_ID, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Review_Model_Review
	 */
	public function setDetail($value) {
		df_param_string($value, 0);
		$this->setData(self::P__DETAIL, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Review_Model_Review
	 */
	public function setNickname($value) {
		df_param_string($value, 0);
		$this->setData(self::P__NICKNAME, $value);
		return $this;
	}

	/**
	 * @param int|null $value [optional]
	 * @return Df_Review_Model_Review
	 */
	public function setStoreId($value = null) {
		if (!is_null($value)) {
			df_param_integer($value, 0);
		}
		$this->setData(self::P__STORE_ID, $value);
		return $this;
	}

	/**
	 * @param int[] $value [optional]
	 * @return Df_Review_Model_Review
	 */
	public function setStores(array $value = array()) {
		$this->setData(self::P__STORES, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Review_Model_Review
	 */
	public function setTitle($value) {
		df_param_string($value, 0);
		$this->setData(self::P__TITLE, $value);
		return $this;
	}

	const _CLASS = __CLASS__;
	const P__CUSTOMER_ID = 'customer_id';
	const P__DETAIL = 'detail';
	const P__NICKNAME = 'nickname';
	const P__STORE_ID = 'store_id';
	const P__STORES = 'stores';
	const P__TITLE = 'title';
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Review_Model_Review
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/** @return Df_Review_Model_Review */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}