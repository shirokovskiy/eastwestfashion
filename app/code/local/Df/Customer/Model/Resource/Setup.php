<?php
class Df_Customer_Model_Resource_Setup extends Mage_Customer_Model_Entity_Setup {
	/**
	 * @return Df_Customer_Model_Resource_Setup
	 * @return void
	 */
	public function upgrade_2_23_7() {
		Df_Customer_Model_Setup_2_23_7::s()->process();
	}
	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @return string
	 */
	public static function mf() {
		return 'df_customer/setup';
	}
}