<?php
class Df_Customer_Block_Account_Navigation extends Mage_Customer_Block_Account_Navigation {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @param string $path
	 * @return Df_Customer_Block_Account_Navigation
	 */
	public function removeLinkByPath($path) {
		$linkNamesToRemove = array();
		/** @var array $linkNamesToRemove */
		foreach ($this->_links as $name => $link) {
			/** @var Varien_Object $link */
			if ($path == $link->getData("path")) {
				$linkNamesToRemove[]= $name;
			}
		}
		foreach ($linkNamesToRemove as $name) {
			/** @var string $name */
			unset($this->_links[$name]);
		}
		return $this;
	}
}