<?php
class Df_Wishlist_Block_Customer_Sidebar extends Mage_Wishlist_Block_Customer_Sidebar {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @return string[]
	 */
	public function getCacheKeyInfo() {
		/** @var string[] $result */
		$result = parent::getCacheKeyInfo();
		if (
				df_module_enabled(Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->wishlistCustomerSidebar()
		) {
			$result = array_merge($result, array(get_class($this)), $this->getProductIds());
		}
		return $result;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		if (
				df_module_enabled(Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->wishlistCustomerSidebar()
		) {
			$this->setData('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
		}
	}

	/** @return array */
	private function getProductIds() {
		/** @var array $result */
		$result = array();
		foreach ($this->getWishlistItems() as $item) {
			/** @var Mage_Wishlist_Model_Item $item */

			$result[]= $item->getProductId();
		}
		df_result_array($result);
		return $result;
	}
}