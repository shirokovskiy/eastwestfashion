<?php
interface Df_Eav_Const {
	const CLASS__ENTITY_ATTRIBUTE = 'Mage_Eav_Model_Entity_Attribute';
	const CLASS__ENTITY_ATTRIBUTE_SET = 'Mage_Eav_Model_Entity_Attribute_Set';
	const CLASS_MF__ENTITY_ATTRIBUTE_SET = 'eav/entity_attribute_set';
	const CLASS_MF__ENTITY_ATTRIBUTE_SET_COLLECTION = 'eav/entity_attribute_set_collection';
	/**
	 * Имя свойства (или колонки таблицы),
	 * хранящего идентификатор учётного объекта,
	 * используемый системой 1С: Управление торговлей
	 * при информационном обмене с сайтом.
	 */
	const ENTITY_EXTERNAL_ID = 'rm_1c_id';
	const ENTITY_EXTERNAL_ID_OLD = 'df_1c_id';
	const FRONTEND_CLASS__NATURAL_NUMBER = 'validate-number validate-greater-than-zero';
}