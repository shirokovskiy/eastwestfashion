<?php
class Df_Eav_Model_Config extends Mage_Eav_Model_Config {
	/**
	 * @override
	 * @param mixed $obj
	 * @param mixed $id
	 * @return Mage_Eav_Model_Config
	 */
	protected function _save($obj, $id) {
		if (Mage::isInstalled() && ($obj instanceof Mage_Eav_Model_Entity_Attribute)) {
			/** @var Mage_Eav_Model_Entity_Attribute $obj */
			Df_Eav_Model_Translator::s()->translateAttribute($obj);
		}
		parent::_save($obj, $id);
		return $this;
	}
}