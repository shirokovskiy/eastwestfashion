<?php
class Df_Eav_Model_Translator extends Df_Core_Model_Abstract {
	/**
	 * @override
	 * @return void
	 */
	public function __destruct() {
		if (
				df_is_it_my_local_pc()
			&&
				!df_is_admin()
			&&
				Mage::isInstalled()
			&&
				$this->_untranslated
		) {
			Mage::log(rm_sprintf(
				'Добавьте в файл app/locale/ru_DF/Df_Eav.csv'
				. " перевод следующих экранных названий свойств:\r\n%s"
				, implode("\r\n", rm_array_unique_fast($this->_untranslated))
			), null, 'rm.translation.log');
		}
		parent::__destruct();
	}

	/**
	 * @param Mage_Eav_Model_Entity_Attribute_Abstract $attribute
	 * @return void
	 */
	public function translateAttribute(Mage_Eav_Model_Entity_Attribute_Abstract $attribute) {
		if (
				df_h()->eav()->isAttributeBelongsToProduct($attribute)
			&&
				!isset($attribute->{self::$RM_TRANSLATED})
			&&
				(
						df_is_admin()
					||
						rm_bool($attribute->getIsVisibleOnFront())
					||
						// Для свойства «Special Price» («special_price») is_visible_on_front = false,
						// но is_visible = true
						rm_bool($attribute->getData('is_visible'))
				)
		) {
			foreach (self::$LABEL_NAMES as $labelName) {
				/** @var string $labelName */
				/** @var string|null $labelValue */
				$labelValue = $attribute->getData($labelName);
				if ($labelValue) {
					$attribute->setData($labelName, $this->translateLabel($labelValue));
				}
			}
			$attribute->{self::$RM_TRANSLATED} = true;
		}
	}

	/**
	 * @param array(string => mixed) $attributeData
	 * @return void
	 */
	public function translateAttributeAssoc(array &$attributeData) {
		/** @var array(string => mixed $attributeData) */
		if (
				df_is_admin()
			||
				rm_bool(df_a($attributeData, 'is_visible_on_front'))
			||
				// Для свойства «Special Price» («special_price») is_visible_on_front = false,
				// но is_visible = true
				rm_bool(df_a($attributeData, 'is_visible'))
		) {
			foreach (self::$LABEL_NAMES as $labelName) {
				/** @var string $labelName */
				/** @var string|null $labelValue */
				$labelValue = df_a($attributeData, $labelName);
				if ($labelValue) {
					$attributeData[$labelName] = $this->translateLabel($labelValue);
				}
			}
		}
	}

	/**
	 * @param string $label
	 * @return string
	 */
	public function translateLabel($label) {
		if (!isset($this->{__METHOD__}[$label])) {
			/** @var Df_Core_Model_Translate $translator */
			static $translator;
			if (!isset($translator)) {
				$translator = Mage::app()->getTranslator();
			}
			/**
			 * Раньше тут стояло:
				$result = df_mage()->catalogHelper()->__($label);
				if ($result === $label) {
					$result = df_h()->eav()->__($label);
				}
			 * Изменил код ради ускорения.
			 */
			/** @var string $result */
			$result = $translator->translateFast($label, 'Mage_Catalog');
			if ($result === $label) {
				$result = $translator->translateFast($label, 'Df_Eav');
			}
			if (
					df_is_it_my_local_pc()
				&&
					($result === $label)
				&&
					!df_text()->isTranslated($label)
			) {
				$this->_untranslated[]= $label;
			}
			else {
				//Mage::log(sprintf('«%s» => «%s»', $label, $result));
			}
			$this->{__METHOD__}[$label] = $result;
		}
		return $this->{__METHOD__}[$label];
	}

	/** @var string[] */
	private $_untranslated = array();

	/** @return Df_Eav_Model_Translator */
	public static function s() {static $r; return $r ? $r : $r = new self;}
	/** @var string[] */
	private static $LABEL_NAMES = array('store_label', 'frontend_label');
	/** @var string */
	private static $RM_TRANSLATED = 'rm_translated';
}