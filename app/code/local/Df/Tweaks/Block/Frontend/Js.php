<?php
class Df_Tweaks_Block_Frontend_Js extends Df_Core_Block_Template {
	/**
	 * @override
	 * @return string[]
	 */
	public function getCacheKeyInfo() {
		return
			array_merge(
				parent::getCacheKeyInfo()
				,array(get_class($this))
				,rm_layout()->getUpdate()->getHandles()
			)
		;
	}
	
	/** @return string */
	public function getOptionsAsJson() {
		if (!isset($this->{__METHOD__})) {
			/** @var string $theme */
			$theme = df_mage()->core()->design()->packageSingleton()->getTheme('skin');
			if (!$theme) {
				$theme = Mage_Core_Model_Design_Package::DEFAULT_THEME;
			}
			/** @var string $result */
			$this->{__METHOD__} = df_output()->json(array(
				'package' => df_mage()->core()->design()->packageSingleton()->getPackageName()
				,'theme' => $theme
				,'formKey' => rm_session_core()->getFormKey()
			));
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getDefaultTemplate() {return 'df/tweaks/js.phtml';}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->setData('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
	}
	const _CLASS = __CLASS__;
}