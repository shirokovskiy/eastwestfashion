<?php
class Df_Bundle_Block_Adminhtml_Catalog_Product_Edit_Tab_Bundle_Option_Selection
	extends Mage_Bundle_Block_Adminhtml_Catalog_Product_Edit_Tab_Bundle_Option_Selection {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @return string
	 */
	protected function _toHtml() {
		return
			df_concat(
				parent::_toHtml()
				,"<script type='text/javascript'>
					jQuery(window).trigger ('bundle.product.edit.bundle.option.selection');
				</script>"
			)
		;
	}
}