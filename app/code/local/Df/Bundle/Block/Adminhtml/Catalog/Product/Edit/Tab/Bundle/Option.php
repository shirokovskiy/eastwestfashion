<?php
class Df_Bundle_Block_Adminhtml_Catalog_Product_Edit_Tab_Bundle_Option
	extends Mage_Bundle_Block_Adminhtml_Catalog_Product_Edit_Tab_Bundle_Option {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @return Df_Bundle_Block_Adminhtml_Catalog_Product_Edit_Tab_Bundle_Option
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		/** @var Mage_Adminhtml_Block_Widget_Button $buttonDelete */
		$buttonDelete = $this->getChild('option_delete_button');
		if (false === $buttonDelete) {
			$buttonDelete = null;
		}

		if (!is_null($buttonDelete)) {
			$buttonDelete
				->setData(
					'label'
					,df_mage()->bundleHelper()->__('Delete Option')
				)
			;
		}
		return $this;
	}
}