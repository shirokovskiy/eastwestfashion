<?php
class Df_Zf_Validate_Class extends Df_Zf_Validate_Type {
	/**
	 * @override
	 * @return string
	 */
	public function getMessage() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				!is_object($this->getValue())
				? parent::getMessage()
				: strtr(
					'Система получила объект класса «{полученный класс}» вместо требуемого класса «{требуемый класс}».'
					,array(
						'{полученный класс}' => get_class($this->getValue())
						,'{требуемый класс}' => $this->getClassExpected()
					)
				)
			;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @param object $value
	 * @return boolean
	 */
	public function isValid($value) {
		$this->prepareValidation($value);
		/** @var string $expectedClass */
		$expectedClass = $this->getClassExpected();
		return is_object($value) && ($value instanceof $expectedClass);
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getExpectedTypeInAccusativeCase() {
		return rm_sprintf('объект класса «%s»', $this->getClassExpected());
	}

	/** @return string */
	private function getClassExpected() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->cfg(self::$PARAM__CLASS);
			df_result_string_not_empty($this->{__METHOD__});
		}
		return $this->{__METHOD__};
	}

	/** @var string */
	private static $PARAM__CLASS = 'class';

	/**
	 * @param string $className
	 * @return Df_Zf_Validate_Class
	 */
	public static function s($className) {
		/** @var array(string => Df_Zf_Validate_Class) */
		static $result = array();
		if (!isset($result[$className])) {
			df_param_string_not_empty($className, 0);
			$result[$className] = new self(array(self::$PARAM__CLASS => $className));
		}
		return $result[$className];
	}
}