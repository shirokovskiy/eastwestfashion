<?php
abstract class Df_Zf_Validate_Type implements Zend_Validate_Interface {
	/** @return string */
	abstract protected function getExpectedTypeInAccusativeCase();

	/**
	 * @param array $params
	 */
	public function __construct(array $params = array()) {
		if (!is_array($params)) {
			df_error();
		}
		$this->_params = $params;
	}

	/** @return string */
	public function getMessage() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				strtr(
					'Система не смогла распознать значение «{значение}» типа «{тип}» как {требуемый тип}.'
					,array(
						'{значение}' => df_string_debug($this->getValue())
						,'{тип}' => gettype($this->getValue())
						,'{требуемый тип}' => $this->getExpectedTypeInAccusativeCase()
					)
				)
			;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return array(string => string)
	 */
	public function getMessages() {return array(__CLASS__ => $this->getMessage());}

	/**
	 * @param string $paramName
	 * @param mixed $defaultValue[optional]
	 * @return mixed
	 */
	protected function cfg($paramName, $defaultValue = null) {
		return df_a($this->_params, $paramName, $defaultValue);
	}

	/** @return mixed */
	protected function getValue() {return $this->cfg(self::$PARAM__VALUE);}

	/**
	 * @param mixed $value
	 * @return void
	 */
	protected function prepareValidation($value) {
		$this->reset();
		$this->setValue($value);
	}

	/** @return void */
	protected function reset() {
		unset($this->{__CLASS__ . '::getMessage'});
		/**
		 * Раньше тут стоял код $this->_params = array()
		 * который сбрасывает сразу все значения параметров.
		 * Однако этот код неверен!
		 * Негоже родительскому классу безапелляционно решать за потомков,
		 * какие данные им сбрасывать.
		 * Например, потомок @see Df_Zf_Validate_Class
		 * хранит в параметре @see Df_Zf_Validate_Class::$PARAM__CLASS
		 * требуемый класс результата,
		 * и сбрасывать это значение между разными валидациями не нужно!
		 * Вместо сброса значения между разными валидациями
		 * класс @see Df_Zf_Validate_Class ведёт статический кэш своих экземпляров
		 * для каждого требуемого класса результата:
		 * @see Df_Zf_Validate_Class::s().
		 * Сброс значения параметра @see Df_Zf_Validate_Class::$PARAM__CLASS
		 * не только не нужен, но и приведёт к сбою!
		 * Пусть потомки сами решают
		 * посредством перекрытия метода @see Df_Zf_Validate_Type::reset(),
		 * значения каких параметров им надо сбрасывать между разными валидациями.
		 */
		unset($this->_params[self::$PARAM__VALUE]);
	}

	/**
	 * @param string $message
	 * @return void
	 */
	protected function setMessage($message) {
		$this->{__CLASS__ . '::getMessage'} = $message;
	}

	/**
	 * @param mixed $value
	 * @return void
	 */
	private function setValue($value) {
		$this->reset();
		$this->_params[self::$PARAM__VALUE] = $value;
	}

	/** @var array(string => mixed) */
	private $_params = array();
	/** @var string */
	private static $PARAM__VALUE = 'value';
}