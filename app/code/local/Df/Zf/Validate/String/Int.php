<?php
class Df_Zf_Validate_String_Int extends Df_Zf_Validate_String_Parser {
	/**
	 * @override
	 * @return string
	 */
	protected function getExpectedTypeInAccusativeCase() {return 'целое число';}
	/**
	 * @override
	 * @return string
	 */
	protected function getZendValidatorClass() {return 'Zend_Validate_Int';}

	/** @return Df_Zf_Validate_String_Int */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}