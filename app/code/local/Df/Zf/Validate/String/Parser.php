<?php
abstract class Df_Zf_Validate_String_Parser extends Df_Zf_Validate_Type {
	/** @return string */
	abstract protected function getZendValidatorClass();

	/**
	 * @override
	 * @param string $value
	 * @return bool
	 */
	public function isValid($value) {
		$this->prepareValidation($value);
		return
				$this->getZendValidator('en_US')->isValid($value)
			||
				$this->getZendValidator('ru_RU')->isValid($value)
		;
	}

	/**
	 * @param string $locale
	 * @return Zend_Validate_Interface
	 */
	private function getZendValidator($locale) {
		df_param_string_not_empty($locale, 0);
		if (!isset($this->{__METHOD__}[$locale])) {
			/** @var string $class */
			$class = $this->getZendValidatorClass();
			$this->{__METHOD__}[$locale] = new $class($locale);
			df_assert($this->{__METHOD__}[$locale] instanceof Zend_Validate_Interface);
		}
		return $this->{__METHOD__}[$locale];
	}
}