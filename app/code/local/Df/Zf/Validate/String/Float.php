<?php
class Df_Zf_Validate_String_Float extends Df_Zf_Validate_String_Parser {
 	/**
	 * @override
	 * @return string
	 */
	protected function getExpectedTypeInAccusativeCase() {return 'вещественное число';}
	/**
	 * @override
	 * @return string
	 */
	protected function getZendValidatorClass() {return 'Zend_Validate_Float';}

	/** @return Df_Zf_Validate_String_Float */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}