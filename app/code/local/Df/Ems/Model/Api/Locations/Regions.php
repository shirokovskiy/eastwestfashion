<?php
class Df_Ems_Model_Api_Locations_Regions extends Df_Ems_Model_Api_Locations_Abstract {
	/**
	 * @override
	 * @return string
	 */
	protected function getLocationType() {
		return 'regions';
	}

	/** @return array(int => string) */
	public function getMapFromMagentoRegionIdToEmsRegionId() {
		if (!isset($this->{__METHOD__})) {
			/**
			 * Обратите внимание, что не используем в качестве ключа __METHOD__,
			 * потому что данный метод может находиться
			 * в родительском по отношени к другим классе.
			 * @var string $cacheKey
			 */
			$cacheKey = implode('::', array(get_class($this), __FUNCTION__));
			/** @var array(int => string) $result */
			$result = null;
			/** @var string|bool $resultSerialized */
			$resultSerialized = $this->getCache()->load($cacheKey);
			if (false !== $resultSerialized) {
				$result = @unserialize($resultSerialized);
			}
			if (!is_array($result)) {
				$result =
					df_array_combine(
						array_map(
							array($this, 'getRegionIdInMagentoByRegionNameInEmsFormat')
							,df_column(
								$this->getLocationsAsRawArray()
								,'name'
							)
						)
						,df_column(
							$this->getLocationsAsRawArray()
							,'value'
						)
					)
				;
				$resultSerialized = serialize($result);
				$this->getCache()->save($resultSerialized, $cacheKey);
			}
			df_result_array($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return array(string => int) */
	private function getMapFromMagentoRegionNameToMagentoRegionId() {
		if (!isset($this->{__METHOD__})) {
			/** @var array(string => int) $result */
			$result = array();
			foreach (df_h()->directory()->getRussianRegions() as $region) {
				/** @var Df_Directory_Model_Region $region */
				$result[mb_strtoupper($region->getName())] = $region->getId();
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param string $regionNameInEmsFormat
	 * @return int
	 */
	public function getRegionIdInMagentoByRegionNameInEmsFormat($regionNameInEmsFormat) {
		df_param_string($regionNameInEmsFormat, 0);
		/** @var array $replacements */
		$replacements =
			array(
				'СЕВЕРНАЯ ОСЕТИЯ-АЛАНИЯ РЕСПУБЛИКА' => 'СЕВЕРНАЯ ОСЕТИЯ — АЛАНИЯ РЕСПУБЛИКА'
				,'ТЫВА РЕСПУБЛИКА' => 'ТЫВА (ТУВА) РЕСПУБЛИКА'
				,'ХАНТЫ-МАНСИЙСКИЙ-ЮГРА АВТОНОМНЫЙ ОКРУГ' => 'ХАНТЫ-МАНСИЙСКИЙ АВТОНОМНЫЙ ОКРУГ'
			)
		;
		/** @var string $regionNameInMagentoFormat */
		$regionNameInMagentoFormat =
			df_a(
				$replacements
				,$regionNameInEmsFormat
				,$regionNameInEmsFormat
			)
		;
		df_assert_string($regionNameInMagentoFormat);
		/** @var string $result */
		$result =
			df_a(
				$this->getMapFromMagentoRegionNameToMagentoRegionId()
				,$regionNameInMagentoFormat
				,0
			)
		;
		/** @var array $expectedlyNotTranslated */
		$expectedlyNotTranslated =
			array(
				'КАЗАХСТАН'
				,'ТАЙМЫРСКИЙ АО'
				,'ТАЙМЫРСКИЙ ДОЛГАНО-НЕНЕЦКИЙ РАЙОН'
			)
		;
		if ((0 === $result) && !in_array($regionNameInMagentoFormat, $expectedlyNotTranslated)) {
			//df_notify('Не могу перевести: ' . $regionNameInMagentoFormat);
		}
		df_result_integer($result);
		return $result;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Ems_Model_Api_Locations_Regions
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}