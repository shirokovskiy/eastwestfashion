<?php
class Df_1C_Helper_Cml2_Registry extends Mage_Core_Helper_Abstract {
	/** @return Df_1C_Model_Cml2_Registry_Export */
	public function export() {return Df_1C_Model_Cml2_Registry_Export::s();}

	/** @return Df_1C_Model_Cml2_Import_Data_Collection_PriceTypes */
	public function getPriceTypes() {return Df_1C_Model_Cml2_Import_Data_Collection_PriceTypes::s();}

	/** @return Df_1C_Model_Cml2_Registry_Import */
	public function import() {return Df_1C_Model_Cml2_Registry_Import::s();}

	/** @return Df_1C_Helper_Cml2_Registry */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}