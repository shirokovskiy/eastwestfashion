<?php
class Df_1C_Cml2Controller extends Mage_Core_Controller_Front_Action {
	/** @return void */
	public function indexAction() {
		/**
		 * Обратите внимание, что проверку на наличие и доступности лицензии
		 * мы выполняем не здесь, а в классе @see Df_1C_Model_Cml2_Action,
		 * потому что данные проверки должны при необходимости возбуждать исключительные ситуации,
		 * и именно в том классе расположен блок try... catch, который обрабатывает их
		 * надлежащим для 1C: Управление торговлей способом
		 * (возвращает диагностическое сообщение в 1C: Управление торговлей
		 * по стандарту CommerceML 2)
		 */
		Df_1C_Model_Cml2_Action_Front::i($this)->process();
	}
}