<?php
abstract class Df_1C_Model_Cml2_Import extends Df_Core_Model_Abstract {
	/**
	 * @abstract
	 * @return Df_1C_Model_Cml2_Import
	 */
	abstract public function process();

	/** @return Df_1C_Helper_Cml2_Registry */
	protected function getRegistry() {return Df_1C_Helper_Cml2_Registry::s();}

	/** @return Df_Varien_Simplexml_Element */
	protected function getSimpleXmlElement() {
		return $this->cfg(self::P__SIMPLE_XML);
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_prop(self::P__SIMPLE_XML, Df_Varien_Simplexml_Element::_CLASS);
	}
	const _CLASS = __CLASS__;
	const P__SIMPLE_XML = 'simple_xml';
}