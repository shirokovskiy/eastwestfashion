<?php
class Df_1C_Model_Cml2_Import_Processor_Product_Type_Configurable
	extends Df_1C_Model_Cml2_Import_Processor_Product_Type {
	/**
	 * @override
	 * @return Df_1C_Model_Cml2_Import_Processor
	 */
	public function process() {
		if ($this->getEntityOffer()->isTypeConfigurableParent()) {
			// Сначала импортируем настраиваемые варианты в виде простых товаров
			$this->importChildren();
			// Затем создаём настраиваемый товар
			$this->importParent();
		}
		return $this;
	}

	/**
	 * @override
	 * @return float
	 */
	protected function getPrice() {
		if (!isset($this->{__METHOD__})) {
			/** @var float $result */
			$result = null;
			foreach ($this->getEntityOffer()->getConfigurableChildren() as $offer) {
				/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
				/** @var float|null $currentPrice */
				$currentPrice = $offer->getProduct()->getPrice();
				if (!is_null($currentPrice)) {
					/** @var float $currentPriceAsFloat */
					$currentPriceAsFloat = rm_float($currentPrice);
					if (is_null($result) || $result > $currentPriceAsFloat) {
						$result = $currentPriceAsFloat;
					}
				}
			}
			df_result_float($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return string[] */
	private function getProductDataNewOrUpdateAttributeValueIdsCustom() {
		/** @var string[] $result */
		$result = array();
		foreach ($this->getEntityProduct()->getAttributeValuesCustom() as $attributeValue) {
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_ProductPart_AttributeValue_Custom|null $attributeValue */
			if ($attributeValue->getAttributeMagento()) {
				$result[$attributeValue->getAttributeName()] = $attributeValue->getValueForObject();
			}
		}
		return $result;
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getSku() {
		if (!isset($this->{__METHOD__})) {
			/** @var string|null $result */
			$result = null;
 			if (!is_null($this->getExistingMagentoProduct())) {
				$result = $this->getExistingMagentoProduct()->getSku();
			}
			else {
				$result = $this->getEntityProduct()->getSku();
				if (!$result) {
					df_h()->_1c()->log(
						'У товара «%s» в 1С отсутствует артикул.', $this->getEntityProduct()->getName()
					);
					$result = $this->getEntityOffer()->getExternalId();
				}
				if (df_h()->catalog()->product()->isExists($result)) {
					// Вдруг товар с данным артикулом уже присутствует в системе?
					df_h()->_1c()->log('Товар с артикулом «%s» уже присутствует в магазине.', $result);
					df_assert_ne($result, $this->getEntityOffer()->getExternalId());
					$result = $this->getEntityOffer()->getExternalId();
				}
			}
			df_result_string_not_empty($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getType() {
		return Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE;
	}

	/** @return array(string => int) */
	private function getUsedProductAttributeIds() {
		if (!isset($this->{__METHOD__})) {
			/** @var array(string => int) $result */
			$result = array();
			/** @var string[] $labels */
			$labels = array();
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer|null $firstChild */
			$firstChild = df_array_first($this->getEntityOffer()->getConfigurableChildren());
			if ($firstChild) {
				foreach ($firstChild->getOptionValues() as $optionValue) {
					/** @var Df_1C_Model_Cml2_Import_Data_Entity_OfferPart_OptionValue $optionValue */
					/** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
					$attribute = $optionValue->getAttributeMagento();
					$result[$attribute->getName()] = $attribute->getId();
					$labels[]= $attribute->getFrontendLabel();
				}
			}
			df_h()->_1c()->log(
				"Для товара настраиваются параметры:\r\n%s."
				,implode(', ', df_text()->quote($labels))
			);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return Df_1C_Model_Cml2_Import_Processor_Product_Type_Configurable */
	private function importChildren() {
		/** @var int $count */
		$count = count($this->getEntityOffer()->getConfigurableChildren());
		if (0 === $count) {
			df_h()->_1c()->log('Простые варианты настраиваемых товаров отсутствуют.');
		}
		else {
			df_h()->_1c()->log('Найдено простых вариантов настраиваемых товаров: %d.', $count);
			df_h()->_1c()->log('Импорт простых вариантов настраиваемых товаров начат.');
			foreach ($this->getEntityOffer()->getConfigurableChildren() as $offer) {
				/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
				Df_1C_Model_Cml2_Import_Processor_Product_Type_Configurable_Child::i($offer)->process();
			}
			df_h()->_1c()->log('Импорт простых вариантов настраиваемых товаров завершён.');
		}
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Import_Processor_Product_Type_Configurable */
	private function importParent() {
		if (is_null($this->getExistingMagentoProduct())) {
			$this->importParentNew();
		}
		else {
			$this->importParentUpdate();
		}
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Import_Processor_Product_Type_Configurable */
	private function importParentNew() {
		$this->getImporter()->import();
		/** @var Df_Catalog_Model_Product $product */
		$product = $this->getImporter()->getProduct();
		$product
			->addData(
				array(
					'can_save_configurable_attributes' => true
					,'can_save_custom_options' => true
				)
			)
		;
		/** @var Mage_Catalog_Model_Product_Type_Configurable $productTypeConfigurable */
		$productTypeConfigurable = $product->getTypeInstance();
		df_assert($productTypeConfigurable instanceof Mage_Catalog_Model_Product_Type_Configurable);
		// This array is is an array of attribute ID's
		// which the configurable product swings around
		// (i.e; where you say when you create a configurable product in the admin area
		// what attributes to use as options)
		// $_attributeIds is an array which maps the attribute(s) used for configuration
		// to their numerical counterparts.
		// (there's probably a better way of doing this, but i was lazy, and it saved extra db calls);
		// $_attributeIds = array("size" => 999, "color", => 1000, "material" => 1001); // etc..
		$productTypeConfigurable
			->setUsedProductAttributeIds(
				$this->getUsedProductAttributeIds()
			)
		;
		// Now we need to get the information back in Magento's own format,// and add bits of data to what it gives us..
		$attributes_array = $productTypeConfigurable->getConfigurableAttributesAsArray();
		foreach ($attributes_array as $key => $attribute_array) {
			$attributes_array[$key]['use_default'] = 1;
			$attributes_array[$key]['position'] = 0;
			if (isset($attribute_array['frontend_label'])) {
				$attributes_array[$key]['label'] = $attribute_array['frontend_label'];
			}
			else {
				$attributes_array[$key]['label'] = $attribute_array['attribute_code'];
			}
		}
		// Add it back to the configurable product..
		$product->setData('configurable_attributes_data', $attributes_array);
		// Remember that $simpleProducts array we created earlier? Now we need that data..
		$dataArray = array();
		foreach ($this->getEntityOffer()->getConfigurableChildren() as $offer) {
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_OfferPart_OptionValue|null $firstOption */
			$firstOption = null;
			foreach ($offer->getOptionValues() as $optionValue) {
				$firstOption = $optionValue;
				break;
			}
			if ($firstOption) {
				$dataArray[$offer->getProduct()->getId()] =
					array(
						'attribute_id' => $firstOption->getAttributeMagento()->getId()
						,'label' => df_string($firstOption->getData('value'))
						,'is_percent' => false
						,'pricing_value' => $offer->getProduct()->getPrice()
					)
				;
			}
		}
		// This tells Magento to associate the given simple products to this configurable product..
		$product->setData('configurable_products_data', $dataArray);
		// Set stock data. Yes, it needs stock data.
		// No qty, but we need to tell it to manage stock, and that it's actually
		// in stock, else we'll end up with problems later..
		$product
			->setData(
				'stock_data'
				,array(
					'use_config_manage_stock' => 1
					,'is_in_stock' => 1
					,'is_salable' => 1
				)
			)
		;
		$product->saveRm($isMassUpdate = true);
		$product->reload();
		df_h()->_1c()->log('Создан товар «%s».', $product->getName());
		df()->registry()->products()->addEntity($product);
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Import_Processor_Product_Type_Configurable */
	private function importParentUpdate() {
		$this->getExistingMagentoProduct()
			->addData(
				array_merge(
					$this->getProductDataNewOrUpdateAttributeValueIdsCustom()
					,$this->getProductDataNewOrUpdateBase()
					,$this->getProductDataUpdateOnly()
				)
			)
		;
		/**
		 * Код выше уже установил товару значение свойства category_ids,
		 * но в данном контексте — неправильно, в виде строки.
		 * Устанавливаем по-правильному.
		 */
		$this->getExistingMagentoProduct()->setCategoryIds($this->getEntityProduct()->getCategoryIds());
		$this->getExistingMagentoProduct()->saveRm($isMassUpdate = true);
		$this->getExistingMagentoProduct()->reload();
		df()->registry()->products()->addEntity($this->getExistingMagentoProduct());
		df_h()->_1c()->log('Обновлён товар «%s».', $this->getExistingMagentoProduct()->getName());
		return $this;
	}

	/**
	 * @override
	 * @return int
	 */
	protected function getVisibility() {
		return Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer
	 * @return Df_1C_Model_Cml2_Import_Processor_Product_Type_Configurable
	 */
	public static function i(Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer) {
		return new self(array(self::P__ENTITY => $offer));
	}
}