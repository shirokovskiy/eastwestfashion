<?php
abstract class Df_1C_Model_Cml2_Import_Data_Collection
	extends Df_Core_Model_SimpleXml_Parser_Collection {
	/**
	 * @param string $externalId
	 * @return Df_1C_Model_Cml2_Import_Data_Entity|null
	 */
	public function findByExternalId($externalId) {return $this->findById($externalId);}

	/**
	 * Данный метод никак не связан данным с классом,
	 * однако включён в класс для удобного доступа объектов класса к реестру
	 * (чтобы писать $this->getRegistry() вместо Df_1C_Helper_Cml2_Registry::s())
	 * @return Df_1C_Helper_Cml2_Registry
	 */
	protected function getRegistry() {return Df_1C_Helper_Cml2_Registry::s();}

	const _CLASS = __CLASS__;
}