<?php
class Df_1C_Model_Cml2_Import_Data_Entity_Offer extends Df_1C_Model_Cml2_Import_Data_Entity {
	/** @return Df_1C_Model_Cml2_Import_Data_Entity_Offer[] */
	public function getConfigurableChildren() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer[] $result */
			$result = array();
			if (!$this->isTypeConfigurableChild()) {
				foreach ($this->getRegistryOffers() as $offer) {
					/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
					if ($offer->isTypeConfigurableChild()) {
						if ($this->getExternalId() === $offer->getExternalIdForConfigurableParent()) {
							$result[]= $offer;
						}
					}
				}
			}
			if (0 < count($result)) {
				/** @var string[] $names */
				$names = array();
				foreach ($result as $offer) {
					/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
					$names[]= $offer->getName();
				}
				df_h()->_1c()->log(
					"У товара «%s» найдено %d вариантов:\r\n%s"
					,$this->getName()
					,count($result)
					,implode("\r\n", $names)
				);
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Entity_Offer|null */
	public function getConfigurableParent() {
		return
			!$this->isTypeConfigurableChild()
			? null
			: $this->getRegistryOffers()->findByExternalId(
				$this->getExternalIdForConfigurableParent()
			)
		;
	}
	
	/** @return Df_1C_Model_Cml2_Import_Data_Entity_Product */
	public function getEntityProduct() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				$this->getRegistryProductEntities()->findByExternalId(
					$this->getExternalIdForConfigurableParent()
				)
			;
			df_assert(!!$this->{__METHOD__});
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	public function getExternalIdForConfigurableParent() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = df_array_first($this->getExternalIdExploded());
			df_result_string_not_empty($this->{__METHOD__});
		}
		return $this->{__METHOD__};
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Collection_OfferPart_OptionValues */
	public function getOptionValues() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				Df_1C_Model_Cml2_Import_Data_Collection_OfferPart_OptionValues::i($this, $this->e())
			;
		}
		return $this->{__METHOD__};
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Collection_OfferPart_Prices */
	public function getPrices() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				Df_1C_Model_Cml2_Import_Data_Collection_OfferPart_Prices::i($this->e(), $this)
			;
		}
		return $this->{__METHOD__};
	}
	
	/** @return Df_Catalog_Model_Product */
	public function getProduct() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = df()->registry()->products()->findByExternalId($this->getExternalId());
			if (!$this->{__METHOD__}) {
				df_error('Товар не найден в реестре: «%s»', $this->getExternalId());
			}
		}
		return $this->{__METHOD__};
	}

	/** @return int */
	/**
	 * Как ни странно, в 1С количество может быть дробным:
	 * @link http://magento-forum.ru/topic/4389/
	 * @return float
	 */
	public function getQuantity() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = rm_float($this->getEntityParam('Количество'));
		}
		return $this->{__METHOD__};
	}
	
	/** @return bool */
	public function isTypeConfigurableChild() {
		if (!isset($this->{__METHOD__})) {
			/** @var bool $result */
			$result = false;
			if (1 < count($this->getExternalIdExploded())) {
				/**
				 * Обратите внимание, что в данном месте мы не можем вызывать метод
				 * @see Df_1C_Model_Cml2_Import_Data_Entity_Offer::isTypeConfigurableParent(),
				 * иначе попадём в рекурсию.
				 */
				if ($this->getRegistryOffers()->findByExternalId(
					$this->getExternalIdForConfigurableParent()
				)) {
					$result = true;
				}
				else {
					/**
					 * Заметил в магазине термобелье.su,
					 * что «1С: Управление торговлей» передаёт в интернет-магазин в файле offers.xml
					 * простые варианты настраиваемого товара,
					 * не передавая при этом сам настраиваемый товар!
					 * Версия «1С: Управление торговлей»: 11.1.2.22
					 * Версия платформы «1С:Предприятие»: 8.2.19.80
					 * Похоже, система так ведёт себя,
					 * когда в «1С: Управление торговлей» характеристики заданы индивидуально для товара,
					 * а не общие для вида номенклатуры.
					 * Цитата из интерфейса «1С: Управление торговлей»:
					 * «Рекомендуется использовать характеристики общие для вида номенклатуры.
					 * Тогда, например, можно задать единую линейку размеров
					 * для всей номенклатуры этого вида».
					 * @link http://magento-forum.ru/topic/4197/
					 *
					 * В этом случае считаем товарное предложение
					 * не простым вариантом настраиваемого товара,
					 * а простым товаром.
					 *
					 * 2014-04-11
					 * Заметил в магазине зоомир.укр,
					 * что «1С: Управление торговлей» передаёт в интернет-магазин в файле offers.xml
					 * простые варианты настраиваемого товара,
					 * не передавая при этом сам настраиваемый товар!
					 * При этом в «1С: Управление торговлей» используются характеристики,
					 * общие для вида номенклатуры.
					 * Версия «1С: Управление торговлей»: Управление торговлей для Украины, редакция 3.0
					 * Версия платформы «1С:Предприятие»: 8.3.4.408
					 * @link http://magento-forum.ru/topic/4347/
					 * При этом система в состоянии идентифицировать товарные предложения
					 * как составные части настраиваемых товаров,
					 * потому что внешние идентификаторы таких товарных предложений
					 * имеют формат
					 * <Ид>816d609d-b8ae-11e3-bba1-08606ed36063#816d6099-b8ae-11e3-bba1-08606ed36063</Ид>,
					 * где часть до «#» — общая для всех простых вариантов настраиваемого товара,
					 * причём эта часть присутствует в файле products.xml.
					 *
					 * products.xml:
						<Товар>
							<Ид>816d609d-b8ae-11e3-bba1-08606ed36063</Ид>
							<Наименование>Аквариум тест 1</Наименование>
							(...)
						</Товар>
					 *
					 * offers.xml:
						<Предложение>
							<Ид>816d609d-b8ae-11e3-bba1-08606ed36063#816d6099-b8ae-11e3-bba1-08606ed36063</Ид>
							<Наименование>Аквариум тест 1 (Белый)</Наименование>
							(...)
						</Предложение>
						<Предложение>
							<Ид>816d609d-b8ae-11e3-bba1-08606ed36063#816d609a-b8ae-11e3-bba1-08606ed36063</Ид>
							<Наименование>Аквариум тест 1 (Черный)</Наименование>
							(...)
						</Предложение>
					 */
					$result =
						!!$this->getRegistry()->import()->collections()->getProducts()->findByExternalId(
							$this->getExternalIdForConfigurableParent()
						)
					;
				}
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return bool */
	public function isTypeConfigurableParent() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = (0 < count($this->getConfigurableChildren()));
		}
		return $this->{__METHOD__};
	}

	/** @return bool */
	public function isTypeSimple() {
		return !$this->isTypeConfigurableChild() && !$this->isTypeConfigurableParent();
	}

	/** @return string[] */
	private function getExternalIdExploded() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = explode('#', $this->getExternalId());
		}
		return $this->{__METHOD__};
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Collection_Offers */
	private function getRegistryOffers() {
		return $this->getRegistry()->import()->collections()->getOffers();
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Collection_Products */
	private function getRegistryProductEntities() {
		return $this->getRegistry()->import()->collections()->getProducts();
	}

	/**
	 * Используется из @see Df_1C_Model_Cml2_Import_Data_Collection_Offers::getItemClass()
	 */
	const _CLASS = __CLASS__;
}