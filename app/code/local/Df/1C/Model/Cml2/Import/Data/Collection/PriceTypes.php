<?php
class Df_1C_Model_Cml2_Import_Data_Collection_PriceTypes
	extends Df_1C_Model_Cml2_Import_Data_Collection {
	/** @return Df_1C_Model_Cml2_Import_Data_Entity_PriceType */
	public function getMain() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->findByName(df_cfg()->_1c()->product()->prices()->getMain());
			if (!$this->{__METHOD__}) {
				df_error(
					'Тип цен «%s», указанный администратором как основной,'
					.' отсутствует в 1С:Управление торговлей.'
					,df_cfg()->_1c()->product()->prices()->getMain()
				);
			}
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return Df_Varien_Simplexml_Element
	 */
	public function getSimpleXmlElement() {
		return
			$this->getRegistry()->import()->files('catalog')->getByRelativePath(
				Df_1C_Model_Cml2_Registry_Import_Files::FILE__OFFERS
			)
		;
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getItemClass() {
		return Df_1C_Model_Cml2_Import_Data_Entity_PriceType::_CLASS;
	}

	/**
	 * @override
	 * @return string[]
	 */
	protected function getItemsXmlPathAsArray() {
		return array(
			''
			,'КоммерческаяИнформация'
			,'ПакетПредложений'
			,'ТипыЦен'
			,'ТипЦены'
		);
	}

	const _CLASS = __CLASS__;
	/** @return Df_1C_Model_Cml2_Import_Data_Collection_PriceTypes */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}