<?php
abstract class Df_1C_Model_Cml2_Import_Processor_Product_Type_Simple_Abstract
	extends Df_1C_Model_Cml2_Import_Processor_Product_Type {
	/**
	 * @override
	 * @return float
	 */
	protected function getPrice() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = rm_float(
				is_null($this->getEntityOffer()->getPrices()->getMain())
				? 0
				: $this->getEntityOffer()->getPrices()->getMain()->getPriceBase()
			);
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getType() {
		return Mage_Catalog_Model_Product_Type::TYPE_SIMPLE;
	}

	/**
	 * @override
	 * @return int
	 */
	protected function getVisibility() {
		return Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
	}

	const _CLASS = __CLASS__;
}