<?php
class Df_1C_Model_Cml2_Import_Data_Entity_ProductPart_AttributeValue_Custom
	extends Df_1C_Model_Cml2_Import_Data_Entity {
	/**
	 * Добавили к названию метода окончание «Magento»,
	 * чтобы избежать конфликта с родительским методом
	 * Df_Core_Model_SimpleXml_Parser_Entity::getAttribute()
	 * @return Mage_Catalog_Model_Resource_Eav_Attribute
	 */
	public function getAttributeMagento() {
		if (!isset($this->{__METHOD__})) {
			/** @var Mage_Catalog_Model_Resource_Eav_Attribute|null $result */
			$result = $this->findMagentoAttributeInRegistry();
			if (!$result) {
				// Вот здесь-то мы можем добавить в Magento нестандартные свойства товаров,
				// учёт которых ведётся в 1С: Управление торговлей.
				$result = $this->createMagentoAttribute();
				df_h()->_1c()->log('Создано свойство «%s»', $result->getName());
				df()->registry()->attributes()->addEntity($result);
			}
			df_assert($result instanceof Mage_Catalog_Model_Resource_Eav_Attribute);
			/**
			 * Мало, чтобы свойство присутствовало в системе:
			 * надо добавить его к прикладному типу товара.
			 */
			df_h()->_1c()->create1CAttributeGroupIfNeeded(
				$this->getProduct()->getAttributeSet()->getId()
			);
			/** @var string $status */
			$status =
				Df_Catalog_Model_Installer_AddAttributeToSet::processStatic(
					$result->getAttributeCode()
					,$this->getProduct()->getAttributeSet()->getId()
					,$this->getGroupForAttribute($result)
				)
			;
			switch($status) {
				case Df_Catalog_Model_Resource_Installer_Attribute::ADD_ATTRIBUTE_TO_SET__ADDED:
					df_h()->_1c()->log(
						'К типу «%s» добавлено свойство «%s».'
						,$this->getProduct()->getAttributeSet()->getAttributeSetName()
						,$result->getName()
					);
					break;
				case Df_Catalog_Model_Resource_Installer_Attribute::ADD_ATTRIBUTE_TO_SET__CHANGED_GROUP:
					df_h()->_1c()->log(
						'В типе «%s» свойство «%s» сменило группу на «%s».'
						,$this->getProduct()->getAttributeSet()->getAttributeSetName()
						,$result->getName()
						,$this->getGroupForAttribute($result)
					);
					break;
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	public function getAttributeExternalId() {
		/** @var string $result */
		$result = $this->getEntityParam('Ид');
		df_result_string($result);
		return $result;
	}

	/** @return string */
	public function getAttributeName() {
		/** @var string $result */
		$result = $this->getAttributeMagento()->getName();
		df_result_string($result);
		return $result;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getExternalId() {
		return implode('::', array($this->getAttributeExternalId(), $this->getValue()));
	}

	/** @return string */
	public function getValue() {
		/** @var string $result */
		$result = $this->getEntityParam('Значение');
		df_result_string($result);
		return $result;
	}

	/** @return string */
	public function getValueForDataflow() {
		/** @var string $result */
		$result =
			$this->getAttributeEntity()->convertValueToMagentoFormat(
				$this->getValue()
			)
		;
		df_result_string($result);
		return $result;
	}

	/** @return string */
	public function getValueForObject() {
		/** @var string $result */
		$result =
			$this->getAttributeEntity()->convertValueToMagentoFormat(
				$this->getValue()
			)
		;
		df_result_string($result);
		return $result;
	}

	/** @return bool */
	public function isAttributeExistAndBelongToTheProductType() {
		if (!isset($this->{__METHOD__})) {
			/** @var bool $result */
			$result = false;
			/** @var Mage_Eav_Model_Entity_Attribute|null $attribute */
			$attribute = df()->registry()->attributes()->findByExternalId($this->getAttributeExternalId());
			if ($attribute) {
				df_assert($attribute instanceof Mage_Eav_Model_Entity_Attribute);
				// Смотрим, принадлежит ли свойство типу товара
				/** @var Mage_Eav_Model_Resource_Entity_Attribute_Collection $attributes */
				$attributes = Mage::getResourceModel('eav/entity_attribute_collection');
				$attributes->setEntityTypeFilter(rm_eav_id_product());
				$attributes->addSetInfo();
				$attributes->addFieldToFilter('attribute_code', $attribute->getAttributeCode());
				$attributes->load();
				/** @var Mage_Eav_Model_Entity_Attribute $attributeInfo */
				$attributeInfo = null;
				foreach ($attributes as $attributeInfoCurrent) {
					$attributeInfo = $attributeInfoCurrent;
					break;
				}
				df_assert($attributeInfo instanceof Mage_Eav_Model_Entity_Attribute);
				/** @var mixed[] $setsInfo */
				$setsInfo = $attributeInfo->getData('attribute_set_info');
				df_assert_array($setsInfo);
				$result = in_array($this->getProduct()->getAttributeSet()->getId(), array_keys($setsInfo));
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return Mage_Catalog_Model_Resource_Eav_Attribute */
	protected function createMagentoAttribute() {
		/** @var string $attributeCode */
		$attributeCode =
			df_h()->_1c()->generateAttributeCode(
				$this->getAttributeEntity()->getName()
				// Намеренно убрал второй параметр ($this->getProduct()->getAppliedTypeName()),
				// потому что счёл ненужным в данном случае
				// использовать приставку для системных имён товарных свойств,
				// потому что приставка (прикладной тип товара),
				// как правило, получается слишком длинной,
				// а название системного имени товарного свойства
				// ограничено 32 символами
			)
		;
		/** @var mixed[] $attributeData */
		$attributeData =
			array(
				'entity_type_id' => rm_eav_id_product()
				,'attribute_code' => $attributeCode
				/**
				 * В Magento CE 1.4, если поле «attribute_model» присутствует,
				 * то его значение не может быть пустым
				 * @see Mage_Eav_Model_Config::_createAttribute
				 */
				,'backend_model' => $this->getAttributeEntity()->getBackendModel()
				,'backend_type' => $this->getAttributeEntity()->getBackendType()
				,'backend_table' => null
				,'frontend_model' => null
				,'frontend_input' => $this->getAttributeEntity()->getFrontendInput()
				,'frontend_label' => $this->getAttributeEntity()->getName()
				,'frontend_class' => null
				,'source_model' => $this->getAttributeEntity()->getSourceModel()
				,'is_required' => 0
				,'is_user_defined' => 1
				,'default_value' => null
				,'is_unique' => 0
				// В Magento CE 1.4 значением поля «note» не может быть null
				,'note' => ''
				,'frontend_input_renderer' => null
				,'is_global' => 1
				,'is_visible' => 1
				,'is_searchable' => 1
				,'is_filterable' => 1
				,'is_comparable' => 1
				,'is_visible_on_front' =>
					rm_01(df_cfg()->_1c()->product()->other()->showAttributesOnProductPage())
				,'is_html_allowed_on_front' => 0
				,'is_used_for_price_rules' => 0
				,'is_filterable_in_search' => 1
				,'used_in_product_listing' => 0
				,'used_for_sort_by' => 0
				,'is_configurable' => 1
				,'is_visible_in_advanced_search' => 1
				,'position' => 0
				,'is_wysiwyg_enabled' => 0
				,'is_used_for_promo_rules' => 0
				,Df_Eav_Const::ENTITY_EXTERNAL_ID => $this->getAttributeExternalId()
			)
		;
		/** @var Mage_Catalog_Model_Resource_Eav_Attribute $result */
		$result =
			df()->registry()->attributes()->findByCodeOrCreate(
				$attributeCode, $attributeData
			)
		;
		df_assert($result->_getData(Df_Eav_Const::ENTITY_EXTERNAL_ID));
		df_h()->_1c()->log('Добавлено свойство «%s».', $this->getAttributeEntity()->getName());
		return $result;
	}

	/** @return Mage_Catalog_Model_Resource_Eav_Attribute */
	protected function findMagentoAttributeInRegistry() {
		return df()->registry()->attributes()->findByExternalId($this->getAttributeExternalId());
	}

	/**
	 * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
	 * @return string
	 */
	protected function getGroupForAttribute(Mage_Catalog_Model_Resource_Eav_Attribute $attribute) {
		return Df_1C_Const::PRODUCT_ATTRIBUTE_GROUP_NAME;
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Entity_Product */
	protected function getProduct() {return $this->cfg(self::P__PRODUCT);}

	/** @return Df_1C_Model_Cml2_Import_Data_Entity_Attribute */
	private function getAttributeEntity() {
		/** @var Df_1C_Model_Cml2_Import_Data_Entity_Attribute $result */
		$result =
			$this->getRegistry()->import()->collections()->getAttributes()->findByExternalId(
				$this->getAttributeExternalId()
			)
		;
		if (is_null($result)) {
			df_error(
				'В реестре отсутствует требуемое свойство с внешним идентификатором «%s»'
				,$this->getAttributeExternalId()
			);
		}
		return $result;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_prop(self::P__PRODUCT, Df_1C_Model_Cml2_Import_Data_Entity_Product::_CLASS);
	}
	/** Используется из @see Df_1C_Model_Cml2_Import_Data_Collection_ProductPart_AttributeValues_Custom::getItemClass() */
	const _CLASS = __CLASS__;
	const P__PRODUCT = 'product';
}