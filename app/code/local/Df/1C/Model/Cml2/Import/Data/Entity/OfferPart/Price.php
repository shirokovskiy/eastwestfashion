<?php
class Df_1C_Model_Cml2_Import_Data_Entity_OfferPart_Price
	extends Df_1C_Model_Cml2_Import_Data_Entity {
	/** @return string */
	public function getCurrencyCode() {
		/** @var string $result */
		$result =
			df_h()->_1c()->cml2()->convertCurrencyCodeToMagentoFormat(
				$this->getEntityParam('Валюта')
			)
		;
		df_result_string_not_empty($result);
		return $result;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getExternalId() {
		/** @var string $result */
		$result = $this->getEntityParam('ИдТипаЦены');
		df_result_string($result);
		return $result;
	}

	/** @return float */
	public function getPrice() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = rm_float($this->getEntityParam('ЦенаЗаЕдиницу'));
		}
		return $this->{__METHOD__};
	}

	/** @return float */
	public function getPriceBase() {
		return rm_currency()->convertToBase($this->getPrice(), $this->getCurrencyCode());
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Entity_PriceType */
	public function getPriceType() {
		/** @var Df_1C_Model_Cml2_Import_Data_Entity_PriceType $result */
		$result = $this->getRegistry()->getPriceTypes()->findByExternalId($this->getId());
		df_assert($result instanceof Df_1C_Model_Cml2_Import_Data_Entity_PriceType);
		return $result;
	}

	/**
	 * Используется из @see Df_1C_Model_Cml2_Import_Data_Collection_OfferPart_Prices::getItemClass()
	 */
	const _CLASS = __CLASS__;
}