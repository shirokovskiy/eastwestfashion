<?php
/**
 * Удаляет из заказа (PARAM__ENTITY_ORDER) строку PARAM__PRODUCT_ID
 */
class Df_1C_Model_Cml2_Import_Processor_Order_Item_Delete
	extends Df_1C_Model_Cml2_Import_Processor_Order_Item {
	/** @return Df_1C_Model_Cml2_Import_Processor_Order_Item_Delete */
	public function process() {
		return $this;
	}

	const _CLASS = __CLASS__;

}