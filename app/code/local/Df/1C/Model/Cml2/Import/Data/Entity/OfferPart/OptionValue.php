<?php
class Df_1C_Model_Cml2_Import_Data_Entity_OfferPart_OptionValue
	extends Df_1C_Model_Cml2_Import_Data_Entity {
	/**
	 * Добавили к названию метода окончание «Magento»,
	 * чтобы избежать конфликта с родительским методом
	 * Df_Core_Model_SimpleXml_Parser_Entity::getAttribute()
	 * @return Mage_Catalog_Model_Resource_Eav_Attribute
	 */
	public function getAttributeMagento() {
		if (!isset($this->{__METHOD__})) {
			/** @var Mage_Catalog_Model_Resource_Eav_Attribute|null $result */
			$result = df()->registry()->attributes()->findByExternalId($this->getAttributeExternalId());
			if ($result) {
				/**
				 * Вот здесь, похоже, удачное место,
				 * чтобы добавить в уже присутствующий в Magento справочник
				 * значение текущей опции, если его там нет
				 */
				$attributeData =
					array_merge(
						$result->getData()
						,array(
							'option' =>
								Df_Eav_Model_Entity_Attribute_Option_Calculator
									::calculateStatic(
										$result
										,array('option_0' => array($this->getValue()))
										,$isModeInsert = true
										,$caseInsensitive = true
									)
						)
					)
				;
				/**
				 * Какое-то значение тут надо установить,
				 * потому что оно будет одним из ключей в реестре
				 * (второй ключ — название справочника)
				 */
//				if (is_null($result->getData(Df_Eav_Const::ENTITY_EXTERNAL_ID))) {
//					$attributeData[Df_Eav_Const::ENTITY_EXTERNAL_ID] = $this->getName();
//				}
				$result =
					df()->registry()->attributes()->findByCodeOrCreate(
						df_a($attributeData, 'attribute_code'), $attributeData
					)
				;
			}
			else {
				$attributeData =
					array(
						'entity_type_id' => rm_eav_id_product()
						,'attribute_code' => $this->getAttributeCodeGenerated()
						/**
						 * В Magento CE 1.4, если поле «attribute_model» присутствует,
						 * то его значение не может быть пустым
						 * @see Mage_Eav_Model_Config::_createAttribute						 */

						,'backend_model' => null
						,'backend_type' => 'int'
						,'backend_table' => null
						,'frontend_model' => null
						,'frontend_input' => 'select'
						,'frontend_label' => $this->getName()
						,'frontend_class' => null
						,'source_model' => null
						,'is_required' => 0
						,'is_user_defined' => 1
						,'default_value' => null
						,'is_unique' => 0
						,/**
						 * В Magento CE 1.4 значением поля «note» не может быть null
						 */
						'note' => ''
						,'frontend_input_renderer' => null
						,'is_global' => 1
						,'is_visible' => 1
						,'is_searchable' => 1
						,'is_filterable' => 1
						,'is_comparable' => 1
						,'is_visible_on_front' => 1
						,'is_html_allowed_on_front' => 0
						,'is_used_for_price_rules' => 0
						,'is_filterable_in_search' => 1
						,'used_in_product_listing' => 0
						,'used_for_sort_by' => 0
						,'is_configurable' => 1
						,'is_visible_in_advanced_search' => 1
						,'position' => 0
						,'is_wysiwyg_enabled' => 0
						,'is_used_for_promo_rules' => 0
						,/**
						 * Какое-то значение тут надо установить,
					 	 * потому что оно будет одним из ключей в реестре
						 * (второй ключ — название справочника)
						 */
						Df_Eav_Const::ENTITY_EXTERNAL_ID => $this->getAttributeExternalId()
						,'option' =>
							array(
								'value' => array('option_0' => array($this->getValue()))
								,'order' => array('option_0' => 0)
								,'delete' => array('option_0' => 0)
							)
					)
				;
				$result =
					df()->registry()->attributes()->findByCodeOrCreate(
						df_a($attributeData, 'attribute_code'), $attributeData
					)
				;
			}
			// Назначаем справочным значениям идентификаторы из 1С
			/** @var Mage_Eav_Model_Entity_Attribute_Option $option */
			$option = $this->getOptionByAttribute($result);
			$option->setData(Df_Eav_Const::ENTITY_EXTERNAL_ID, $this->getExternalId());
			$option->save();
			df()->registry()->attributes()->addEntity($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return string
	 */
	public function getExternalId() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				mb_strtolower(implode(': ', array($this->getName(), $this->getValue())))
			;
		}
		return $this->{__METHOD__};
	}

	/** @return Mage_Eav_Model_Entity_Attribute_Option */
	public function getOption() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->getOptionByAttribute($this->getAttributeMagento());
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	public function getValue() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->getEntityParam('Значение');
			df_result_string($this->{__METHOD__});
		}
		return $this->{__METHOD__};
	}

	/** @return int */
	public function getValueId() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = rm_nat0($this->getOption()->getData('value'));
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	protected function getAttributeCodeGenerated() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				df_h()->_1c()->generateAttributeCode(
					$this->getName()
					// Намеренно убрал второй параметр ($this->getEntityProduct()->getAppliedTypeName()),
					// потому что счёл ненужным в данном случае
					// использовать приставку для системных имён товарных свойств,
					// потому что приставка (прикладной тип товара),
					// как правило, получается слишком длинной,
					// а название системного имени товарного свойства
					// ограничено 32 символами
				)
			;
		}
		return $this->{__METHOD__};
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Entity_Product */
	protected function getEntityProduct() {return $this->getOffer()->getEntityProduct();}

	/** @return Df_1C_Model_Cml2_Import_Data_Entity_Offer */
	protected function getOffer() {return $this->cfg(self::P__OFFER);}

	/** @return string */
	private function getAttributeExternalId() {return 'RM 1С - ' .  $this->getName();}

	/**
	 * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
	 * @return Mage_Eav_Model_Entity_Attribute_Option
	 */
	private function getOptionByAttribute(Mage_Catalog_Model_Resource_Eav_Attribute $attribute) {
		/** @var Mage_Eav_Model_Entity_Attribute_Source_Table $source */
		$source = $attribute->getSource();
		df_assert($source instanceof Mage_Eav_Model_Entity_Attribute_Source_Table);
		/** @var Df_Eav_Model_Resource_Entity_Attribute_Option_Collection $options */
		$options = Df_Eav_Model_Resource_Entity_Attribute_Option_Collection::i();
		df_h()->eav()->assert()->entityAttributeOptionCollection($options);
		$options->setPositionOrder('asc');
		$options->setAttributeFilter($attribute->getId());
		$options->setStoreFilter($attribute->getStoreId());
		$options->addFieldToFilter('tdv.value', $this->getValue());
		df_assert_eq(1, count($options));
		/** @var Mage_Eav_Model_Entity_Attribute_Option $option */
		$result = $options->fetchItem();
		df_assert($result instanceof Mage_Eav_Model_Entity_Attribute_Option);
		return $result;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_prop(self::P__OFFER, Df_1C_Model_Cml2_Import_Data_Entity_Offer::_CLASS);
	}

	/**
	 * Используется из @see Df_1C_Model_Cml2_Import_Data_Collection_OfferPart_OptionValues::getItemClass()
	 */
	const _CLASS = __CLASS__;
	const P__OFFER = 'offer';
}