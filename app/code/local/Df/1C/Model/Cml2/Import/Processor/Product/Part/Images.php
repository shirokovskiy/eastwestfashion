<?php
class Df_1C_Model_Cml2_Import_Processor_Product_Part_Images
	extends Df_1C_Model_Cml2_Import_Processor_Product {
	/**
	 * @override
	 * @return Df_1C_Model_Cml2_Import_Processor
	 */
	public function process() {
		if (is_null($this->getExistingMagentoProduct())) {
			df_error(
				'Попытка импорта картинок для отсутствующего в системе товара «%s»'
				,$this->getEntityOffer()->getExternalId()
			);
		}
		Df_Dataflow_Model_Importer_Product_Images::i(
			$this->getExistingMagentoProduct()
			, $this->getEntityProduct()->getImagesPaths()
			, df_h()->_1c()
		)->process();
		return $this;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer
	 * @return Df_1C_Model_Cml2_Import_Processor_Product_Part_Images
	 */
	public static function i(Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer) {
		return new self(array(self::P__ENTITY => $offer));
	}
}