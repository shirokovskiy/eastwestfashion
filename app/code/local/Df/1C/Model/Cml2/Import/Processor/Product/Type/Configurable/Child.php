<?php
class Df_1C_Model_Cml2_Import_Processor_Product_Type_Configurable_Child
	extends Df_1C_Model_Cml2_Import_Processor_Product_Type_Simple_Abstract {
	/**
	 * @override
	 * @return Df_1C_Model_Cml2_Import_Processor
	 */
	public function process() {
		if ($this->getEntityOffer()->isTypeConfigurableChild()) {
			$this->getImporter()->import();
			df_h()->_1c()->log(
				'%s товар «%s».'
				,!is_null($this->getExistingMagentoProduct()) ? 'Обновлён' : 'Создан'
				,$this->getImporter()->getProduct()->getName()
			);
			df()->registry()->products()
				->addEntity(
					$this->getImporter()->getProduct()
				)
			;
		}
		return $this;
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getSku() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				!is_null($this->getExistingMagentoProduct())
				? $this->getExistingMagentoProduct()->getSku()
				: $this->getEntityOffer()->getExternalId()
			;
			df_result_string_not_empty($this->{__METHOD__});
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return int
	 */
	protected function getVisibility() {
		return Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer
	 * @return Df_1C_Model_Cml2_Import_Processor_Product_Type_Configurable_Child
	 */
	public static function i(Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer) {
		return new self(array(self::P__ENTITY => $offer));
	}
}