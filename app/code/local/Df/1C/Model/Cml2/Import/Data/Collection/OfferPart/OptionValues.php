<?php
class Df_1C_Model_Cml2_Import_Data_Collection_OfferPart_OptionValues
	extends Df_1C_Model_Cml2_Import_Data_Collection {
	/**
	 * @override
	 * @return Df_1C_Model_Cml2_Import_Data_Entity_OfferPart_OptionValue[]
	 */
	public function getItems() {
		if (!isset($this->{__METHOD__})) {
			// Важно здесь сразу инициализировать переменную $this->{__METHOD__},
			// чтобы не попадать в рекурсию в коде ниже.
			$this->{__METHOD__} = parent::getItems();
			if ((0 === count($this->{__METHOD__})) && $this->getOffer()->isTypeConfigurableChild()) {
				$this->addItem(
					Df_1C_Model_Cml2_Import_Data_Entity_OfferPart_OptionValue_Anonymous::i(
						$this->getOffer()
					)
				);
			}
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getItemClass() {
		return Df_1C_Model_Cml2_Import_Data_Entity_OfferPart_OptionValue::_CLASS;
	}

	/**
	 * @override
	 * Позволяет добавлять к создаваемым элементам
	 * дополнительные, единые для всех элементов, параметры
	 * @return array(string => mixed)
	 */
	protected function getItemParamsAdditional() {
		return array_merge(
			parent::getItemParamsAdditional()
			, array(self::P__OFFER => $this->getOffer())
		);
	}

	/**
	 * @override
	 * @return string[]
	 */
	protected function getItemsXmlPathAsArray() {
		return array('ХарактеристикиТовара', 'ХарактеристикаТовара');
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Entity_Offer */
	private function getOffer() {return $this->cfg(self::P__OFFER);}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_prop(self::P__OFFER, Df_1C_Model_Cml2_Import_Data_Entity_Offer::_CLASS);
	}
	const _CLASS = __CLASS__;
	const P__OFFER = 'offer';
	/**
	 * @static
	 * @param Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer
	 * @param Df_Varien_Simplexml_Element $element
	 * @return Df_1C_Model_Cml2_Import_Data_Collection_OfferPart_OptionValues
	 */
	public static function i(
		Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer, Df_Varien_Simplexml_Element $element
	) {
		return new self(array(self::P__OFFER => $offer, self::P__SIMPLE_XML => $element));
	}
}