<?php
abstract class Df_1C_Model_Cml2_Import_Data_Entity extends Df_Core_Model_SimpleXml_Parser_Entity {
	/** @return string */
	public function getExternalId() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->getEntityParam('Ид');
			// Идентификатор у передаваемого из 1С:Управление торговлей
			// в интернет-магазин товара должен быть всегда
			df_result_string_not_empty($this->{__METHOD__});
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return string
	 */
	public function getId() {
		/**
		 * Обратите внимание, что именно getId должен вызывать getExternalId,
		 * а не наоборот, потому что раньше у нас был только метод getExternalId,
		 * и потомки данного класса перекрывают именно метод getExternalId
		 */
		return $this->getExternalId();
	}

	/**
	 * @override
	 * @return string
	 */
	public function getName() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->getEntityParam('Наименование');
			df_result_string_not_empty($this->{__METHOD__});
		}
		return $this->{__METHOD__};
	}

	/**
	 * Данный метод никак не связан данным с классом,
	 * однако включён в класс для удобного доступа объектов класса к реестру
	 * (чтобы писать $this->getRegistry() вместо Df_1C_Helper_Cml2_Registry::s())
	 * @return Df_1C_Helper_Cml2_Registry
	 */
	protected function getRegistry() {return Df_1C_Helper_Cml2_Registry::s();}

	const _CLASS = __CLASS__;
}