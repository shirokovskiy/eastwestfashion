<?php
abstract class Df_1C_Model_Cml2_Import_Processor_Product_Type
	extends Df_1C_Model_Cml2_Import_Processor_Product {
	/** @return float */
	abstract protected function getPrice();
	/** @return string */
	abstract protected function getSku();
	/** @return string */
	abstract protected function getType();
	/** @return int */
	abstract protected function getVisibility();

	/** @return Df_Dataflow_Model_Importer_Product */
	protected function getImporter() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = Df_Dataflow_Model_Importer_Product::i(
				Df_Dataflow_Model_Import_Product_Row::i(
					array_merge(
						$this->getExistingMagentoProduct() ? $this->getProductDataUpdateOnly()
						: $this->getProductDataNewOnly()
						, $this->getProductDataNewOrUpdate()
					)
				)
			);
		}
		return $this->{__METHOD__};
	}

	/** @return array(string => string) */
	protected function getProductDataNewOrUpdateBase() {
		/** @var int[] $categoryIds */
		$categoryIds = $this->getEntityProduct()->getCategoryIds();
		/**
		 * Сохраняем уже имеющиеся привязки товара к разделам
		 * @link http://magento-forum.ru/topic/3432/
		 */
		if (!is_null($this->getExistingMagentoProduct())) {
			$categoryIds =
				array_merge(
					$categoryIds
					,$this->getExistingMagentoProduct()->getCategoryIds()
				)
			;
		}
		/** @var array(string => string) $result */
		$result =
			array(
				Df_Eav_Const::ENTITY_EXTERNAL_ID => $this->getEntityOffer()->getExternalId()
				,'sku' => $this->getSku()
				,Df_Dataflow_Model_Import_Product_Row::FIELD__SKU_NEW => $this->getSkuToImport()
				,'category_ids' => implode(',', $categoryIds)
				,'name' => $this->getName()
				// Поле является обязательным при импорте нового товара
				,'price' => $this->getPrice()
				,'product_name' => $this->getEntityOffer()->getName()
				,'description' => $this->getDescription()
				,'short_description' => $this->getDescriptionShort()
				,'qty' => $this->getEntityOffer()->getQuantity()
			)
		;
		if ($this->getEntityProduct()->getWeight()) {
			$result['weight'] = $this->getEntityProduct()->getWeight();
		}
		else {
			/** @var float|null $currentWeight */
			$currentWeight = null;
			if (!is_null($this->getExistingMagentoProduct())) {
				$currentWeight =
					$this->getExistingMagentoProduct()->getDataUsingMethod(
						Df_Catalog_Model_Product::P__WEIGHT
					)
				;
			}
			$result['weight'] = !$currentWeight ? 0.0 : $currentWeight;
		}
		if (!is_null($this->getExistingMagentoProduct())) {
			/** @var Mage_CatalogInventory_Model_Stock_Item|null $stockItem  */
			$stockItem = $this->getExistingMagentoProduct()->getDataUsingMethod('stock_item');
			if (is_null($stockItem)) {
				$stockItem->assignProduct($this->getExistingMagentoProduct());
			}
			$result['is_in_stock'] =
				(rm_nat0($stockItem->getMinQty()) < $this->getEntityOffer()->getQuantity())
			;
		}
		else {
			/** @var int $minQty */
			$minQty =
				rm_nat0(
					Mage::getStoreConfig(
						Mage_CatalogInventory_Model_Stock_Item::XML_PATH_MIN_QTY
						,df()->state()->getStoreProcessed()
					)
				)
			;
			$result['is_in_stock'] = ($minQty < $this->getEntityOffer()->getQuantity());
		}
		/** @var array(string => string) $tierPrices */
		$tierPrices = $this->getTierPricesInImporterFormat();
		if ($tierPrices) {
			$result = array_merge($result, $tierPrices);
		}
		return $result;
	}
	
	/** @return string[] */
	protected function getProductDataUpdateOnly() {
		return array('store' => $this->getExistingMagentoProduct()->getStore()->getCode());
	}

	/** @return string */
	private function getDescription() {
		return
			$this->getDescriptionAbstract(
				$productField =	Df_Catalog_Model_Product::P__DESCRIPTION
				,$fieldsToUpdate =
					array(
						Df_1C_Model_Config_Source_WhichDescriptionFieldToUpdate::VALUE__DESCRIPTION
						,Df_1C_Model_Config_Source_WhichDescriptionFieldToUpdate::VALUE__BOTH
					)
			)
		;
	}

	/**
	 * @param string $productField
	 * @param string[] $fieldsToUpdate
	 * @return string
	 */
	private function getDescriptionAbstract($productField, array $fieldsToUpdate) {
		df_param_string($productField, 0);
		/** @var string $result */
		$result = df_cfg()->_1c()->product()->description()->getDefault();
		/** @var string|null $currentDescription */
		$currentDescription =
			is_null($this->getExistingMagentoProduct())
			? null
			: $this->getExistingMagentoProduct()->getDataUsingMethod($productField)
		;
		/** @var bool $canUpdateCurrentDescription */
		$canUpdateCurrentDescription =
				!df_cfg()->_1c()->product()->description()->preserveInUnique()
			||
				!$currentDescription
			||
				($currentDescription === df_cfg()->_1c()->product()->description()->getDefault())
		;
		/**
		 * Обрабатываем случай,
		 * когда в 1С на товарной карточке заполнено поле «Файл описания для сайта»
		 */
		if (
				(Df_Catalog_Model_Product::P__DESCRIPTION === $productField)
			&&
				$this->getEntityProduct()->getDescriptionFull()
		) {
			$result =
				$canUpdateCurrentDescription
				? $this->getEntityProduct()->getDescriptionFull()
				: $currentDescription
			;
		}
		else {
			if (
				!in_array(
					df_cfg()->_1c()->product()->description()->whichFieldToUpdate()
					,$fieldsToUpdate
				)
			) {
				if ($currentDescription) {
					$result = $currentDescription;
				}
			}
			else {
				$result =
					$canUpdateCurrentDescription
					? $this->getEntityProduct()->getDescription()
					: $currentDescription
				;
			}
		}
		df_result_string($result);
		return $result;
	}

	/** @return string */
	private function getDescriptionShort() {
		return
			$this->getDescriptionAbstract(
				$productField =	Df_Catalog_Model_Product::P__SHORT_DESCRIPTION
				,$fieldsToUpdate =
					array(
						Df_1C_Model_Config_Source_WhichDescriptionFieldToUpdate::VALUE__SHORT_DESCRIPTION
						,Df_1C_Model_Config_Source_WhichDescriptionFieldToUpdate::VALUE__BOTH
					)
			)
		;
	}

	/** @return string */
	private function getName() {
		if (!isset($this->{__METHOD__})) {
			/** @var string $result */
			$result = $this->getEntityOffer()->getName();
			if (
					(
						/**
						 * @link http://magento-forum.ru/topic/3655/
						 */
							Df_1C_Model_Config_Source_ProductNameSource::VALUE__NAME_FULL
						===
							df_cfg()->_1c()->product()->name()->getSource()
					)
				&&
					/**
					 * Небольшая тонкость.
					 * Дело в том, что имя в товарном предложении может быть
					 * «Active Kids Norveg кальсоны детские (112)»,
					 * а имя в товаре — «Active Kids Norveg кальсоны детские».
					 * Так бывает, когда в «1С: Управление торговлей»
					 * характеристики заданы индивидуально для товара,
					 * а не общие для вида номенклатуры.
					 * В этом случае нам разумней подставить в товар в интернет-магазине
					 * более информативное имя из товарного предложения
					 * («Active Kids Norveg кальсоны детские (112)»).
					 */
					(
							$this->getEntityOffer()->getName()
						===
							$this->getEntityProduct()->getName()
					)
			) {
				$result = $this->getEntityProduct()->getNameFull();
			}
			df_result_string($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return string[] */
	private function getProductDataNewOnly() {
		/** @var string[] $result */
		$result =
			array(
				'websites' => df()->state()->getStoreProcessed()->getWebsite()->getId()
				,'attribute_set' =>
					$this->getEntityProduct()->getAttributeSet()->getAttributeSetName()
				,'type' => $this->getType()
				,'product_type_id' => $this->getType()
				,'store' => df()->state()->getStoreProcessed()->getCode()
				,'store_id' => df()->state()->getStoreProcessed()->getId()
				,'has_options' => false
				,'meta_title' => null
				,'meta_description' => null
				,'image' => null
				,'small_image' => null
				,'thumbnail' => null
				,'url_key' => null
				,'url_path' => null
				,'image_label' => null
				,'small_image_label'	=> null
				,'thumbnail_label' => null
				,'country_of_manufacture' => null
				,'visibility' => $this->getVisibilityAsString()
				,'tax_class_id' => df_mage()->taxHelper()->__('None')
				,'meta_keyword' => null
				,'use_config_min_qty' => true
				,'is_qty_decimal' => null
				,'use_config_backorders' => true
				,'use_config_min_sale_qty' => true
				,'use_config_max_sale_qty' => true
				,'low_stock_date' => null
				,'use_config_notify_stock_qty' => true
				,'manage_stock' => true
				,'use_config_manage_stock' => true
				,'stock_status_changed_auto' => null
				,'use_config_qty_increments' => true
				,'use_config_enable_qty_inc' => true
				,'is_decimal_divided' => null
				,'stock_status_changed_automatically' => null
				,'use_config_enable_qty_increments' => true
			)
		;
		return $result;
	}

	/** @return string[] */
	private function getProductDataNewOrUpdate() {
		return array_merge(
			$this->getProductDataNewOrUpdateBase()
			,$this->getProductDataNewOrUpdateAttributeValuesCustom()
			,$this->getProductDataNewOrUpdateOptionValues()
		);
	}

	/** @return array(string => string) */
	private function getProductDataNewOrUpdateAttributeValuesCustom() {
		/** @var array(string => string) $result */
		$result = array();
		foreach ($this->getEntityProduct()->getAttributeValuesCustom() as $value) {
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_ProductPart_AttributeValue_Custom $value */
			if (
				// 1C для каждого товара
				// указывает не только значения свойств, относящихся к товару,
				// но и значения свойств, к товару никак не относящихся,
				// при этом значения — пустые, например:
				//
				//	<ЗначенияСвойства>
				//		<Ид>b79b0fe0-c8a5-11e1-a928-4061868fc6eb</Ид>
				//		<Значение/>
				//	</ЗначенияСвойства>
				//
				// Мы не обрабатываем эти свойства, потому что их обработка приведёт к добавлению
				// к прикладному типу товара данного свойства, а нам это не нужно, потому что
				// свойство не имеет отношения к прикладному типу товара.
					$value->getEntityParam('Значение')
				||
					$value->getEntityParam('ИдЗначения')
				// 11 февраля 2014 года заметил,
				// что 1С:Управление торговлей 11.1 при использовании версии 2.05 протокола CommerceML
				// и версии 8.3 платформы 1С:Предприятие
				// при обмене данными с интернет-магазином передаёт информацию о производителе
				// не в виде стандартного атрибута, а иначе:
				//
				//	<КоммерческаяИнформация ВерсияСхемы="2.05" ДатаФормирования="2014-02-11T15:32:13">
				//		(...)
				//		<Каталог СодержитТолькоИзменения="false">
				//			(...)
				//			<Товары>
				//				<Товар>
				//					(...)
				//					<Изготовитель>
				//						<Ид>9bf2b1bf-8e9a-11e3-bd2c-742f68ccd0fb</Ид>
				//						<Наименование>Tecumseh</Наименование>
				//						<ОфициальноеНаименование>Tecumseh</ОфициальноеНаименование>
				//					</Изготовитель>
				//					(...)
				//				</Товар>
				//			</Товары>
				//		</Каталог>
				//	</КоммерческаяИнформация>
				||
					$value->getEntityParam('Наименование')
				||
					$value->isAttributeExistAndBelongToTheProductType()
			)  {
				$result[$value->getAttributeName()] = $value->getValueForDataflow();
			}
		}
		return $result;
	}

	/** @return array(string => string) */
	private function getProductDataNewOrUpdateOptionValues() {
		/** @var array(string => string) $result */
		$result = array();
		if (0 < count($this->getEntityOffer()->getOptionValues())) {
			df_h()->_1c()->create1CAttributeGroupIfNeeded(
				$this->getEntityProduct()->getAttributeSet()->getId()
			);
		}
		// Импорт значений настраиваемых опций
		foreach ($this->getEntityOffer()->getOptionValues() as $optionValue) {
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_OfferPart_OptionValue $optionValue */
			Df_Catalog_Model_Installer_AddAttributeToSet::processStatic(
				$optionValue->getAttributeMagento()->getAttributeCode()
				,$this->getEntityProduct()->getAttributeSet()->getId()
				,Df_1C_Const::PRODUCT_ATTRIBUTE_GROUP_NAME
			);
			/** @var Mage_Eav_Model_Entity_Attribute_Option $option */
			$option = $optionValue->getOption();
			$result[$optionValue->getAttributeMagento()->getName()] = $option->getData('value');
		}
		return $result;
	}

	/** @return string */
	private function getSkuToImport() {
		/** @var string $result */
		$result = $this->getEntityProduct()->getSku();
		if ($result) {
			/**
			 * Вдруг этот артикул уже использует другой товар?
			 * @var int|null $productIdBySku
			 */
			$productIdBySku = df_h()->catalog()->product()->getIdBySku($result);
			if (
					$productIdBySku
				&&
					(
							is_null($this->getExistingMagentoProduct())
						||
							($this->getExistingMagentoProduct()->getId() != $productIdBySku)
					)
			) {
				/**
				 * Товар с данным артикулом уже присутствует в магазине.
				 */
				$result = null;
			}
		}
		if (!$result) {
			$result = $this->getSku();
		}
		df_result_string_not_empty($result);
		return $result;
	}

	/** @return mixed[] */
	private function getTierPricesInImporterFormat() {
		if (!isset($this->{__METHOD__})) {
			/** @var mixed[] $result  */
			$result = array();
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_OfferPart_Price|null $mainPrice */
			$mainPrice = $this->getEntityOffer()->getPrices()->getMain();
			foreach ($this->getEntityOffer()->getPrices()->getItems() as $price) {
				/** @var Df_1C_Model_Cml2_Import_Data_Entity_OfferPart_Price $price */
				if (is_null($mainPrice) || ($price->getId() !== $mainPrice->getId())) {
					/** @var int|null $customerGroupId */
					$customerGroupId = $price->getPriceType()->getCustomerGroupId();
					if ($customerGroupId) {
						/** @var string $groupPriceKey */
						$groupPriceKey =
							rm_sprintf(
								'rm_tier_price_%d_%d_%d'
								,df()->state()->getStoreProcessed()->getWebsiteId()
								,$customerGroupId
								,1
							)
						;
						$result[$groupPriceKey] = $price->getPriceBase();
					}
				}
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	private function getVisibilityAsString() {
		/** @var string $result */
		$result =
			df_a(
				Mage_Catalog_Model_Product_Visibility::getOptionArray()
				,$this->getVisibility()
			)
		;
		df_result_string($result);
		return $result;
	}

	const _CLASS = __CLASS__;
}