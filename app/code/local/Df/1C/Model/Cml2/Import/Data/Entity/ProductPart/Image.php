<?php
class Df_1C_Model_Cml2_Import_Data_Entity_ProductPart_Image
	extends Df_1C_Model_Cml2_Import_Data_Entity {
	/**
	 * @override
	 * @return string
	 */
	public function getExternalId() {
		return $this->getFilePathRelative();
	}

	/** @return string */
	public function getFilePathFull() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				str_replace(
					DS, '/'
					, $this->getRegistry()->import()->files('catalog')->getFullPathByRelativePath(
						$this->getFilePathRelative()
					)
				)
			;
		}
		return $this->{__METHOD__};
	}

	/**
	 * Обратите внимание, что результат метода может быть пустой строкой.
	 * В частности, такое замечено в магазине belle.com.ua:
	 * там 1С передаёт интернет-магазину пустой тег <Картинка/>.
	 * @param bool $skipValidation [optional]
	 * @return string
	 */
	public function getFilePathRelative($skipValidation = false) {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = (string)($this->e());
		}
		if (!$skipValidation) {
			df_assert(
				$this->{__METHOD__}
				,'1C: Управление торговлей почему-то передала в интернет-магазин'
				.' пустой путь к файлу товарного изображения.'
			);
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return string
	 */
	public function getName() {
		/** @var string $result */
		$result = $this->e()->getAttribute('Описание');
		df_result_string($result);
		return $result;
	}

	/**
	 * От разультата этого метода зависит добавление данного объекта
	 * в коллекцию Df_Core_Model_SimpleXml_Parser_Collection
	 * @ovverride
	 * @see Df_Core_Model_SimpleXml_Parser_Collection::getItems()
	 * @return bool
	 */
	public function isValid() {
		return parent::isValid() && $this->getFilePathRelative($skipValidation = true);
	}

	/**
	 * Используется из @see Df_1C_Model_Cml2_Import_Data_Collection_ProductPart_Images::getItemClass()
	 */
	const _CLASS = __CLASS__;
}