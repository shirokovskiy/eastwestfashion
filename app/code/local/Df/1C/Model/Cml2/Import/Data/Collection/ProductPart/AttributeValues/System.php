<?php
class Df_1C_Model_Cml2_Import_Data_Collection_ProductPart_AttributeValues_System
	extends Df_1C_Model_Cml2_Import_Data_Collection {
	/**
	 * @override
	 * @return string
	 */
	protected function getItemClass() {
		return Df_1C_Model_Cml2_Import_Data_Entity_ProductPart_AttributeValue_System::_CLASS;
	}

	/**
	 * @override
	 * @return string[]
	 */
	protected function getItemsXmlPathAsArray() {
		return array('ЗначенияРеквизитов', 'ЗначениеРеквизита');
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param Df_Varien_Simplexml_Element $element
	 * @return Df_1C_Model_Cml2_Import_Data_Collection_ProductPart_AttributeValues_System
	 */
	public static function i(Df_Varien_Simplexml_Element $element) {
		return new self(array(self::P__SIMPLE_XML => $element));
	}
}