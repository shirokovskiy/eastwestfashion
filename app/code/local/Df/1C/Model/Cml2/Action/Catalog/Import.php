<?php
class Df_1C_Model_Cml2_Action_Catalog_Import extends Df_1C_Model_Cml2_Action_Catalog {
	/**
	 * @override
	 * @return Df_1C_Model_Cml2_Action
	 */
	protected function processInternal() {
		switch($this->getFileRelativePath()) {
			case Df_1C_Model_Cml2_Registry_Import_Files::FILE__IMPORT:
				$this->importCategories();
				$this->importReferenceLists();
				break;
			case Df_1C_Model_Cml2_Registry_Import_Files::FILE__OFFERS:
				/** @var int $count */
				$count = count($this->getCollections()->getOffers());
				if (0 === $count) {
					df_h()->_1c()->log('Товарные предложения отсутствуют.');
				}
				else {
					df_h()->_1c()->log('Товарных предложений: %d.', $count);
					$this->importProductsSimple();
					$this->importProductsSimplePartImages();
					$this->importProductsConfigurable();
					$this->importProductsConfigurablePartImages();
					df_h()->_1c()->log('Начата перестройка расчётных таблиц.');
					df_h()->index()->reindexEverything();
					df_h()->_1c()->log('Завершена перестройка расчётных таблиц.');
				}
				break;
			default:
				df_error('Непредусмотренный файл: %s', $this->getFileRelativePath());
				break;
		}
		$this->setResponseBodyAsArrayOfStrings(array('success', ''));
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Registry_Import_Collections */
	private function getCollections() {return $this->getRegistry()->import()->collections();}

	/** @return Df_Varien_Simplexml_Element */
	protected function getSimpleXmlElement() {
		return $this->getFiles()->getByRelativePath($this->getFileRelativePath());
	}

	/** @return Df_1C_Model_Cml2_Action_Catalog_Import */
	private function importCategories() {
		/** @var int $count */
		$count = count($this->getCollections()->getCategories());
		if (0 === $count) {
			df_h()->_1c()->log('Товарные разделы отсутствуют.');
		}
		else {
			df_h()->_1c()->log('Товарных разделов: %d.', $count);
			df_h()->_1c()->log('Импорт товарных разделов начат.');
			foreach ($this->getCollections()->getCategories() as $category) {
				/** @var Df_1C_Model_Cml2_Import_Data_Entity_Category $category */
				Df_1C_Model_Cml2_Import_Processor_Category::i(
					$this->getRegistry()->import()->getRootCategory(), $category
				)->process();
			}
			df_h()->_1c()->log('Импорт товарных разделов завершён.');
		}
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Catalog_Import */
	private function importProductsConfigurable() {
		/** @var int $countParent */
		$countParent = count($this->getCollections()->getOffersConfigurableParent());
		if (0 === $countParent) {
			/** @var int $countChildren */
			$countChildren = count($this->getCollections()->getOffersConfigurableChild());
			if (0 === $countChildren) {
				df_h()->_1c()->log('Настраиваемые товары отсутствуют.');
			}
			else {
				df_h()->_1c()->log(
					'ВНИМАНИЕ: отсутствуют настраиваемые товары, однако присутствуют их составные части!.'
				);
			}
		}
		else {
			df_h()->_1c()->log('Настраиваемых товаров: %d.', $countParent);
			df_h()->_1c()->log('Импорт настраиваемых товаров начат.');
			foreach ($this->getCollections()->getOffersConfigurableParent() as $offer) {
				/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
				Df_1C_Model_Cml2_Import_Processor_Product_Type_Configurable::i($offer)->process();
			}
			df_h()->_1c()->log('Импорт настраиваемых товаров завершён.');
		}
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Catalog_Import */
	private function importProductsConfigurablePartImages() {
		foreach ($this->getCollections()->getOffers() as $offer) {
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
			if ($offer->isTypeConfigurableParent()) {
				Df_1C_Model_Cml2_Import_Processor_Product_Part_Images::i($offer)->process();
			}
		}
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Catalog_Import */
	private function importProductsSimple() {
		/** @var int $count */
		$count = count($this->getCollections()->getOffersSimple());
		if (0 === $count) {
			df_h()->_1c()->log('Простые товары отсутствуют.');
		}
		else {
			df_h()->_1c()->log('Простых товаров: %d.', $count);
			df_h()->_1c()->log('Импорт простых товаров начат.');
			foreach ($this->getCollections()->getOffersSimple() as $offer) {
				/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
				Df_1C_Model_Cml2_Import_Processor_Product_Type_Simple::i($offer)->process();
			}
			df_h()->_1c()->log('Импорт простых товаров завершён.');
		}
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Catalog_Import */
	private function importProductsSimplePartImages() {
		foreach ($this->getCollections()->getOffers() as $offer) {
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
			if ($offer->isTypeSimple()) {
				Df_1C_Model_Cml2_Import_Processor_Product_Part_Images::i($offer)->process();
			}
		}
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Catalog_Import */
	private function importReferenceLists() {
		df_h()->_1c()->log('Импорт справочников начат.');
		foreach ($this->getCollections()->getAttributes() as $attribute) {
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_Attribute $attribute */
			// Обратываем только свойства, у которых тип значений — «Справочник».
			if ($attribute instanceof Df_1C_Model_Cml2_Import_Data_Entity_Attribute_ReferenceList) {
				/** @var Df_1C_Model_Cml2_Import_Data_Entity_Attribute_ReferenceList $attribute */
				Df_1C_Model_Cml2_Import_Processor_ReferenceList::i($attribute)->process();
			}
		}
		df_h()->_1c()->log('Импорт справочников завершён.');
		return $this;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_1C_Model_Cml2_Action_Catalog_Import
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}