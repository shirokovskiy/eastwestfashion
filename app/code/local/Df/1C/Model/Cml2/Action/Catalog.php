<?php
abstract class Df_1C_Model_Cml2_Action_Catalog extends Df_1C_Model_Cml2_Action {
	/** @return string */
	protected function getFileFullPath() {
		/** @var string $result */
		$result = $this->getFiles()->getFullPathByRelativePath($this->getFileRelativePath());
		df_result_string_not_empty($result);
		return $result;
	}

	/**
	 * Обратите внимание,
	 * что этот метод может вернуть не просто имя файла (catalog.xml, offers.xml),
	 * но и имя с относительным путём (для файлов картинок), например:
	 * import_files/cb/cbcf4934-55bc-11d9-848a-00112f43529a_b5cfbe1a-c400-11e1-a851-4061868fc6eb.jpeg
	 * @return string
	 */
	protected function getFileRelativePath() {
		/** @var string $result */
		$result = $this->getRmRequest()->getParam('filename');
		if (!df_check_string_not_empty($result)) {
			df_error(
				'Учётная система нарушает протокол обмена данными.'
				.' В данном сценарии она должна была передать в адресной строке параметр «filename».'
			);
		}
		df_result_string_not_empty($result);
		return $result;
	}

	/** @return Df_1C_Model_Cml2_Registry_Import_Files */
	protected function getFiles() {
		return $this->getRegistry()->import()->files('catalog');
	}

	const _CLASS = __CLASS__;
}