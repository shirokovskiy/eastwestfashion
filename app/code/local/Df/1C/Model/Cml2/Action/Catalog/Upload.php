<?php
class Df_1C_Model_Cml2_Action_Catalog_Upload extends Df_1C_Model_Cml2_Action_Catalog {
	/**
	 * @override
	 * @return Df_1C_Model_Cml2_Action
	 */
	protected function processInternal() {
		rm_file_put_contents($this->getFileFullPath(), file_get_contents('php://input'));
		$this->setResponseBodyAsArrayOfStrings(array('success', ''));
		return $this;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_1C_Model_Cml2_Action_Catalog_Upload
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}