<?php
class Df_1C_Model_Cml2_Action_Front extends Df_1C_Model_Cml2_Action {
	/**
	 * @override
	 * @return Df_1C_Model_Cml2_Action
	 */
	protected function processInternal() {
		if (
				Df_1C_Model_Cml2_InputRequest_Generic::MODE__CHECK_AUTH
			===
				$this->getRmRequest()->getMode()
		) {
			$this->action_login();
		}
		else {
			$this->checkLoggedIn();
			if (
					Df_1C_Model_Cml2_InputRequest_Generic::MODE__INIT
				===
					$this->getRmRequest()->getMode()
			) {
				$this->action_init();
			}
			else {
				if (
						Df_1C_Model_Cml2_InputRequest_Generic::TYPE__CATALOG
					===
						$this->getRmRequest()->getType()
				) {
					switch($this->getRmRequest()->getMode()) {
						case Df_1C_Model_Cml2_InputRequest_Generic::MODE__FILE:
							$this->action_catalogUpload();
							break;
						case Df_1C_Model_Cml2_InputRequest_Generic::MODE__IMPORT:
							$this->action_catalogImport();
							break;
					}
				}
				else if (
						Df_1C_Model_Cml2_InputRequest_Generic::TYPE__ORDERS
					===
						$this->getRmRequest()->getType()
				) {
					switch($this->getRmRequest()->getMode()) {
						case Df_1C_Model_Cml2_InputRequest_Generic::MODE__QUERY:
							$this->action_ordersExport();
							break;
						case Df_1C_Model_Cml2_InputRequest_Generic::MODE__SUCCESS:
							$this->action_ordersExportSuccess();
							break;
						case Df_1C_Model_Cml2_InputRequest_Generic::MODE__FILE:
							$this->action_ordersImport();
							break;
					}
				}
			}
		}
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Front */
	private function action_catalogImport() {
		Df_1C_Model_Cml2_Action_Catalog_Import::i($this->getData())->process();
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Front */
	private function action_catalogUpload() {
		Df_1C_Model_Cml2_Action_Catalog_Upload::i($this->getData())->process();
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Front */
	private function action_init() {
		Df_1C_Model_Cml2_Action_Init::i($this->getData())->process();
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Front */
	private function action_login() {
		Df_1C_Model_Cml2_Action_Login::i($this->getData())->process();
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Front */
	private function action_ordersExport() {
		Df_1C_Model_Cml2_Action_Orders_Export::i($this->getData())->process();
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Front */
	private function action_ordersExportSuccess() {
		$this->setResponseBodyAsArrayOfStrings(array('success', ''));
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Front */
	private function action_ordersImport() {
		Df_1C_Model_Cml2_Action_Orders_Import::i($this->getData())->process();
		return $this;
	}

	/** @return Df_1C_Model_Cml2_Action_Front */
	private function checkLoggedIn() {
		$this->getSession()->setSessionId($this->getRequest()->getCookie(self::SESSION_NAME));
		if (!$this->getSession()->isLoggedIn($this->getSession()->getSessionId())) {
			df_error(
				'Доступ к данной операции запрещён,'
				. ' потому что система не смогла распознать администратора (неверная сессия)'
			);
		}
		return $this;
	}

	/**
	 * @static
	 * @param Df_1C_Cml2Controller $controller
	 * @return Df_1C_Model_Cml2_Action_Front
	 */
	public static function i(Df_1C_Cml2Controller $controller) {
		return new self(array(self::P__CONTROLLER => $controller));
	}
}