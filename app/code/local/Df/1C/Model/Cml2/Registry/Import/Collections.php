<?php
class Df_1C_Model_Cml2_Registry_Import_Collections extends Df_Core_Model_Abstract {
	/** @return Df_1C_Model_Cml2_Import_Data_Collection_Attributes */
	public function getAttributes() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				Df_1C_Model_Cml2_Import_Data_Collection_Attributes::i(
					self::registry()->import()->files('catalog')->getByRelativePath(
						Df_1C_Model_Cml2_Registry_Import_Files::FILE__IMPORT
					)
				)
			;
		}
		return $this->{__METHOD__};
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Collection_Categories */
	public function getCategories() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				Df_1C_Model_Cml2_Import_Data_Collection_Categories::i(
					self::registry()->import()->files('catalog')->getByRelativePath(
						Df_1C_Model_Cml2_Registry_Import_Files::FILE__IMPORT
					)
					,array(
						''
						,'КоммерческаяИнформация'
						,'Классификатор'
						,'Группы'
						,'Группа'
					)
				)
			;
		}
		return $this->{__METHOD__};
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Collection_Offers */
	public function getOffers() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				Df_1C_Model_Cml2_Import_Data_Collection_Offers::i(
					self::registry()->import()->files('catalog')->getByRelativePath(
						Df_1C_Model_Cml2_Registry_Import_Files::FILE__OFFERS
					)
				)
			;
		}
		return $this->{__METHOD__};
	}
	
	/** @return Df_1C_Model_Cml2_Import_Data_Entity_Offer[] */
	public function getOffersConfigurableChild() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer[] $result  */
			$result = array();
			foreach ($this->getOffers() as $offer) {
				/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
				if ($offer->isTypeConfigurableChild()) {
					$result[]= $offer;
				}
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}
	
	/** @return Df_1C_Model_Cml2_Import_Data_Entity_Offer[] */
	public function getOffersConfigurableParent() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer[] $result  */
			$result = array();
			foreach ($this->getOffers() as $offer) {
				/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
				if ($offer->isTypeConfigurableParent()) {
					$result[]= $offer;
				}
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}
	
	/** @return Df_1C_Model_Cml2_Import_Data_Entity_Offer[] */
	public function getOffersSimple() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer[] $result  */
			$result = array();
			foreach ($this->getOffers() as $offer) {
				/** @var Df_1C_Model_Cml2_Import_Data_Entity_Offer $offer */
				if ($offer->isTypeSimple()) {
					$result[]= $offer;
				}
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return Df_1C_Model_Cml2_Import_Data_Collection_Products */
	public function getProducts() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				Df_1C_Model_Cml2_Import_Data_Collection_Products::i(
					self::registry()->import()->files('catalog')->getByRelativePath(
						Df_1C_Model_Cml2_Registry_Import_Files::FILE__IMPORT
					)
				)
			;
		}
		return $this->{__METHOD__};
	}

	/** @return Df_1C_Helper_Cml2_Registry */
	public static function registry() {return Df_1C_Helper_Cml2_Registry::s();}

	/** @return Df_1C_Model_Cml2_Registry_Import_Collections */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}