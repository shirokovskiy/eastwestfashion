<?php
class Df_1C_Model_Cml2_Registry_Import extends Df_Core_Model_Abstract {
	/** @return Df_1C_Model_Cml2_Registry_Import_Collections */
	public function collections() {
		return Df_1C_Model_Cml2_Registry_Import_Collections::s();
	}

	/**
	 * @param string $area
	 * @return Df_1C_Model_Cml2_Registry_Import_Files
	 */
	public function files($area) {
		df_param_string($area, 0);
		if (!isset($this->{__METHOD__}[$area])) {
			$this->{__METHOD__}[$area] = Df_1C_Model_Cml2_Registry_Import_Files::i($area);
		}
		return $this->{__METHOD__}[$area];
	}

	/** @return Df_Catalog_Model_Category */
	public function getRootCategory() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = Df_Catalog_Model_Category::ld($this->getRootCategoryId());
		}
		return $this->{__METHOD__};
	}

	/** @return int */
	private function getRootCategoryId() {
		if (!isset($this->{__METHOD__})) {
			/** @var int $result */
			$result = rm_nat0(df()->state()->getStoreProcessed()->getRootCategoryId());
			if (0 === $result) {
				df_error('В обрабатываемом магазине должен присутствовать корневой товарный раздел');
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	const _CLASS = __CLASS__;
	/** @return Df_1C_Model_Cml2_Registry_Import */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}