<?php
class Df_1C_Model_Cml2_Registry_Import_Files extends Df_Core_Model_Abstract {
	/** @return string */
	public function getBasePath() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = df_concat_path(Mage::getBaseDir('var'), 'rm', '1c', $this->getArea());
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param string $name
	 * @return Df_Varien_Simplexml_Element
	 */
	public function getByRelativePath($name) {
		df_param_string($name, 0);
		if (!isset($this->{__METHOD__}[$name])) {
			$this->{__METHOD__}[$name] = rm_xml(file_get_contents($this->getFullPathByRelativePath($name)));
		}
		return $this->{__METHOD__}[$name];
	}

	/**
	 * @param string $relativePath
	 * @return string
	 */
	public function getFullPathByRelativePath($relativePath) {
		return df_concat_path($this->getBasePath(), $relativePath);
	}

	/** @return string */
	private function getArea() {return $this->cfg(self::P__AREA);}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_prop(self::P__AREA, self::V_STRING_NE);
	}
	const _CLASS = __CLASS__;
	const FILE__IMPORT = 'import.xml';
	const FILE__OFFERS = 'offers.xml';
	const P__AREA = 'area';
	/**
	 * @static
	 * @param string $area
	 * @return Df_1C_Model_Cml2_Registry_Import_Files
	 */
	public static function i($area) {
		return new self(array(self::P__AREA => $area));
	}
}