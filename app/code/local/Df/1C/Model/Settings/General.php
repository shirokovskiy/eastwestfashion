<?php
class Df_1C_Model_Settings_General extends Df_1C_Model_Settings_Cml2 {
	/** @return array(string => string) */
	public function getCurrencyCodesMapFrom1CToMagento() {
		if (!isset($this->{__METHOD__})) {
			/** @var array(string => string) $result */
			$result = array();
			/** @var string|null $mapSerialized */
			$mapSerialized = $this->getStringNullable('df_1c/general/non_standard_currency_codes');
			if ($mapSerialized) {
				df_assert_string_not_empty($mapSerialized);
				/** @var array[] $map */
				$map = @unserialize($mapSerialized);
				if (is_array($map)) {
					foreach ($map as $mapItem) {
						/** @var string[] $mapItem */
						df_assert_array($mapItem);
						/** @var string $nonStandardCode */
						$nonStandardCode =
							df_nts(
								$mapItem[
									Df_1C_Block_System_Config_Form_Field_NonStandardCurrencyCodes
										::COLUMN__NON_STANDARD
								]
							)
						;
						/** @var string $standardCode */
						$standardCode =
							df_nts(
								$mapItem[
									Df_1C_Block_System_Config_Form_Field_NonStandardCurrencyCodes
										::COLUMN__STANDARD
								]
							)
						;
						if ($nonStandardCode && $standardCode) {
							$nonStandardCode =
								df_h()->_1c()->cml2()->normalizeNonStandardCurrencyCode(
									$nonStandardCode
								)
							;
							$result[$nonStandardCode] = $standardCode;
						}
					}
				}
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return array(string => string) */
	public function getCurrencyCodesMapFromMagentoTo1C() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = array_flip($this->getCurrencyCodesMapFrom1CToMagento());
			/** При сбое @see array_flip может вернуть null */
			df_result_array($this->{__METHOD__});
		}
		return $this->{__METHOD__};
	}

	/** @return boolean */
	public function isEnabled() {return $this->getYesNo('df_1c/general/enabled');}

	/** @return bool */
	public function needLogging() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->getYesNo('df_1c/general/enable_logging');
		}
		return $this->{__METHOD__};
	}

	/** @return Df_1C_Model_Settings_General */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}