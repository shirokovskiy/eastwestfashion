<?php
class Df_1C_Model_Resource_Setup extends Df_Catalog_Model_Resource_Installer_Attribute {
	/** @return Df_1C_Model_Resource_Setup */
	public function install_1_0_0() {
		$this->add1CIdToEntity('catalog_category', 'General Information');
		$this->add1CIdToEntity('catalog_product');
		$this->add1CIdColumnToTable('eav/attribute_option');
		$this->add1CIdColumnToTable('catalog/eav_attribute');
		Mage::app()->getCache()->clean();
		return $this;
	}

	/** @return Df_1C_Model_Resource_Setup */
	public function install_1_0_2() {
		$this->add1CIdToEntity('catalog_category', 'General Information');
		$this->add1CIdColumnToTable('eav/attribute_option');
		$this->add1CIdColumnToTable('catalog/eav_attribute');
		Mage::app()->getCache()->clean();
		return $this;
	}

	/**
	 * Область действия внешнего идентификатора 1C сменена с витрины на глобальную
	 * (при витринной области действия модуль будет работать некорректно с неосновными витринами)
	 * @return Df_1C_Model_Resource_Setup
	 */
	public function upgrade_1_0_1() {
		$this
			->addAttribute(
				'catalog_category'
				,Df_Eav_Const::ENTITY_EXTERNAL_ID
				,self::get1CIdProperties()
			)
		;
		$this
			->addAttribute(
				'catalog_product'
				,Df_Eav_Const::ENTITY_EXTERNAL_ID
				,self::get1CIdProperties()
			)
		;
		Mage::app()->getCache()->clean();
		return $this;
	}

	/**
	 * @param string $tablePlaceholder
	 * @return Df_1C_Model_Resource_Setup
	 */
	private function add1CIdColumnToTable($tablePlaceholder) {
		df_param_string($tablePlaceholder, 0);
		/** @var string $tableName */
		$tableName = $this->getTable($tablePlaceholder);
		/**
		 * Обратите внимание, что напрямую писать {Df_Eav_Const::ENTITY_EXTERNAL_ID} нельзя:
		 * интерпретатор PHP не разбирает константы внутри {}.
		 * Поэтому заводим переменную.
		 *
		 * @var string $columnName
		 */
		$columnName = Df_Eav_Const::ENTITY_EXTERNAL_ID;
		/**
		 * @var string $columnNameOld
		 */
		$columnNameOld = Df_Eav_Const::ENTITY_EXTERNAL_ID_OLD;
		$this->runSilent("
			ALTER TABLE {$tableName}
				DROP COLUMN `{$columnNameOld}`
			;
		");
		$this->runSilent("
			ALTER TABLE {$tableName}
				DROP COLUMN `{$columnName}`
			;
		");
		$this->runSilent("
			ALTER TABLE {$tableName}
				ADD COLUMN `{$columnName}`
					VARCHAR(255)
					DEFAULT null
			;
		");
		return $this;
	}

	/**
	 * @param string $entityType
	 * @param string|null $groupName[optional]
	 * @param int $ordering[optional]
	 * @return Df_1C_Model_Resource_Setup
	 */
	private function add1CIdToEntity(
		$entityType
		,$groupName = null
		,$ordering = 10
	) {
		df_param_string($entityType, 0);
		if (is_null($groupName)) {
			$groupName = $this->getGeneralGroupName();
		}
		df_param_string($groupName, 1);
		df_param_integer($ordering, 2);
		$this->cleanCache();
		if ($this->getAttributeId($entityType, Df_Eav_Const::ENTITY_EXTERNAL_ID_OLD)) {
			$this->removeAttribute($entityType, Df_Eav_Const::ENTITY_EXTERNAL_ID_OLD);
		}
		if ($this->getAttributeId($entityType, Df_Eav_Const::ENTITY_EXTERNAL_ID)) {
			$this->removeAttribute($entityType, Df_Eav_Const::ENTITY_EXTERNAL_ID);
		}
		/** @var int $entityTypeId */
		$entityTypeId = $this->getEntityTypeId($entityType);
		/** @var int $attributeSetId */
		$attributeSetId = $this->getDefaultAttributeSetId($entityTypeId);
		$this
			->addAttribute(
				$entityType
				,Df_Eav_Const::ENTITY_EXTERNAL_ID
				,self::get1CIdProperties()
			)
		;
		$this
			->addAttributeToGroup(
				$entityTypeId
				,$attributeSetId
				/**
				 * Не используем синтаксис
				 * $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId)
				 * потому что он при предварительно включенной русификации
				 * может приводить к созданию дополнительной вкладки ("Основное")
				 * вместо размещения свойства на главной вкладке ("Главное").
				 */
				,$groupName
				,Df_Eav_Const::ENTITY_EXTERNAL_ID
				,$ordering
			)
		;
		return $this;
	}

	/**
	 * @param string $tablePlaceholder
	 * @return Df_1C_Model_Resource_Setup
	 */
	private function remove1CIdColumnFromTable($tablePlaceholder) {
		df_param_string($tablePlaceholder, 0);
		/** @var string $tableName */
		$tableName = $this->getTable($tablePlaceholder);
		/**
		 * Обратите внимание, что напрямую писать {Df_Eav_Const::ENTITY_EXTERNAL_ID} нельзя:
		 * интерпретатор PHP не разбирает константы внутри {}.
		 * Поэтому заводим переменную.
		 *
		 * @var string $columnName
		 */
		$columnName = Df_Eav_Const::ENTITY_EXTERNAL_ID;
		$this->runSilent("
			ALTER TABLE {$tableName}
				DROP COLUMN `{$columnName}`
			;
		");
		return $this;
	}

	/**
	 * @param string $sql
	 * @return Df_1C_Model_Resource_Setup
	 */
	private function runSilent($sql) {
		df_param_string($sql, 0);
		try {
			$this->run($sql);
		}
		catch(Exception $e) {
			/**
			 * Думаю, никакой обработки тут не требуется.
			 */
		}
		return $this;
	}

	/**
	 * @static
	 * @return string[]
	 */
	public static function get1CIdProperties() {
		return
			array(
				'type' => 'varchar'
				,'backend' => ''
				,'frontend' => ''
				,'label' => '1С ID'
				,'input' => 'text'
				,'class' => ''
				,'source' => ''
				,'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
				,'visible' => true
				,'required' => false
				,'user_defined' => false
				,'default' => ''
				,'searchable' => false
				,'filterable' => false
				,'comparable' => false
				,'visible_on_front' => false
				,'unique' => false
			)
		;
	}

	const _CLASS = __CLASS__;
}