<?php
class Df_Page_Block_Js_Cookie extends Mage_Page_Block_Js_Cookie {
	/**
	 * @override
	 * @return string[]
	 */
	public function getCacheKeyInfo() {
		/** @var string[] $result */
		$result =
			array_merge(
				parent::getCacheKeyInfo()
				,array(
					get_class($this)
					,$this->getDomain()
					,$this->getPath()
				)
			)
		;
		return $result;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this
			->addData(
				array(
					'cache_lifetime' => Df_Core_Block_Template::CACHE_LIFETIME_STANDARD
				)
			)
		;
	}
}