<?php
class Df_Page_Block_Html_Notices extends Mage_Page_Block_Html_Notices {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @return string[]
	 */
	public function getCacheKeyInfo() {
		/** @var string[] $result */
		$result =
			array_merge(
				parent::getCacheKeyInfo()
				,array(
					get_class($this)
					,$this->displayNoscriptNotice()
					,$this->displayDemoNotice()
				)
			)
		;
		if (@class_exists('Mage_Core_Helper_Cookie')) {
			$result[]= df_mage()->core()->cookieHelper()->isUserNotAllowSaveCookie();
		}
		return $result;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->setData('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
	}
}