<?php
class Df_Page_Block_Html_Topmenu extends Mage_Page_Block_Html_Topmenu {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * Обратите внимание, что Mage_Page_Block_Html_Topmenu::_construct
	 * по ошибке объявлен публичным методом, поэтому и у нас он должен быть публичным
	 * @override
	 * @return void
	 */
	public function _construct() {
		parent::_construct();
		if (
				df_module_enabled(Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->pageHtmlTopmenu()
		) {
			$this->setData('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
		}
	}

	/**
	 * @override
	 * @return string[]
	 */
	public function getCacheKeyInfo() {
		/** @var string[] $result */
		$result = parent::getCacheKeyInfo();
		if (
				df_module_enabled(Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->pageHtmlTopmenu()
		) {
			$result =
				array_merge(
					$result
					,array(
						get_class($this)
						,$this->getRequest()->getRequestUri()
					)
				)
			;
		}
		return $result;
	}
}