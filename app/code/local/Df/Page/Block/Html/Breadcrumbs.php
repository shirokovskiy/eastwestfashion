<?php
class Df_Page_Block_Html_Breadcrumbs extends Mage_Page_Block_Html_Breadcrumbs {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @param string $crumbName
	 * @param array(string => string) $crumbInfo
	 * @param bool $after [optional]
	 * @return Df_Page_Block_Html_Breadcrumbs
	 */
	public function addCrumb($crumbName, $crumbInfo, $after = false) {
		$crumbInfo['label'] = $this->__(df_a($crumbInfo, 'label', ''));
		$crumbInfo['title'] = $this->__(df_a($crumbInfo, 'title', ''));
		parent::addCrumb($crumbName, $crumbInfo, $after);
		return $this;
	}

	/**
	 * @override
	 * @return string[]
	 */
	public function getCacheKeyInfo() {
		/** @var string[] $result */
		$result = parent::getCacheKeyInfo();
		if (
				df_module_enabled(Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->pageHtmlBreadcrumbs()
		) {
			$result =
				array_merge(
					$result
					,array(get_class($this))
					,$this->calculateCrumbCacheKeys()
				)
			;
		}
		return $result;
	}

	/** @return array */
	private function calculateCrumbCacheKeys() {
		/** @var array $result */
		$result = array();
		if (is_array($this->_crumbs)) {
			$result = array_keys($this->_crumbs);
		}
		df_result_array($result);
		return $result;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		if (
				df_module_enabled(Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->pageHtmlBreadcrumbs()
		) {
			$this->setData('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
		}
	}
}