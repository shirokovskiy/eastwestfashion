<?php
class Df_Page_Block_Switch extends Mage_Page_Block_Switch {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		// Обратите внимание, что этот метод нельзя записать в одну строку,
		// потому что функция func_get_args() не может быть параметром другой функции.
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @return string[]
	 */
	public function getCacheKeyInfo() {
		/** @var string[] $result */
		$result = parent::getCacheKeyInfo();
		if (
				df_module_enabled(Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->pageSwitch()
			&&
				$this->isTemplateStandard()
		) {
			$result[]= get_class($this);
			if (self::TEMPLATE__STORES === $this->getTemplate()) {
				$result[]= $this->getCurrentGroupId();
			}
			else {
				$result[]= $this->getCurrentStoreId();
				$result[]= $this->getRequest()->getRequestUri();
			}
		}
		return $result;
	}

	/**
	 * Используем этот метод вместо установки cache_lifetime в конструкторе,
	 * потому что конструкторе мы ещё не знаем шаблон блока
	 * @override
	 * @return int|bool
	 */
	public function getCacheLifetime() {
		/** @var int|bool $result */
		$result =
				(
						df_module_enabled(Df_Core_Module::SPEED)
					&&
						df_cfg()->speed()->blockCaching()->pageSwitch()
					&&
						$this->isTemplateStandard()
				)
			?
				Df_Core_Block_Template::CACHE_LIFETIME_STANDARD
			:
				parent::getCacheLifetime()
		;
		return $result;
	}

	/** @return bool */
	private function isTemplateStandard() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				in_array(
					$this->getTemplate()
					, array(self::TEMPLATE__FLAGS, self::TEMPLATE__LANGUAGES, self::TEMPLATE__STORES)
				)
			;
		}
		return $this->{__METHOD__};
	}

	const _CLASS = __CLASS__;
	const TEMPLATE__FLAGS = 'page/switch/flags.phtml';
	const TEMPLATE__LANGUAGES = 'page/switch/languages.phtml';
	const TEMPLATE__STORES = 'page/switch/stores.phtml';
}