<?php
class Df_Page_Model_Html_Head extends Df_Core_Model_Abstract {
	/**
	 * @param array $staticItems
	 * @return array
	 */
	public function addVersionStamp(array $staticItems) {
		foreach ($staticItems as &$rows) {
			foreach ($rows as &$name) {
				if (0 === strpos($name, 'df/')) {
					$name = df()->url()->addVersionStamp($name);
				}
			}
		}
		return $staticItems;
	}

	/**
	 * @param string $format
	 * @param array $staticItems
	 * @return string
	 */
	public function prependAdditionalTags($format, array &$staticItems) {
		/** @var string $result */
		$result = '';
		/** @var bool $jQueryInjected */
		static $jQueryInjected = false;
		if (!$jQueryInjected && (false !== strpos($format, 'script'))) {
			$jQueryInjected = true;
			/**
			 * Добавляем библиотеку jQuery
			 * в соответствии с предпочтениями администратора
			 */

			/** @var Df_Core_Model_Settings_Jquery $settings */
			$settings =
				df_is_admin()
				? df_cfg()->admin()->jquery()
				: df_cfg()->tweaks()->jquery()
			;
			if (
					Df_Admin_Model_Config_Source_JqueryLoadMode::VALUE__LOAD_FROM_LOCAL
				===
					$settings->getLoadMode()
			) {
				$row = df_a($staticItems, null);
				/** @var string $jqueryPath */
				$jqueryPath = (string)(Mage::getConfig()->getNode('df/jquery/core/local'));
				/** @var string $jqueryMigratePath */
				$jqueryMigratePath = (string)(Mage::getConfig()->getNode('df/jquery/migrate/local'));
				df_array_unshift_assoc(
					$row
					,$jqueryMigratePath
					,$jqueryMigratePath
				);
				df_array_unshift_assoc(
					$row
					,$jqueryPath
					,$jqueryPath
				);
				$staticItems[null] = $row;
			}

			else if (
					Df_Admin_Model_Config_Source_JqueryLoadMode::VALUE__LOAD_FROM_GOOGLE
				===
					$settings->getLoadMode()
			) {
				/** @var string $jqueryPath */
				$jqueryPath = (string)(Mage::getConfig()->getNode('df/jquery/core/cdn'));
				/** @var string $jqueryMigratePath */
				$jqueryMigratePath = (string)(Mage::getConfig()->getNode('df/jquery/migrate/cdn'));
				$result =
					implode(
						"\r\n"
						,array(
							Df_Core_Model_Format_Html_Tag::output(
								''
								,'script'
								,array(
									'type' => 'text/javascript'
									,'src' => $jqueryPath
								)
							)
							,Df_Core_Model_Format_Html_Tag::output(
								'jQuery.noConflict(); jQuery.migrateMute = true;'
								,'script'
								,array(
									'type' => 'text/javascript'
								)
							)
							,Df_Core_Model_Format_Html_Tag::output(
								''
								,'script'
								,array(
									'type' => 'text/javascript'
									,'src' => $jqueryMigratePath
								)
							)
							,''
						)
					)
				;
			}
		}
		df_result_string($result);
		return $result;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Page_Model_Html_Head
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}