<?php
class Df_Tag_Block_Popular extends Mage_Tag_Block_Popular {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/** @return string|null */
	public function getTemplate() {
		/** @var string $result */
		$result = parent::getTemplate();
		if (
				df_module_enabled(Df_Core_Module::TWEAKS)
			&&
				df_enabled(Df_Core_Feature::TWEAKS)
			&&
				(
						df_cfg()->tweaks()->tags()->popular()->removeFromAll()
					||
						(
								df_cfg()->tweaks()->tags()->popular()->removeFromFrontpage()
							&&
								rm_handle_presents(Df_Core_Model_Layout_Handle::CMS_INDEX_INDEX)
						)
					||
						(
								df_cfg()->tweaks()->tags()->popular()->removeFromCatalogProductList()
							&&
								rm_handle_presents(Df_Core_Model_Layout_Handle::CATALOG_CATEGORY_VIEW)
						)
					||
						(
								df_cfg()->tweaks()->tags()->popular()->removeFromCatalogProductView()
							&&
								rm_handle_presents(Df_Core_Model_Layout_Handle::CATALOG_PRODUCT_VIEW)
						)
				)
		) {
			/**
			 * Обратите внимание,
			 * что в демо-данных для главной страницы блок tag/popular
			 * создаётся синтаксисом {{block type="tag/popular" template="tag/popular.phtml"}}
			 * уже после события controller_action_layout_generate_blocks_after.
			 *
			 * Поэтому приходится скрывать блок перекрытием метода getTemplate
			 *
			 */
			$result = null;
		}
		return $result;
	}
}