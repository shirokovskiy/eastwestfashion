<?php
class Df_Adminhtml_Block_Catalog_Product_Edit_Tab_Categories extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Categories {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @param Varien_Data_Tree_Node $node
	 * @param int $level
	 * @return array
	 */
	protected function _getNodeJson($node, $level=1)
	{
		$result = parent::_getNodeJson($node, $level);
		if (
				df_module_enabled(Df_Core_Module::ACCESS_CONTROL)
			&&
				df_cfg()->admin()->access_control()->getEnabled()
			&&
				df_h()->accessControl()->getCurrentRole()->isModuleEnabled()
		) {
			if (
				!in_array(
					df_a($result, 'id')
					,df_h()->accessControl()->getCurrentRole()->getCategoryIds()
				)
			) {
				$result['disabled'] = true;
			}
		}
		return $result;
	}

}