<?php
class Df_Adminhtml_Block_Tax_Rule_Edit_Form extends Mage_Adminhtml_Block_Tax_Rule_Edit_Form {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @return Df_Adminhtml_Block_Tax_Rule_Edit_Form
	 */
	protected function _prepareForm() {
		parent::_prepareForm();
		/** @var Varien_Data_Form_Element_Fieldset $fieldset */
		$fieldset = $this->getForm()->getElement('base_fieldset');
		df_assert($fieldset);
		/** @var Varien_Data_Form_Element_Abstract $taxRateField */
		$taxRateField = $fieldset->getElements()->searchById('tax_rate');
		df_assert($taxRateField);
		$taxRateField->setData(
			'note'
			,'Если Вы выберите несколько ставок — система применит только ставку с наибольшим процентом.'
		);
		return $this;
	}
}