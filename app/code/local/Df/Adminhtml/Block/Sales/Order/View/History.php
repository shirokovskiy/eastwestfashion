<?php
class Df_Adminhtml_Block_Sales_Order_View_History extends Mage_Adminhtml_Block_Sales_Order_View_History {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * Escape html entities
	 * @override
	 * @param mixed $data
	 * @param string[] $allowedTags[optional]
	 * @return string
	 */
	public function escapeHtml($data, $allowedTags = null) {
		if (df_enabled(Df_Core_Feature::SALES)) {
			if (is_null($allowedTags)) {
				$allowedTags = array();
			}
			if (df_cfg()->sales()->orderComments()->preserveSomeTagsInAdminOrderView()) {
				$allowedTags =
					array_merge(
						$allowedTags
						,df_cfg()->sales()->orderComments()
							->getTagsToPreserveInAdminOrderView()
					)
				;
			}
			if (df_cfg()->sales()->orderComments()->preserveLineBreaksInAdminOrderView()) {
				$allowedTags[]= 'br';
				$data = nl2br($data);
			}
			$allowedTags = rm_array_unique_fast($allowedTags);
		}
		/** @var string $result */
		$result =
			df_text()->escapeHtml($data, $allowedTags)
		;
		df_result_string($result);
		return $result;
	}
}