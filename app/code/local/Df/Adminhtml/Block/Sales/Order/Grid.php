<?php
class Df_Adminhtml_Block_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var array $args */
		$args = func_get_args();
		/** @var string $result */
		$result = df_h()->localization()->translation()->translateByParent($args, $this);
		return $result;
	}

	/**
	 * @param Varien_Data_Collection $collection
	 * @return void
	 */
	public function setCollection($collection) {
		/**
		 * Нам недостаточно события _load_before,
		 * потому что не все коллекции заказов используются для таблицы заказов,
		 * а в Magento 1.4 по коллекции невозможно понять,
		 * используется ли она для таблицы заказов или нет
		 * (в более поздних версиях Magento понять можно, потому что
		 * коллекция, используемая для таблицы заказов, принадлежит особому классу)
		 */
		Mage::dispatchEvent(
			Df_Core_Model_Event_Adminhtml_Block_Sales_Order_Grid_PrepareCollection::EVENT
			,array('collection' => $collection)
		);
		parent::setCollection($collection);
	}

	/**
	 * @override
	 * @return Df_Adminhtml_Block_Sales_Order_Grid
	 */
	protected function _prepareColumns() {
		parent::_prepareColumns();
		Mage::dispatchEvent(
			Df_Core_Model_Event_Adminhtml_Block_Sales_Order_Grid_PrepareColumnsAfter::EVENT
			,array('grid' => $this)
		);
		/**
		 * Учитывая, что обработчики вызванного выше события могли изменить столбцы,
		 * столбцы надо упорядочить заново.
		 */
		$this->sortColumnsByOrder();
		return $this;
	}
}