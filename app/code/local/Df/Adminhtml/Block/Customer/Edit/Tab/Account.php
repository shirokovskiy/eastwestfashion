<?php
class Df_Adminhtml_Block_Customer_Edit_Tab_Account extends Mage_Adminhtml_Block_Customer_Edit_Tab_Account {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @return Df_Adminhtml_Block_Customer_Edit_Tab_Account
	 */
	public function initForm() {
		parent::initForm();
		if (
				df_enabled(Df_Core_Feature::TWEAKS_ADMIN)
			&&
				df_cfg()->admin()->sales()->customers()->getEnableWebsiteChanging()
		) {
			// Позволяем администратору редактировать поле website_id
			/** @var Varien_Object $websiteIdElement */
			$websiteIdElement = $this->getForm()->getElement('website_id');
			df_assert($websiteIdElement);
			$websiteIdElement->unsetData('disabled');
		}
		return $this;
	}
}