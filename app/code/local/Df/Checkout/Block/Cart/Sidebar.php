<?php
class Df_Checkout_Block_Cart_Sidebar extends Mage_Checkout_Block_Cart_Sidebar {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @return string[]
	 */
	public function getCacheKeyInfo() {
		/** @var string[] $result */
		$result = parent::getCacheKeyInfo();
		if (
				df_module_enabled(Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->checkoutCartSidebar()
		) {
			$result =
				array_merge(
					$result
					,array(get_class($this))
					,$this->getAdditionalKeys()
				)
			;
		}
		return $result;
	}

	/** @return array */
	private function getAdditionalKeys() {
		/** @var array $result */
		$result =
			array(
				$this->getSummaryCount()
				,$this->getSubtotal()
			)
		;
		foreach ($this->getRecentItems() as $quoteItem) {
			/** @var Mage_Sales_Model_Quote_Item $quoteItem */
			$result[]= $quoteItem->getProductId();
			$result[]= $quoteItem->getQty();
		}
		return $result;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		if (
				df_module_enabled(Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->checkoutCartSidebar()
		) {
			$this->setData('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
		}
	}
}