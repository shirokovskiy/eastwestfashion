<?php
class Df_Checkout_Block_Frontend_Ergonomic_Address_Field_Text
	extends Df_Checkout_Block_Frontend_Ergonomic_Address_Field {
	/**
	 * @override
	 * @return array
	 */
	protected function getCssClasses() {
		/** @var array $result */
		$result =
			array_merge(
				parent::getCssClasses()
				,array(
					'input-text'
				)
			)
		;
		df_result_array($result);
		return $result;
	}

	/**
	 * @override
	 * @return string|null
	 */
	protected function getDefaultTemplate() {
		return self::DEFAULT_TEMPLATE;
	}

	const DEFAULT_TEMPLATE = 'df/checkout/ergonomic/address/field/text.phtml';
	const _CLASS = __CLASS__;
}