<?php
class Df_Qiwi_Block_Api_PaymentConfirmation_Success extends Df_Core_Block_Template {
	/** @return string */
	public function getBillNumber() {
		/** @var string $result */
		$result = self::P__BILL_NUMBER;
		df_result_string($result);
		return $result;
	}

	/** @return string */
	public function getPacketDate() {
		/** @var string $result */
		$result = self::P__PACKET_DATE;
		df_result_string($result);
		return $result;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getTemplate() {
		return self::RM__TEMPLATE;
	}

	const P__BILL_NUMBER = 'bill_number';
	const P__PACKET_DATE = 'packet_date';
	const RM__TEMPLATE = 'df/qiwi/api/payment-confirmation/success.xml';
	const _CLASS = __CLASS__;
}