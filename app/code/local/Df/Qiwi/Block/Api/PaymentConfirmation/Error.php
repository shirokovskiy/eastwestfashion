<?php
class Df_Qiwi_Block_Api_PaymentConfirmation_Error extends Df_Core_Block_Template {
	/** @return int */
	public function getFirstCode() {return 1;}
	/** @return int */
	public function getSecondCode() {return 0;}
	/**
	 * @override
	 * @return string
	 */
	public function getTemplate() {return self::RM__TEMPLATE;}
	/** @return Exception */
	private function getException() {return $this->cfg(self::P__EXCEPTION);}
	const _CLASS = __CLASS__;
	const P__EXCEPTION = 'exception';
	const RM__TEMPLATE = 'df/qiwi/api/payment-confirmation/error.xml';
}