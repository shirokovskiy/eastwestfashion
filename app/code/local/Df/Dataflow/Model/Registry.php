<?php
/**
 * Реестры нам нужны для ускорения доступа к одним и тем же объектам и коллекциям объектов.
 * Эти реестры должны использоваться всеми модулями Российской сборки Magento.
 */
class Df_Dataflow_Model_Registry extends Df_Core_Model_Abstract {
	/** @return Df_Dataflow_Model_Registry_Collection_Attributes */
	public function attributes() {
		/** @var Df_Dataflow_Model_Registry_Collection_Attributes $result */
		static $result;
		if (!isset($result)) {
			$result = Df_Dataflow_Model_Registry_Collection_Attributes::i();
		}
		return $result;
	}

	/** @return Df_Dataflow_Model_Registry_Collection_AttributeSets */
	public function attributeSets() {
		/** @var Df_Dataflow_Model_Registry_Collection_AttributeSets $result */
		static $result;
		if (!isset($result)) {
			$result = Df_Dataflow_Model_Registry_Collection_AttributeSets::i();
		}
		return $result;
	}
	
	/** @return Df_Dataflow_Model_Registry_Collection_Categories */
	public function categories() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = Df_Dataflow_Model_Registry_Collection_Categories::i();
		}
		return $this->{__METHOD__};
	}

	/** @return Mage_Core_Model_Store */
	public function getStoreProcessed() {
		if (!isset($this->{__METHOD__})) {
			/**
			 * Обратите внимание, что магазин можно установить вручную
			 * методом @see Df_Dataflow_Model_Registry::setStoreProcessed()
			 */
			$this->{__METHOD__} = df()->state()->getStoreProcessed();
		}
		return $this->{__METHOD__};
	}

	/** @return Df_Dataflow_Model_Registry_Collection_Products */
	public function products() {
		/** @var Df_Dataflow_Model_Registry_Collection_Products $result */
		static $result;
		if (!isset($result)) {
			$result = Df_Dataflow_Model_Registry_Collection_Products::i();
		}
		return $result;
	}

	/** @return Df_Dataflow_Model_Registry */
	public function resetCategories() {
		unset($this->{__CLASS__ . '::categories'});
		return $this;
	}

	/**
	 * @param Mage_Core_Model_Store $storeProcessed
	 * @return Df_Dataflow_Model_Registry
	 */
	public function setStoreProcessed(Mage_Core_Model_Store $storeProcessed) {
		$this->{__CLASS__ . '::getStoreProcessed'} = $storeProcessed;
		return $this;
	}

	/** @return Df_Dataflow_Model_Registry */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}