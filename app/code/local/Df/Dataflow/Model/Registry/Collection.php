<?php
abstract class Df_Dataflow_Model_Registry_Collection
	extends Df_Core_Model_Abstract
	implements IteratorAggregate, Countable {
	/**
	 * @abstract
	 * @return Varien_Data_Collection
	 */
	abstract protected function createCollection();

	/**
	 * @param Varien_Object $entity
	 * @return Df_Dataflow_Model_Registry_Collection
	 */
	public function addEntity(Varien_Object $entity) {
		$this
			->addEntityToExternalIdMap($entity)
			->addEntityToLabelMap($entity)
		;
		return $this;
	}

	/**
	 * @override
	 * @return int
	 */
	public function count() {return $this->getCollectionRm()->count();}

	/**
	 * @param string $externalId
	 * @return Varien_Object|null
	 */
	public function findByExternalId($externalId) {
		return df_a($this->getMapFromExternalIdToEntity(), $externalId);
	}

	/**
	 * @param int $id
	 * @return Varien_Object|null
	 */
	public function findById($id) {return $this->getCollectionRm()->getItemById($id);}

	/**
	 * @param string $label
	 * @return Varien_Object|null
	 */
	public function findByLabel($label) {return df_a($this->getMapFromLabelToEntity(), $label);}

	/**
	 * @override
	 * @return Traversable
	 */
	public function getIterator() {return $this->getCollectionRm()->getIterator();}

	/**
	 * @param Varien_Object $entity
	 * @return Df_Dataflow_Model_Registry_Collection
	 */
	public function removeEntity(Varien_Object $entity) {
		$this
			->removeEntityFromExternalIdMap($entity)
			->removeEntityFromLabelMap($entity)
		;
		return $this;
	}

	/** @return Varien_Data_Collection */
	protected function getCollectionRm() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->createCollection();
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param Varien_Object $entity
	 * @return string|null
	 */
	protected function getEntityExternalId(Varien_Object $entity) {
		return $entity->getData(Df_Eav_Const::ENTITY_EXTERNAL_ID);
	}

	/**
	 * @param Varien_Object $entity
	 * @return string|null
	 */
	protected function getEntityLabel(Varien_Object $entity) {return null;}

	/**
	 * @param Varien_Object $entity
	 * @return Df_Dataflow_Model_Registry_Collection
	 */
	private function addEntityToExternalIdMap(Varien_Object $entity) {
		$this->getMapFromExternalIdToEntity();
		/** @var string|null $externalId */
		$externalId = $this->getEntityExternalId($entity);
		if ($externalId) {
			df_assert_string($externalId);
			$this->{__CLASS__ . '::getMapFromExternalIdToEntity'}[$externalId] = $entity;
		}
		return $this;
	}

	/**
	 * @param Varien_Object $entity
	 * @return Df_Dataflow_Model_Registry_Collection
	 */
	private function addEntityToLabelMap(Varien_Object $entity) {
		$this->getMapFromLabelToEntity();
		/** @var string|null $label */
		$label = $this->getEntityLabel($entity);
		if ($label) {
			df_assert_string($label);
			$this->{__CLASS__ . '::getMapFromLabelToEntity'}[$label] = $entity;
		}
		return $this;
	}

	/** @return array(string => Varien_Object) */
	private function getMapFromExternalIdToEntity() {
		if (!isset($this->{__METHOD__})) {
			/** @var array(string => Varien_Object) $result */
			$result = array();
			foreach ($this->getCollectionRm() as $entity) {
				/** @var Varien_Object $entity */
				/** @var string|null $externalId */
				$externalId = $this->getEntityExternalId($entity);
				if (!is_null($externalId)) {
					df_assert_string($externalId);
					$result[$externalId] = $entity;
				}
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return array(string => Varien_Object) */
	private function getMapFromLabelToEntity() {
		if (!isset($this->{__METHOD__})) {
			/** @var array(string => Varien_Object) $result */
			$result = array();
			foreach ($this->getCollectionRm() as $entity) {
				/** @var Varien_Object $entity */
				/** @var string|null $label */
				$label = $this->getEntityLabel($entity);
				if ($label) {
					df_assert_string($label);
					$result[$label] = $entity;
				}
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param Varien_Object $entity
	 * @return Df_Dataflow_Model_Registry_Collection
	 */
	private function removeEntityFromExternalIdMap(Varien_Object $entity) {
		$this->getMapFromExternalIdToEntity();
		/** @var string|null $externalId */
		$externalId = $this->getEntityExternalId($entity);
		if ($externalId) {
			df_assert_string($externalId);
			unset($this->{__CLASS__ . '::getMapFromExternalIdToEntity'}[$externalId]);
		}
		return $this;
	}

	/**
	 * @param Varien_Object $entity
	 * @return Df_Dataflow_Model_Registry_Collection
	 */
	private function removeEntityFromLabelMap(Varien_Object $entity) {
		$this->getMapFromLabelToEntity();
		/** @var string|null $label */
		$label = $this->getEntityLabel($entity);
		if ($label) {
			df_assert_string($label);
			unset($this->{__CLASS__ . '::getMapFromLabelToEntity'}[$label]);
		}
		return $this;
	}

	const _CLASS = __CLASS__;
}