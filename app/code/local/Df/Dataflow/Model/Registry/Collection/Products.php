<?php
class Df_Dataflow_Model_Registry_Collection_Products extends Df_Dataflow_Model_Registry_Collection {
	/**
	 * @override
	 * @param Varien_Object $entity
	 * @return Df_Dataflow_Model_Registry_Collection
	 */
	public function addEntity(Varien_Object $entity) {
		if (
				!$this->getEntityExternalId($entity)
			&&
				('Df_1C_Cml2Controller' === get_class(df()->state()->getController()))
		) {
			df_error('Добавляемому в реестр товару должен быть присвоен внешний идентификатор');
		}
		parent::addEntity($entity);
		return $this;
	}

	/**
	 * @override
	 * @return Varien_Data_Collection
	 */
	protected function createCollection() {
		/** @var Df_Catalog_Model_Resource_Product_Collection $result */
		$result =
			/**
			 * Отключение денормализации позволяет иметь в коллекции товаров все необходимые нам свойства.
			 * Вместо отключения денормализации есть и другой способ иметь все необходиые свойства:
			 * указать в установочном скрипте,
			 * что требуемые свойства должны попадать в коллекцию в режиме денормализации.
			 * @see Df_Shipping_Model_Setup_2_16_2::process()
			 * Однако методу Df_1C_Model_Cml2_Import_Processor_Product_Type::getDescription()
			 * требуется, чтобы в коллекции товаров присутствовало свойство «описание».
			 * Однако значения поля «описание» могут быть очень длинными,
			 * и если добавить колонку для этого свойства в денормализованную таблицу товаров,
			 * то тогда мы можем превысить устанавливаемый MySQL предел для одной строки таблицы
			 *
			 * «Magento по умолчанию отводит на хранение значения одного свойства товара
			 * в своей базе данных 255 символов, для хранения которых MySQL выделяет 255 * 3 + 2 = 767 байтов.
			 * Magento объединяет все свойства товаров в единой расчётной таблице,
			 * колонками которой служат свойства, а строками — товары.
			 * Если свойств товаров слишком много,
			 * то Magento превышает системное ограничение MySQL на одну строку таблицы:
			 * 65535 байтов,что приводит к сбою построения расчётной таблицы товаров»
			 *
			 * Либо же значение поля описание будет обрезаться в соответствии с установленным администратором
			 * значением опции «Российская сборка» → «Административная часть» → «Расчётные таблицы» →
			 * «Максимальное количество символов для хранения значения свойства товара».
			 */
			Df_Catalog_Model_Resource_Product_Collection::i(
				array(
					Df_Catalog_Model_Resource_Product_Collection::P__DISABLE_FLAT => true
				)
			)
		;
		$result->setStore(df()->registry()->getStoreProcessed());
		/**
		 * По мотивам модуля Яндекс.Маркет
		 */
		$result->addStoreFilter(df()->registry()->getStoreProcessed());
		$result
			->addAttributeToSelect(
				array(
					/**
					 * Нужно методу
					 * Df_1C_Model_Cml2_Import_Processor_Product_Type::getDescriptionAbstract
					 */
					Df_Catalog_Model_Product::P__DESCRIPTION
					,Df_Eav_Const::ENTITY_EXTERNAL_ID
					,Df_Catalog_Model_Product::P__NAME
					,Df_Catalog_Model_Product::P__PRICE
					/**
					 * Нужно методу
					 * Df_1C_Model_Cml2_Import_Processor_Product_Type::getDescriptionAbstract
					 */
					,Df_Catalog_Model_Product::P__SHORT_DESCRIPTION
					,Df_Catalog_Model_Product::P__SKU
					/**
					 * Нужно методу
					 * Df_1C_Model_Cml2_Import_Processor_Product_Type::getProductDataNewOrUpdateBase
					 */
					,Df_Catalog_Model_Product::P__WEIGHT
				)
			)
		;
		/**
		 * По мотивам модуля Яндекс.Маркет.
		 *
		 * Обратите внимание, что метод addCategoryIds
		 * работает только после загрузки коллекции.
		 * Товарные разделы нужны нам
		 * в методе Df_1C_Model_Cml2_Import_Processor_Product_Type::getProductDataNewOrUpdateBase.
		 *
		 * Метод Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection::addCategoryIds
		 * отсутствует в Magento CE 1.4.0.1
		 */
		$result->load();
		if (method_exists ($result, 'addCategoryIds')) {
			call_user_func(array($result, 'addCategoryIds'));
		}
		else {
			$result->addCategoryIdsRm();
		}
		return $result;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Dataflow_Model_Registry_Collection_Products
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}