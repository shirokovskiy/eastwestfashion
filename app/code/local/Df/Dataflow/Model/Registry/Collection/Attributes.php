<?php
/**
 * @method Mage_Catalog_Model_Resource_Eav_Attribute|null findByLabel(string $label)
 */
class Df_Dataflow_Model_Registry_Collection_Attributes extends Df_Dataflow_Model_Registry_Collection {
	/**
	 * @override
	 * @param Varien_Object $entity
	 * @return Df_Dataflow_Model_Registry_Collection
	 */
	public function addEntity(Varien_Object $entity) {
		df_assert($entity instanceof Mage_Catalog_Model_Resource_Eav_Attribute);
		parent::addEntity($entity);
		/** @var Mage_Catalog_Model_Resource_Eav_Attribute $entity */
		$this->addEntityToCodeMap($entity);
		return $this;
	}

	/**
	 * @override
	 * @param string $code
	 * @return Mage_Catalog_Model_Resource_Eav_Attribute|null
	 */
	public function findByCode($code) {return df_a($this->getMapFromCodeToEntity(), $code);}

	/**
	 * @override
	 * @param string $code
	 * @param array $attributeData
	 * @return Mage_Catalog_Model_Resource_Eav_Attribute
	 */
	public function findByCodeOrCreate($code, array $attributeData) {
		df_param_string($code, 0);
		/** @var Mage_Catalog_Model_Resource_Eav_Attribute $result */
		$result = $this->findByCode($code);
		if (!is_null($result)) {
			$attributeData = array_merge($result->getData(), $attributeData);
		}
		$result =
			Df_Catalog_Model_Resource_Installer_AddAttribute::s()
				->addAttributeRm($code, $attributeData)
		;
		df_assert($result->getId());
		$this->addEntity($result);
		return $result;
	}

	/**
	 * @param Varien_Object $entity
	 * @return Df_Dataflow_Model_Registry_Collection
	 */
	public function removeEntity(Varien_Object $entity) {
		df_assert($entity instanceof Mage_Catalog_Model_Resource_Eav_Attribute);
		parent::removeEntity($entity);
		/** @var Mage_Catalog_Model_Resource_Eav_Attribute $entity */
		$this->removeEntityFromCodeMap($entity);
		return $this;
	}

	/**
	 * @override
	 * @return Varien_Data_Collection
	 */
	protected function createCollection() {
		/** @var Mage_Catalog_Model_Resource_Product_Attribute_Collection|Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Attribute_Collection $result */
		$result = Mage::getResourceModel('catalog/product_attribute_collection');
		/**
		 * addFieldToSelect (Df_Eav_Const::ENTITY_EXTERNAL_ID)
		 * нам не нужно (ибо это поле — не из основной таблицы, а из дополнительной)
		 * и даже приводит к сбою (по той же причине)
		 */
		/**
		 * Пока используем это вместо $result->addHasOptionsFilter(),
		 * потому что addHasOptionsFilter отбраковывает пустые справочники
		 */
		//$result->setFrontendInputTypeFilter('select');
		return $result;
	}

	/**
	 * @override
	 * @param Varien_Object $entity
	 * @return string|null
	 */
	protected function getEntityLabel(Varien_Object $entity) {
		df_assert($entity instanceof Mage_Catalog_Model_Resource_Eav_Attribute);
		/** @var Mage_Catalog_Model_Resource_Eav_Attribute $entity */
		return $entity->getFrontendLabel();
	}

	/**
	 * @param Mage_Catalog_Model_Resource_Eav_Attribute $entity
	 * @return Df_Dataflow_Model_Registry_Collection
	 */
	private function addEntityToCodeMap(Mage_Catalog_Model_Resource_Eav_Attribute $entity) {
		$this->getMapFromCodeToEntity();
		/** @var string $code */
		$code = $entity->getAttributeCode();
		df_assert_string_not_empty($code);
		$this->{__CLASS__ . '::getMapFromCodeToEntity'}[$code] = $entity;
		return $this;
	}	


	/** @return Varien_Object[] */
	private function getMapFromCodeToEntity() {
		if (!isset($this->{__METHOD__})) {
			/** @var Varien_Object[] $result */
			$result = array();
			foreach ($this->getCollectionRm() as $entity) {
				/** @var Mage_Catalog_Model_Resource_Eav_Attribute $entity */
				/** @var string|null $code */
				$code = $entity->getAttributeCode();
				if ($code) {
					df_assert_string($code);
					$result[$code] = $entity;
				}
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param Mage_Catalog_Model_Resource_Eav_Attribute $entity
	 * @return Df_Dataflow_Model_Registry_Collection
	 */
	private function removeEntityFromCodeMap(Mage_Catalog_Model_Resource_Eav_Attribute $entity) {
		$this->getMapFromCodeToEntity();
		/** @var string $code */
		$code = $entity->getAttributeCode();
		df_assert_string_not_empty($code);
		unset($this->{__CLASS__ . '::getMapFromCodeToEntity'}[$code]);
		return $this;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Dataflow_Model_Registry_Collection_Attributes
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}