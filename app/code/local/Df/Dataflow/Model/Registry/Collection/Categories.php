<?php
class Df_Dataflow_Model_Registry_Collection_Categories extends Df_Dataflow_Model_Registry_Collection {
	/**
	 * @override
	 * @return Varien_Data_Collection
	 */
	protected function createCollection() {
		/** @var Df_Catalog_Model_Resource_Category_Collection $result */
		$result = Df_Catalog_Model_Resource_Category_Collection::i();
		$result->setStore(df()->registry()->getStoreProcessed());
		$result->addAttributeToSelect(Df_Eav_Const::ENTITY_EXTERNAL_ID);
		/**
		 * Раньше тут стояло
		 * $result->addAttributeToSelect(Df_Eav_Const::ENTITY_EXTERNAL_ID);
		 * потому что реестр использовался только модулем 1С:Управление торговлей.
		 * Теперь же реестр начинает использоваться прикладным решением «Lamoda»,
		 * которому требуется загружать в коллекцию товарных разделов
		 * свойство @see Df_Catalog_Model_Category::P__EXTERNAL_URL
		 */
		$result->addAttributeToSelect('*');
		return $result;
	}
	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Dataflow_Model_Registry_Collection_Categories
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}