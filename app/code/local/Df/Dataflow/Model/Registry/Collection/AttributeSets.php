<?php
/**
 * @method Mage_Eav_Model_Entity_Attribute_Set|null findByLabel(string $label)
 */
class Df_Dataflow_Model_Registry_Collection_AttributeSets extends Df_Dataflow_Model_Registry_Collection {
	/**
	 * @override
	 * @return Varien_Data_Collection
	 */
	protected function createCollection() {
		/** @var Mage_Eav_Model_Resource_Entity_Attribute_Set_Collection|Mage_Eav_Model_Mysql4_Entity_Attribute_Set_Collection $result */
		$result = Mage::getResourceModel(Df_Eav_Const::CLASS_MF__ENTITY_ATTRIBUTE_SET_COLLECTION);
		$result->setEntityTypeFilter(rm_eav_id_product());
		return $result;
	}

	/**
	 * @override
	 * @param Varien_Object $entity
	 * @return string|null
	 */
	protected function getEntityLabel(Varien_Object $entity) {
		df_assert($entity instanceof Mage_Eav_Model_Entity_Attribute_Set);
		/** @var Mage_Eav_Model_Entity_Attribute_Set $entity */
		return $entity->getAttributeSetName();
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Dataflow_Model_Registry_Collection_AttributeSets
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}