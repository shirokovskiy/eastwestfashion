<?php
class Df_Autolux_Model_Request_Locations extends Df_Autolux_Model_Request {
	/** @return int[] */
	public function getLocations() {
		if (!isset($this->{__METHOD__})) {
			/** @var int[] $result */
			$result = null;
			/**
			 * Кэширование результата
			 * Обратите внимание, что не используем в качестве ключа __METHOD__,
			 * потому что данный метод может находиться
			 * в родительском по отношени к другим классе.
			 * @var string $cacheKey
			 */
			$cacheKey = implode('::', array(get_class($this), __FUNCTION__));
			/** @var string|bool $resultSerialized */
			$resultSerialized = $this->getCache()->load($cacheKey);
			if (false !== $resultSerialized) {
				$result = @unserialize($resultSerialized);
			}
			if (!is_array($result)) {
				$result = $this->parseLocations();
				/** @var string $resultSerialized */
				$resultSerialized = serialize($result);
				$this->getCache()->save($resultSerialized, $cacheKey);
			}
			df_result_array($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return array(string => string)
	 */
	protected function getQueryParams() {
		return array_merge(parent::getQueryParams(), array('language' => 'ru'));
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getQueryPath() {return '/Autolux/inc/Pages/PatternStd/img/cities.php';}

	/** @return int[] */
	private function parseLocations() {
		/** @var int[] $result */
		$result = array();
		/** @var string $pattern */
		$pattern = '#<option value=\'(\d+)\'>([^<]+)</option>#mui';
		/** @var string[][] $matches */
		$matches = array();
		/** @var int|bool $matchingResult */
		$matchingResult =
			preg_match_all(
				$pattern
				,$this->response()->text()
				,$matches
				,$flags = PREG_SET_ORDER
			)
		;
		df_assert_gt(1, $matchingResult);
		df_assert_array($matches);
		foreach ($matches as $match) {
			/** @var string[] $match */
			df_assert_array($match);
			/** @var int $locationId */
			$locationId = rm_nat0(df_a($match, 1));
			if (0 === $locationId) {
				continue;
			}
			/** @var string $locationNameRaw */
			$locationNameRaw = df_a($match, 2);
			df_assert_string_not_empty($locationNameRaw);
			/** @var string $locationName */
			$locationName = df_array_first(explode(',', $locationNameRaw));
			df_assert_string_not_empty($locationName);
			$locationName = mb_strtoupper($locationName);
			if (!isset($result[$locationName])) {
				$result[$locationName] = $locationId;
			}
		}
		return $result;
	}

	const _CLASS = __CLASS__;
	/** @return Df_Autolux_Model_Request_Locations */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}