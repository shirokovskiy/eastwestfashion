<?php
class Df_Catalog_Helper_Category extends Mage_Core_Helper_Abstract {
	/**
	 * Перед созданием и сохранением товарного раздела
	 * надо обязательно надо установить текущим магазином административный,
	 * иначе возникают неприятные проблемы.
	 *
	 * Lля успешного сохранения товарного раздела
	 * надо отключить на время сохранения режим денормализации.
	 * Так вот, в стандартном программном коде Magento автоматически отключает
	 * режим денормализации при создании товарного раздела из административного магазина
	 * (в конструкторе товарного раздела).
	 *
	 * А если сохранять раздел, чей конструктор вызван при включенном режиме денормализации —
	 * то произойдёт сбой:
	 *
	 * SQLSTATE[23000]: Integrity constraint violation:
	 * 1452 Cannot add or update a child row:
	 * a foreign key constraint fails
	 * (`catalog_category_flat_store_1`,
	 * CONSTRAINT `FK_CAT_CTGR_FLAT_STORE_1_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID`
	 * FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`en)
	 *
	 * @param array $data
	 * @param int $storeId
	 * @return Df_Catalog_Model_Category
	 * @throws Exception
	 */
	public function createAndSave(array $data, $storeId) {
		/** @var Df_Catalog_Model_Category $result */
		$result = Df_Catalog_Model_Category::i($data);
		$this->save($result, $storeId);
		return $result;
	}

	/**
	 * @param Df_Catalog_Model_Category $category
	 * @param int|null $storeId [optional]
	 * @return Df_Catalog_Helper_Category
	 * @throws Exception
	 */
	public function save(Df_Catalog_Model_Category $category, $storeId = null) {
		/** @var Mage_Core_Model_Store $currentStore */
		$currentStore = Mage::app()->getStore();
		/** @var bool $updateMode */
		$updateMode = Mage::app()->getUpdateMode();
		/**
		 * Очень важный момент!
		 * Если Magento находится в режиме обновления,
		 * то Mage_Core_Model_App::getStore()
		 * всегда будет возвращать Mage_Core_Model_App::getDefaultStore(),
		 * даже для такого кода: Mage_Core_Model_App::getStore(999).
		 * Это приводит к весьма некорректному поведению системы в некоторых ситцациях,
		 * когда мы обновляем товарные разделы своим установочным скриптом:
		 * @see Mage_Catalog_Model_Resource_Abstract::_saveAttributeValue():
		 * $storeId = (int)Mage::app()->getStore($object->getStoreId())->getId();
		 * Этот код заведомо вернёт неправильный результат!
		 */
		Mage::app()->setUpdateMode(false);
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		try {
			if (!is_null($storeId)) {
				$category->setStoreId($storeId);
			}
			$category->save();
		}
		catch(Exception $e) {
			Mage::app()->setCurrentStore($currentStore);
			Mage::app()->setUpdateMode($updateMode);
			throw $e;
		}
		Mage::app()->setCurrentStore($currentStore);
		Mage::app()->setUpdateMode($updateMode);
		return $this;
	}

	/** @return Df_Catalog_Helper_Category */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}