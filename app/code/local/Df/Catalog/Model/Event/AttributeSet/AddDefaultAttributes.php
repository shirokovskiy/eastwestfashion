<?php
/**
 * Cообщение:		«df_catalog__attribute_set__add_default_attributes»
 *
 * Источник:		Df_Catalog_Model_Installer_AttributeSet::addAttributesDefault()
 * [code]
		Mage
			::dispatchEvent(
				Df_Catalog_Model_Event_AttributeSet_AddDefaultAttributes::EVENT
				,array(
					Df_Catalog_Model_Event_AttributeSet_AddDefaultAttributes
						::EVENT_PARAM__ATTRIBUTE_SET => $attributeSet
				)
			)
		;
 * [/code]
 */
class Df_Catalog_Model_Event_AttributeSet_AddDefaultAttributes extends Df_Core_Model_Event {
	/** @return Mage_Eav_Model_Entity_Attribute_Set */
	public function getAttributeSet() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->getEventParam(self::EVENT_PARAM__ATTRIBUTE_SET);
			df_assert($this->{__METHOD__} instanceof Mage_Eav_Model_Entity_Attribute_Set);
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getExpectedEventPrefix() {
		return self::EVENT;
	}

	const _CLASS = __CLASS__;
	const EVENT = 'df_catalog__attribute_set__add_default_attributes';
	const EVENT_PARAM__ATTRIBUTE_SET = 'attribute_set';
}