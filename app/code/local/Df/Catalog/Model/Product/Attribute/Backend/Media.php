<?php
class Df_Catalog_Model_Product_Attribute_Backend_Media
	extends Mage_Catalog_Model_Product_Attribute_Backend_Media {
	/**
	 * @override
	 * @param Df_Catalog_Model_Product $object
	 * @return Df_Catalog_Model_Product_Attribute_Backend_Media
	 */
	public function beforeSave($object) {
		$this->setProduct($object);
		parent::beforeSave($object);
		return $this;
	}

	/**
	 * @override
	 * @param string $file
	 * @return string
	 */
	protected function _moveImageFromTmp($file) {
		/** @var bool $patchNeeded */
		static $patchNeeded;
		if (!isset($patchNeeded)) {
			$patchNeeded =
					df_enabled(Df_Core_Feature::SEO)
				&&
					df_cfg()->seo()->images()->getUseDescriptiveFileNames()
			;
		}
		return
			$patchNeeded
			?$this->_moveImageFromTmpDf($file)
			:parent::_moveImageFromTmp($file)
		;
	}

	/**
	 * @param string $file
	 * @return string
	 */
	protected function _moveImageFromTmpDf($file) {
		df_param_string($file, 0);
		/** @var Varien_Io_File $ioObject */
		$ioObject = new Varien_Io_File();
		/** @var string $destDirectory */
		$destDirectory = dirname($this->_getConfig()->getMediaPath($file));
		df_assert_string_not_empty($destDirectory);
		try {
			$ioObject->open(array('path'=>$destDirectory));
		}
		catch(Exception $e) {
			try {
				$ioObject->mkdir($destDirectory, 0777, true);
				$ioObject->open(array('path'=>$destDirectory));
			}
			catch(Exception $e) {
				df_error(
					strtr(
						"[%method%]:\nСогласно текущему алгоритму,"
						."\nсистема должна переместить загруженную картинку в папку"
						."\n«%destionationDirectory%»"
						."\nОднако, папка «%destionationDirectory%» отсутствует на сервере,"
						."\nи система не в состоянии её вручную создать по причине:\n«%exceptionMessage%»."
						,array(
							'%method%' => __METHOD__
							,'%destionationDirectory%' => $destDirectory
							,'%exceptionMessage%»' => $e->getMessage()
						)
					)
				);
			}
		}
		if (mb_strrpos($file, '.tmp') === mb_strlen($file)-4) {
			$file = mb_substr($file, 0, mb_strlen($file)-4);
			df_assert_string_not_empty($file);
		}
		/** @var string $destionationFilePath */
		$destionationFilePath = df_path()->adjustSlashes($this->_getConfig()->getMediaPath($file));
		/** @var Df_Catalog_Model_Product $product */
		$product = $this->getProduct();
		df_assert($product instanceof Df_Catalog_Model_Product);
		/** @var string $destionationFilePathOptimizedForSeo */
		$destionationFilePathOptimizedForSeo =
			df_h()->seo()->getProductImageRenamer()->getSeoFileName($destionationFilePath, $product)
		;
		/** @var string $destionationFilePathOptimizedForSeoAndUnique */
		$destionationFilePathOptimizedForSeoAndUnique =
			df_h()->core()->file()->getUniqueFileName($destionationFilePathOptimizedForSeo)
		;
		/** @var string $sourceFilePath */
		$sourceFilePath = $this->_getConfig()->getTmpMediaPath($file);
		if (!is_file($sourceFilePath)) {
			df_error(
				strtr(
					"[%method%]:\nСогласно текущему алгоритму,"
					."\nсистема должна была временно сохранить загруженную картинку по пути"
					."\n«%sourceFilePath%»"
					."\nОднако, файл «%sourceFilePath%» отсутствует на сервере."
					,array(
						'%method%' => __METHOD__
						,'%sourceFilePath%' => $sourceFilePath
					)
				)
			);
		}
		/** @var bool $r */
		$r =
			$ioObject->mv(
				$this->_getConfig()->getTmpMediaPath($file)
				,$destionationFilePathOptimizedForSeoAndUnique
			)
		;
		df_assert_boolean($r);
		if (!$r || !is_file($destionationFilePathOptimizedForSeoAndUnique)) {
			df_error(
				strtr(
					"[%method%]:\nСистеме не удалось переместить файл"
					. "\nс пути «%sourceFilePath%»"
					. "\nна путь «%destionationFilePathOptimizedForSeoAndUnique%»."
					,array(
						'%method%' => __METHOD__
						,'%sourceFilePath%' => $sourceFilePath
						,'%destionationFilePathOptimizedForSeoAndUnique%' =>
							$destionationFilePathOptimizedForSeoAndUnique
					)
				)
			);
		}
		$result =
			str_replace(
				$ioObject->dirsep()
				,/**
				 * Похоже, в качества разделителя частей пути в данном случае
				 * надо всегда использовать именно символ /
				 */
				'/'
				,str_replace(
					df_path()->adjustSlashes($this->_getConfig()->getBaseMediaPath() . DS)
					,''
					,df_path()->adjustSlashes($destionationFilePathOptimizedForSeoAndUnique)
				)
			)
		;
		return $result;
	}

	/**
	 * @param Df_Catalog_Model_Product $product
	 * @return Df_Catalog_Model_Product_Attribute_Backend_Media
	 */
	private function setProduct(Df_Catalog_Model_Product $product) {
		$this->_product = $product;
		return $this;
	}

	/** @return Df_Catalog_Model_Product */
	private function getProduct() {
		return $this->_product;
	}
	/** @var  Df_Catalog_Model_Product */
	private $_product;
}