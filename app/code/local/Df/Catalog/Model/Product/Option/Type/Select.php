<?php
class Df_Catalog_Model_Product_Option_Type_Select extends Mage_Catalog_Model_Product_Option_Type_Select {
	/**
	  * Return formatted option value for quote option
	  *
	  * @override
	  * @param string $optionValue Prepared for cart option value
	  * @return string
	  */
	 public function getFormattedOptionValue($optionValue)
	 {
		 if ($this->_formattedOptionValue === null) {
			 $this->_formattedOptionValue = Mage::helper('core')->escapeHtml(
				 $this->getEditableOptionValue($optionValue)
			 );
		 }
		 return $this->_formattedOptionValue;
	 }
	/** @var string */
	private $_formattedOptionValue;
}

 