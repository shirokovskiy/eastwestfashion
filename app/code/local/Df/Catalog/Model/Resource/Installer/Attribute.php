<?php
class Df_Catalog_Model_Resource_Installer_Attribute extends Mage_Catalog_Model_Resource_Eav_Mysql4_Setup {
	/**
	 * @param string $entityType
	 * @param string $attributeId
	 * @param string $attributeLabel
	 * @param string|null $groupName [optional]
	 * @param array(string => string|int|bool) $attributeCustomProperties [optional]
	 * @param int $ordering [optional]
	 * @return Df_Catalog_Model_Resource_Installer_Attribute
	 */
	public function addAdministrativeAttribute(
		$entityType
		, $attributeId
		, $attributeLabel
		, $groupName = null
		, array $attributeCustomProperties = array()
		, $ordering = 10
	) {
		df_param_string($entityType, 0);
		if (is_null($groupName)) {
			$groupName = $this->getGeneralGroupName();
		}
		df_param_string($groupName, 1);
		df_param_integer($ordering, 2);
		$this->cleanCache();
		if ($this->getAttributeId($entityType, $attributeId)) {
			$this->removeAttribute($entityType, $attributeId);
		}
		/** @var int $entityTypeId */
		$entityTypeId = $this->getEntityTypeId($entityType);
		/** @var int $attributeSetId */
		$attributeSetId = $this->getDefaultAttributeSetId($entityTypeId);
		$this
			->addAttribute(
				$entityType
				, $attributeId
				, array_merge(
					$this->getAdministrativeAttributeProperties()
					,array('label' => $attributeLabel)
					,$attributeCustomProperties
				)
			)
		;
		$this
			->addAttributeToGroup(
				$entityTypeId
				,$attributeSetId
				/**
				 * Не используем синтаксис
				 * $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId)
				 * потому что он при предварительно включенной русификации
				 * может приводить к созданию дополнительной вкладки ("Основное")
				 * вместо размещения свойства на главной вкладке ("Главное").
				 */
				,$groupName
				,$attributeId
				,$ordering
			)
		;
		return $this;
	}

	/**
	 * Этот метод отличается от родительского метода
	 * Mage_Eav_Model_Entity_Setup::addAttributeToSet
	 * только расширенной диагностикой
	 * (мы теперь знаем, что произошло в результате работы метода:
	 * действительно ли товарное свойство было добавлено к прикладному типу товаров,
	 * или же оно уже принадлежало этому прикладному типу,
	 * а если уже принадлежало — не сменалась ли его группа).
	 *
	 * @param mixed $entityTypeId
	 * @param mixed $setId
	 * @param mixed $groupId
	 * @param mixed $attributeId
	 * @param int $sortOrder
	 * @return string
  	 */
	public function addAttributeToSetRm(
		$entityTypeId
		,$setId
		,$groupId
		,$attributeId
		,$sortOrder=null
	) {
		/** @var string $result */
		$result = self::ADD_ATTRIBUTE_TO_SET__NOT_CHANGED;
		$entityTypeId = $this->getEntityTypeId($entityTypeId);
		$setId = $this->getAttributeSetId($entityTypeId, $setId);
		$groupId = $this->getAttributeGroupId($entityTypeId, $setId, $groupId);
		$attributeId = $this->getAttributeId($entityTypeId, $attributeId);
		$table = $this->getTable('eav/entity_attribute');
		$bind =
			array(
				'attribute_set_id' => $setId,'attribute_id' => $attributeId
			)
		;
		$select =
			$this->_conn->select()
				->from($table)
				->where('attribute_set_id = :attribute_set_id')
				->where('attribute_id = :attribute_id')
		;
		$row = $this->_conn->fetchRow($select, $bind);
		if ($row) {
			if ($row['attribute_group_id'] != $groupId) {
				$where = array('entity_attribute_id =?' => $row['entity_attribute_id']);
				$data  = array('attribute_group_id' => $groupId);
				$this->_conn->update($table, $data, $where);
				$result = self::ADD_ATTRIBUTE_TO_SET__CHANGED_GROUP;
			}
		}
		else {
			$data =
				array(
					'entity_type_id' => $entityTypeId,'attribute_set_id' => $setId,'attribute_group_id' => $groupId,'attribute_id' => $attributeId,'sort_order' =>
						$this->getAttributeSortOrder(
							$entityTypeId
							,$setId
							,$groupId
							,$sortOrder
						)
				)
			;
			$this->_conn->insert($table, $data);
			$result = self::ADD_ATTRIBUTE_TO_SET__ADDED;
		}
		df_result_string($result);
		return $result;
	}
	
	/** @return int */
	public function getCategoryAttributeSetId() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				self::s()->getDefaultAttributeSetId(
					self::s()->getEntityTypeId(
						Mage_Catalog_Model_Category::ENTITY
					)
				)
			;
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	public function getGeneralGroupName() {return $this->_generalGroupName;}

	/**
	 * @override
	 * @return Df_Catalog_Model_Resource_Installer_Attribute
	 */
	public function startSetup() {
		parent::startSetup();
		Df_Core_Model_Lib::s()->init();
		Df_Zf_Model_Lib::s()->init();
		return $this;
	}

	/** @return Df_Catalog_Model_Resource_Installer_Attribute */
	public function cleanQueryCache() {
		$this->_setupCache = array();
		return $this;
	}

	/** @return array(string => string|int|bool) */
	private function getAdministrativeAttributeProperties() {
		return array(
			'type' => 'varchar'
			,'backend' => ''
			,'frontend' => ''
			,'input' => 'text'
			,'class' => ''
			,'source' => ''
			,'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
			,'visible' => true
			,'required' => false
			,'user_defined' => false
			,'default' => ''
			,'searchable' => false
			,'filterable' => false
			,'comparable' => false
			,'unique' => false
			// Показывать ли на витринной товарной карточке?
			,'is_visible_on_front' => 0
			// «Загружать ли данное свойство в товарные коллекции?»
			// Включенность опции «used_in_product_listing»
			// говорит системе загружать данное товарное свойство
			// в коллекцию товаров при включенном режиме денормализации товаров.
			,'used_in_product_listing' => 1
		);
	}

	const _CLASS = __CLASS__;
	const ADD_ATTRIBUTE_TO_SET__ADDED = 'added';
	const ADD_ATTRIBUTE_TO_SET__NOT_CHANGED = 'not_changed';
	const ADD_ATTRIBUTE_TO_SET__CHANGED_GROUP = 'changed_group';
	/** @return Df_Catalog_Model_Resource_Installer_Attribute */
	public static function s() {static $r; return $r ? $r : $r = new self('df_catalog_setup');}
}