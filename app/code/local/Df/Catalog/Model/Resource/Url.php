<?php
class Df_Catalog_Model_Resource_Url extends Mage_Catalog_Model_Resource_Eav_Mysql4_Url {
	/**
	 * Save rewrite URL
	 *
	 * @param array $rewriteData
	 * @param Varien_Object $rewrite
	 * @return Mage_Catalog_Model_Resource_Url|Mage_Catalog_Model_Resource_Eav_Mysql4_Url
	 */
	public function saveRewrite($rewriteData, $rewrite)
	{
		parent::saveRewrite($rewriteData, $rewrite);
		// В старых версиях Magento (в частности, Magento 1.4.0.1)
		// отсутствует функция автоматического перенаправления старых адресов на новые
		// при изменении адресного ключа (в новых версиях эта функция присутствует
		// и называется «Create Permanent Redirect for old URLs if Url key changed»).
		//
		// Т.к. эта функция очень важна при смене адресов на кириллические,
		// мы поддерживаем её, даже если она отсутствует в стандартной сборке

		/** @var bool[] $needSaveRewriteHistoryPatch */
		static $needSaveRewriteHistoryPatch = array();
		/** @var int $storeId */
		$storeId = $rewriteData['store_id'];
		if (!isset($needSaveRewriteHistoryPatch[$storeId])) {
			$needSaveRewriteHistoryPatch[$storeId] =
					df_enabled(Df_Core_Feature::SEO, $storeId)
				&&
					!method_exists($this, 'saveRewriteHistory')
				/**
				 * Раньше тут ещё стояла проверка
				 * на включенность соответствующей опции администратором.
				 * Теперь переключатель этой опции убрал,
				 * и опция включена всегда для старых версий Magento
				 * (за 2 года не припомню случаев, когда её надо было бы отключать).
				 * Тем самым административный интерфейс упрощён удалением малозначимой
				 * и редкой (только для Magento CE 1.4.0.1) опции
				 */
			;
		}
		if ($needSaveRewriteHistoryPatch[$storeId]) {
			$this->saveRewriteHistory_DfLegacyPatch($rewriteData, $rewrite);
		}
		return $this;
	}

	/**
	 * @param array $rewriteData
	 * @param Varien_Object $rewrite
	 * @return Mage_Catalog_Model_Resource_Url|Mage_Catalog_Model_Resource_Eav_Mysql4_Url
	 */
	private function saveRewriteHistory_DfLegacyPatch($rewriteData, $rewrite) {
		if ($rewrite instanceof Varien_Object && $rewrite->getId()) {
			$rewriteData['target_path'] = $rewriteData['request_path'];
			$rewriteData['request_path'] = $rewrite->getRequestPath();
			$rewriteData['id_path'] = $this->generateUniqueIdPath_DfLegacyPatch();
			$rewriteData['is_system'] = 0;
			$rewriteData['options'] = 'RP'; // Redirect = Permanent
			$this->saveRewriteHistory_DfLegacyPatch2($rewriteData);
		}
	}

	/**
	 * @param array(string => mixed) $rewriteData
	 * @return Df_Catalog_Model_Resource_Url
	 */
	private function saveRewriteHistory_DfLegacyPatch2($rewriteData)
	{
		$rewriteData = new Varien_Object($rewriteData);
		// check if rewrite exists with save request_path
		$rewrite = $this->getRewriteByRequestPath($rewriteData->getRequestPath(), $rewriteData->getStoreId());
		if ($rewrite === false) {
			// create permanent redirect
			$this->_getWriteAdapter()->insert($this->getMainTable(), $rewriteData->getData());
		}
		return $this;
	}

	/**
	 * Return unique string based on the time in microseconds.
	 * @return string
	 */
	private function generateUniqueIdPath_DfLegacyPatch()
	{
		return str_replace('0.', '', str_replace(' ', '_', microtime()));
	}

	/**
	 * @param array $params
	 * @return Df_Catalog_Model_Url
	 */
	public function makeRedirect(array $params) {
		$rewriteFrom = df_a($params, 'from');
		/** @var Varien_Object $rewriteFrom */

		$rewriteTo = df_a($params, 'to');
		/** @var Varien_Object $rewriteTo */
		df_assert($rewriteFrom->getData('request_path') != $rewriteTo->getData('request_path'));
		$this
			->_getWriteAdapter()
				->update(
					$this->getMainTable()
					,array(
						'options' => 'RP'
						,'target_path' => $rewriteTo->getData('request_path')
					)
					,$this
						->_getWriteAdapter()
							->quoteInto(
								$this->getIdFieldName() . '=?', $rewriteFrom->getId()
							)
				)
		;
		$this->relinkRewrites($params);
		return $this;
	}

	/**
	 * Чтобы избежать нескольких перенаправлений подряд, * мы смотрим, кто ссылается на $rewriteFrom, * и переводим стрелки на $rewriteTo
	 *
	 * @param array $params
	 * @return Df_Catalog_Model_Resource_Url
	 */
	private function relinkRewrites(array $params) {
		$rewriteFrom = df_a($params, 'from');
		/** @var Varien_Object $rewriteFrom */

		$rewriteTo = df_a($params, 'to');
		/** @var Varien_Object $rewriteTo */


		$where =
			$this
				->_getWriteAdapter()
					->quoteInto(
						'(target_path=?)'
						,$rewriteFrom->getData('request_path')
					)
		;
		if ($rewriteFrom->getData('store_id')) {
			$where .=
				$this
					->_getWriteAdapter()
						->quoteInto(
							' AND (store_id=?)'
							,$rewriteFrom->getData('store_id')
						)
			;
		}


		$where .=
			$this
				->_getWriteAdapter()
					->quoteInto(
						' AND (request_path<>?)'
						,$rewriteTo->getData('request_path')
					)
		;
		$this
			->_getWriteAdapter()
				->update(
					$this->getMainTable()
					,array('target_path' => $rewriteTo->getData('request_path'))
					,$where
				)
		;
		return $this;
	}

	/**
	 * Result rewrites are grouped by product
	 *
	 * @param array $productIds
	 * @param int $storeId
	 * @return array
	 */
	public function getRewritesForProducts(array $productIds, $storeId) {
		$result = array();
		/** @var Zend_Db_Statement_Pdo $query */
		$query =
			$this
				->_getWriteAdapter()
					->query(
						(string)
							$this
								->_getWriteAdapter()
								->select()
								->from($this->getMainTable())
								->where('store_id=?', $storeId)
								->where('is_system=?', 1)
								->where('product_id IN(?)', $productIds)
								->order('product_id')
					)
		;
		while (true) {
			$row = $query->fetch();
			if (!$row) {
				break;
			}

			$rewrite = new Varien_Object($row);
			$rewrite->setIdFieldName($this->getIdFieldName());
			$productId = $rewrite->getData('product_id');
			if (!isset($result[$productId])) {
				$result[$productId] = array();
			}
			$result[$productId][]= $rewrite;
		}
		return $result;
	}

	/**
	 * @param array $categoryIds
	 * @return array
	 */
	public function getCategoriesLevelInfo(array $categoryIds) {
		$result = array();
		$rowSet =
			$this
				->_getWriteAdapter()
					->fetchAll(
						(string)
							$this
								->_getWriteAdapter()
								->select()
								->from($this->getTable('catalog/category'))
								->where('entity_id IN(?)', $categoryIds)
					)
		;
		foreach ($rowSet as $row) {
			$result[df_a($row, 'entity_id')] = df_a($row, 'level');
		}
		return $result;
	}

	/**
	 * Finds and deletes old rewrites for store
	 * a) category rewrites left from the times when store had some other root category
	 * b) product rewrites left from products that once belonged to this site, but then deleted or just removed from website
	 *
	 * @param int $storeId
	 * @return Mage_Catalog_Model_Resource_Url|Mage_Catalog_Model_Resource_Eav_Mysql4_Url
	 */
	public function clearStoreInvalidRewrites($storeId)
	{
		if (df_enabled(Df_Core_Feature::SEO, $storeId)) {
			/**
			 * @todo Для подтоваров (вариантов для настраиваемых товаров)
			 * мы можем сделать перенаправление на настраиваемый товар — это самое разумное
			 */
			$this->clearRewritesForInvisibleProducts($storeId);
		}
		return parent::clearStoreInvalidRewrites($storeId);
	}

	/**
	 * Удаляем перенаправления для
	 *
	 * @param int $storeId[optional]
	 * @return Df_Catalog_Model_Resource_Url
	 */
	public function clearRewritesForInvisibleProducts($storeId = null) {
		if ($this->getInvisibleProductIds($storeId)) {
			$adapter = $this->_getWriteAdapter();
			$condition = $adapter->quoteInto('product_id IN(?)', $this->getInvisibleProductIds($storeId));
			if ($storeId !== null) {
				$condition .= $adapter->quoteInto(' AND store_id IN(?)', $storeId);
			}
			$adapter->delete($this->getMainTable(), $condition);
		}
		return $this;
	}

	/**
	 * @param int $storeId[optional]
	 * @return Df_Catalog_Model_Resource_Url
	 */
	public function clearSystemRewrites($storeId = null) {
		$adapter = $this->_getWriteAdapter();
		$condition = $adapter->quoteInto('is_system=?', 1);
		if ($storeId !== null) {
			$condition.= $adapter->quoteInto(' AND store_id IN(?)', $storeId);
		}
		$adapter->delete($this->getMainTable(), $condition);
		return $this;
	}

	/**
	 * @param int $storeId
	 * @return array
	 */
	private function getInvisibleProductIds($storeId) {
		if (!isset($this->{__METHOD__}[$storeId])) {
			$this->{__METHOD__}[$storeId] =
				Mage::getResourceModel('catalog/product_collection')
					->setStoreId($storeId)
					->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE)
					->getAllIds()
			;
		}
		return $this->{__METHOD__}[$storeId];
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		/**
		 * Нельзя вызывать parent::_construct(),
		 * потому что это метод в родительском классе — абстрактный.
		 */
		$this->_init(self::TABLE_NAME, Df_Catalog_Model_Url::P__ID);
	}
	const _CLASS = __CLASS__;
	const TABLE_NAME = 'core/url_rewrite';

	/** @return string */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Catalog_Model_Resource_Url */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}