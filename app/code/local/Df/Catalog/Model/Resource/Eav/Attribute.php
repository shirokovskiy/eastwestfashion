<?php
class Df_Catalog_Model_Resource_Eav_Attribute extends Mage_Catalog_Model_Resource_Eav_Attribute {
	/**
	 * @param string $attributeCode
	 * @param string|string[] $options
	 * @return Mage_Catalog_Model_Resource_Eav_Attribute
	 */
	public static function addOptions($attributeCode, $options) {
		if (!is_array($options)) {
			$options = array($options);
		}
		/** @var Mage_Catalog_Model_Resource_Eav_Attribute $result */
		$result = df()->registry()->attributes()->findByCode($attributeCode);
		/** @var array(string => mixed) $attributeData */
		$attributeData =
			array_merge(
				$result->getData()
				,array(
					'option' =>
						Df_Eav_Model_Entity_Attribute_Option_Calculator
							::calculateStatic(
								$result
								,$options
								,$isModeInsert = true
								,$caseInsensitive = true
							)
				)
			)
		;
		$result = df()->registry()->attributes()->findByCodeOrCreate($attributeCode, $attributeData);
		return $result;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Catalog_Model_Resource_Eav_Attribute
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}

