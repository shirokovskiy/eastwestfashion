<?php
class Df_Catalog_Model_Resource_Setup extends Df_Core_Model_Resource_Setup_Abstract {
	/** @return Df_Core_Model_Resource_Setup */
	public function upgrade_2_23_5() {
		Df_Catalog_Model_Setup_2_23_5::s()->process();
		return $this;
	}
}