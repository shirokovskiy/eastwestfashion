<?php
class Df_Catalog_Block_Product_Price extends Mage_Catalog_Block_Product_Price {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * @override
	 * @return string|null
	 */
	public function getTemplate() {
		/** @var string $result */
		$result =
					df_module_enabled(Df_Core_Module::TWEAKS)
				&&
					df_enabled(Df_Core_Feature::TWEAKS)
				&&
					(
							(
									rm_handle_presents(Df_Core_Model_Layout_Handle::CATALOG_PRODUCT_VIEW)
								&&
									df_cfg()->tweaks()->catalog()->product()->view()->needHidePrice()
							)
						||
							(
									df_cfg()->tweaks()->catalog()->product()->_list()->needHidePrice()
								&&
									df_h()->tweaks()->isItCatalogProductList()
							)
					)
			? null
			: parent::getTemplate()
		;
		if (!is_null($result)) {
			df_result_string($result);
		}
		return $result;
	}

}