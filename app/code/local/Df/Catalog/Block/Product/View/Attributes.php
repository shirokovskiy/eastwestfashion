<?php
class Df_Catalog_Block_Product_View_Attributes extends Mage_Catalog_Block_Product_View_Attributes {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * $excludeAttr is optional array of attribute codes to
	 * exclude them from additional data array
	 * @override
	 * @param array $excludeAttr
	 * @return array
	 */
	public function getAdditionalData(array $excludeAttr = array()) {
		/** @var bool $patchNeeded */
		static $patchNeeded;
		if (!isset($patchNeeded)) {
			$patchNeeded =
					df_module_enabled(Df_Core_Module::TWEAKS)
				&&
					df_enabled(Df_Core_Feature::TWEAKS)
				&&
					df_cfg()->tweaks()->catalog()->product()->view()->needHideEmptyAttributes()
			;
		}
		return
			$patchNeeded
			?$this->getAdditionalDataDf($excludeAttr)
			:parent::getAdditionalData($excludeAttr)
		;
	}

	/**
	 * $excludeAttr is optional array of attribute codes to
	 * exclude them from additional data array
	 *
	 * @param array $excludeAttr
	 * @return array
	 */
	private function getAdditionalDataDf(array $excludeAttr = array()) {
		/** @var array $data */
		$data = array();
		/** @var Mage_Catalog_Model_Product $product */
		$product = $this->getProduct();
		/** @var array $attributes */
		$attributes = $product->getAttributes();
		foreach ($attributes as $attribute) {
			/** @var Mage_Eav_Model_Attribute $attribute */
			if (
					$attribute->getIsVisibleOnFront()
				&&
					!in_array(
						$attribute->getAttributeCode()
						,$excludeAttr
					)
			) {
				/**
 				 * Обратите внимание, что оба условия важны:
				 * свойство может как отсутствовать у товара,
				 * так и присутствовать со значением null
				 */
				if (
						!$product->hasData($attribute->getAttributeCode())
					||
						(is_null($product->getData($attribute->getAttributeCode())))
				) {
					continue;
				}
				/** @var Mage_Eav_Model_Entity_Attribute_Frontend_Abstract $frontend */
				$frontend = $attribute->getFrontend();
				/** @var mixed $value */
				$value = $frontend->getValue($product);
				if (!$product->hasData($attribute->getAttributeCode())) {
					continue;
				} else if ('' === (string)$value) {
					continue;
				} else if (
						('price' === $attribute->getDataUsingMethod('frontend_input'))
					&&
						is_string($value)
				) {
					$value = Mage::app()->getStore()->convertPrice($value, true);
				}
				if (is_string($value) && $value) {
					$data[$attribute->getAttributeCode()] = array(
						'label' => $attribute->getStoreLabel()
						,'value' => $value
						,'code'  => $attribute->getAttributeCode()
					);
				}
			}
		}
		return $data;
	}

}