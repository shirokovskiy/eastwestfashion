<?php
class Df_Catalog_Block_Product_View extends Mage_Catalog_Block_Product_View {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/**
	 * Check if product can be emailed to friend
	 * @override
	 * @return bool
	 */
	public function canEmailToFriend() {
		$result = parent::canEmailToFriend();
		if (
				df_module_enabled(Df_Core_Module::TWEAKS)
			&&
				df_enabled(Df_Core_Feature::TWEAKS)
			&&
				rm_handle_presents(Df_Core_Model_Layout_Handle::CATALOG_PRODUCT_VIEW)
			&&
				df_cfg()->tweaks()->catalog()->product()->view()->needHideEmailToFriend()
		) {
			$result = false;
		}
		return $result;
	}

	/**
	 * Retrieve current product model
	 * @return Mage_Catalog_Model_Product
	 */
	public function getProduct() {
		$result = parent::getProduct();
		if (!$this->_dfProductPrepared) {
			if (
					df_module_enabled(Df_Core_Module::TWEAKS)
				&&
					df_enabled(Df_Core_Feature::TWEAKS)
				&&
					rm_handle_presents(Df_Core_Model_Layout_Handle::CATALOG_PRODUCT_VIEW)
				&&
					df_cfg()->tweaks()->catalog()->product()->view()->needHideShortDescription()
			) {
				$result->unsetData('short_description');
			}
			$this->_dfProductPrepared = true;
		}
		return $result;
	}
	/** @var bool */
	private $_dfProductPrepared = false;
}