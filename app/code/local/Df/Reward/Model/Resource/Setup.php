<?php
class Df_Reward_Model_Resource_Setup extends Mage_Sales_Model_Mysql4_Setup {
	/**
	 * @param string|integer $entityTypeId
	 * @param string $code
	 * @param array $attr
	 * @return Mage_Eav_Model_Entity_Setup
	 */
	public function addAttribute($entityTypeId, $code, array $attr) {
		$this->_currentEntityTypeId = $entityTypeId;
		return parent::addAttribute($entityTypeId, $code, $attr);
	}
	/** @var string */
	protected $_currentEntityTypeId;

	/**
	 * @override
	 * @return Df_Directory_Model_Resource_Setup
	 */
	public function startSetup() {
		parent::startSetup();
		Df_Core_Model_Lib::s()->init();
		Df_Zf_Model_Lib::s()->init();
		return $this;
	}

	/** @return Df_Reward_Model_Resource_Setup */
	public function upgrade_2_20_6() {
		Df_Reward_Model_Setup_2_20_6::i(
			array(
				Df_Reward_Model_Setup_2_20_6::P__SETUP => $this
			)
		)->process();
		return $this;
	}

	/**
	 * @override
	 * @param array $attr
	 * @return array
	 */
	protected function _prepareValues($attr) {
		$data = parent::_prepareValues($attr);
		if ('customer' === $this->_currentEntityTypeId) {
			$data =
				array_merge(
					$data
					,array(
						'is_visible' => $this->_getValue($attr, 'visible', 1)
						,'is_visible_on_front' =>
							$this->_getValue($attr, 'visible_on_front', 0)
						,'input_filter' => $this->_getValue($attr, 'input_filter', '')
						,'lines_to_divide_multiline' =>
							$this->_getValue($attr, 'lines_to_divide', 0)
						,'min_text_length' => $this->_getValue($attr, 'min_text_length', 0)
						,'max_text_length' => $this->_getValue($attr, 'max_text_length', 0)
					)
				)
			;
		}
		return $data;
	}
}