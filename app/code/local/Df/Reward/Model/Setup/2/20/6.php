<?php
class Df_Reward_Model_Setup_2_20_6 extends Df_Core_Model_Setup {
	/**
	 * @override
	 * @return Df_Reward_Model_Setup_2_20_6	 */
	public function process() {
		/** @var string $tableRewardHistory */
		$tableRewardHistory = $this->getSetup()->getTable('df_reward/reward_history');
		$this->getSetup()
			->run(
				/**
				 * Обратите внимание,
				 * что использование двойных кавычек вместо одинарных здесь обязательно
				 */
				"ALTER TABLE `{$tableRewardHistory}`
						MODIFY `expired_at_static` datetime NULL DEFAULT NULL"
				)
			->run(
				"ALTER TABLE `{$tableRewardHistory}`
					MODIFY `expired_at_dynamic` datetime NULL DEFAULT NULL"
			)
			->run(
				"UPDATE `{$tableRewardHistory}`
					SET `expired_at_static` = NULL
					WHERE `expired_at_static` < NOW() - INTERVAL 1 YEAR"
			)
			->run(
				"UPDATE `{$tableRewardHistory}`
					SET `expired_at_dynamic` = NULL
					WHERE `expired_at_dynamic` < NOW() - INTERVAL 1 YEAR"
			)
		;
		return $this;
	}
	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Reward_Model_Setup_2_20_6
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}