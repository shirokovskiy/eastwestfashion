<?php
class Df_Forum_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract {
	/**
	 * @param $observer
	 */
	public function ___initialize($observer) {
		$front = $observer->getEvent()->getFront();
		$forum = new Df_Forum_Controller_Router();
		$forum->_observer_request = $observer->getEvent()->getData('front')->getRequest();
		$front->addRouter('forum' ,$forum);
	}

	/**
	 * @param Zend_Controller_Request_Http $request
	 * @return bool
	 */
	public function match(Zend_Controller_Request_Http $request) {
		/** @var bool $result */
		$result = false;
		if (!Mage::isInstalled()) {
			Mage::app()
				->getFrontController()
				->getResponse()
				->setRedirect(Mage::getUrl('install'))
				->sendResponse();
		}
		else {
			$route = 'forum';
			$identifier = $request->getPathInfo();
			if (
					$route
				===
					mb_substr(
						str_replace('/', '', $identifier),
						0,
						mb_strlen($route)
					)
			) {
				//*****************change language if topic url key not exists*****************/
				$___from_store = $this->_observer_request->getParam('___from_store', false);
				$___store = $this->_observer_request->getParam('___store', false);
				if ($___from_store && $___store) {
					$request->setModuleName('forum')->setControllerName('topic')->setActionName('index');
					$result = true;
				}
			}
		}
		return $result;
	}
	private $_observer_request = false;
}