<?php
class Df_Forum_Block_Head extends Df_Core_Block_Template {
	/** @return string */
	public function getTitle() {
		return df_cfg()->forum()->general()->getForumTitle();
	}

	public function isModerator() {
		return df_h()->forum()->topic()->isModerator();
	}

	/** @return bool */
	public function getTopBannerIsLink() {
		return !!df_cfg()->forum()->design()->topBanner()->getTargetUrl();
	}

	/** @return string */
	public function getMessagesLinkStyle() {
		/** @var string $result */
		$result = '';
		$customer = rm_session_customer()->getCustomer();
		if ($customer->getId()) {
			$collection = Df_Forum_Model_PrivateMessage::c();
			$collection->getSelect()
				->where('? = is_trash', 0)
				->where('? = sent_to', rm_session_customer()->getCustomer()->getId())
				->where('? = is_deleteinbox', 0)
				->where('? = is_primary', 1)
				->where('? = is_read', 0)
			;
			if ($collection->getSize()) {
				$result = self::CONST_STYLE_BOLD;
			}
		}
		return $result;
	}

	/** @return string */
	public function getMessagesCount() {
		/** @var string $result */
		$result = '';
		$customer = rm_session_customer()->getCustomer();
		if ($customer->getId()) {
			$collection = Df_Forum_Model_PrivateMessage::c();
			$collection->getSelect()
				->where('? = is_trash', 0)
				->where('? = sent_to', rm_session_customer()->getCustomer()->getId())
				->where('? = is_deleteinbox', 0)
				->where('? = is_primary', 1)
				->where('? = is_read', 0)
			;
			if ($collection->getSize()) {
				$result = '(' . $collection->getSize() . ')';
			}
		}
		return $result;
	}

	const _CLASS = __CLASS__;
	const CONST_STYLE_BOLD = ' style="font-weight:bold;" ';
}