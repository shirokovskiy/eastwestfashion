<?php
class Df_Forum_Block_Breadcrumbs extends Df_Core_Block_Template {
	/** @return null */
	public function _getCrumbs() {
		return $this->_crumbs;
	}

	/**
	 * @param $crumbName
	 * @param $crumbInfo
	 * @param bool $after
	 * @return $this
	 */
	public function addCrumb($crumbName, $crumbInfo, $after = false) {
		$this->_prepareArray($crumbInfo, array(
				'label','title','link','first','last','readonly'
			)
		);
		if ((!isset($this->_crumbs[$crumbName])) || (!$this->_crumbs[$crumbName]['readonly'])) {
			$this->_crumbs[$crumbName] = $crumbInfo;
		}
		return $this;
	}

	/**
	 * @param null $store
	 * @return string
	 */
	public function getTitleSeparator($store = null) {
		return
			implode(
				(string)Mage::getStoreConfig('catalog/seo/title_separator', $store)
				,array(' ', ' ')
			)
		;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Breadcrumbs
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this
			->addCrumb(
				'home'
				,array(
					'label' => 'Главная страница'
					,'title' => 'Главная страница'
					,'link' => Mage::getBaseUrl()
				)
			)
		;
		$title = array();
		$path = df_h()->forum()->topic()->getBreadcrumbPath(Mage::getBaseUrl());
		foreach ($path as $name => $breadcrumb) {
			if (empty($breadcrumb['link'])) {
				$breadcrumb['last'] = true;
			}
			$this->addCrumb($name, $breadcrumb);
			$title[]= $breadcrumb['label'];
		}
		if ($this->getBlockHead()) {
			$this->getBlockHead()
				->setTitle(
					implode($this->getTitleSeparator(), array_reverse($title))
				)
			;
		}
		return $this;
	}

	const _CLASS = __CLASS__;
	private $created = false;
	public $_crumbs = null;
}