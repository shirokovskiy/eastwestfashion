<?php
class Df_Forum_Block_Controls extends Df_Core_Block_Template {
	/** @return Df_Forum_Block_Controls */
	public function chooseControlsTemplate() {
		$this
			->setTemplate(
				$this->isCategory()
				? $this->getTopicsControlsTemplate()
				: $this->getPostsControlsTemplate()
			)
		;
		return $this;
	}

	/** @return string */
	public function getAddNewPostUrl() {
		return
			$this->getUrl(
				'df_forum/topic/new/id/'
				.Df_Forum_Model_Action::s()->getCurrentTopicId()
			)
		;
	}

	/** @return string */
	public function getAddNewTopicUrl() {
		return
			$this->getUrl(
				'df_forum/topic/new/parent_id/'
				. rm_nat0(Df_Forum_Model_Action::s()->getCurrentTopic()->getParentId())
			)
		;
	}

	/** @return bool */
	public function getAllowFastReplyButton() {
		return $this->_getData('block_bottom');
	}

	/** @return string */
	public function getAddNewTopicForumUrl() {
		return
			$this->getUrl(
				'df_forum/topic/new/parent_id/'
				. rm_nat0(Df_Forum_Model_Action::s()->getCurrentTopicId())
			)
		;
	}

	/** @return string */
	public function getInitTiny() {
		return $this->getChildHtml('bottom_tiny');
	}

	/** @return string */
	public function getLangLocaleShort() {
		return
			df_h()->forum()->topic()->getAvLocale(
				Mage::app()->getLocale()->getDefaultLocale()
			)
		;
	}

	/** @return bool */
	public function isCategory() {
		/** @var bool $result */
		$result = false;
		if (!is_null(Df_Forum_Model_Action::s()->getCurrentTopic())) {
			if (Df_Forum_Model_Action::s()->getCurrentTopic()->isCategory()) {
				$result = Df_Forum_Model_Action::s()->getCurrentTopic()->isCategory();
			}
			else if (Df_Forum_Model_Action::s()->getCurrentTopic()->hasSubTopics()) {
				$result = Df_Forum_Model_Action::s()->getCurrentTopic()->hasSubTopics();
			}
		}
		return $result;
	}

	/** @return bool */
	public function isLoggedIn() {
		return !!df_a(rm_session_customer()->getCustomer()->getData(), 'entity_id');
	}

	const _CLASS = __CLASS__;
}