<?php
class Df_Forum_Block_Top_Search extends Df_Core_Block_Template {
	/** @return string */
	public function getActionSubmitUrl() {
		return
			df_concat(
				$this->getBaseUrl()
				,
					!is_null(Df_Forum_Model_Action::s()->getCurrentTopic())
				&&
					!Df_Forum_Model_Action::s()->getCurrentTopic()->isCategory()
				? Df_Forum_Model_Action::s()->getCurrentTopic()->getUrlText()
				: 'df_forum/search'
			)
		;
	}

	/** @return bool|null|string */
	public function getRetUrl() {
		return
			is_null(Df_Forum_Model_Action::s()->getCurrentTopic())
			? false
			: Df_Forum_Model_Action::s()->getCurrentTopic()->getUrlText()
		;
	}

	/** @return string */
	public function getSearchInputName() {
		return self::SEARCH_VAR;
	}

	/** @return null|string */
	public function getSearchValue() {
		return
			Df_Forum_Model_Session::s()->getSearchValue()
			? Df_Forum_Model_Session::s()->getSearchValue()
			: $this->getDefaultValue()
		;
	}

	/** @return string */
	public function getSubmitButtonTitle() {
		return $this->isTopic() ? 'Поиск по теме' : 'Поиск по форуму';
	}

	/** @return string */
	public function getDefaultValue() {
		return 'Поиск';
	}

	/** @return string */
	public function getResetSearchUrl() {
		/** @var string $result */
		$result = $this->getBaseUrl() . 'df_forum/search?' . self::RESET_VAR . '=true';
		if ($this->getRetUrl()) {
			$result .= '&' . self::REDIRECT_URL . '=' . $this->getRetUrl();
		}
		return $result;
	}

	/** @return bool */
	public function haveSearchValue() {return !!Df_Forum_Model_Session::s()->getSearchValue();}

	/** @return bool */
	public function isTopic() {
		return
				!is_null(Df_Forum_Model_Action::s()->getCurrentTopic())
			&&
				!Df_Forum_Model_Action::s()->getCurrentTopic()->isCategory()
		;
	}

	const _CLASS = __CLASS__;
	const REDIRECT_URL = 'r_url';
	const RESET_VAR = 'r';
	const SEARCH_VAR = 'q_f';
}