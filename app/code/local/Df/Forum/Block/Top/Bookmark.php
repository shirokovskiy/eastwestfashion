<?php
class Df_Forum_Block_Top_Bookmark extends Df_Core_Block_Template {
	/** @return string */
	public function getAddBookmarkTitle() {
		return 'добавить в закладки';
	}

	/** @return string */
	public function getBookmarkUrl() {
		return df_concat($this->getBaseUrl(), 'df_forum/', $this->bookmark_url);
	}

	/** @return int|null */
	public function getLimit() {
		return
			Df_Forum_Model_Session::s()->getPageLimitPost()
			? Df_Forum_Model_Session::s()->getPageLimitPost()
			: 0
		;
	}

	/** @return int|null */
	public function getPageCurrent() {
		return
			Df_Forum_Model_Session::s()->getPageCurrentPost()
			? Df_Forum_Model_Session::s()->getPageCurrentPost()
			: 1
		;
	}

	/** @return int */
	public function getTopicId() {
		return Df_Forum_Model_Action::s()->getCurrentTopicId();
	}

	/** @return bool */
	public function isCategory() {
		return
			is_null(Df_Forum_Model_Action::s()->getCurrentTopic())
			? false
			: Df_Forum_Model_Action::s()->getCurrentTopic()->isCategory()
		;
	}

	/** @return bool */
	public function needDisplayBookmark() {
		return !$this->isCategory() && !is_null(Df_Forum_Model_Action::s()->getCurrentTopic());
	}

	/** @var string */
	public $bookmark_url = 'bookmark';
	const _CLASS = __CLASS__;
}