<?php
class Df_Forum_Block_Top_Jump extends Df_Core_Block_Template {
	/** @return array[] */
	public function prepareData() {
		return
			df_h()->forum()->topic()->getOptionsTopics(
				false
				,0
				,$where = array(1=> 'status=?')
				,'Forum index'
				,false
				,false
				,true
				,Mage::app()->getStore()->getId()
			)
		;
	}

	/** @return null|string */
	public function getSelected() {
		return
			is_null(Df_Forum_Model_Action::s()->getCurrentTopic())
			? ''
			: Df_Forum_Model_Action::s()->getCurrentTopic()->getUrlText()
		;
	}

	/** @var string */
	public $selected_url = '';
	const _CLASS = __CLASS__;
}