<?php
class Df_Forum_Block_Myprivatemessages_View extends Df_Core_Block_Template {
	/** @return string */
	public function getAvatarUrl() {
		/** @var int|null $id */
		$id = null;
		if (
				$this->getMessage()->getSentFrom()
			===
				rm_session_customer()->getCustomer()->getId()
		) {
			$id = $this->getMessage()->getSentTo();
		}
		if (
				$this->getMessage()->getSentTo()
			===
				rm_session_customer()->getCustomer()->getId()
		) {
			$id = $this->getMessage()->getSentFrom();
		}
		/** @var Df_Forum_Model_Usersettings $user */
		$user =
			Df_Forum_Model_Usersettings::i()->load(
				$id
				,Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
			)
		;
		return $user->getAvatarUrl();
	}

	/** @return string */
	public function getBackLink() {
		/** @var string $result */
		$result =
			df_a(
				array(
					'index' => $this->getIncomingLink()
					,'sent' => $this->getSentLink()
					,'trash' => $this->getTrashLink()
				)
				,$this->getRedirectTarget()
				,$this->getIncomingLink()
			)
		;
		return $result;
	}

	/** @return string */
	public function getBackLinkTitle() {
		/** @var string $result */
		$result =
			df_a(
				array(
					'index' => 'Back to Inbox'
					,'sent' => 'Back to Sent'
					,'trash' => 'Back to Trash'
				)
				,$this->getRedirectTarget()
				,'Back to Inbox'
			)
		;
		return $result;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getBackUrl() {
		return df()->state()->getRefererUrl($defaultUrl = $this->getUrl('df_forum/myprivatemessages/'));
	}

	/** @return null|string */
	public function getCId() {
		/** @var string|null $result */
		$result = null;
		if (
				$this->getMessage()->getSentTo()
			===
				rm_session_customer()->getCustomer()->getId()
		) {
			$result = $this->getMessage()->getSentFrom();
		}
		else if (
				$this->getMessage()->getSentFrom()
			===
				rm_session_customer()->getCustomer()->getId()
		) {
			$result = $this->getMessage()->getSentTo();
		}
		return $result;
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @return string
	 */
	public function getCustomerNick(Df_Forum_Model_PrivateMessage $message) {
		if (Mage::registry('myprivatemessages_inbox') || Mage::registry('myprivatemessages_trash')) {
			$id = $message->getSentFrom();
		}
		else {
			$id = $message->getSentTo();
		}
		return df_h()->forum()->customer()->getCustomerNick($id);
	}

	/**
	 * @param $date
	 * @return string
	 */
	public function getDateFormated($date) {
		return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
	}

	/** @return string */
	public function getDeleteTitle() {
		return 'удалить';
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @param string $redirect
	 * @return string
	 */
	public function getDeleteUrl(
		Df_Forum_Model_PrivateMessage $message
		,$redirect = ''
	) {
		if ($redirect == '') {
			$redirect = $this->getRedirectTarget();
		}
		return
			$this->getUrl(
				'df_forum/myprivatemessages/delete'
				,array(
					'mid' => $message->getId()
					,'r' => $redirect
				)
			)
		;
	}

	/** @return string */
	public function getForumUrl() {
		return $this->getUrl('forum');
	}

	/** @return string */
	public function getIncomingLink() {
		return $this->getUrl('df_forum/myprivatemessages/index');
	}

	/** @return bool */
	public function getIsInbox() {
		return
				!$this->getMessage()->getIsTrash()
			&&
				(
						$this->getMessage()->getSentTo()
					===
						rm_session_customer()->getCustomer()->getId()
				)
		;
	}

	/** @return bool */
	public function getIsTrash() {
		return $this->getMessage()->getIsTrash();
	}
	
	/** @return Df_Forum_Model_PrivateMessage */
	public function getMessage() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				Df_Forum_Model_PrivateMessage::i()->load(
					Mage::app()->getRequest()->getParam(
						Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__MESSAGE_ID
					)
				)
			;
		}
		return $this->{__METHOD__};
	}

	/** @return int */
	public function getMessageId() {
		return $this->getMessage()->getId();
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage|null $message [optional]
	 * @return string|null
	 */
	public function getMessageText(Df_Forum_Model_PrivateMessage $message = null) {
		if (!$message) {
			$message = $this->getMessage();
		}
		return $message->getMessage();
	}

	/** @return string */
	public function getMessageTextarea() {
		/** @var string $result */
		$result = '';
		$data = Df_Forum_Model_Session::s()->getPostPMData();
		if ($data) {
			if (!empty($data['privatemessage'])) {
				$result = $data['privatemessage'];
			}
		}
		return $result;
	}

	/** @return string|null */
	public function getMessageTitle() {
		return $this->getMessage()->getSubject();
	}

	/** @return string */
	public function getMoveTrashTitle() {
		return 'В мусорку';
	}

	/** @return int */
	public function getParentId() {
		return $this->getMessage()->getId();
	}

	/** @return Df_Forum_Model_PrivateMessage[] */
	public function getParents() {
		return $this->getMessage()->getAncestors();
	}

	/** @return string */
	public function getReplyFormAction() {
		return $this->getUrl('df_forum/myprivatemessages/save');
	}

	/** @return string|null */
	public function getRecaptchaField() {
		return
			df_cfg()->forum()->recaptcha()->isEnabledForPM()
			? $this->getChildHtml('recaptcha')
			: null
		;
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage|null $message [optional]
	 * @return string
	 */
	public function getSentDate($message = null) {
		if (!$message) {
			$message = $this->getMessage();
		}
		return $this->getDateFormated($message->getDateSent());
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage|null $message [optional]
	 * @return string
	 */
	public function getSentFrom($message = null) {
		/** @var string $result */
		if (!$message) {
			$message = $this->getMessage();
		}
		if (
				$message->getSentFrom()
			===
				rm_session_customer()->getCustomer()->getId()
		) {
			/** @var Df_Customer_Model_Customer $customer */
			$customer = rm_session_customer()->getCustomer();
			$result =
				rm_sprintf(
					'Я (%s %s)'
					,$customer->getNameFirst()
					,$customer->getNameLast()
				)
			;
		}
		else {
			/** @var string|null $id */
			$id = $message->getSentFrom();
			/** @var Df_Forum_Model_Usersettings $userSettings */
			$userSettings =
				Df_Forum_Model_Usersettings::i()->load(
					$id
					,Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
				)
			;
			if ($userSettings->getId()) {
				$result =
					df_h()->forum()->customer()->getCustomerLink(
						$id, $userSettings->getNickname()
					)
				;
			}
			else {
				/** @var Df_Customer_Model_Customer $customer */
				$customer = Df_Customer_Model_Customer::ld($id);
				$result =
					df_h()->forum()->customer()
						->getCustomerLink(
							$id
							,implode(
								' '
								,array($customer->getNameFirst(), $customer->getNameLast())
							)
						)
				;
			}
		}
		return $result;
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @return null|string
	 */
	public function getSentId(Df_Forum_Model_PrivateMessage $message) {
		/** @var string|null $result */
		$result = null;
		if (Mage::registry('myprivatemessages_inbox') || Mage::registry('myprivatemessages_trash')) {
			$result = $message->getSentFrom();
		}
		else {
			$result = $message->getSentTo();
		}
		return $result;
	}

	/** @return string */
	public function getSendPMAction() {
		return $this->getUrl('df_forum/myprivatemessages/save');
	}

	/** @return string */
	public function getSentLink() {
		return $this->getUrl('df_forum/myprivatemessages/sent');
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage|null $message [optional]
	 * @return string
	 */
	public function getSentTo($message = null) {
		/** @var string $result */
		if (!$message) {
			$message = $this->getMessage();
		}
		if (
				$message->getSentTo()
			===
				rm_session_customer()->getCustomer()->getId()) {
			/** @var Df_Customer_Model_Customer $customer */
			$customer = rm_session_customer()->getCustomer();
			$result =
				rm_sprintf(
					'Я (%s %s)'
					,$customer->getNameFirst()
					,$customer->getNameLast()
				)
			;
		}
		else {
			$id = $message->getSentTo();
			/** @var Df_Forum_Model_Usersettings $userSettings */
			$userSettings =
				Df_Forum_Model_Usersettings::i()->load(
					$id
					,Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
				)
			;
			if ($userSettings->getId()) {
				$result =
					df_h()->forum()->customer()->getCustomerLink(
						$id, $userSettings->getNickname()
					)
				;
			}
			else {
				/** @var Df_Customer_Model_Customer $customer */
				$customer = Df_Customer_Model_Customer::ld($id);
				$result =
					df_h()->forum()->customer()
						->getCustomerLink(
							$id
							,implode(
								' '
								,array($customer->getNameFirst(), $customer->getNameLast())
							)
						)
				;
			}
		}
		return $result;
	}

	/** @return string */
	public function getSubject() {
		$result = '';
		$data = Df_Forum_Model_Session::s()->getPostPMData();
		if ($data) {
			if (!empty($data['subject'])) {
				$result = $data['subject'];
			}
		}
		if (!$result) {
			$result = 'RE: ' . $this->getMessage()->getSubject();
		}
		return $result;
	}

	/** @return string */
	public function getSubmitTitle() {return 'Отправить';}

	/** @return string */
	public function getTrashLink() {return $this->getUrl('df_forum/myprivatemessages/trash');}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @param string $redirect
	 * @return string
	 */
	public function getTrashUrl(
		Df_Forum_Model_PrivateMessage $message
		,$redirect = ''
	) {
		if ($redirect == '') {
			$redirect = $this->getRedirectTarget();
		}
		$id = $message->getId();
		return
			$this->getUrl(
				'df_forum/myprivatemessages/movetrash'
				,array(
					'mid' => $id
					,'r' => $redirect
				)
			)
		;
	}

	/** @return string */
	public function getUserType() {
		return
			df_a(
				array(
					$this->getMessage()->getSentFrom() => 'Получатель'
					,$this->getMessage()->getSentTo() => 'Отправитель'
				)
				,rm_session_customer()->getCustomer()->getId()
			)
		;
	}

	/** @return string */
	public function getUndoTrashTitle() {
		return 'Переместить во входящие';
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @param string $redirect
	 * @return string
	 */
	public function getUndoTrashUrl(
		Df_Forum_Model_PrivateMessage $message
		,$redirect = ''
	) {
		if ($redirect == '') {
			$redirect = $this->getRedirectTarget();
		}
		$id = $message->getId();
		return
			$this->getUrl(
				'df_forum/myprivatemessages/undotrash'
				,array(
					'mid' => $id
					,'r' => $redirect
				)
			)
		;
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @param string $redirect
	 * @return string
	 */
	public function getViewUrl(
		Df_Forum_Model_PrivateMessage $message
		,$redirect = ''
	) {
		return
			$this->getUrl(
				'df_forum/myprivatemessages/view'
				,array(
					'mid' => $message->getId()
					,'r' => $redirect
				)
			)
		;
	}

	/** @return bool */
	public function haveReplyBox() {
		return true;
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @return bool
	 */
	public function isRead(Df_Forum_Model_PrivateMessage $message) {
		return
				$message->isRead()
			||
				(
						$message->getSentTo()
					!==
						rm_session_customer()->getCustomer()->getId()
				)
		;
	}

	/** @return string */
	public function getRedirectTarget() {
		return
			$this->getRequest()->getParam(
				Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__REDIRECT_TARGET
			)
		;
	}

	const _CLASS = __CLASS__;
}