<?php
class Df_Forum_Block_Search extends Df_Core_Block_Template {
	/**
	 * @override
	 * @return Df_Forum_Block_Search
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$root = $this->getLayout()->getBlock('root');
		$root->setTemplate(df_h()->forum()->getLayout());
		$this->initCollection();
		Df_Forum_Block_Search_Breadcrumbs::i();
		return $this;
	}

	/** @return mixed */
	protected function getSearchValue() {
		if (!$this->search) {
			$this->search = Mage::registry('search_value_page');
		}
		return $this->search;
	}

	/** @return mixed */
	protected function getSearchValueSQL() {
		$ret = $this->getSearchValue();
		return $ret;
	}

	/** @return bool|Df_Forum_Model_Resource_Post_Collection|Varien_Data_Collection */
	protected function getSearchResult() {
		return $this->initCollection();
	}

	/** @return string */
	protected function getTitleContent() {
		return rm_sprintf('Вы искали: «%s»', $this->getSearchValue());
	}

	/**
	 * @param $date
	 * @return string
	 */
	protected function getDateFormated($date) {
		return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
	}

	/** @return string */
	protected function getToolbarHtml() {
		return $this->getChildHtml('toolbar');
	}

	/** @return string */
	protected function getHeadHtml() {
		return $this->getChildHtml('head');
	}

	/** @return string */
	protected function getControls() {
		return $this->getChildHtml('controls');
	}

	/** @return string */
	public function getTop() {
		return $this->getChildHtml('forum_top');
	}

	/** @return bool|Df_Forum_Model_Resource_Post_Collection|Varien_Data_Collection */
	public function initCollection() {
		$search_type = $this->getSearchType();
		if (!$this->_objectsCollection) {
			if (
					is_null(Df_Forum_Model_Session::s()->getSearchType())
				||
					('search_by_post' === Df_Forum_Model_Session::s()->getSearchType())
			)	{
				$this->_objectsCollection =
					Df_Forum_Model_Post::c()
						->setPageSize($this->getLimit())
						->setOrder('created_time', 'desc')
						->setCurPage($this->getCurPage())
				;
				$table_topics =
					$this->_objectsCollection
						->getTable(Df_Forum_Model_Resource_Topic::TABLE_NAME)
				;
				$this->_objectsCollection->getSelect()
					->where('main_table.post_orig RLIKE ?', $this->getSearchValueSQL())
					->where('? = main_table.status', 1)
					->joinLeft(
						array('table_topics' => $table_topics)
						,'main_table.parent_id = table_topics.topic_id'
						,'table_topics.title as parent_title'
					)
					->where('? = table_topics.status', 1)
					->joinLeft(
						array('table_forums' => $table_topics)
						,'table_topics.parent_id = table_forums.topic_id'
						,'table_forums.title as forum_title'
					)
					->where('? = table_forums.status', 1)
				;
				$this->_objectsCollection->addStoreFilter(Mage::app()->getStore()->getId());
				$this->setAdditionalData();
			}
			else if ('search_by_topic' === Df_Forum_Model_Session::s()->getSearchType()) {
				$this->_objectsCollection =
					Df_Forum_Model_Topic::c()
						->setPageSize($this->getLimit())
						->setOrder('created_time', 'desc')
						->setCurPage($this->getCurPage())
					;
					$this->_objectsCollection->getSelect()
						->where('main_table.title RLIKE ?', $this->getSearchValueSQL())
						->where('? = main_table.status', 1)
					;
					$this->_objectsCollection->addStoreFilter(Mage::app()->getStore()->getId());
					$this->setAdditionalDataTopic();
			}
		}
		return $this->_objectsCollection;
	}

	/** @return int|mixed|null */
	public function getLimit() {
		/** @var int $limit */
		$limit = intval($this->getRequest()->getParam($this->getLimitVarName()));
		return
			(0 !== $limit) && in_array($limit, $this->limits)
			? $this->_setLimit($limit)
			: (
				Df_Forum_Model_Session::s()->getPageSearchLimit()
			    ? Df_Forum_Model_Session::s()->getPageSearchLimit()
				: $this->limits[0]
			)
		;
	}

	/** @return int|null */
	public function getCurPage() {
		/** @var int $page */
		$page = intval($this->getRequest()->getParam(self::PAGE_VAR_NAME));
		return
			(0 !== $page)
			? $this->_setCurPage($page)
			: (
				!is_null(Df_Forum_Model_Session::s()->getPageSearchCurrent())
				? Df_Forum_Model_Session::s()->getPageSearchCurrent()
				: 1
			)
		;
	}

	/** @return string */
	public function getPageVarName() {
		return self::PAGE_VAR_NAME;
	}

	/** @return string */
	public function getLimitVarName() {
		return self::LIMIT_VAR_NAME;
	}

	/**
	 * @param $id
	 * @param Varien_Object|bool $obj [optional]
	 * @return string
	 */
	public function getViewUrl($id, $obj = false) {
		return
			$obj && $obj->getUrlText()
			? $this->_getUrlrewrited(array(self::PAGE_VAR_NAME => 1), $obj->getUrlText())
			: $this->_getUrl(array(self::PAGE_VAR_NAME => 1), '/view/id/' . $id)
		;
	}

	/** @return string */
	public function getSearchByVarName() {
		return self::SEARCH_BY_VAR_NAME;
	}

	/** @return null|string */
	public function getSearchType() {
		$result = null;
		$var = $this->getSearchByVarName();
		$search_type = $this->getRequest()->getParam($var, null);
		if ($search_type && array_key_exists( $search_type, $this->search_by_types )) {
			$search_type = $this->setSearchType($search_type);
			$result = $search_type;
		}
		else {
			$result =
				!is_null(Df_Forum_Model_Session::s()->getSearchType())
				? Df_Forum_Model_Session::s()->getSearchType()
				: $this->search_by_types['search_by_post']
			;
		}
		return $result;
	}

	/**
	 * @param $value
	 * @return mixed
	 */
	public function setSearchType($value) {
		Df_Forum_Model_Session::s()->setSearchType($value);
		return $value;
	}

	/** @return bool */
	public function getIsSearchTypeByPost() {
		return
				is_null(Df_Forum_Model_Session::s()->getSearchType())
			||
				('search_by_post' === Df_Forum_Model_Session::s()->getSearchType())
		;
	}

	/** @return bool */
	public function getIsSearchTypeByTopic() {
		return 'search_by_topic' === Df_Forum_Model_Session::s()->getSearchType();
	}

	/** @return string */
	public function getSearchByPostContentUrl() {
		return
			$this->getUrl(
				'*/*/*'
				,array(
					'_current' => false
					,$this->getSearchByVarName() => $this->search_by_types['search_by_post']
				)
			)
		;
	}

	/** @return string */
	public function getSearchByTopicTitleUrl() {
		return
			$this->getUrl(
				'*/*/*'
				,array(
					'_current' => false
					,$this->getSearchByVarName() => $this->search_by_types['search_by_topic']
				)
			)
		;
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function _getUrlrewrited($params, $urlAddon = '') {
		return
			$this->getUrl(
				$urlAddon
				,array(
					'_current' => false
					,'_escape' => false
					,'_use_rewrite' => false
					,'_query' => $params
				)
			)
		;
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function _getUrl($params, $urlAddon = '') {
		return
			$this->getUrl(
				'*/*' . $urlAddon
				,array(
					'_current' => false
					,'_escape' => true
					,'_use_rewrite' => true
					,'_query' => $params
				)
			)
		;
	}

	/**
	 * @param int $page
	 * @return int
	 */
	private function _setCurPage($page = 1) {
		Df_Forum_Model_Session::s()->setPageSearchCurrent($page);
		return $page;
	}

	/**
	 * @param $limit
	 * @return mixed
	 */
	private function _setLimit($limit) {
		Df_Forum_Model_Session::s()->setPageSearchLimit($limit);
		return $limit;
	}

	/**
	 *
	 */
	private function setAdditionalData() {
		if ($this->_objectsCollection->getSize()) {
			foreach ($this->_objectsCollection as $key => $val) {
				$this->_objectsCollection->getItemById($key)->setPostSearch($this->getPostForSearch($val->getPost()));
				$this->_objectsCollection->getItemById($key)->setParentTopic($this->getParentTopic($val->getParentId()));
			}
		}
	}

	/**
	 *
	 */
	private function setAdditionalDataTopic() {
		if ($this->_objectsCollection->getSize())
		{
			foreach ($this->_objectsCollection as $key=>$val)
			{
				//$this->_objectsCollection->getItemById($key)->setPostSearch($this->getPostForSearch($val->getPost()));
				$this->_objectsCollection->getItemById($key)->setTopicTitle($this->prepareTopicTitle($val->getTitle()));
			}
		}
	}

	/**
	 * @param $_title
	 * @return mixed
	 */
	private function prepareTopicTitle($_title) {
		return df_h()->forum()->post()->preparePostBySearchValue($_title);
	}

	/**
	 * @param $_post
	 * @return mixed
	 */
	private function getPostForSearch($_post) {
		return df_h()->forum()->post()->preparePostBySearchValue($_post);
	}

	/**
	 * @param $_id
	 * @return Mage_Core_Model_Abstract
	 */
	private function getParentTopic($_id) {
		return Df_Forum_Model_Topic::i()->load($_id);
	}

	const _CLASS = __CLASS__;
	public $limits
		= array(
			5,10,15
		);
	const PAGE_VAR_NAME = 'p';
	const LIMIT_VAR_NAME = 'limit';
	const SEARCH_BY_VAR_NAME        = 'search_by';
	private $search_by_types = array (
		'search_by_post'  => 'search_by_post'
		,'search_by_topic' => 'search_by_topic'
	);
	/** @var Df_Forum_Model_Resource_Post_Collection|bool */
	protected $_objectsCollection = false;
	public $search;
}