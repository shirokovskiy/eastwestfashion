<?php
class Df_Forum_Block_MassActions extends Df_Core_Block_Template {
	/** @return array(array(string => string)) */
	public function getActions() {
		return
			array(
				array(
					'label' => ''
					,'action' => 'no_action'
					,'confirm' => false
				)
				,array(
					'label' => 'скрыть'
					,'action' => 'df_forum/topic/massDisable'
					,'confirm' => true
				)
				,array(
					'label' => 'опубликовать'
					,'action' => 'df_forum/topic/massEnable'
					,'confirm' => true
				)
				,array(
					'label' => 'удалить'
					,'action' => 'df_forum/topic/massDelete'
					,'confirm' => true
				)
			)
		;
	}

	/** @return string */
	public function getEntityType() {
		return $this->getData(self::P__ENTITY_TYPE);
	}

	/** @return string */
	public function getUrlReturn() {
		return $this->getData(self::P__URL_RETURN);
	}

	/**
	 * @override
	 * @return string|null
	 */
	protected function getDefaultTemplate() {
		return 'df/forum/massActions.phtml';
	}

	const _CLASS = __CLASS__;
	const P__ENTITY_TYPE = 'entity_type';
	const P__URL_RETURN = 'url_return';

	/**
	 * @param string $entityType
	 * @param string $urlReturn
	 * @return Df_Forum_Block_MassActions
	 */
	public static function i($entityType, $urlReturn) {
		return df_block(new self(array(
			self::P__ENTITY_TYPE => $entityType, self::P__URL_RETURN => $urlReturn
		)));
	}
}