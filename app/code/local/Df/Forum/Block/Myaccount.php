<?php
class Df_Forum_Block_Myaccount extends Df_Core_Block_Template {
	/** @return string */
	public function getBackUrl() {
		return
			df()->state()->getRefererUrl(
				$defaultUrl = $this->getUrl('df_forum/myaccount/')
			)
		;
	}

	/** @return string */
	public function getForumUrl() {
		return $this->getUrl('forum');
	}

	/** @return string */
	public function getActionForm() {
		return $this->getUrl('df_forum/myaccount/save');
	}

	/** @return string */
	public function getAvatarUrl() {
		return $this->getCurrentCustomerSettings()->getAvatarUrl();
	}

	/** @return bool */
	public function isAvatarExist() {
		/** @var Df_Forum_Model_Usersettings $model */
		$model = $this->getCurrentCustomerSettings();
		$result =
				$model->getAvatarName()
			&&
				file_exists(
					df_concat_path(
						Mage::getBaseDir()
						, df_cfg()->forum()->avatar()->getPath()
						, $model->getAvatarName()
					)
				)
		;
		return $result;
	}

	/** @return string */
	public function getDeleteAvatarUrl() {
		return $this->getUrl('df_forum/myaccount/delavatar');
	}

	/** @return null|string */
	public function getNickName() {
		$data = Df_Forum_Model_Session::s()->getPostData();
		if (!$data) {
			$data = array();
		}
		return df_a($data, 'nickname', $this->getCurrentCustomerSettings()->getNickname());
	}

	/** @return null|string */
	public function getSignature() {
		$data = Df_Forum_Model_Session::s()->getPostData();
		if (!$data) {
			$data = array();
		}
		return df_a($data, 'signature', $this->getCurrentCustomerSettings()->getSignature());
	}

	/** @return string */
	public function getSubmitTitle() {return 'отправить';}

	/** @return Df_Forum_Model_Usersettings */
	private function getCurrentCustomerSettings() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				Df_Forum_Model_Usersettings::i()->load(
					rm_session_customer()->getCustomer()->getId()
					,Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
				)
			;
		}
		return $this->{__METHOD__};
	}

	const _CLASS = __CLASS__;
}