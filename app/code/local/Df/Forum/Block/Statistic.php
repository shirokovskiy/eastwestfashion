<?php
class Df_Forum_Block_Statistic extends Df_Core_Block_Template {
	/** @return int */
	public function getTotalForums() {
		$storeId = Mage::app()->getStore()->getId();
		$c = Df_Forum_Model_Topic::c();
		$c->getSelect()
			->where('? = status', 1)
			->where('? = is_category', 1)
		;
		$c->addStoreFilter($storeId);
		return $c->getSize();
	}

	/** @return int */
	public function getTotalTopics() {
		$storeId = Mage::app()->getStore()->getId();
		$c = Df_Forum_Model_Topic::c();
		$c->addStoreFilter($storeId);
		$c->getSelect()
			->where('? = main_table.status', 1)
			->where('? = main_table.is_category', 0)
		;
		$table_topics = $c->getTable(Df_Forum_Model_Resource_Topic::TABLE_NAME);
		$c->getSelect()
			->joinLeft(
				array('table_topics' => $table_topics)
				,'main_table.parent_id = table_topics.topic_id'
				,'table_topics.title as parent_title'
			)
			->where('? = table_topics.status', 1)
		;
		$c->getSelect()
			->where(
				'(table_topics.store_id IN (?)) OR (table_topics.store_id IS null)'
				,array(0, $storeId)
			)
		;
		return $c->getSize();
	}

	/** @return int */
	public function getTotalPosts() {
		$c = Df_Forum_Model_Post::c();
		$table_topics = $c->getTable(Df_Forum_Model_Resource_Topic::TABLE_NAME);
		$c->getSelect()
			->where('? = main_table.status', 1)
			->joinLeft(
				array('table_topics' => $table_topics)
				,'main_table.parent_id = table_topics.topic_id'
				,'table_topics.title as parent_title'
			)
			->where('? = table_topics.status', 1)
			->joinLeft(
				array('table_forums' => $table_topics)
				,'table_topics.parent_id = table_forums.topic_id'
				,'table_forums.title as forum_title'
			)
			->where('? = table_forums.status', 1)
		;
		$c
			->addStoreFilter(
				Mage::app()->getStore()->getId()
			)
		;
		return $c->getSize();
	}

	/** @return int */
	public function getTotalActiveCustomers() {
		$c = Df_Forum_Model_User::c();
		$storeId = Mage::app()->getStore()->getId();
		$c->addStoreFilter($storeId);
		return $c->getSize();
	}

	const _CLASS = __CLASS__;
}