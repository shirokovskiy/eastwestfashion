<?php
class Df_Forum_Block_Customer extends Df_Core_Block_Template {
	/**
	 * @param Df_Forum_Model_Usersettings $forumUser
	 * @return string
	 */
	public function getAvatarUrl(Df_Forum_Model_Usersettings $forumUser) {
		return $forumUser->getAvatarUrl();
	}

	/** @return Df_Forum_Model_Customer */
	public function getCustomer() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = Df_Forum_Model_Customer::i(array(
				Df_Forum_Model_Customer::P__ID =>
					Df_Forum_Model_Action::s()->getCurrentCustomerId()
				,Df_Forum_Model_Customer::P__USER_SETTINGS_DATA => $this->_getCustomerData()
				,Df_Forum_Model_Customer::P__SYSTEM_USER_DATA =>
					Df_Forum_Model_Registry::s()->getCustomer()
				,Df_Forum_Model_Customer::P__TOTAL_TOPICS =>
					$this->_getCustomerTotalTopics()
				,Df_Forum_Model_Customer::P__TOTAL_POSTS =>
					$this->getCustomerTotalPosts()
				,Df_Forum_Model_Customer::P__STATUS => $this->getCustomerStatusText()
			));
		}
		return $this->{__METHOD__};
	}

	/** @return mixed */
	public function getCustomerJoinedDate() {
		$user = Df_Forum_Model_User::i();
		$user->load(
			Df_Forum_Model_Action::s()->getCurrentCustomerId()
			,Df_Forum_Model_User::P__SYSTEM_USER_ID
		);
		return $user->getFirstPost();
	}

	/** @return string */
	public function getCustomerName() {
		/** @var string $result */
		$result = '';
		if ($this->getCustomer()->getUserSettingsData()->getNickname()
			&& $this->getCustomer()->getUserSettingsData()->getNickname() != ''
		) {
			$result = $this->getCustomer()->getUserSettingsData()->getNickname();
		}
		else if (Df_Forum_Model_Action::ADMIN__ID !== Df_Forum_Model_Action::s()->getCurrentCustomerId()) {
			if ($this->getCustomer()->getSystemUserData()->getFirstname()
				&& $this->getCustomer()->getSystemUserData()->getLastname()
			) {
				$result =
					$this->getCustomer()->getSystemUserData()->getFirstname()
					. ' '
					. $this->getCustomer()->getSystemUserData()->getLastname()
				;
			}
		}
		else {
			if (Df_Forum_Model_Action::ADMIN__ID === Df_Forum_Model_Action::s()->getCurrentCustomerId()) {
				$result = 'Администратор форума';
			}
			else {
				$result = 'Имя отсутствует';
			}
		}
		return $result;
	}

	/**
	 * @param $date
	 * @return string
	 */
	public function getDateFormated($date) {
		return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
	}

	/**
	 * @param $customer
	 * @return string
	 */
	public function getPMUrl($customer) {
		if (is_object($customer)) {
			$id = $customer->getId();
		}
		else {
			$id = $customer;
		}
		return df_h()->forum()->customer()->getCustomerPMUrl($id);
	}

	/** @return bool */
	public function isItAdmin() {
		return
				Df_Forum_Model_Action::ADMIN__ID
			===
				Df_Forum_Model_Action::s()->getCurrentCustomerId()
		;
	}

	/** @return bool */
	public function isItMe() {
		return
				rm_session_customer()->getCustomer()->getId()
			===
				Df_Forum_Model_Action::s()->getCurrentCustomerId()
		;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Customer
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$root = $this->getLayout()->getBlock('root');
		$root->setTemplate(df_h()->forum()->getLayout());
		return $this;
	}

	/** @return Df_Forum_Model_Usersettings */
	private function _getCustomerData() {
		return Df_Forum_Model_Action::s()->getCurrentCustomerSettings();
	}

	/** @return int */
	private function _getCustomerTotalTopics() {
		$collection = Df_Forum_Model_Topic::c();
		/** @var Varien_Db_Select $select */
		$select = $collection->getSelect();
		$select
			->where('? = status', 1)
			->where('? = system_user_id', Df_Forum_Model_Action::s()->getCurrentCustomerId())
		;
		return $collection->getSize();
	}

	/** @return int */
	private function getCustomerTotalPosts() {
		$collection = Df_Forum_Model_Post::c();
		/** @var Varien_Db_Select $select */
		$select = $collection->getSelect();
		$select
			->where('? = status', 1)
			->where('? = system_user_id', Df_Forum_Model_Action::s()->getCurrentCustomerId())
		;
		return $collection->getSize();
	}

	/** @return string */
	private function getCustomerStatusText() {
		/** @var string $result */
		$result = df_a($this->customer_statuses, self::CONST_USER);
		if (
				Df_Forum_Model_Action::ADMIN__ID
			===
				Df_Forum_Model_Action::s()->getCurrentCustomerId()
		) {
			$result = df_a($this->customer_statuses, self::CONST_ADMIN);
		}
		else if (
			df_h()->forum()->topic()->getAllowedModerateWebsites(
				Df_Forum_Model_Action::s()->getCurrentCustomerId()
			)
		) {
			$result = df_a($this->customer_statuses, self::CONST_MODERATOR);
		}
		df_result_string($result);
		return $result;
	}

	/** @var string[] */
	private $customer_statuses = array('Администратор','Модератор','Пользователь');

	const _CLASS = __CLASS__;
	const CONST_ADMIN = 0;
	const CONST_MODERATOR = 1;
	const CONST_USER = 2;
}