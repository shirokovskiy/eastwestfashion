<?php
class Df_Forum_Block_Toolbar_Forums extends Df_Forum_Block_Toolbar {
	/**
	 * @override
	 * @return string
	 */
	public function getItemsNameInGenetiveCase() {
		return 'форумов';
	}
	/**
	 * @override
	 * @return string
	 */
	public function getItemsNameInNominativeCase() {
		return 'форумы';
	}
	const _CLASS = __CLASS__;
}