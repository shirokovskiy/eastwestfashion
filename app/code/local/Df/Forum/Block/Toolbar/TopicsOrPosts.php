<?php
class Df_Forum_Block_Toolbar_TopicsOrPosts extends Df_Forum_Block_Toolbar {
	/**
	 * @override
	 * @return string
	 */
	public function getItemsNameInGenetiveCase() {
		return $this->isCategory() ? 'тем' : 'сообщений';
	}
	/**
	 * @override
	 * @return string
	 */
	public function getItemsNameInNominativeCase() {
		return $this->isCategory() ? 'темы' : 'сообщения';
	}
	/**
	 * @override
	 * @return string|null
	 */
	protected function getDefaultTemplate() {
		return 'df/forum/toolbar/topicsOrPosts.phtml';
	}
	const _CLASS = __CLASS__;
}