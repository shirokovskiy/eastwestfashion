<?php
class Df_Forum_Block_Toolbar_MyPosts extends Df_Forum_Block_Toolbar {
	/**
	 * @override
	 * @return string
	 */
	public function getItemsNameInGenetiveCase() {
		return 'Ваших сообщений';
	}
	/**
	 * @override
	 * @return string
	 */
	public function getItemsNameInNominativeCase() {
		return 'Ваши сообщения';
	}
	const _CLASS = __CLASS__;
}