<?php
class Df_Forum_Block_Toolbar_Found extends Df_Forum_Block_Toolbar {
	/**
	 * @override
	 * @return string
	 */
	public function getItemsNameInGenetiveCase() {
		return 'найденных записей';
	}
	/**
	 * @override
	 * @return string
	 */
	public function getItemsNameInNominativeCase() {
		return 'найденные записи';
	}
	const _CLASS = __CLASS__;
}