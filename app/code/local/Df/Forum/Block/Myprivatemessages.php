<?php
class Df_Forum_Block_Myprivatemessages extends Df_Core_Block_Template {
	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @return string
	 */
	public function getCustomerNick(Df_Forum_Model_PrivateMessage $message) {
		if (Mage::registry('myprivatemessages_inbox') || Mage::registry('myprivatemessages_trash')) {
			$id = $message->getSentFrom();
		}
		else {
			$id = $message->getSentTo();
		}
		return df_h()->forum()->customer()->getCustomerNick($id);
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Myprivatemessages
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->limits = df_h()->forum()->topic()->getPagesLimits();
		return $this;
	}

	/** @return string */
	public function getBackUrl() {
		return df()->state()->getRefererUrl($defaultUrl = $this->getUrl('df_forum/myprivatemessages/'));
	}

	/** @return string */
	public function getIncomingLink() {
		return $this->getUrl('df_forum/myprivatemessages/index');
	}

	/** @return bool */
	public function isInboxActive() {
		return self::INDEX_ACTION === Mage::app()->getRequest()->getActionName();
	}

	/** @return bool */
	public function isTrashActive() {
		return self::TRASH_ACTION === Mage::app()->getRequest()->getActionName();
	}

	/** @return bool */
	public function isSentActive() {
		return self::SENT_ACTION === Mage::app()->getRequest()->getActionName();
	}

	/** @return string */
	public function getTypeOfMessages() {
		return
			df_a(
				array(
					self::INDEX_ACTION => 'входящие'
					,self::SENT_ACTION => 'отправленные'
					,self::TRASH_ACTION => 'мусорка'
				)
				,Mage::app()->getRequest()->getActionName()
			)
		;
	}

	/** @return int */
	public function getInboxCount() {
		$collection = Df_Forum_Model_PrivateMessage::c();
		$collection->getSelect()
			->where('? = is_trash', 0)
			->where('? = sent_to', rm_session_customer()->getCustomer()->getId())
			->where('? = is_deleteinbox', 0)
			->where('? = is_primary', 1)
		;
		return $collection->getSize();
	}

	/** @return int */
	public function getSentCount() {
		$collection = Df_Forum_Model_PrivateMessage::c();
		$collection->getSelect()
			->where('? = sent_from', rm_session_customer()->getCustomer()->getId())
			->where('? = is_deletesent', 0)
			->where('? = is_primary', 1)
		;
		return $collection->getSize();
	}

	/** @return int */
	public function getTrashCount() {
		$collection = Df_Forum_Model_PrivateMessage::c();
		$collection->getSelect()
			->where('? = is_trash', 1)
			->where('? = sent_to', rm_session_customer()->getCustomer()->getId())
			->where('? = is_deleteinbox', 0)
			->where('? = is_primary', 1)
		;
		return $collection->getSize();
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @param int $all
	 * @return string
	 */
	public function countAllMessages(Df_Forum_Model_PrivateMessage $message, $all = 0) {
		/** @var string $result */
		$result = '';
		$parent_id = $message->getParentId();
		$new_message = Df_Forum_Model_PrivateMessage::i()->load($parent_id);
		if ($new_message->getId()) {
			$all++;
			if ($new_message->getParentId()) {
				$result = $this->countAllMessages($new_message, $all);
			}
			else {
				if ($all != 0) {
					$all++;
					$result = '(' . $all . ')';
				}
			}
		}
		else {
			if ($all != 0) {
				$result = '(' . $all++ . ')';
			}
		}
		return $result;
	}

	/** @return string */
	public function getSentLink() {
		return $this->getUrl('df_forum/myprivatemessages/sent');
	}

	/** @return string */
	public function getTrashLink() {
		return $this->getUrl('df_forum/myprivatemessages/trash');
	}

	/** @return string */
	public function getForumUrl() {
		return $this->getUrl('forum');
	}

	/** @return bool */
	public function getSendToCustomer() {
		if (!$this->send_to_customer) {
			$this->loadSendToCustomer();
		}
		return $this->send_to_customer;
	}

	/**
	 * @param Df_Forum_Model_Usersettings $forumUser
	 * @return string
	 */
	public function getAvatarUrl(Df_Forum_Model_Usersettings $forumUser) {
		return $forumUser->getAvatarUrl();
	}

	/** @return string */
	public function getCustomerName() {
		/** @var string $result */
		$result = '';
		if ($this->send_to_customer->getUserSettingsData()->getNickname()
			&& $this->send_to_customer->getUserSettingsData()->getNickname() != ''
		) {
			$result = $this->send_to_customer->getUserSettingsData()->getNickname();
		}
		else if (
				$this->send_to_customer->getSystemUserData()->getFirstname()
			&&
				$this->send_to_customer->getSystemUserData()->getLastname()
		) {
			$result = $this->send_to_customer->getSystemUserData()->getFirstname() . ' ' . $this->send_to_customer->getSystemUserData()->getLastname();
		}
		else {
			if ($this->send_to_customer_id == Df_Forum_Model_Action::ADMIN__ID) {
				$result = 'Администратор форума';
			}
			else {
				$result = 'Имя отсутствует';
			}
		}
		return $result;
	}

	/** @return string */
	public function getCustomerUrl() {
		return df_h()->forum()->customer()->getCustomerUrl($this->send_to_customer_id);
	}

	/** @return string */
	public function getSubmitTitle() {
		return 'отправить';
	}

	/** @return string */
	public function getSendPMAction() {
		return $this->getUrl('df_forum/myprivatemessages/save');
	}

	/** @return mixed */
	public function getCId() {
		return $this->send_to_customer_id;
	}

	/** @return bool */
	public function getCollection() {
		if (!$this->_objectsCollection) {
			$this->initCollection();
		}
		return $this->_objectsCollection;
	}

	/** @return string|null */
	public function getSubjectSend() {
		/** @var array(string => string)|null $data */
		$data = Df_Forum_Model_Session::s()->getPostPMData();
		if (!$data) {
			$data = array();
		}
		return df_a($data, 'subject');
	}

	/** @return string|null */
	public function getMessageSend() {
		/** @var array(string => string)|null $data */
		$data = Df_Forum_Model_Session::s()->getPostPMData();
		if (!$data) {
			$data = array();
		}
		return df_a($data, 'privatemessage');
	}

	/** @return string|null */
	public function getRecaptchaField() {
		return
			df_cfg()->forum()->recaptcha()->isEnabledForPM()
			? $this->getChildHtml('recaptcha')
			: null
		;
	}

	/** @return string */
	public function getToolbar() {
		return $this->getChildHtml('toolbar');
	}

	/** @return Df_Forum_Model_Resource_PrivateMessage_Collection */
	public function initCollection() {
		$collection = Df_Forum_Model_PrivateMessage::c();
		if (Mage::registry('myprivatemessages_inbox')) {
			$collection->getSelect()
				->where('? = is_trash', 0)
				->where('sent_to=?', rm_session_customer()->getCustomer()->getId())
				->where('is_deleteinbox=?', 0)
				->where('is_primary=?', 1)
			;
		}
		else if (Mage::registry('myprivatemessages_sent')) {
			$collection->getSelect()
				->where(
					'sent_from=?'
					,rm_session_customer()->getCustomer()->getId())
				->where('is_deletesent=?', 0)
				->where('is_primary=?', 1)
			;
		}
		else if (Mage::registry('myprivatemessages_trash')) {
			$collection->getSelect()
				->where('? = is_trash', 1)
				->where('? = sent_to', rm_session_customer()->getCustomer()->getId())
				->where('? = is_deleteinbox', 0)
				->where('? = is_primary', 1)
			;
		}
		$collection->setPageSize($this->getLimit())->setCurPage($this->getCurPage())->setOrder('date_sent', 'desc');
		$this->_objectsCollection = $collection;
		return $this->_objectsCollection;
	}

	/** @return int */
	public function getLimit() {
		/** @var int $limit */
		$limit = intval($this->getRequest()->getParam($this->getLimitVarName()));
		return
			(0 !== $limit) && in_array($limit, $this->limits)
			? $this->_setLimit($limit)
			: (
				Df_Forum_Model_Session::s()->getPageLimitPrivateMessages()
			    ? Df_Forum_Model_Session::s()->getPageLimitPrivateMessages()
				: $this->limits[0]
			)
		;
	}

	/** @return string */
	public function getLimitVarName() {
		return self::LIMIT_VAR_NAME;
	}

	/** @return int */
	public function getCurPage() {
		/** @var int $page */
		$page = intval($this->getRequest()->getParam(self::PAGE_VAR_NAME));
		return
			(0 !== $page)
			? $this->_setCurPage($page)
			: (
				!is_null(Df_Forum_Model_Session::s()->getPageMessagesCurrent())
				? Df_Forum_Model_Session::s()->getPageMessagesCurrent()
				: 1
			)
		;
	}

	/** @return string */
	public function getPageVarName() {
		return self::PAGE_VAR_NAME;
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @return null|string
	 */
	public function getSentId(Df_Forum_Model_PrivateMessage $message) {
		if (Mage::registry('myprivatemessages_inbox') || Mage::registry('myprivatemessages_trash')) {
			$id = $message->getSentFrom();
		}
		else {
			$id = $message->getSentTo();
		}
		return $id;
	}

	/**
	 * @param $date
	 * @return string
	 */
	public function getDateFormated($date) {
		return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @param string $redirect
	 * @return string
	 */
	public function getTrashUrl(
		Df_Forum_Model_PrivateMessage $message
		, $redirect = ''
	) {
		return
			$this
				->getUrl(
					'df_forum/myprivatemessages/movetrash'
					,array(
						'mid' => $message->getId()
						,'r' => $redirect
					)
				)
		;
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @param string $redirect
	 * @return string
	 */
	public function getUndoTrashUrl(
		Df_Forum_Model_PrivateMessage $message
		,$redirect = ''
	) {
		return
			$this->getUrl(
				'df_forum/myprivatemessages/undotrash'
				,array(
					'mid' => $message->getId()
					,'r' => $redirect
				)
			)
		;
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @param string $redirect
	 * @return string
	 */
	public function getDeleteUrl(
		Df_Forum_Model_PrivateMessage $message
		,$redirect = ''
	) {
		return
			$this->getUrl(
				'df_forum/myprivatemessages/delete'
				,array(
					'mid' => $message->getId()
					,'r' => $redirect
				)
			)
		;
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @param string $redirect
	 * @return string
	 */
	public function getViewUrl(
		Df_Forum_Model_PrivateMessage $message
		,$redirect = ''
	) {
		return
			$this->getUrl(
				'df_forum/myprivatemessages/view'
				,array(
					'mid' => $message->getId()
					,'r' => $redirect
				)
			)
		;
	}

	/**
	 * @param string $redirect
	 * @return string
	 */
	public function getMassActionMarkRead($redirect) {
		return $this->getUrl('df_forum/myprivatemessages/massread', array('r' => $redirect));
	}

	/**
	 * @param string $redirect
	 * @return string
	 */
	public function getMassActionTrash($redirect) {
		return $this->getUrl('df_forum/myprivatemessages/masstrash', array('r' => $redirect));
	}

	/**
	 * @param string $redirect
	 * @return string
	 */
	public function getMassActionDelete($redirect) {
		return $this->getUrl('df_forum/myprivatemessages/massdelete', array('r' => $redirect));
	}

	/**
	 * @param string $redirect
	 * @return string
	 */
	public function getMassActionUndoTrash($redirect) {
		return $this->getUrl('df_forum/myprivatemessages/massundo', array('r' => $redirect));
	}

	/**
	 * @param Df_Forum_Model_PrivateMessage $message
	 * @return bool
	 */
	public function isRead(Df_Forum_Model_PrivateMessage $message) {
		return
				!$message->isRead()
			||
				(
						$message->getSentTo()
					!==
						rm_session_customer()->getCustomer()->getId()
				)
		;
	}

	/**
	 * @param int $page
	 * @return int
	 */
	private function _setCurPage($page = 1) {
		Df_Forum_Model_Session::s()->setPageMessagesCurrent($page);
		return $page;
	}

	/**
	 * @param $limit
	 * @return mixed
	 */
	private function _setLimit($limit) {
		Df_Forum_Model_Session::s()->setPageLimitPrivateMessages($limit);
		return $limit;
	}

	/**
	 *
	 */
	private function loadSendToCustomer() {
		$this->send_to_customer = new Varien_Object();
		$this->send_to_customer_id = Df_Forum_Model_Action::s()->getCurrentCustomerId();
		$this->send_to_customer->setUserSettingsData($this->_getCustomerData());
		$this->send_to_customer->setSystemUserData($this->_getCustomerSystemData());
	}

	/** @return Df_Forum_Model_Usersettings */
	private function _getCustomerData() {
		return Df_Forum_Model_Action::s()->getCurrentCustomerSettings();
	}

	/** @return mixed */
	private function _getCustomerSystemData() {
		return Mage::registry('customer_system');
	}

	const _CLASS = __CLASS__;
	public $_objectsCollection = false;
	public $limits = array(5,10,15);
	private $send_to_customer = false;
	const INDEX_ACTION = 'index';
	const SENT_ACTION = 'sent';
	const TRASH_ACTION = 'trash';
	const LIMIT_VAR_NAME = 'limit';
	const PAGE_VAR_NAME = 'p';
}