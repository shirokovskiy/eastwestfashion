<?php
class Df_Forum_Block_Pager extends Mage_Page_Block_Html_Pager {
	/**
	 * @override
	 * @return bool
	 */
	public function canShowFirst() {return (1 < $this->getJump()) && (1 < $this->getFrameStart());}
	/**
	 * @override
	 * @return bool
	 */
	public function canShowLast() {
		return $this->getJump() > 1 && $this->getFrameEnd() < $this->getLastPageNum();
	}
	/**
	 * @override
	 * @return bool
	 */
	public function canShowNextJump() {return !!$this->getNextJumpPage();}
	/**
	 * @override
	 * @return bool
	 */
	public function canShowPreviousJump() {return !!$this->getPreviousJumpPage();}

	/**
	 * @override
	 * @return int
	 */
	public function getFrameEnd() {
		$this->_initFrame();
		return $this->_frameEnd;
	}

	/**
	 * @override
	 * @return int
	 */
	public function getFrameLength() {return $this->_frameLength;}

	/**
	 * @override
	 * @return int[]
	 */
	public function getFramePages() {return range($this->getFrameStart(), $this->getFrameEnd());}

	/**
	 * @override
	 * @return int
	 */
	public function getFrameStart() {
		$this->_initFrame();
		return $this->_frameStart;
	}

	/**
	 * @override
	 * @return int
	 */
	public function getJump() {return $this->_jump;}

	/**
	 * @override
	 * @return int
	 */
	public function getLimit() {
		return
			!is_null($this->_limit)
			? $this->_limit
			: parent::getLimit()
		;
	}

	/**
	 * @override
	 * @return null|int
	 */
	public function getNextJumpPage() {
		/** @var int|null $result */
		$result = null;
		if ($this->getJump()) {
			$frameEnd = $this->getFrameEnd();
			if ($this->getLastPageNum() - $frameEnd > 1) {
				$result = min($this->getLastPageNum() - 1, $frameEnd + $this->getJump());
			}
		}
		return $result;
	}

	/**
	 * @override
	 * @return null|int
	 */
	public function getPreviousJumpPage() {
		$result = null;
		if ($this->getJump()) {
			$frameStart = $this->getFrameStart();
			if ($frameStart - 1 > 1) {
				$result = max(2, $frameStart - $this->getJump());
			}
		}
		return $result;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getPreviousJumpUrl() {return $this->getPageUrl($this->getPreviousJumpPage());}

	/**
	 * @override
	 * @return string
	 */
	public function getNextJumpUrl() {return $this->getPageUrl($this->getNextJumpPage());}

	/**
	 * @override
	 * @return bool
	 */
	public function isFrameInitialized() {return $this->_frameInitialized;}

	/**
	 * @override
	 * @param Varien_Data_Collection $collection
	 * @return Df_Forum_Block_Pager
	 */
	public function setCollection($collection) {
		$this->_collection = $collection;
		$this->_setFrameInitialized(false);
		return $this;
	}

	/**
	 * @override
	 * @param int $frame
	 * @return Df_Forum_Block_Pager
	 */
	public function setFrameLength($frame) {
		/** @var int $frame */
		$frame = abs(intval($frame));
		if ($this->getFrameLength() != $frame) {
			$this->_setFrameInitialized(false);
			$this->_frameLength = $frame;
		}
		return $this;
	}

	/**
	 * @override
	 * @param int $jump
	 * @return Df_Forum_Block_Pager
	 */
	public function setJump($jump) {
		/** @var int $jump */
		$jump = abs(intval($jump));
		if ($this->getJump() != $jump) {
			$this->_setFrameInitialized(false);
			$this->_jump = $jump;
		}
		return $this;
	}

	/**
	 * @override
	 * @param int $limit
	 * @return Df_Forum_Block_Pager
	 */
	public function setLimit($limit) {
		$this->_limit = abs(intval($limit));
		return $this;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Pager
	 */
	protected function _initFrame() {
		if (!$this->isFrameInitialized()) {
			$start = 0;
			$end = 0;
			$collection = $this->getCollection();
			if ($collection->last_page_number_tmp && $collection->last_page_number_tmp != 0) {
				$last_page_number = $collection->getLastPageNumberTMP();
			}
			else {
				$last_page_number = $collection->getLastPageNumber();
			}
			if ($last_page_number <= $this->getFrameLength()) {
				$start = 1;
				$end = $last_page_number;
			}
			else {
				$half = ceil ($this->getFrameLength() / 2);
				if ($collection->getCurPage() >= $half && $collection->getCurPage() <= $last_page_number - $half) {
					$start = ($collection->getCurPage() - $half) + 1;
					$end = ($start + $this->getFrameLength()) - 1;
				}
				else if ($collection->getCurPage() < $half) {
					$start = 1;
					$end = $this->getFrameLength();
				}
				else if ($collection->getCurPage() > ($last_page_number - $half)) {
					$end = $last_page_number;
					$start = $end - $this->getFrameLength() + 1;
				}
			}
			$this->_frameStart = $start;
			$this->_frameEnd = $end;
			$this->_setFrameInitialized(true);
		}
		return $this;
	}

	/**
	 * @override
	 * @param bool $flag
	 * @return Df_Forum_Block_Pager
	 */
	protected function _setFrameInitialized($flag) {
		$this->_frameInitialized = !!$flag;
		return $this;
	}

	/**
	 * @override
	 * @return string
	 */
	protected function _toHtml() {
		return
			(1 < $this->getLastPageNum())
			? parent::_toHtml()
			: '&nbsp;'
		;
	}

	/** @return int */
	protected function getLastPageNumberTmp() {
		$collection = $this->getCollection();
		if (
				isset($collection->last_page_number_tmp)
			&&
				$collection->last_page_number_tmp
			&&
				$collection->last_page_number_tmp != 0
			&&
				method_exists($collection, 'getLastPageNumberTMP')
		) {
			$last_page_number = call_user_func(array($collection, 'getLastPageNumberTMP'));
		}
		else {
			$last_page_number = $collection->getLastPageNumber();
		}
		return $last_page_number;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->setTemplate('df/forum/topics/toolbar/pager.phtml');
	}

	/**
	 * Pages quantity per frame
	 * @var int
	 */
	protected $_frameLength = 5;

	/**
	 * Next/previous page position relatively to the current frame
	 * @var int
	 */
	protected $_jump = 5;

	/**
	 * Frame initialization flag
	 * @var bool
	 */
	protected $_frameInitialized = false;

	/**
	 * Start page position in frame
	 * @var int
	 */
	protected $_frameStart;

	/**
	 * Finish page position in frame
	 * @var int
	 */
	protected $_frameEnd;

	/**
	 * Custom limit
	 * @var int
	 */
	protected $_limit;
	const _CLASS = __CLASS__;
}