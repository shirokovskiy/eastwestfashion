<?php
class Df_Forum_Block_Editor extends Df_Core_Block_Template {
	/** @return string */
	public function getLangLocaleShort() {
		return df_h()->forum()->topic()->getAvLocale(Mage::app()->getLocale()->getDefaultLocale());
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Editor
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->getBlockHead()
			->setDataUsingMethod(
				Df_Page_Block_Html_Head::P__CAN_LOAD_TINY_MCE
				,true
			)
		;
		return $this;
	}

	const _CLASS = __CLASS__;
}