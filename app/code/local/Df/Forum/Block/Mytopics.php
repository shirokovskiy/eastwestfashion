<?php
class Df_Forum_Block_Mytopics extends Df_Core_Block_Template {
	/**
	 * @override
	 * @return Df_Forum_Block_Mytopics
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->limits = df_h()->forum()->topic()->getPagesLimits();
		$this->initCollection();
		return $this;
	}

	/** @return array(array(string => string|bool)) */
	public function getMassActions() {
		return $this->massactions;
	}
	/** @var array(array(string => string)) */
	public $massactions =
		array(
			array(
				'label' => ''
				,'action' => 'no_action'
				,'confirm' => false
			)
			,array(
				'label' => 'скрыть'
				,'action' => 'df_forum/topic/massDisable'
				,'confirm' => true
			)
			,array(
				'label' => 'опубликовать'
				,'action' => 'df_forum/topic/massEnable'
				,'confirm' => true
			)
			,array(
				'label' => 'удалить'
				,'action' => 'df_forum/topic/massDelete'
				,'confirm' => true
			)
		)
	;

	/** @return bool|Df_Forum_Model_Resource_Topic_Collection */
	public function getAllTopics() {
		if (!$this->_objectsCollection) {
			$this->initCollection();
		}
		return $this->_objectsCollection;
	}

	/** @return Df_Forum_Model_Resource_Topic_Collection */
	public function initCollection() {
		/** @var bool $isModerator */
		$isModerator = df_h()->forum()->topic()->isModerator();
		if (rm_session_customer()->getCustomer()->getId()) {
			$customerId = rm_session_customer()->getCustomer()->getId();
			$sort   = $this->getSort();
			$status = false;
			if ($isModerator) {
				/** @var int $status */
				$status = $this->getStatus() - 1;
			}
			$this->_objectsCollection = Df_Forum_Model_Topic::c();
			$this->_objectsCollection
				->setPageSize($this->getLimit())
				->setOrder('main_table.created_time', $sort)
				->setCurPage($this->getCurPage())
			;
			$this->_objectsCollection->getSelect()
				->where('? = main_table.is_category', 0)
			;
			if (!$isModerator )
			{
				$this->_objectsCollection->getSelect()
					->where('? = main_table.status', 1)
					->where('? = main_table.system_user_id', $customerId)
				;
			}
			else if ($status && $isModerator)
			{
				$status--;
				$this->_objectsCollection->getSelect()
					->where('? = main_table.status', $status)
				;
			}
			$this->_objectsCollection->addStoreFilter(Mage::app()->getStore()->getId());
			$this->setAdditionalData();
		}
		return $this->_objectsCollection;
	}

	/** @return bool */
	public function isModerator() {
		if (!$this->isModerator_checked) {
			$this->isModer = df_h()->forum()->topic()->isModerator();
			$this->isModerator_checked = true;
		}
		return $this->isModer;
	}

	/**
	 *
	 */
	public function setAdditionalData() {
		$this->setPostsQuantity();
	}

	/**
	 *
	 */
	public function setPostsQuantity() {
		foreach ($this->_objectsCollection as $key => $val) {
			$this->_objectsCollection->getItemById($key)->setTotalPosts(df_h()->forum()->topic()->getPostsQuantity($val, 0, $val)
			);
		}
	}

	/** @return int|null */
	public function getLimit() {
		/** @var int $limit */
		$limit = intval($this->getRequest()->getParam(self::LIMIT_VAR_NAME));
		return
			(0 !== $limit) && in_array($limit, $this->limits)
			? $this->_setLimit($limit)
			: (
				!is_null(Df_Forum_Model_Session::s()->getPageMyTopicsLimit())
				? Df_Forum_Model_Session::s()->getPageMyTopicsLimit()
				: $this->limits[0]
			)
		;
	}

	/** @return string */
	public function getSort() {
		/** @var string $result */
		$result = null;
		/** @var int $sort */
		$sort = intval($this->getRequest()->getParam($this->getSortVarName()));
		if ((0 !== $sort) && array_key_exists($sort, $this->sort_type)) {
			$result = $this->_setSort($sort);
		}
		else {
			$sort =
				Df_Forum_Model_Session::s()->getPageSortMyTopic()
				? Df_Forum_Model_Session::s()->getPageSortMyTopic()
				: 1
			;
			$result =
				$this->sort_type[
					array_key_exists($sort, $this->sort_type)
					? $sort
					: 1
				]
			;
		}
		return $result;
	}

	/**
	 * @param $sort
	 * @return mixed
	 */
	public function _setSort($sort) {
		Df_Forum_Model_Session::s()->setPageSortMyTopic($sort);
		return $this->sort_type[$sort];
	}

	/**
	 * @param $sort
	 * @return string
	 */
	public function getSortUrl($sort) {
		return
			$this->getPagerUrl(
				array(
					self::SORT_VAR_NAME => $sort
					,self::LIMIT_VAR_NAME => null
					,self::PAGE_VAR_NAME => null
					,self::APPROVED_VAR => null
				)
			)
		;
	}

	/**
	 * @param $a
	 * @return string
	 */
	public function getApprovedUrl($a) {
		return
			$this->getPagerUrl(
				array(
					self::SORT_VAR_NAME => null
					,self::LIMIT_VAR_NAME => null
					,self::PAGE_VAR_NAME => null
					,self::APPROVED_VAR => $a
				)
			)
		;
	}

	/**
	 * @param $date
	 * @return string
	 */
	public function getDateFormated($date) {
		return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_LONG);
	}

	/**
	 * @param array $params
	 * @return string
	 */
	private function getPagerUrl($params = array()) {
		return
			$this->getUrl(
				'*/*/*'
				,array(
					'_current' => true
					,'_escape' => true
					,'_use_rewrite' => true
					,'_query' => $params
				)
			)
		;
	}

	/**
	 * Возвращает натуральное число из множества (1, 2, 3).
	 * @return int
	 */
	public function getStatus() {
		/** @var int[] $statuses */
		$statuses = array(1,2,3);
		/** @var int $result */
		$result = null;
		$status = intval($this->getRequest()->getParam(self::APPROVED_VAR));
		if (
				(0 !== $status)
			&&
				in_array($status, $statuses)
		) {
			Df_Forum_Model_Session::s()->setPageMyTopicStatus($status);
			$result = $status;
		}
		else {
			$result =
				!is_null(Df_Forum_Model_Session::s()->getPageMyTopicStatus())
				? Df_Forum_Model_Session::s()->getPageMyTopicStatus()
				: $statuses[0]
			;
		}
		df_result_integer($result);
		return $result;
	}

	/** @return string */
	public function getSortVarName() {
		return self::SORT_VAR_NAME;
	}

	/** @return string */
	public function getUpproved() {
		return self::APPROVED_VAR;
	}

	/** @return string */
	public function getPageVarName() {
		return self::PAGE_VAR_NAME;
	}

	/** @return string */
	public function getLimitVarName() {
		return self::LIMIT_VAR_NAME;
	}

	/**
	 * @param $limit
	 * @return mixed
	 */
	public function _setLimit($limit) {
		Df_Forum_Model_Session::s()->setPageMyTopicsLimit($limit);
		return $limit;
	}

	/** @return int|null */
	public function getCurPage() {
		/** @var int $page */
		$page = intval($this->getRequest()->getParam(self::PAGE_VAR_NAME));
		return
			(0 !== $page)
			? $this->_setCurPage($page)
			: (
				!is_null(Df_Forum_Model_Session::s()->getPageMyTopicsCurrent())
				? Df_Forum_Model_Session::s()->getPageMyTopicsCurrent()
				: 1
			)
		;
	}

	/**
	 * @param int $page
	 * @return int
	 */
	public function _setCurPage($page = 1) {
		Df_Forum_Model_Session::s()->setPageMyTopicsCurrent($page);
		return $page;
	}

	/** @return string */
	public function getToolbarHtml() {
		return $this->getChildHtml('toolbar');
	}

	/**
	 * @param $_id
	 * @return string
	 */
	public function getDeleteUrl($_id) {
		return
			df_concat(
				$this->getBaseUrl()
				,'df_forum/topic/deleteTopic/topic_id/'
				,$_id
				,'?ret='
				,$this->getRetUrl()
			)
		;
	}

	/**
	 * @param $_id
	 * @param $_obj
	 * @return string
	 */
	public function getEditUrl($_id, $_obj) {
		$firstPost = $this->getFirstPost($_obj);
		return
			df_concat(
				$this->getBaseUrl()
				,'df_forum/topic/edit/id/'
				,$_id
				,'/parent_id/'
				,intval($_obj->getParentId())
				,'/post_id/'
				,($firstPost ? $firstPost->getId() : '0')
				,'?ret='
				,$this->getRetUrl()
			)
		;
	}

	/**
	 * @param $_obj
	 * @return mixed
	 */
	public function getFirstPost($_obj) {
		/** @var Df_Forum_Model_Resource_Post_Collection $posts */
		$posts = Df_Forum_Model_Post::c();
		$posts
			->setPageSize(1)
			->setOrder('created_time', 'ASC')
		;
		$posts->getSelect()
			->where('? = parent_id', $_obj->getId())
			->where('? = system_user_id', $_obj->getSystemUserId())
		;
		return df_a($posts->getItems(), 0);
	}

	/** @return string */
	public function getRetUrl() {
		return urlencode ('df_forum/mytopics');
	}

	/** @return string */
	public function getBackUrl() {
		return
			df()->state()->getRefererUrl(
				$defaultUrl = $this->getUrl('df_forum/mytopics/')
			)
		;
	}

	/**
	 * @param $id
	 * @param Varien_Object|bool $obj [optional]
	 * @return string
	 */
	public function getViewUrl($id, $obj = false) {
		return
			$obj && $obj->getUrlText()
			? $this->_getUrlrewrited(array(self::PAGE_VAR_NAME => 1), $obj->getUrlText())
			: $this->_getUrl(array(self::PAGE_VAR_NAME => 1), '/view/id/' . $id)
		;
	}

	/**
	 * @param $_id
	 * @return string
	 */
	public function getDeactivateTopicUrl($_id) {
		return
			df_concat(
				$this->getBaseUrl()
				,'df_forum/topic/disableTopic/topic_id/'
				,$_id
				,'?ret='
				,$this->getRetUrl()
			)
		;
	}

	/**
	 * @param $_id
	 * @return string
	 */
	public function getActivateTopicUrl($_id) {
		return
			df_concat(
				$this->getBaseUrl()
				,'df_forum/topic/enableTopic/topic_id/'
				,$_id
				,'?ret='
				,$this->getRetUrl()
			)
		;
	}

	/** @return string */
	public function getForumUrl() {
		return $this->getUrl('forum');
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function _getUrlrewrited($params, $urlAddon = '') {
		return
			$this->getUrl(
				$urlAddon
				,array(
					'_current' => false
					,'_escape' => false
					,'_use_rewrite' => false
					,'_query' => $params
				)
			)
		;
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function _getUrl($params, $urlAddon = '') {
		return
			$this->getUrl(
				'*/*' . $urlAddon
				,array(
					'_current' => true
					,'_escape' => true
					,'_use_rewrite' => true
					,'_query' => $params
				)
			)
		;
	}

	const _CLASS = __CLASS__;
	private $isModerator_checked = false;
	private $isModer = false;
	public $limits
		= array(
			5,10,15
		);
	const PAGE_VAR_NAME = 'p';
	const LIMIT_VAR_NAME = 'limit';
	const APPROVED_VAR = 'ap';
	const SORT_VAR_NAME = 's';
	/** @var Df_Forum_Model_Resource_Topic_Collection|bool */
	public $_objectsCollection = false;
	/** @var array(int => string) */
	public $sort_type =
		array(
			1 => Varien_Data_Collection::SORT_ORDER_DESC
			,2 => Varien_Data_Collection::SORT_ORDER_ASC
		)
	;
}