<?php
class Df_Forum_Block_Edit_Breadcrumbs extends Df_Core_Block_Template {
	/** @return array[]|null */
	public function _getCrumbs() {
		return $this->_crumbs;
	}

	/**
	 * @param string $crumbName
	 * @param mixed[] $crumbInfo
	 * @param bool $after
	 * @return Df_Forum_Block_Edit_Breadcrumbs
	 */
	public function addCrumb($crumbName, array $crumbInfo, $after = false) {
		$this
			->_prepareArray(
				$crumbInfo
				,array('label','title','link','first','last','readonly')
			)
		;
		if (
				(!isset($this->_crumbs[$crumbName]))
			||
				(!$this->_crumbs[$crumbName]['readonly'])
		) {
			$this->_crumbs[$crumbName] = $crumbInfo;
		}
		return $this;
	}

	/**
	 * @param Mage_Core_Model_Store|int|null $store
	 * @return string
	 */
	public function getTitleSeparator($store = null) {
		return
			implode(
				(string)Mage::getStoreConfig('catalog/seo/title_separator', $store)
				,array(' ', ' ')
			)
		;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Edit_Breadcrumbs
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this
			->addCrumb(
				'home'
				,array(
					'label' => 'Главная страница'
					,'title' => 'Главная страница'
					,'link' => Mage::getBaseUrl()
				)
			)
		;
		/** @var string[] $title */
		$title = array();
		$path =
			df_h()->forum()->topic()->getBreadcrumbPath(
				Mage::getBaseUrl()
				,1
			)
		;
		foreach ($path as $name => $breadcrumb) {
			/** @var string $name */
			/** @var mixed[] $breadcrumb */
			if (empty($breadcrumb['link'])) {
				$breadcrumb['last'] = true;
			}
			$this->addCrumb($name, $breadcrumb);
			$title[]= $breadcrumb['label'];
		}
		/** @var Mage_Page_Block_Html_Head|null $headBlock */
		$headBlock = $this->getLayout()->getBlock('head');
		if ($headBlock) {
			df_assert($headBlock instanceof Mage_Page_Block_Html_Head);
			$headBlock->setTitle(implode($this->getTitleSeparator(), array_reverse($title)));
		}
		return $this;
	}
	/** @var array[]|null */
	public $_crumbs = null;

	const _CLASS = __CLASS__;
}