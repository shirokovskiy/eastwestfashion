<?php
class Df_Forum_Block_Top extends Df_Core_Block_Template {
	/** @return null|string */
	public function getSearchBlock() {
		return
			df_cfg()->forum()->general()->showSearhBlock()
			? $this->getChildHtml('forum_top_search')
			: null
		;
	}

	/** @return null|string */
	public function getJumpBlock() {
		return
			df_cfg()->forum()->general()->showJumpToTop()
			? $this->getChildHtml('forum_top_jump')
			: null
		;
	}

	/** @return null|string */
	public function getBookmarkItemsBlock() {
		return
			df_cfg()->forum()->general()->showAddToBookmarks()
			? $this->getChildHtml('forum_top_bookmarks')
			: null
		;
	}

	/** @return string */
	public function getCurrentUrl() {
		return df_current_url();
	}

	/** @return int */
	public function getStoreId() {
		return Mage::app()->getStore()->getId();
	}

	const _CLASS = __CLASS__;
}