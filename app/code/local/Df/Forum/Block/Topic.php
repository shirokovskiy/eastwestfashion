<?php
class Df_Forum_Block_Topic extends Df_Core_Block_Template {
	/**
	 * @param $id
	 * @param Varien_Object|bool $obj [optional]
	 * @return string|void
	 */
	public function getViewUrl($id, $obj = false) {
		return
			$obj && $obj->getUrlText()
			? $this->_getUrlrewrited(array(self::PAGE_VAR_NAME => 1), $obj->getUrlText())
			: ''
		;
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function _getUrlrewrited($params, $urlAddon = '') {
		return
			$this->getUrl(
				$urlAddon
				,array(
					'_current' => false
					,'_escape' => false
					,'_use_rewrite' => false
					,'_query' => $params
				)
			)
		;
	}

	const _CLASS = __CLASS__;
	public $limits
		= array(
			5,10,15
		);
	const PAGE_VAR_NAME = 'p';
	const LIMIT_VAR_NAME = 'limit';
}