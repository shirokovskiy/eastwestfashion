<?php
class Df_Forum_Block_Product_Detail extends Df_Core_Block_Template {
	/** @return string */
	public function getAddNewUrl() {
		return
			$this->getUrl(
				'df_forum/topic/new/product_id/'
				.$this->getProductId()
				,array(
					'_query' =>
						array(
							self::SB => urlencode(df_current_url())
						)
				)
			)
		;
	}

	/** @return string */
	public function getButtonName() {
		return 'открыть новую тему';
	}

	/** @return Df_Forum_Model_Resource_Topic_Collection */
	public function getCollection() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_Forum_Model_Resource_Topic_Collection $result */
			$result = Df_Forum_Model_Topic::c();
			$result->setOrder($this->order_by, $this->order_type);
			$result->setPageSize(df_cfg()->forum()->productView()->numberOfTopicsToShow());
			$result->addStoreFilter(Mage::app()->getStore()->getId());
			$result->getSelect()
				->where('? = main_table.product_id', $this->getProductId())
				->where('? = status', 1)
			;
			$this->{__METHOD__} = $result;
			$this->setAdditionalData();
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param $id
	 * @param Df_Forum_Model_Topic|null $topic [optional]
	 * @return string|void
	 */
	public function getViewUrl($id, $topic = null) {
		return
			!$topic || !$topic->getUrlText()
			? ''
			: $this->_getUrlrewrited(
				array(
					self::PAGE_VAR_NAME => 1,
					self::SB => urlencode(df_current_url())
				)
				,$topic->getUrlText()
			)
		;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Product_Detail
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		if (df_cfg()->forum()->productView()->needToShow()) {
			$this->setTemplate('df/forum/product/detail.phtml');
		}
		return $this;
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function _getUrlrewrited($params, $urlAddon = '') {
		return
			$this->getUrl(
				$urlAddon
				,array(
					'_current' => false
					,'_escape' => false
					,'_use_rewrite' => false
					,'_query' => $params
				)
			)
		;
	}

	/** @return mixed */
	private function getProductId() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = Mage::registry('product')->getId();
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param int|null $id
	 * @return int	 */
	private function getTotalPosts($id) {
		$c = Df_Forum_Model_Post::c();
		if (!$id) {
			$c->getSelect()->where('parent_id is null');
		}
		else {
			$c->getSelect()->where('? = parent_id', $id);
		}
		return $c->getSize();
	}

	/**
	 *
	 */
	private function setAdditionalData() {
		if ($this->{__CLASS__ . '::getCollection'}) {
			foreach ($this->getCollection() as $key => $val) {
				$this->getCollection()->getItemById($key)
					->setTotalPosts($this->getTotalPosts($val->getId()))
				;
			}
		}
	}
	private $order_type = 'desc';
	private $order_by = 'created_time';
	const _CLASS = __CLASS__;
	const LIMIT_VAR_NAME = 'limit';
	const PAGE_VAR_NAME = 'p';
	const SB = 'sb';
}