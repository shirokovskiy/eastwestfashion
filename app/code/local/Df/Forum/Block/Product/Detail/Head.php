<?php
class Df_Forum_Block_Product_Detail_Head extends Df_Core_Block_Template {
	/** @return string */
	public function getForumTitle() {
		return df_cfg()->forum()->general()->getForumTitle();
	}

	/** @return array|Varien_Data_Collection */
	public function getGalleryImages() {
		return
			$this->_isGalleryDisabled
			? array()
			: $this->getProduct()->getMediaGalleryImages()
		;
	}

	/** @return Df_Catalog_Model_Product */
	public function getProduct() {
		return Mage::registry('current_product');
	}

	/** @return string */
	public function getProductBackLink() {
		/** @var string[] $links */
		$links = Df_Forum_Model_Session::s()->getBackProductLinks();
		/** @var string $result */
		$result =
			df_a(
				$links
				,Mage::app()->getStore()->getId() . '_' . Mage::registry('current_product')->getId()
			)
		;
		if (!$result) {
			$result = $this->getBaseUrl() . Mage::registry('current_product')->getUrl_path();
		}
		return $result;
	}

	/** @return string */
	public function getTierPriceHtml() {return '';}

	/** @var bool */
	private $_isGalleryDisabled = false;
	const _CLASS = __CLASS__;
}