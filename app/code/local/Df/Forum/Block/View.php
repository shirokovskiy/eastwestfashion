<?php
class Df_Forum_Block_View extends Df_Core_Block_Template {
	/** @return void */
	public function chooseMainTemplate() {
		$this
			->setTemplate(
				$this->getDataUsingMethod(
					$this->isCategory() ? 'topics_template' : 'posts_template'
				)
			)
		;
	}

	/**
	 * @param $_id
	 * @return string
	 */
	public function getActivatePostUrl($_id) {
		return
			df_concat(
				$this->getBaseUrl()
				,'df_forum/topic/enablePost/post_id/'
				,$_id
				,'?ret='
				,$this->getRetUrl(Df_Forum_Model_Action::s()->getCurrentTopic())
			)
		;
	}

	/**
	 * @param $_id
	 * @return string
	 */
	public function getActivateTopicUrl($_id) {
		return
			df_concat(
				$this->getBaseUrl()
				,'df_forum/topic/enableTopic/topic_id/'
				,$_id
				,'?ret='
				,$this->getRetUrl(
				Df_Forum_Model_Action::s()->getCurrentTopic())
			)
		;
	}

	/** @return bool|Df_Forum_Model_Resource_Post_Collection|Varien_Data_Collection */
	public function getAllObjects() {
		return $this->initCollection();
	}

	/**
	 * @param Df_Forum_Model_Usersettings $forumUser
	 * @return string
	 */
	public function getAvatarUrl(Df_Forum_Model_Usersettings $forumUser) {
		return $forumUser->getAvatarUrl();
	}

	/** @return string */
	public function getBackFormUrl() {
		return
			df_concat(
				'df_forum/topic/edit/id/'
				,Df_Forum_Model_Action::s()->getCurrentTopic()->getId()
				,'/parent_id/'
				,intval(Df_Forum_Model_Action::s()->getCurrentTopic()->getParentId())
				,'/ret/'
				,$this->getRetUrl(Df_Forum_Model_Action::s()->getCurrentTopic())
			)
		;
	}

	/** @return string|null */
	public function getBreadCrumbs() {
		return
			df_cfg()->forum()->general()->showBreadcrumbs()
			? $this->getChildHtml('util_breadcrumbs')
			: null
		;
	}

	/**
	 * @param int $show_fast_replay_button
	 * @return string
	 */
	public function getControls($show_fast_replay_button = 0) {
		return
			$this->getChildHtml(
				$show_fast_replay_button && df_cfg()->forum()->general()->allowFastReply()
				? 'controls_bottom'
				: 'controls'
			)
		;
	}

	/**
	 * @param $topic
	 * @return mixed
	 */
	public function getCreateTopicUser($topic) {
		return
			df_cfg()->forum()->general()->allowNicks() && $topic->getUserNick()
			? $topic->getUserNick()
			: $topic->getUserName()
		;
	}

	/**
	 * Этот метод публичен,
	 * потому что используется классом Df_Forum_Block_Toolbar
	 * @return int
	 */
	public function getCurPage() {
		/** @var int $page */
		$page = intval($this->getRequest()->getParam(self::PAGE_VAR_NAME));
		return
			(0 !== $page)
			? $this->setCurPage($page)
			: (
				$this->isCategory()
				? (
					Df_Forum_Model_Session::s()->getPageCurrentForum()
					? Df_Forum_Model_Session::s()->getPageCurrentForum()
					: 1
				)
				: (
					Df_Forum_Model_Session::s()->getPageCurrentPost()
					? Df_Forum_Model_Session::s()->getPageCurrentPost()
					: 1
				)
			)
		;
	}

	/**
	 * @param $date
	 * @return string
	 */
	public function getDateFormated($date) {
		return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
	}

	/**
	 * @param $_id
	 * @return string
	 */
	public function getDeactivatePostUrl($_id) {
		return
			df_concat(
				$this->getBaseUrl()
				,'df_forum/topic/disablePost/post_id/'
				,$_id
				,'?ret='
				,$this->getRetUrl(Df_Forum_Model_Action::s()->getCurrentTopic()				)
			)
		;
	}

	/**
	 * @param $_id
	 * @return string
	 */
	public function getDeactivateTopicUrl($_id) {
		return
			df_concat(
				$this->getBaseUrl()
				,'df_forum/topic/disableTopic/topic_id/'
				,$_id
				,'?ret='
				,$this->getRetUrl(Df_Forum_Model_Action::s()->getCurrentTopic())
			)
		;
	}

	/**
	 * @param $_id
	 * @return string
	 */
	public function getDeletePostUrl($_id) {
		return
			df_concat(
				$this->getBaseUrl()
				, 'df_forum/topic/deletePost/post_id/'
				, $_id
				, '?ret='
				, $this->getRetUrl(Df_Forum_Model_Action::s()->getCurrentTopic())
			)
		;
	}

	/**
	 * @param $_id
	 * @return string
	 */
	public function getDeleteTopicUrl($_id) {
		return
			df_concat(
				$this->getBaseUrl()
				,'df_forum/topic/deleteTopic/topic_id/'
				,$_id
				,'?ret='
				,$this->getRetUrl(Df_Forum_Model_Action::s()->getCurrentTopic())
			)
		;
	}

	/**
	 * @param $_id
	 * @param $obj
	 * @return string
	 */
	public function getEditPostUrl($_id, $obj) {
		return
			df_concat(
				$this->getBaseUrl()
				, 'df_forum/topic/edit/id/'
				, intval($obj->getParentId())
				, '/parent_id/'
				, intval(Df_Forum_Model_Topic::i()->load($obj->getParentId())->getParentId())
				, '/post_id/'
				, $_id
				, '/quote_id/'
				, $_id . '?ret='
				, $this->getRetUrl(Df_Forum_Model_Action::s()->getCurrentTopic())
			)
		;
	}

	/**
	 * @param $_id
	 * @param $_obj
	 * @return string
	 */
	public function getEditTopicUrl($_id, $_obj) {
		$firstPost = $this->getFirstPost($_obj);
		return
			df_concat(
				$this->getBaseUrl()
				,'df_forum/topic/edit/id/'
				,$_id
				,'/parent_id/'
				,intval($_obj->getParentId())
				,'/post_id/'
				,($firstPost ? $firstPost->getId() : '0')
				,'?ret='
				,$this->getParentRetUrl($_obj->getParentId())
			)
		;
	}

	/**
	 * @param $_obj
	 * @return mixed
	 */
	public function getFirstPost($_obj) {
		/** @var Df_Forum_Model_Resource_Post_Collection $posts */
		$posts =
			Df_Forum_Model_Post::c()
				->setPageSize(1)
				->setOrder('created_time', 'ASC')
		;
		$posts->getSelect()
			->where('? = parent_id', $_obj->getId())
			->where('? = system_user_id', $_obj->getSystemUserId())
		;
		return df_a($posts->getItems(), 0);
	}

	/** @return string */
	public function getHeadHtml() {
		return $this->getChildHtml('head');
	}

	/** @return string */
	public function getHeadProductHtml() {
		return $this->getChildHtml('headproduct');
	}

	/** @return string */
	public function getImgFolderHotTopic() {
		return self::TOPIC_FOLDER_HOT_IMG;
	}

	/** @return string */
	public function getImgFolderNoanswerTopic() {
		return self::TOPIC_FOLDER_NO_ANSWER_IMG;
	}

	/** @return string */
	public function getImgFolderTopic() {
		return self::TOPIC_FOLDER_IMG;
	}

	/** @return string */
	public function getImgFolderTopicParent() {
		return self::TOPIC_FOLDER_PARENT_IMG;
	}

	/**
	 * @param $obj
	 * @return mixed
	 */
	public function getLatestReply($obj) {
		return
			df_cfg()->forum()->general()->allowNicks() && $obj->getLatestUserNick()
			? $obj->getLatestUserNick()
			: $obj->getLatestUserName()
		;
	}

	/**
	 * Этот метод публичен,
	 * потому что используется классом Df_Forum_Block_Toolbar
	 * @param bool $get_for_sql_only
	 * @return int
	 */
	public function getLimit($get_for_sql_only = false) {
		/** @var int $result */
		$result = 0;
		/** @var int $limit */
		$limit = intval($this->getRequest()->getParam($this->getLimitVarName()));
		if ((0 !== $limit) && in_array($limit, $this->limits)) {
			$limit = $this->setLimit($limit);
			if (df_cfg()->forum()->advanced()->useStickyPosts() && $get_for_sql_only) {
				if ($this->_objectsStickyCollection->getSize()) {
					$limit = $limit - $this->_objectsStickyCollection->getSize();
				}
			}
			$result = $limit;
		}
		else {
			if (!$this->isCategory()) {
				$limit =
					Df_Forum_Model_Session::s()->getPageLimitPost()
					? Df_Forum_Model_Session::s()->getPageLimitPost()
					: $this->limits[0]
				;
				if (df_cfg()->forum()->advanced()->useStickyPosts() && $get_for_sql_only) {
					if ($this->_objectsStickyCollection->getSize()) {
						$limit = $limit - $this->_objectsStickyCollection->getSize();
					}
				}
				$result = $limit;
			}
			else {
				$result =
					Df_Forum_Model_Session::s()->getPageLimitForum()
					? Df_Forum_Model_Session::s()->getPageLimitForum()
					: $this->limits[0]
				;
			}
		}
		return $result;
	}

	/**
	 * @param $val
	 * @return string
	 */
	public function getNickName($val) {
		/** @var string $result */
		$result = '';
		$user_id = $val->getSystemUserId() ? $val->getSystemUserId() : Df_Forum_Model_Action::ADMIN__ID;
		if (empty($this->user_profiles[$user_id])) {
			$this->loadUserSettings($user_id);
		}
		if (!empty($this->user_profiles[$user_id])) {
			if ($this->user_profiles[$user_id]->getNickname()) {
				$result = $this->user_profiles[$user_id]->getNickname();
			}
		}
		return $result ? $result : $this->getCreateTopicUser($val);
	}

	/** @return string */
	public function getPageVarName() {return self::PAGE_VAR_NAME;}

	/** @return int */
	public function getParentId() {
		return Df_Forum_Model_Action::s()->getCurrentTopic()->getParentId();
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function getParentRetUrl($id) {return urldecode($this->getParentUrlText($id));}

	/**
	 * @param $_id
	 * @return mixed
	 */
	public function getParentUrlText($_id) {
		return Df_Forum_Model_Topic::i()->load($_id)->getUrlText();
	}

	/**
	 * @param $customer
	 * @return string
	 */
	public function getPMUrl($customer) {
		if (is_object($customer)) {
			$id = $customer->getId();
		}
		else {
			$id = $customer;
		}
		return df_h()->forum()->customer()->getCustomerPMUrl($id);
	}

	/** @return int */
	public function getProductId() {
		return
			!Mage::registry('current_product')
			? 0
			: intval(Mage::registry('current_product')->getId())
		;
	}

	/**
	 * @param $_id
	 * @param $obj
	 * @return string
	 */
	public function getQuotePostUrl($_id, $obj) {
		return
			df_concat(
				$this->getBaseUrl()
				,'df_forum/topic/new/id/'
				,$obj->getParentId()
				,'/quote/'
				,$_id
			)
		;
	}

	/** @return string|null */
	public function getRecaptchaField() {
		return
				!df_cfg()->forum()->recaptcha()->isEnabledForPosts()
			||
				df_h()->forum()->topic()->isModerator()
			? null
			: $this->getChildHtml('recaptcha')
		;
	}

	/** @return string */
	public function getReplyIdBegin() {
		return df_h()->forum()->viewreply()->getReplyIdBegin();
	}

	/**
	 * @param $obj
	 * @return string
	 */
	public function getRetUrl($obj) {
		return urlencode($obj->getUrlText());
	}

	/** @return string|null */
	public function getSaveUrl() {
		return
			is_null(Df_Forum_Model_Action::s()->getCurrentTopic())
			? null
			: $this->getUrl('*/*/save/id/' . Df_Forum_Model_Action::s()->getCurrentTopicId())
		;
	}

	/** @return bool|null|string */
	public function getSearchValue() {
		return
			Df_Forum_Model_Session::s()->getSearchValue()
			? Df_Forum_Model_Session::s()->getSearchValue()
			: false
		;
	}

	/** @return string */
	public function getSort() {
		/** @var string $result */
		$result = null;
		/** @var int $sort */
		$sort = intval($this->getRequest()->getParam($this->getSortVarName()));
		if ((0 !== $sort) && array_key_exists($sort, $this->sort_type)) {
			$result = $this->setSort($sort);
		}
		else {
			/** @var int $default_sort_post */
			$default_sort_post =
				df_cfg()->forum()->general()->haveNewestPostsHigherPriority()
				? 1
				: 2
			;
			$default_sort_topic =
				df_cfg()->forum()->general()->haveNewestTopicsHigherPriority()
				? 1
				: 2
			;
			if (!$this->isCategory()) {
				$sort =
					Df_Forum_Model_Session::s()->getPageSortPost()
					? Df_Forum_Model_Session::s()->getPageSortPost()
					: $default_sort_post
				;
			}
			else {
				$sort =
					Df_Forum_Model_Session::s()->getPageSortForum()
					? Df_Forum_Model_Session::s()->getPageSortForum()
					: $default_sort_topic
				;
			}
			$result =
				$this->sort_type[
					array_key_exists($sort, $this->sort_type)
					? $sort
					: 1
				]
			;
		}
		return $result;
	}

	/** @return string */
	public function getTitleContent() {
		return
			(!$this->isCategory() || $this->hasSubTopics()) && $this->getSearchValue()
			? rm_sprintf(
				'Результаты поиска фразы «%s» в теме «%s»'
				,$this->getSearchValue()
				,df_h()->forum()->post()->preparePostBySearchValue(
					$this->_current_object->getTitle()
				)
			)
			: $this->_current_object->getTitle()
		;
	}

	/** @return string */
	public function getToolbarHtml() {
		return $this->getChildHtml('toolbar');
	}

	/** @return int */
	public function getTopicId() {
		return Df_Forum_Model_Action::s()->getCurrentTopicId();
	}

	/** @return string */
	public function getLimitVarName() {
		return self::LIMIT_VAR_NAME;
	}

	/** @return string */
	public function getSortVarName() {
		return self::SORT_VAR_NAME;
	}

	/** @return string */
	public function getTop() {
		return $this->getChildHtml('forum_top');
	}

	/**
	 * @param $topic
	 * @return string
	 */
	public function getTopicFolderImg($topic) {
		/** @var string $result */
		$result = '';
		if ($topic->hasSubTopics()) {
			$result = $this->getImgFolderTopicParent();
		}
		else {
			$collectionPosts = Df_Forum_Model_Post::c();
			$collectionPosts->getSelect()
				->where('? = status', 1)
				->where('? = parent_id', $topic->getId())
			;
			$allPosts = $collectionPosts->getSize();
			if ($allPosts >= df_h()->forum()->getQuantityPostsHotTopic()
			) {
				$result = $this->getImgFolderHotTopic();
			}
			else {
				$result =
					($allPosts <= 1)
					? $this->getImgFolderNoanswerTopic()
					: $this->getImgFolderTopic()
				;
			}
		}
		return $result;
	}

	/**
	 * @param $id
	 * @param Varien_Object|bool $obj [optional]
	 * @return string
	 */
	public function getViewUrl($id, $obj = false) {
		return
			$obj && $obj->getUrlText()
			? $this->getUrlRewrited(array(self::PAGE_VAR_NAME => 1), $obj->getUrlText())
			: $this->_getUrl(array(self::PAGE_VAR_NAME => 1), '/view/id/' . $id)
		;
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function getViewUrlLatest($id) {
		return $this->getUrl('df_forum/topic/viewreply', array('id' => $id));
	}

	/**
	 * @param $val
	 * @return mixed
	 */
	public function getUserPostId($val) {
		return
			$val->getSystemUserId()
			? $val->getSystemUserId()
			: Df_Forum_Model_Action::ADMIN__ID
		;
	}

	/**
	 * @param $_id
	 * @return bool
	 */
	public function hasRightsToEdit($_id) {
		return
				df_h()->forum()->topic()->getAllowedModerateWebsites(
					rm_session_customer()->getCustomer()->getId()
				)
			||
				(
						!is_null(rm_session_customer()->getCustomer())
					&&
						($_id === rm_session_customer()->getCustomer()->getId())
				)
		;
	}

	/** @return bool */
	public function hasSubTopics() {
		return Df_Forum_Model_Action::s()->getCurrentTopic()->hasSubTopics();
	}

	/**
	 * Этот метод публичен, потому что используется классом Df_Forum_Block_Toolbar
	 * @param bool $include_limit
	 * @return bool|Df_Forum_Model_Resource_Post_Collection|Varien_Data_Collection
	 */
	public function initCollection($include_limit = false) {
		$sort = $this->getSort();
		$isModerator = df_h()
			->forum()
			->topic()
			->isModerator();
		if (!$this->_objectsCollection && !$this->isCategory()) {
			if (df_cfg()->forum()->advanced()->useStickyPosts()) {
				$this->_objectsStickyCollection = Df_Forum_Model_Post::c();
				$this->_objectsStickyCollection->getSelect()
					->where('? = parent_id', $this->_current_object->getId())
					->where('? = is_sticky', 1)
				;
			}
			$this->_objectsCollection =
				Df_Forum_Model_Post::c() //->setPageSize($this->getLimit())
					->setOrder('created_time', $sort)
					->setCurPage($this->getCurPage())
			;
			$this->_objectsCollection->getSelect()
				->where('? = parent_id', $this->_current_object->getId())
			;
			if (!$isModerator) {
				$this->_objectsCollection->getSelect()
					->where('? = status', 1)
				;
				if (df_cfg()->forum()->advanced()->useStickyPosts()) {
					$this->_objectsStickyCollection->getSelect()
						->where('? = status', 1)
					;
				}
			}
			if ($search = $this->getSearchValue()) {
				$this->updatePostSearch();
			}
			if (df_cfg()->forum()->advanced()->useStickyPosts()) {
				$this->_objectsCollection->getSelect()
					->where('? = is_sticky', 0)
				;
				if ($this->_objectsStickyCollection->getSize()) {
					$this->_countStickyPosts = $this->_objectsStickyCollection->getSize();
				}
			}
			if (!$include_limit) {
				$this->_objectsCollection->setPageSize($this->getLimit(true));
			}
			else {
				$this->_objectsCollection->setPageSize($this->getLimit(true));
			}
			$this->updateCollectionByStickyPosts();
			$this->setAdditionalDataPost();
		}
		else {
			if (!$this->_objectsCollection) {
				$this->_objectsCollection = Df_Forum_Model_Topic::c()
					->setPageSize($this->getLimit())
					->setOrder(
					'created_time',
					$sort
				)
					->setCurPage($this->getCurPage());
				$this->_objectsCollection->getSelect()
					->where('? = parent_id', $this->_current_object->getId())
				;
				if (!$isModerator) {
					$this->_objectsCollection->getSelect()
						->where('? = status', 1)
					;
				}
				$this->setAdditionalData();
			}
		}
		return $this->_objectsCollection;
	}

	/** @return bool */
	public function isCategory() {
		$result =
				!is_null(Df_Forum_Model_Action::s()->getCurrentTopic())
			&&
				(
						Df_Forum_Model_Action::s()->getCurrentTopic()->isCategory()
					||
						Df_Forum_Model_Action::s()->getCurrentTopic()->hasSubTopics()
				)
		;
		return $result;
	}

	/**
	 * @param int $id
	 * @return bool
	 */
	public function isItAdmin($id) {return intval($id) === Df_Forum_Model_Action::ADMIN__ID;}

	/**
	 * @param int $id
	 * @return bool
	 */
	public function isItMe($id) {return $id === rm_session_customer()->getCustomer()->getId();}

	/** @return bool */
	public function isModerator() {return df_h()->forum()->topic()->isModerator();}

	/** @return bool */
	public function isOnProduct() {return !!Mage::registry('current_product');}

	/**
	 * @override
	 * @return Df_Forum_Block_View
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$parent_object = false;
		$this->limits = df_h()->forum()->topic()->getPagesLimits();
		/** @var Mage_Core_Block_Template $rootBlock */
		$rootBlock = $this->getLayout()->getBlock('root');
		$rootBlock->setTemplate(df_h()->forum()->getLayout());
		$this->parent_id =
			/**
			 * Обратите внимание, что вызывать нужно именно $this->getData('parent_id'),
			 * а не $this->getParentId(),
			 * потому что эти два вызова из-за дурного проектирования
			 * имеют разное назначение.
			 */
			$this->getData('parent_id')
		;
		if ($this->parent_id) {
			/** @var Df_Forum_Model_Topic $parent_object */
			$parent_object = Df_Forum_Model_Topic::i()->load($this->parent_id);
		}
		if (!$parent_object) {
			$this->_current_object = Df_Forum_Model_Action::s()->getCurrentTopic();
		}
		else {
			Df_Forum_Model_Action::s()->setCurrentTopic($parent_object);
			$this->_current_object = $parent_object;
		}
		return $this;
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function _getUrl($params, $urlAddon = '') {
		return
			$this->getUrl(
				'*//*//' . $urlAddon
				,array(
					'_current' => true
					,'_escape' => true
					,'_use_rewrite' => true
					,'_query' => $params
				)
			)
		;
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function getUrlRewrited($params, $urlAddon = '') {
		return
			$this->getUrl(
				$urlAddon
				,array(
					'_current' => false
					,'_escape' => false
					,'_use_rewrite' => false
					,'_query' => $params
				)
			)
		;
	}

	/**
	 * @param $user_id
	 * @param bool $storeId
	 * @return array
	 */
	private function getUserTotalPosts($user_id, $storeId = false) {
		$c = Df_Forum_Model_Post::c();
		if ($user_id == Df_Forum_Model_Action::ADMIN__ID) {
			$c
				->getSelect()
				->where(
				'system_user_id IN (?)',
				array(
					0,
					$user_id
				)
			)
				->where(
				'? = status',
				1
			);
		}
		else {
			$c
				->getSelect()
				->where(
				'? = system_user_id',
				$user_id
			)
				->where(
				'? = status',
				1
			);
		}
		$c->setOrder(
			'created_time',
			'asc'
		);
		$c->setPageSize(1);
		$all = 0;
		$joined = now();
		if ($c->getSize()) {
			$all = $c->getSize();
			foreach ($c as $val) {
				$joined = $val->getCreatedTime();
			}
		}
		return array(
			'all' => $all,
			'joined' => $joined
		);
	}

	/**
	 * @param $user_id
	 * @return mixed
	 */
	private function loadUserSettings($user_id) {
		if (!empty($this->user_profiles[$user_id])) {
			return $this->user_profiles[$user_id];
		}
		$this->user_profiles[$user_id] =
			Df_Forum_Model_Usersettings::i()->load(
				$user_id,
				Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
			)
		;
		return $this->user_profiles[$user_id];
	}

	/**
	 * @param int $page
	 * @return int
	 */
	private function setCurPage($page = 1) {
		if (!$this->isCategory()) {
			Df_Forum_Model_Session::s()->setPageCurrentPost($page);
		}
		else {
			Df_Forum_Model_Session::s()->setPageCurrentForum($page);
		}
		return $page;
	}

	/** @return void */
	private function setLastPostInfo() {
		foreach ($this->_objectsCollection as $key => $val) {
			$obj = df_h()
				->forum()
				->topic()->latestPosts;
			if (!empty($obj[$val->getId()])) {
				$this->_objectsCollection
					->getItemById($key)
					->setLatestTopic($obj[$val->getId()]['topic_obj']);
				$this->_objectsCollection
					->getItemById($key)
					->setLatestCreatedTime($obj[$val->getId()]['created_time']);
				$this->_objectsCollection
					->getItemById($key)
					->setLatestUserName($this->getNickname($obj[$val->getId()]['obj']));
				$this->_objectsCollection
					->getItemById($key)
					->setLatestPostId($obj[$val->getId()]['post_id']);
				$this->_objectsCollection
					->getItemById($key)
					->setLatestObj($obj[$val->getId()]['obj']);
			}
		}
	}

	/**
	 * @param $limit
	 * @return mixed
	 */
	private function setLimit($limit) {
		if (!$this->isCategory()) {
			Df_Forum_Model_Session::s()->setPageLimitPost($limit);
		}
		else {
			Df_Forum_Model_Session::s()->setPageLimitForum($limit);
		}
		return $limit;
	}

	/** @return void */
	private function setPostsQuantity() {
		foreach ($this->_objectsCollection as $key => $val) {
			$this->_objectsCollection
				->getItemById($key)
				->setTotalPosts(
				df_h()
					->forum()
					->topic()
					->getPostsQuantity(
					$val,
					0,
					$val
				)
			);
		}
	}

	/**
	 * @param int $sort
	 * @return mixed
	 */
	private function setSort($sort = 1) {
		if (!array_key_exists($sort, $this->sort_type)) {
			$sort = $this->default_sort_type;
		}
		if (!$this->isCategory()) {
			Df_Forum_Model_Session::s()->setPageSortPost($sort);
		}
		else {
			Df_Forum_Model_Session::s()->setPageSortForum($sort);
		}
		return $this->sort_type[$sort];
	}

	/** @return void */
	private function setTotalViews() {
		foreach ($this->_objectsCollection as $key => $val) {
			$this->_objectsCollection
				->getItemById($key)
				->setTotalViews(
				df_h()
					->forum()
					->topic()
					->getTotalViews($val)
			);
		}
	}

	/** @return void */
	private function updatePostSearch() {
		foreach ($this->_objectsCollection as $key => $val) {
			$this->_objectsCollection
				->getItemById($key)
				->setPost(
				df_h()
					->forum()
					->post()
					->preparePostBySearchValue($val->getPost())
			);
		}
		if (df_cfg()->forum()->advanced()->useStickyPosts()) {
			foreach ($this->_objectsStickyCollection as $key => $val) {
				$this->_objectsStickyCollection
					->getItemById($key)
					->setPost(
					df_h()
						->forum()
						->post()
						->preparePostBySearchValue($val->getPost())
				);
			}
		}
	}

	/** @return void */
	private function setAdditionalData() {
		$this->setPostsQuantity();
		$this->setLastPostInfo();
		$this->setTotalViews();
	}

	/** @return void */
	private function setAdditionalDataPost() {
		$this->setPostsUsersData();
	}

	/** @return void */
	private function setPostsUsersData() {
		$userObject = array();
		foreach ($this->_objectsCollection as $key => $val) {
			$user_id = $val->getSystemUserId() ? $val->getSystemUserId() : Df_Forum_Model_Action::ADMIN__ID;
			if (empty($userObject[$user_id])) {
				$userObject[$user_id] = $this->getUserTotalPosts($user_id);
			}
			if (empty($user[$user_id])) {
				$user[$user_id] = $this->loadUserSettings($user_id);
			}
			if (!empty($userObject[$user_id])) {
				$this->_objectsCollection
					->getItemById($key)
					->setUserTotalPosts($userObject[$user_id]['all']);
				$this->_objectsCollection
					->getItemById($key)
					->setUserJoined($userObject[$user_id]['joined']);
			}
			if (!empty($user[$user_id])) {
				$this->_objectsCollection
					->getItemById($key)
					->setUserProfile($user[$user_id]);
				if ($user[$user_id]->getNickname()) {
					$this->_objectsCollection
						->getItemById($key)
						->setUserNick($user[$user_id]->getNickname());
				}
				if ($user[$user_id]->getSignature()) {
					$this->_objectsCollection
						->getItemById($key)
						->setSignature($user[$user_id]->getSignature());
				}
			}
		}
	}

	/** @return void */
	private function updateCollectionByStickyPosts() {
		if (df_cfg()->forum()->advanced()->useStickyPosts()) {
			if ($this->_objectsStickyCollection->getSize()) {
				$this->_objectsCollection->setLastPageNumberTMP($this->_objectsCollection->getLastPageNumber());
				foreach ($this->_objectsStickyCollection as $obj) {
					/** @var Df_Forum_Model_Post $obj */
					$obj->markAsSticky(true);
					$this->_objectsCollection->addItem($obj);
				}
				$this->_objectsCollection->_increaseSize($this->_objectsStickyCollection->getSize());
			}
		}
	}

	public $limits
		= array(
			5,
			10,
			15
		);
	private $_current_object = false;
	const PAGE_VAR_NAME = 'p';
	const LIMIT_VAR_NAME = 'limit';
	const SORT_VAR_NAME = 'sort';
	const TOPIC_FOLDER_IMG = 'df/images/forum/folder_topic.png';
	const TOPIC_FOLDER_PARENT_IMG = 'df/images/forum/folder_topic_parent.png';
	const TOPIC_FOLDER_HOT_IMG = 'df/images/forum/folder_topic_hot.png';
	const TOPIC_FOLDER_NO_ANSWER_IMG = 'df/images/forum/folder_topic_nonanswered.png';
	/** @var array(int => string) */
	public $sort_type =
		array(
			1 => Varien_Data_Collection::SORT_ORDER_DESC
			,2 => Varien_Data_Collection::SORT_ORDER_ASC
		)
	;
	private $default_sort_type = 1;
	private $user_profiles = array();
	private $parent_id = false;
	/** @var Df_Forum_Model_Resource_Post_Collection|bool */
	protected $_objectsCollection = false;
	/** @var Df_Forum_Model_Resource_Post_Collection|bool */
	protected $_objectsStickyCollection = false;
	private $_countStickyPosts = 0;
	const _CLASS = __CLASS__;
}