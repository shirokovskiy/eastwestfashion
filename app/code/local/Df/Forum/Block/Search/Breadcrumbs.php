<?php
class Df_Forum_Block_Search_Breadcrumbs extends Df_Core_Block_Template {
	/**
	 * @param Mage_Core_Model_Store|string|int|null $store [optional]
	 * @return string
	 */
	public function getTitleSeparator($store = null) {
		return
			implode(
				(string)Mage::getStoreConfig('catalog/seo/title_separator', $store)
				,array(
					' '
					,' '
				)
			)
		;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Search_Breadcrumbs
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		/** @var Mage_Page_Block_Html_Breadcrumbs|Df_Forum_Block_Breadcrumbs|Df_Forum_Block_Edit_Breadcrumbs|bool $breadcrumbsBlock */
		$breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadcrumbsBlock) {
			$breadcrumbsBlock
				->addCrumb(
					'home'
					,array(
						'label' => 'Главная страница'
						,'title' => 'Главная страница'
						,'link' => Mage::getBaseUrl()
					)
				)
			;
			$title = array();
			$path = df_h()->forum()->topic()->getBreadcrumbPath(Mage::getBaseUrl(), false, true);
			foreach ($path as $name => $breadcrumb) {
				$breadcrumbsBlock->addCrumb($name, $breadcrumb);
				$title[]= $breadcrumb['label'];
			}
			$breadcrumbsBlock
				->addCrumb(
					'search'
					,array(
						'label' => 'Результаты поиска'
						,'title' => 'Результаты поиска'
					)
				)
			;
			/** @var Mage_Page_Block_Html_Head|bool $headBlock */
			$headBlock = $this->getLayout()->getBlock('head');
			if ($headBlock) {
				$headBlock->setTitle(implode($this->getTitleSeparator(), array_reverse($title)));
			}
		}
		return $this;
	}

	/** @return Df_Forum_Block_Search_Breadcrumbs */
	public static function i() {return df_block(__CLASS__);}
}