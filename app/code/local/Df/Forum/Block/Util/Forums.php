<?php
class Df_Forum_Block_Util_Forums extends Df_Core_Block_Template {
	/** @return Df_Forum_Model_Resource_Topic_Collection */
	protected function getForums() {
		/** @var Df_Forum_Model_Resource_Topic_Collection $collection */
		$collection = Df_Forum_Model_Topic::c()->setPageSize($this->getTotalForums());
		$collection->setOrder('created_time', 'desc');
		$parent_id = $this->_getParentId();
		if ($parent_id) {
			$collection->getSelect()
				->where('? = status', 1)
				->where('? = parent_id', $parent_id)
			;
		}
		else {
			$collection->getSelect()
				->where('? = status', 1)
				->where('? = is_category', 1)
			;
		}
		$collection->addStoreFilter(Mage::app()->getStore()->getId());
		return $collection;
	}

	/** @return int */
	protected function getTotalForums() {
		/** @var int $forumCount */
		$forumCount = intval($this->getForumCount());
		return
			(0 === $forumCount)
			? $this->max_forums_in_block
			: $forumCount
		;
	}

	/** @return string */
	protected function _getTitle() {
		return $this->getTitle() ? $this->getTitle() : 'Меню форума';
	}

	/**
	 * @param $obj
	 * @return string
	 */
	protected function _getLink($obj) {
		return
			($obj && $obj->getUrlText() != '' && $obj->getUrlText())
 			? $this->_getUrlrewrited(
				array(self::PAGE_VAR_NAME => 1)
				,$obj->getUrlText()
				,$obj->getStoreId()
			)
			: $this->_getUrl(array(self::PAGE_VAR_NAME => 1), '/view/id/' . $obj->getId())
		;
	}

	/** @return int|null */
	protected function _getParentId() {
		return
			$this->getParentId()
			? intval($this->getParentId())
			: null
		;
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @param bool $storeId
	 * @return string
	 */
	private function _getUrlrewrited($params, $urlAddon = '', $storeId = false) {
		return
			$this->getUrl(
				$urlAddon
				,array(
					'_current' => false
					,'_escape' => false
					,'_query' => $params
				)
			)
		;
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function _getUrl($params, $urlAddon = '') {
		return
			$this->getUrl(
				'*/*' . $urlAddon
				,array(
					'_current' => false
					,'_escape' => false
					,'_use_rewrite' => false
					,'_query' => $params
				)
			)
		;
	}

	/** @var int */
	private $max_forums_in_block = 5;
	const _CLASS = __CLASS__;
	const PAGE_VAR_NAME = 'p';
}