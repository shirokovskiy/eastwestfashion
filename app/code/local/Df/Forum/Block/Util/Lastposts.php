<?php
class Df_Forum_Block_Util_Lastposts extends Df_Core_Block_Template {
	/** @return string */
	public function _getTitle() {return $this->getTitle() ? $this->getTitle() : 'Последние сообщения';}

	/**
	 * @param $obj
	 * @return string
	 */
	public function getDateFormated($obj) {
		return $this->formatDate($obj->getCreatedTime(), Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
	}

	/**
	 * @param $obj
	 * @return string
	 */
	public function getPostedBy($obj) {
		$system_user_id = $obj->getSystemUserId() ? $obj->getSystemUserId() : Df_Forum_Model_Action::ADMIN__ID;
		if ($system_user_id) {
			$o =
				Df_Forum_Model_Usersettings::i()->load(
					$system_user_id
					,Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
				)
			;
			if ($o->getId()) {
				$nick = $o->getNickname();
			}
			else {
				$nick = $obj->getUserName();
			}
		}
		else {
			$nick = $obj->getUserName();
		}
		return df_h()->forum()->customer()->getCustomerLink($system_user_id, $nick);
	}

	/** @return Df_Forum_Model_Resource_Post_Collection */
	public function getPosts() {
		/** @var Df_Forum_Model_Resource_Post_Collection $collection */
		$collection = Df_Forum_Model_Post::c()->setPageSize($this->getTotalPosts());
		$table_topics = $collection->getTable(Df_Forum_Model_Resource_Topic::TABLE_NAME);
		$table_topics_parent = $table_forums = $table_topics;
		$parent_id = $this->_getParentId();
		if ($parent_id) {
			$collection->setOrder('main_table.created_time', 'desc');
			$collection->getSelect()
				->joinLeft(
					array('table_topics_parent_last' => $table_topics)
					,'main_table.parent_id = table_topics_parent_last.topic_id'
					,'table_topics_parent_last.title as parent_name'
				)
				->joinLeft(
					array('table_topics_parent_last_2' => $table_topics_parent)
					,'table_topics_parent_last.parent_id = table_topics_parent_last_2.topic_id'
					,''
				)
			;
			$collection->getSelect()
				->joinLeft(
					array('table_topics_parent_last_3' => $table_forums)
					,implode(
						' AND '
						,
						array(
							'(table_topics_parent_last_2.parent_id = table_topics_parent_last_3.topic_id)'
							,
							'(0 <> table_topics_parent_last_2.parent_id)'
						)
					)
					,
					''
				)
			;
			$collection->getSelect()
				->where('? = table_topics_parent_last.status', 1)
				->where('? = table_topics_parent_last.has_subtopic', 0)
				->where('? = main_table.status', 1)
				->where(
					'(? = table_topics_parent_last_2.topic_id) OR (? = table_topics_parent_last_3.topic_id)'
					,$parent_id
				)
			;
		}
		else {
			$collection->setOrder('main_table.created_time', 'desc');
			$collection->getSelect()
				->where('? = main_table.status', 1)
				->joinLeft(
					array(
						'table_topics_parent_last' => $table_topics)
						,'main_table.parent_id = table_topics_parent_last.topic_id'
						,'table_topics_parent_last.title as parent_name'
					)
					->where('? = table_topics_parent_last.status', 1)
					->where('? = table_topics_parent_last.has_subtopics', 0)
			;
		}
		$collection->addStoreFilter(Mage::app()->getStore()->getId());
		return $collection;
	}

	/** @return int */
	public function getTotalPosts() {
		return $this->max_posts_in_block;
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function getViewUrlLatest($id) {
		return $this->getUrl('df_forum/topic/viewreply', array('id' => $id));
	}

	/** @return int|null */
	private function _getParentId() {
		return
			$this->getParentId()
			? intval($this->getParentId())
			: null
		;
	}
	const _CLASS = __CLASS__;
	const PAGE_VAR_NAME = 'p';
	private $max_posts_in_block = 5;
}