<?php
class Df_Forum_Block_Recaptcha_Recaptcha extends Df_Core_Block_Template {
	/** @return string */
	public function displayRecaptcha() {
		require_once('recaptchalib.php');
		return recaptcha_get_html(df_cfg()->forum()->recaptcha()->getKeyPublic(), null, false, '100%');
	}

	/** @return string */
	public function getLanguage() {
		/** @var string $result */
		$result = '';
		$current_lang = Mage::app()->getLocale()->getDefaultLocale();
		$arr = explode('_', $current_lang);
		if (!empty($arr[0])) {
			if (in_array($arr[0], $this->avaiable_languges)) {
				$result = $arr[0];
			}
			else {
				$result = $this->avaiable_languges[0];
			}
		}
		return $result;
	}
	/** @var string[] */
	private $avaiable_languges = array('en','nl','fr','de','pt','ru','es','tr');
	const _CLASS = __CLASS__;
}