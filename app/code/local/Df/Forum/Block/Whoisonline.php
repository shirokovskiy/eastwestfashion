<?php
class Df_Forum_Block_Whoisonline extends Df_Core_Block_Template {
	/**
	 * @param bool $is_object
	 * @return int
	 */
	public function getGuests($is_object = false) {
		$storeId = Mage::app()->getStore()->getId();
		/** @var Df_Forum_Model_Resource_Visitor_Collection $mc */
		$mc = Df_Forum_Model_Visitor::c()->addStoreFilter($storeId);
		$mc->getSelect()->where('? = system_user_id', 0);
		$add = 0;
		if ($is_object) {
			$id = $this->getTopicId();
			if ($id) {
				$mc->getSelect()->where('? = topic_id', $id);
				$parentId = $this->getParentId();
				if ($parentId) {
					/** @var Df_Forum_Model_Resource_Visitor_Collection $mc2 */
					$mc2 = Df_Forum_Model_Visitor::c()->addStoreFilter($storeId);
					$mc2->getSelect()
						->where('? = system_user_id', 0)
						->where('? = parent_id', $parentId)
					;
					$add = $mc2->getSize();
				}
			}
		}
		return $mc->getSize() + $add;
	}

	/**
	 * @param bool $is_object
	 * @return int
	 */
	public function getRegisteredUsers($is_object = false) {
		$storeId = Mage::app()->getStore()->getId();
		$mc = Df_Forum_Model_Visitor::c();
		$mc->addStoreFilter($storeId);
		$mc->getSelect()->where('? <> system_user_id', 0);
		$add = 0;
		if ($is_object) {
			$id = $this->getTopicId();
			if ($id) {
				$mc->getSelect()->where('? = topic_id', $id);
				$parentId = $this->getParentId();
				if ($parentId) {
					/** @var Df_Forum_Model_Resource_Visitor_Collection $mc2 */
					$mc2 = Df_Forum_Model_Visitor::c()->addStoreFilter($storeId);
					$mc2->getSelect()
						->where('? <> system_user_id', 0)
						->where('? = parent_id', $parentId)
					;
					$add = $mc2->getSize();
				}
			}
		}
		return $mc->getSize() + $add;
	}

	/** @return string */
	public function getTitle() {
		/** @var string $result */
		$result =
			'forums' === $this->getSubType()
			? 'На форуме'
			: (
				$this->isCategory()
				? 'Этот раздел сейчас смотрят'
				: 'Эту тему сейчас смотрят'
			)
		;
		df_result_string($result);
		return $result;
	}

	/**
	 * @param bool $is_object
	 * @return int
	 */
	public function getTotalUsers($is_object = false) {
		$storeId = Mage::app()->getStore()->getId();
		$mc = Df_Forum_Model_Visitor::c();
		$mc->addStoreFilter($storeId);
		$add = 0;
		if ($is_object) {
			$id = $this->getTopicId();
			if ($id) {
				$mc->getSelect()->where('? = topic_id', $id);
				$parentId = $this->getParentId();
				if ($parentId) {
					/** @var Df_Forum_Model_Resource_Visitor_Collection $mc2 */
					$mc2 = Df_Forum_Model_Visitor::c()->addStoreFilter($storeId);
					$mc2->getSelect()->where('? = parent_id', $parentId);
					$add = $mc2->getSize();
				}
			}
		}
		return $mc->getSize() + $add;
	}

	/** @return bool */
	public function isCategory() {
		return
				!is_null($this->getTopic())
			&&
				(
						$this->getTopic()->isCategory()
					||
						$this->getTopic()->hasSubtopics()
				)
		;
	}

	/** @return string|null */
	protected function getDefaultTemplate() {
		return 'df/forum/whoisonline.phtml';
	}

	/** @return int */
	private function getTopicId() {
		return Df_Forum_Model_Action::s()->getCurrentTopicId();
	}

	/** @return int|mixed */
	private function getParentId() {
		return
			!is_null($this->getTopic()) && $this->isCategory()
			? $this->getTopic()->getId()
			: 0
		;
	}

	/** @return string */
	private function getSubType() {
		return $this->getData('sub_type');
	}

	/** @return Df_Forum_Model_Topic|null */
	private function getTopic() {
		return Df_Forum_Model_Action::s()->getCurrentTopic();
	}

	const _CLASS = __CLASS__;
}