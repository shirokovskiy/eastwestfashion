<?php
class Df_Forum_Block_Edit extends Df_Core_Block_Template {
	/** @return string */
	public function getBackFormUrl() {
		/** @var string $result */
		$result =
			df_concat_url(
				df_clean(
					array(
						'df_forum/topic'
						,$this->getRequest()->getActionName()
						,$this->getPathPart(
							'parent_id', (int)df_request(Df_Forum_Model_Topic::P__PARENT_ID, false)
						)
						,$this->getPathPart('post_id', (int)df_request('post_id', false))
						,$this->getPathPart('id', (int)df_request('id', false))
						,$this->getPathPart('ret', df_request('ret', false))
					)
					,$additionalValuesToClean = array(0, false)
				)
			) . '/'
		;
		return $result;
	}

	/** @return string */
	public function getBackUrl() {
		/** @var string $result */
		$result = '';
		if (Mage::registry('redirect') && Mage::registry('redirect') != false) {
			$result = $this->getUrl('' . Mage::registry('redirect'));
		}
		else {
			$sess = rm_session_core()->getData();
			if (!empty($sess["visitor_data"]['http_referer'])) {
				$result = $sess["visitor_data"]["http_referer"];
			}
			else {
				$result = $this->getUrl('df_forum/topic');
			}
		}
		return $result;
	}

	/** @return string|null */
	public function getBreadCrumbs() {
		return
			df_cfg()->forum()->general()->showBreadcrumbs()
			? $this->getChildHtml('util_breadcrumbs')
			: null
		;
	}

	/** @return string */
	public function getModelContent() {
		return
			df_a(
				Df_Forum_Model_Session::s()->getPostForumData()
				,'Post'
				,df_text()->escapeHtml($this->getModelPost()->getPost())
			)
		;
	}

	/** @return string */
	public function getModelDescription() {
		return
			df_a(
				Df_Forum_Model_Session::s()->getPostForumData()
				,'description'
				,df_text()->escapeHtml($this->getModel()->getDescription())
			)
		;
	}

	/** @return string */
	public function getModelNickName() {
		return
			df_a(
				Df_Forum_Model_Session::s()->getPostForumData()
				,'NickName'
				,df_text()->escapeHtml($this->getModelPost()->getUserNick())
			)
		;
	}

	/** @return string */
	public function getModelParentId() {
		return
			df_a(
				Df_Forum_Model_Session::s()->getPostForumData()
				,Df_Forum_Model_Post::P__PARENT_ID
				,df_text()->escapeHtml($this->getModelPost()->getParentId())
			)
		;
	}

	/** @return mixed */
	public function getModelPost() {
		return Mage::registry('current_object_post');
	}

	/** @return string */
	public function getModelTitle() {
		return
			df_a(
				Df_Forum_Model_Session::s()->getPostForumData()
				,'Title'
				,df_text()->escapeHtml($this->getModel()->getTitle())
			)
		;
	}

	/** @return string|null */
	public function getRecaptchaField() {
		return
				df_cfg()->forum()->recaptcha()->isEnabledForPosts()
			&&
				!df_h()->forum()->topic()->isModerator()
			? $this->getChildHtml('recaptcha')
			: null
		;
	}

	/** @return mixed */
	public function getQuoteId() {
		return Mage::registry('quote');
	}

	/** @return string */
	public function getQuoteText() {
		$quoteText = '';
		if (Mage::registry('quote')) {
			$post = Df_Forum_Model_Post::i()->load(rm_nat0(Mage::registry('quote')));
			if ($post->getId()) {
				$quoteText .= '<blockquote class="rm-forum">' . $post->getPost() . '</blockquote><br>';
			}
		}
		return $quoteText;
	}

	/** @return string */
	public function getIconId() {
		return
			df_a(
				Df_Forum_Model_Session::s()->getPostForumData()
				,Df_Forum_Model_Topic::P__ICON_ID
				,df_text()->escapeHtml($this->getModel()->getIconId())
			)
		;
	}

	/**
	 * @param bool $id
	 * @return bool
	 */
	public function getNotifyMe($id = false) {
		$data = Df_Forum_Model_Session::s()->getPostForumData();
		/** @var bool $result */
		$result = df_a($data, 'notify_me');
		if (is_null($result)) {
			if (
					$id
				&&
					!is_null(
						rm_session_customer()->getCustomer()
					)
			) {
				$notify_me =
					df_h()->forum()->notification()->getIsNotified(
						$id
						,rm_session_customer()->getCustomer()->getId()
					)
				;
				$result = $notify_me;
			}
			else {
				$result = false;
			}
		}
		return $result;
	}

	/** @return bool */
	public function getUseNick() {
		return
			df_a(
				Df_Forum_Model_Session::s()->getPostForumData()
				,'use_nick'
				,
					$this->getModelPost()->getUserNick() != ''
				&&
					$this->getModelPost()->getUserNick()
			)
		;
	}

	/** @return mixed */
	public function getAllowNotification() {
		return Mage::getStoreConfig('df_forum/notification/customer__allow_subscription');
	}

	/** @return int|mixed */
	public function getParentId() {
		return
			!is_null(Df_Forum_Model_Action::s()->getCurrentTopic())
			? Df_Forum_Model_Action::s()->getCurrentTopic()->getParentId()
			: $this->getRequest()->getParam(Df_Forum_Model_Topic::P__PARENT_ID)
		;
	}

	/** @return bool */
	public function isOnProduct() {return !!Mage::registry('current_product');}

	/** @return int|null */
	public function getProductId() {
		return Mage::registry('current_product') ? Mage::registry('current_product')->getId() : null;
	}

	/** @return array[] */
	public function getForums() {
		return
			df_h()->forum()->topic()
				->getOptionsTopics(
					$status_all = false
					,$skip_id = 0
					,$where = array('1' => 'is_category=?')
					,$no_text = 'нет раздела'
					,$skip_parent = false
					,$ret_as_it = false
					,$use_store = true
					,$storeId = Mage::app()->getStore()->getId()
					,$no_empty = true
				)
		;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Edit
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$root = $this->getLayout()->getBlock('root');
		$root->setTemplate(df_h()->forum()->getLayout());
		$this->setModel(Df_Forum_Model_Action::s()->getCurrentTopic());
		$this->setParentModel(Mage::registry('current_object_parent'));
		$this->setCustomer(rm_session_customer()->getCustomer());
		return $this;
	}

	/** @return string */
	protected function getHeadHtml() {
		return $this->getChildHtml('head');
	}

	/** @return string */
	protected function getHeadProductHtml() {
		return $this->getChildHtml('headproduct');
	}

	/** @return bool */
	protected function getNotAllowIcons() {
		return
			$this->getModel()->getId()
			&& $this->getModel()->getSystemUserId() != $this->getCustomerId()
			&& !df_h()->forum()->topic()->isModerator()
		;
	}

	/** @return mixed */
	protected function getRedirectUrl() {
		return Mage::registry('redirect');
	}

	/** @return string */
	protected function getTitle() {
		/** @var string $result */
		$result = '';
		if ($this->getModel()->getId()) {
			if (
				Df_Forum_Model_Action::s()->getCurrentTopic()->getSystemUserId()
				===
					rm_session_customer()->getCustomer()->getId()
				&&
					Mage::registry('current_object_post')->getSystemUserId()
			) {
				$result =
					rm_sprintf(
						'Тема «%s»: ответ/редактирование. %s.'
						,$this->getModel()->getTitle()
						,$this->getProductTitle()
					)
				;
			}
			else if (
					Mage::registry('current_object_post')->getSystemUserId()
				===
					rm_session_customer()->getCustomer()->getId()
			) {
				$result =
					rm_sprintf(
						'Редактирование сообщения в теме «%s»'
						,$this->getModel()->getTitle()
					) . $this->getProductTitle()
				;
			}
			else {
				$result = rm_concat_clean(' '
					,rm_sprintf('Вы отвечаете в теме «%s»', $this->getModel()->getTitle())
					,$this->getProductTitle()
				);
			}
		}
		else {
			$result = rm_concat_clean(' ', 'Добавить тему.', $this->getProductTitle());
		}
		return $result;
	}

	/** @return string */
	protected function getSaveUrl() {
		return
			$this->getModel()->getId()
			? $this->getUrl('*/*/save/id/' . $this->getModel()->getId())
			: $this->getUrl('*/*/save')
		;
	}

	/** @return string */
	protected function titleReadOnly() {
		return
			$this->getModel()->getId()
			&& $this->getModel()->getSystemUserId() != $this->getCustomerId()
			&& !df_h()->forum()->topic()->isModerator()
			? ' readonly=1 '
			: ''
		;
	}

	/** @return int */
	protected function getCustomerId() {return rm_session_customer()->getCustomer()->getId();}

	/** @return string */
	protected function getSubmitTitle() {return 'опубликовать';}

	/**
	 * @param string $paramName
	 * @param mixed $paramValue
	 * @return string|null
	 */
	private function getPathPart($paramName, $paramValue) {
		return !$paramValue ? '' : df_concat_url($paramName, $paramValue);
	}

	/** @return string */
	private function getProductTitle() {
		return
			Mage::registry('current_product')
			? rm_sprintf('Товар: «%s»', Mage::registry('current_product')->getName())
			: ''
		;
	}

	const _CLASS = __CLASS__;
	protected $_model;
	protected $_customer;
}