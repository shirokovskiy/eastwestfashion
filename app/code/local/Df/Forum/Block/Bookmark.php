<?php
class Df_Forum_Block_Bookmark extends Df_Core_Block_Template {
	/** @return string */
	public function getBackUrl() {
		/** @var string $result */
		$result =
			(Mage::registry('redirect') && (false != Mage::registry('redirect')))
			? Mage::registry('redirect')
			: $this->getUrl('df_forum/topic')
		;
		return $result;
	}

	/** @return string */
	public function getHeadHtml() {return $this->getChildHtml('head');}

	/** @return array(int => Df_Forum_Model_Topic) */
	public function initCollection() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = array();
			$post = $this->getRequest()->getPost();
			if (
					!empty($post['bookmark_forum']['_id'])
				&&
					is_array($post['bookmark_forum']['_id'])
			) {
				foreach ($post['bookmark_forum']['_id'] as $id) {
					/** @var Df_Forum_Model_Topic $topic */
					$topic = Df_Forum_Model_Topic::i()->load($id);
					if ($topic->getId() && $topic->getStatus()) {
						$this->{__METHOD__}[$id] = $topic;
						if (!$topic->isProgenitor()) {
							if ($topic->getProgenitor()->getStatus()) {
								$topic->setData('parent', $topic->getProgenitor());
							}
						}
					}
				}
			}
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	public function getTop() {return $this->getChildHtml('forum_top');}

	/**
	 * @param $id
	 * @param Varien_Object|bool $obj [optional]
	 * @param int $page [optional]
	 * @return string
	 */
	public function getViewUrl($id, $obj = false, $page = 1) {
		return
			$obj && $obj->getUrlText()
			? $this->_getUrlrewrited(array(self::PAGE_VAR_NAME => $page), $obj->getUrlText())
			: $this->_getUrl(array(self::PAGE_VAR_NAME => $page), '/view/id/' . $id)
		;
	}


	/**
	 * @override
	 * @return Df_Forum_Block_Bookmark
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->initCollection();
		$root = $this->getLayout()->getBlock('root');
		$root->setTemplate(df_h()->forum()->getLayout());
		df_h()->forum()->topic()->breadCrumbBlock = 'Закладки';
		return $this;
	}

	/** @return array|bool */
	protected function getAllBookmarks() {return $this->initCollection();}

	/** @return string */
	protected function getTitleContent() {return 'Ваши закладки';}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function _getUrlrewrited($params, $urlAddon = '') {
		return
			$this->getUrl(
				$urlAddon
				,array(
					'_current' => false
					,'_escape' => false
					,'_use_rewrite' => false
					,'_query' => $params
				)
			)
		;
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function _getUrl($params, $urlAddon = '') {
		return
			$this->getUrl(
				'*/*' . $urlAddon
				,array(
					'_current' => true
					,'_escape' => true
					,'_use_rewrite' => true
					,'_query' => $params
				)
			)
		;
	}
	const _CLASS = __CLASS__;
	const LIMIT_VAR_NAME = 'limit';
	const PAGE_VAR_NAME = 'p';
}