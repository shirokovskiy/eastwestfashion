<?php
class Df_Forum_Block_Forum extends Df_Core_Block_Template {
	/**
	 * @override
	 * @return Df_Forum_Block_Forum
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->limits = df_h()->forum()->topic()->getPagesLimits();
		/** @var Mage_Core_Block_Template $root */
		$root = $this->getLayout()->getBlock('root');
		$root->setTemplate(df_h()->forum()->getLayout());
		return $this;
	}

	/** @return bool|Df_Forum_Model_Resource_Topic_Collection */
	protected function getAllForums() {
		return $this->initCollection();
	}

	/**
	 * @param $date
	 * @return string
	 */
	protected function getDateFormated($date) {
		return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
	}

	/** @return string */
	protected function getTitleContent() {
		return 'Все форумы';
	}

	/** @return string */
	protected function getToolbarHtml() {
		return $this->getChildHtml('toolbar');
	}

	/** @return string */
	protected function getHeadHtml() {
		return $this->getChildHtml('head');
	}

	/** @return string */
	protected function getControls() {
		return $this->getChildHtml('controls');
	}

	/** @return string */
	public function getTop() {
		return $this->getChildHtml('forum_top');
	}

	/** @return string|null */
	public function getBreadCrumbs() {
		return
			df_cfg()->forum()->general()->showBreadcrumbs()
			? $this->getChildHtml('util_breadcrumbs')
			: null
		;
	}

	/** @return bool|Df_Forum_Model_Resource_Topic_Collection */
	public function initCollection() {
		if (!$this->_objectsCollection) {
			$this->_objectsCollection = Df_Forum_Model_Topic::c();
			$this->_objectsCollection
				->setPageSize($this->getLimit())
				->setCurPage($this->getCurPage())
			;
			if (
					Df_Forum_Model_Config_Source_Forum_OrderingType::VALUE__BY_WEIGHTS
				===
					df_cfg()->forum()->general()->getOrderingType()
			) {
				$this->_objectsCollection->setOrder('priority', 'asc');
			}
			else {
				$this->_objectsCollection->setOrder('created_time', 'desc');
			}
			$this->_objectsCollection
				->getSelect()
					->where('? = status', 1)
					->where('? = is_category', 1)
			;
			$this->_objectsCollection->addStoreFilter(Mage::app()->getStore()->getId());
			$this->setAdditionalData();
		}
		return $this->_objectsCollection;
	}

	/** @return int|null */
	public function getLimit() {
		/** @var int $limit */
		$limit = intval($this->getRequest()->getParam($this->getLimitVarName()));
		return
			(0 !== $limit) && in_array($limit, $this->limits)
			? $this->_setLimit($limit)
			: (
				!is_null(Df_Forum_Model_Session::s()->getPageForumLimit())
				? Df_Forum_Model_Session::s()->getPageForumLimit()
				: $this->limits[0]
			)
		;
	}

	/** @return int */
	public function getCurPage() {
		/** @var int $page */
		$page = intval($this->getRequest()->getParam(self::PAGE_VAR_NAME));
		return
			(0 !== $page)
			? $this->_setCurPage($page)
			: (
				!is_null(Df_Forum_Model_Session::s()->getPageForumCurrent())
				? Df_Forum_Model_Session::s()->getPageForumCurrent()
				: 1
			)
		;
	}

	/** @return string */
	public function getPageVarName() {
		return self::PAGE_VAR_NAME;
	}

	/** @return string */
	public function getLimitVarName() {
		return self::LIMIT_VAR_NAME;
	}

	/**
	 * @param $id
	 * @param Df_Forum_Model_Forum|Df_Forum_Model_Topic $topic [optional]
	 * @return string
	 */
	public function getViewUrl($id, Df_Forum_Model_Topic $topic = null) {
		return
			is_null($topic) || !$topic->getUrlText()
			? $this->_getUrl(array(self::PAGE_VAR_NAME => 1), '/view/id/' . $id)
			: $this->_getUrlrewrited(
				array(self::PAGE_VAR_NAME => 1)
				,$topic->getUrlText()
				,$topic->getStoreId()
			)
		;
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function getViewUrlLatest($id) {
		return $this->getUrl('df_forum/topic/viewreply', array('id' => $id));
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @param bool $storeId
	 * @return string
	 */
	private function _getUrlrewrited($params, $urlAddon = '', $storeId = false) {
		return
			$this->getUrl(
				$urlAddon
				,array(
					'_current' => false
					,'_escape' => false
					,'_query' => $params
				)
			)
		;
	}

	/**
	 * @param $params
	 * @param $urlAddon
	 * @return string
	 */
	private function _getUrl($params, $urlAddon = '') {
		return
			$this->getUrl(
				'*/*' . $urlAddon
				,array(
					'_current' => false
					,'_escape' => false
					,'_use_rewrite' => false
					,'_query' => $params
				)
			)
		;
	}

	/**
	 * @param int $page
	 * @return int
	 */
	private function _setCurPage($page = 1) {
		Df_Forum_Model_Session::s()->setPageForumCurrent($page);
		return $page;
	}

	/**
	 * @param $limit
	 * @return mixed
	 */
	private function _setLimit($limit) {
		Df_Forum_Model_Session::s()->setPageForumLimit($limit);
		return $limit;
	}

	/**
	 *
	 */
	private function _setPostsQuantity() {
		foreach ($this->_objectsCollection as $key => $val) {
			$this->_objectsCollection
				->getItemById($key)->setTotalPosts(
					df_h()->forum()->topic()->getPostsQuantity($val, 0, $val)
				)
			;
		}
	}

	/**
	 *
	 */
	private function _setTopicsQuantity() {
		foreach ($this->_objectsCollection as $key => $val) {
			$this->_objectsCollection->getItemById($key)
				->setTotalTopics(df_h()->forum()->topic()->getTopicsQuantity($val->getId()))
			;
		}
	}

	/** @return Df_Forum_Block_Forum */
	private function _setLastPostInfo() {
		foreach ($this->_objectsCollection as $key => $val) {
			/** @var int $key */
			/** @var Df_Forum_Model_Topic $val */
			/** @var Df_Forum_Model_Post[] $obj */
			$obj = df_h()->forum()->topic()->latestPosts;
			/** @var Df_Forum_Model_Post $post */
			$post = df_a($obj, $val->getId());
			if (!empty($obj[$val->getId()])) {
				/** @var Df_Forum_Model_Topic $topic */
				$topic = $this->_objectsCollection->getItemById($key);
				$topic->setLatestTopic($post['topic_obj']);
				$topic->setLatestCreatedTime($post['created_time']);
				$topic
					->setLatestUserName(
						$this->getUserNick(
							$post[Df_Forum_Model_Post::P__SYSTEM_USER_ID]
							,$obj[$val->getId()]
						)
					)
				;
				$topic->setLatestUserId($post[Df_Forum_Model_Post::P__SYSTEM_USER_ID]);
				$topic->setLatestPostId($post['post_id']);
			}
		}
		return $this;
	}

	/**
	 * @param $user_id
	 * @param bool $obj
	 * @return mixed
	 */
	public function getUserNick($user_id, $obj = false) {
		/** @var string $result */
		$result = null;
		$user_id = $user_id != 0 ? $user_id : Df_Forum_Model_Action::ADMIN__ID;
		$profile = $this->_loadUserSettings($user_id);
		if ($profile->getNickname()) {
			$result = $profile->getNickname();
		}
		else {
			if ($obj) {
				if (!empty($obj['user_nick']) || !empty($obj['user_name'])) {
					$result =
						($obj['user_nick'] != ''  && $obj['user_nick']
						? $obj['user_nick']
						: $obj['user_name'])
					;
				}
				else if (is_object($obj)) {
					$result = $obj->getUserNick()	? $obj->getUserNick() : $obj->getUserName();
				}
			}
		}
		return $result;
	}

	/**
	 * @param $user_id
	 * @return mixed
	 */
	private function _loadUserSettings($user_id) {
		$result = null;
		if (!empty($this->user_profiles[$user_id])) {
			$result = $this->user_profiles[$user_id];
		}
		else {
			$this->user_profiles[$user_id] =
				Df_Forum_Model_Usersettings::i()->load(
					$user_id
					,Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
				)
			;
			$result = $this->user_profiles[$user_id];
		}
		return $result;
	}

	/**
	 *
	 */
	private function setAdditionalData() {
		$this->_setPostsQuantity();
		$this->_setTopicsQuantity();
		$this->_setLastPostInfo();
	}

	const _CLASS = __CLASS__;
	/** @var int[] */
	public $limits = array(5,10,15);
	const PAGE_VAR_NAME = 'p';
	const LIMIT_VAR_NAME = 'limit';
	private $user_profiles = array();
	/** @var Df_Forum_Model_Resource_Topic_Collection|bool */
	protected $_objectsCollection = false;
}