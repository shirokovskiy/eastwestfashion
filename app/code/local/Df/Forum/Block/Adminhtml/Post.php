<?php
class Df_Forum_Block_Adminhtml_Post extends Mage_Adminhtml_Block_Widget_Grid_Container {
	/**
	 * Перекрывать надо именно конструктор, а не метод _construct,
	 * потому что родительский класс пихает инициализацию именно в конструктор.
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Post
	 */
	public function __construct() {
		parent::__construct();
		$this->_controller = 'adminhtml_post';
		$this->_blockGroup = 'df_forum';
		$this->_headerText = 'Сообщения';
		$this->_addButtonLabel = 'Добавить сообщение';
		$this->setTemplate('df/forum/posts.phtml');
	}

	/** @return string */
	public function getAddNewButtonHtml() {
		return $this->getChildHtml('add_new_button');
	}

	/** @return int */
	public function getStoreId() {
		return Df_Forum_Model_Registry::s()->getStoreId();
	}

	/** @return bool */
	public function isSingleStoreMode() {
		return Mage::app()->isSingleStoreMode();
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Post
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->setChild(
			'add_new_button'
			,Df_Adminhtml_Block_Widget_Button::i(
				'Добавить ответ'
				, 'add'
				, $this->getUrl('*/*/new', array('store' => $this->getStoreId()))
			)
		);
		return $this;
	}
}