<?php
class Df_Forum_Block_Adminhtml_Forum extends Mage_Adminhtml_Block_Widget_Grid_Container {
	/**
	 * Перекрывать надо именно конструктор, а не метод _construct,
	 * потому что родительский класс пихает инициализацию именно в конструктор.
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Forum
	 */
	public function __construct() {
		parent::__construct();
		$this->_controller = 'adminhtml_forum';
		$this->_blockGroup = 'df_forum';
		$this->_headerText = 'Разделы';
		$this->_addButtonLabel = 'Добавить новый раздел';
		$this->setTemplate('df/forum/forums.phtml');
	}

	/** @return string */
	public function getAddNewButtonHtml() {
		return $this->getChildHtml('add_new_button');
	}

	/** @return bool */
	public function isSingleStoreMode() {
		return Mage::app()->isSingleStoreMode();
	}

	/** @return int */
	public function getStoreId() {
		return Df_Forum_Model_Registry::s()->getStoreId();
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Forum
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->setChild(
			'add_new_button'
			,Df_Adminhtml_Block_Widget_Button::i(
				'Добавить раздел'
				, 'add'
				, $this->getUrl('*/*/new', array('store' => $this->getStoreId()))
			)
		);
		return $this;
	}
}