<?php
class Df_Forum_Block_Adminhtml_Admsettings extends Mage_Adminhtml_Block_Widget_Grid_Container {
	/**
	 * Перекрывать надо именно конструктор, а не метод _construct,
	 * потому что родительский класс пихает инициализацию именно в конструктор.
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Admsettings
	 */
	public function __construct() {
		parent::__construct();
		$this->_controller = 'adminhtml_forum';
		$this->_blockGroup = 'df_forum';
		$this->_headerText = 'Витринные настройки администратора';
	}

	/** @return string */
	public function getAvatarBlock() {
		return self::AVATAR_ID_BLOCK;
	}

	/** @return string */
	public function getAvatarUrl() {
		return Df_Forum_Model_Registry::s()->getAdminSettings()->getAvatarUrl();
	}

	/** @return string */
	public function getDelAvatarUrl() {
		return $this->getUrl('*/*/delavatar');
	}

	/** @return bool */
	public function isAvatarExist() {
		/** @var Df_Forum_Model_Usersettings $model */
		$model = Df_Forum_Model_Registry::s()->getAdminSettings();
		$result =
				$model->getAvatarName()
			&&
				file_exists(
					df_concat_path(
						Mage::getBaseDir()
						, df_cfg()->forum()->avatar()->getPath()
						, $model->getAvatarName()
					)
				)
		;
		return $result;
	}

	const AVATAR_ID_BLOCK = '_dummy';
	const _CLASS = __CLASS__;
}