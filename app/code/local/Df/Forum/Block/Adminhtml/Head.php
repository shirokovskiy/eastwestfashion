<?php
class Df_Forum_Block_Adminhtml_Head extends Df_Core_Block_Admin {
	/** @return string */
	public function getLangDef() {
		return df_h()->forum()->topic()->getAvLocale(Mage::app()->getLocale()->getDefaultLocale());
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Head
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->getLayout()->getBlock('head')
			->setDataUsingMethod(
				Df_Page_Block_Html_Head::P__CAN_LOAD_TINY_MCE
				,true
			)
		;
		return $this;
	}

	const _CLASS = __CLASS__;
}