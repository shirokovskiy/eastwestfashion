<?php
class Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Icons extends Mage_Adminhtml_Block_Widget_Form {
	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Icons
	 */
	protected function _prepareForm() {
		parent::_prepareForm();
		/** @var Df_Varien_Data_Form $form */
		$form = new Df_Varien_Data_Form();
		$this->setForm($form);
		$fieldset =
			$form->addFieldset(
				'post_form'
				,array(
					'legend' => 'Пиктограммы'
					,'class' => 'fieldset-wide'
				)
			)
		;
		/**
		 * Обратите внимание,
		 * что нельзя применять цепной вызов $fieldset->addField()->addField(),
		 * потому что addField() возвращает не $fieldset, а созданное поле.
		 */
		$fieldset
			->addField(
				Df_Forum_Model_Topic::P__ICON_ID
				,'radios'
				,array(
					'label' => 'Пиктограммы'
					,'name' => Df_Forum_Model_Topic::P__ICON_ID
					,'values' => df_h()->forum()->getForumIconsRadios()
					,'separator' => '<br>'
				)
			)
		;
		if (Df_Forum_Model_Registry::s()->getForum()) {
			$form->setValues(Df_Forum_Model_Registry::s()->getForum()->getData());
		}
		else if (rm_session()->hasData('post_data')) {
			$form->setValues(rm_session()->getData('post_data'));
			rm_session()->unsetData('post_data');
		}
		return $this;
	}

	/** @return Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Icons */
	public static function i() {return df_block(__CLASS__);}
}