<?php
class Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {
	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Form
	 */
	protected function _prepareForm() {
		parent::_prepareForm();
		$form = new Df_Varien_Data_Form();
		$this->setForm($form);
		$fieldset =
			$form
				->addFieldset(
					'post_form'
					,array(
						'legend' => 'Параметры раздела'
					)
				)
		;
		/**
		 * Обратите внимание,
		 * что нельзя применять цепной вызов $fieldset->addField()->addField(),
		 * потому что addField() возвращает не $fieldset, а созданное поле.
		 */
		$fieldset
			->addField(
				'title'
				,'text'
				,array(
					'label' => 'Раздел'
					,'class' => 'required-entry'
					,'required' => true
					,'name' => 'title'
				)
			)
		;
		$fieldset
			->addField(
				'priority'
				,'text'
				,array(
					'label' => 'Вес при упорядочивании'
					,'name' => 'priority'
				)
			)
		;
		$fieldset
			->addField(
				Df_Forum_Model_Topic::P__URL_TEXT_SHORT
				,'text'
				,array(
					'name' => Df_Forum_Model_Topic::P__URL_TEXT_SHORT
					,'label' => 'адрес'
					,'class' => 'rm.validate.urlKey'
				)
			)
		;
		$array_stores =
			array(
				'name' => 'store_id'
				,'label' => 'Витрина'
				,'values' => $this->getSelectWebsitesStores()
			)
		;
		if (
				!Df_Forum_Model_Registry::s()->getForum()->getId()
			&&
				(0 !== Df_Forum_Model_Registry::s()->getStoreId())
		) {
			Df_Forum_Model_Registry::s()->getForum()
				->setStoreId(
					Df_Forum_Model_Registry::s()->getStoreId()
				)
			;
		}
		$fieldset->addField('store_id','select', $array_stores);
		$fieldset
			->addField(
				Df_Forum_Model_Post::P__STATUS
				,'select'
				,array(
					'label' => 'опубликован?'
					,'name' => Df_Forum_Model_Post::P__STATUS
					,'values' => Df_Admin_Model_Config_Source_YesNo::s()->toOptionArray()
				)
			)
		;
		$fieldset
			->addField(
				'description'
				,'editor'
				,array(
					'name' => 'description'
					,'label' => 'Описание'
				)
			)
		;
		if (Df_Forum_Model_Registry::s()->getForum()) {
			$form->setValues(Df_Forum_Model_Registry::s()->getForum()->getData());
		}
		else if (rm_session()->hasData('post_data')) {
			$form->setValues(rm_session()->getData('post_data'));
			rm_session()->unsetData('post_data');
		}
		return $this;
	}

	/** @return array */
	private function getSelectWebsitesStores() {
		/** @var Df_Core_Model_Resource_Website_Collection $websites */
		$websites = Df_Core_Model_Resource_Website_Collection::i();
		/** @var Df_Core_Model_Resource_Store_Group_Collection $allgroups */
		$allgroups = Df_Core_Model_Resource_Store_Group_Collection::i();
		/** @var array $result */
		$result =
			array(
				array(
					'label' => 'все витрины'
					,'value' => 0
				)
			)
		;
		foreach ($websites as $website) {
			/** @var Mage_Core_Model_Website $website */
			$values = array();
			foreach ($allgroups as $group) {
				/** @var Mage_Core_Model_Store_Group $group */
				if ($group->getWebsiteId() == $website->getId()) {
					$values[]=
						array(
							'label' => '&nbsp;&nbsp;' . $group->getName()
							,'value' => $this->getStoreViews($group)
						)
					;
				}
			}
			$result[]=
				array(
					'label' => $website->getName()
					,'value' => $values
				)
			;
		}
		return $result;
	}

	/**
	 * @param Mage_Core_Model_Store_Group $group
	 * @return array[]
	 */
	private function getStoreViews(Mage_Core_Model_Store_Group $group) {
		/** @var Mage_Core_Model_Resource_Store_Collection $stores */
		$stores = $group->getStoreCollection();
		/** @var array[] $result */
		$result = array();
		if ($stores->getSize()) {
			foreach ($stores as $store) {
				/** @var Mage_Core_Model_Store $store */
				$result[]= array(
					'label' => $store->getName()
					,'value' => $store->getId()
					,'style' => 'margin-left:15px;'
				);
			}
		}
		return $result;
	}

	/** @return Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Form */
	public static function i() {return df_block(__CLASS__);}
}