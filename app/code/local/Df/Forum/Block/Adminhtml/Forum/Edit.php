<?php
class Df_Forum_Block_Adminhtml_Forum_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {
	/**
	 * Перекрывать надо именно конструктор, а не метод _construct,
	 * потому что родительский класс пихает инициализацию именно в конструктор.
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Forum_Edit
	 */
	public function __construct() {
		parent::__construct();
		$this->_objectId = Df_Forum_Model_Topic::P__ID;
		$this->_blockGroup = 'df_forum';
		$this->_controller = 'adminhtml_forum';
		$this
			->_updateButton(
				'save'
				,'label'
				,'сохранить раздел'
			)
		;
		$this
			->_updateButton(
				'delete'
				,'label'
				,'удалить раздел'
			)
		;
		$this
			->_addButton(
				'saveandcontinue'
				,array(
					'label' => 'сохранить и остаться'
					,'onclick' => 'saveAndContinueEdit()'
					,'class' => 'save'
				)
				,-100
			)
		;
		$this->_formScripts[]
			= "
				function saveAndContinueEdit(){
					editForm.submit($('edit_form').action+'back/edit/');
				}
			"
		;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getBackUrl() {
		return $this->getUrl('*/*/', array('store' => $this->getRequest()->getParam('store', 0)));
	}

	/**
	 * @override
	 * @return string
	 */
	public function getHeaderText() {
		return
				(
						Df_Forum_Model_Registry::s()->getForum()
					&&
						Df_Forum_Model_Registry::s()->getForum()->getId()
				)
			? rm_sprintf(
				'Редактировать раздел «%s»'
				,df_text()->escapeHtml(Df_Forum_Model_Registry::s()->getForum()->getTitle())
			)
			: 'Добавить раздел'
		;
	}

	/** @return Df_Forum_Block_Adminhtml_Forum_Edit */
	public static function i() {return df_block(__CLASS__);}
}