<?php
class Df_Forum_Block_Adminhtml_Forum_Grid extends Df_Adminhtml_Block_Widget_Grid {
	/**
	 * @override
	 * @param Mage_Catalog_Model_Product|Varien_Object $row
	 * @return string
	 */
	public function getRowUrl($row) {
		return $this->getUrl('*/*/edit', array('id' => $row->getId(), 'store' => $this->getStoreId()));
	}

	/** @return int */
	public function getStoreId() {
		return Df_Forum_Model_Registry::s()->getStoreId();
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->setId('forumForumGrid');
		$this->setDefaultSort(Df_Forum_Model_Topic::P__ID);
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Forum_Grid
	 */
	protected function _prepareCollection() {
		/** @var Df_Forum_Model_Resource_Forum_Collection $collection */
		$collection = Df_Forum_Model_Forum::c();
		$collection->getSelect()->where('? = is_category', 1);
		$storeId = Df_Forum_Model_Registry::s()->getStoreId();
		if (0 !== $storeId) {
			$collection->addStoreFilter($storeId);
		}
		$this->setCollection($collection);
		parent::_prepareCollection();
		return $this;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Forum_Grid
	 */
	protected function _prepareColumns() {
		$this
			->addColumn(
				Df_Forum_Model_Topic::P__ID
				,array(
					'header' => '№'
					,'align' => 'right'
					,'width' => '50px'
					,'index' => Df_Forum_Model_Topic::P__ID
				)
			)
			->addColumn(
				'title'
				,array(
					'header' => 'заголовок'
					,'align' => 'left'
					,'index' => 'title'
				)
			)
			->addColumn(
				'url_text'
				,array(
					'header' => 'адрес'
					,'align' => 'left'
					,'width' => '140px'
					,'index' => 'url_text'
				)
			)
			->addColumn(
				'created_time'
				,array(
					'header' => 'дата создания'
					,'align' => 'left'
					,'width' => '140px'
					,'type' => 'datetime'
					,'default' => '--','index' => 'created_time'
				)
			)
			->addColumn(
				'update_time'
				,array(
					'header' => 'дата обновления'
					,'align' => 'left'
					,'width' => '140px'
					,'type' => 'datetime'
					,'default' => '--'
					,'index' => 'update_time'
				)
			)
			->addColumn(
				Df_Forum_Model_Topic::P__STATUS
				,array(
					'header' => 'опубликован?'
					,'align' => 'left'
					,'width' => '80px'
					,'index' => Df_Forum_Model_Topic::P__STATUS
					,'type' => 'options'
					,'options' => Df_Admin_Model_Config_Source_YesNo::s()->toOptionArrayAssoc()
				)
			)
			->addColumn(
				'action'
				,array(
					'header' => 'команда'
					,'width' => '50px'
					,'type' => 'action'
					,'getter' => 'getId'
					,'actions' =>
						array(
							array(
								'caption' => 'изменить'
								,'url' => array(
									'base' => '*/*/edit','params' => array(
										'store' => $this->getRequest()->getParam('store')
									)
								),'field' => 'id'
							)
						)
						,'filter' => false
						,'sortable' => false
						,'index' => 'stores'
				)
			)
		;
		parent::_prepareColumns();
		return $this;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Forum_Grid
	 */
	protected function _prepareMassaction() {
		parent::_prepareMassaction();
		$this->setMassactionIdField('entity_id');
		$this->getMassactionBlock()
			->setDataUsingMethod(
				Df_Adminhtml_Block_Widget_Grid_Massaction::P__FORM_FIELD_NAME
				,
				'forum'
			)
		;
		$this->getMassactionBlock()
			->addItem(
				'delete'
				,array(
					'label' => 'удалить'
					,'url' => $this->getUrl('*/*/massDelete'
					,array(
						'store' => Df_Forum_Model_Registry::s()->getStoreId())
					)
					,'confirm' => 'Удалить?'
				)
			)
		;
		$this->getMassactionBlock()
			->addItem(
				Df_Forum_Model_Topic::P__STATUS
				,array(
					'label' => 'скрыть / опубликовать'
					,'url' =>
						$this->getUrl(
							'*/*/massStatus'
							,array(
								'_current' => true
								,'store' => Df_Forum_Model_Registry::s()->getStoreId()
							)
						)
					,'additional' =>
						array(
							'visibility' =>
								array(
									'name' => Df_Forum_Model_Topic::P__STATUS
									,'type' => 'select'
									,'class' => 'required-entry'
									,'label' => 'опубликован?'
									,'values' =>
										Df_Admin_Model_Config_Source_YesNo::s()->toOptionArrayWithEmpty()
								)
						)
				)
			)
		;
		return $this;
	}

	const _CLASS = __CLASS__;
}