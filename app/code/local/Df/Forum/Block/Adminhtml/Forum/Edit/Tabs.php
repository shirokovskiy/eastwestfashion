<?php
class Df_Forum_Block_Adminhtml_Forum_Edit_Tabs extends Df_Adminhtml_Block_Widget_Tabs {
	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Forum_Edit_Tabs
	 */
	protected function _beforeToHtml() {
		$this
			->addTab(
				'form_section'
				,array(
					'label' => 'Параметры раздела'
					,'title' => 'Параметры раздела'
					,'content' => Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Form::i()->toHtml()
				)
			)
		;
		$this
			->addTab(
				'form_section_meta'
				,array(
					'label' => 'Метаданные'
					,'title' => 'Метаданные'
					,'content' => Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Formmeta::i()->toHtml()
				)
			)
		;
		$this
			->addTab(
				'form_section_icons'
				,array(
					'label' => 'Пиктрограммы'
					,'title' => 'Пиктрограммы'
					,'content' => Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Icons::i()->toHtml()
				)
			)
		;
		parent::_beforeToHtml();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->setId('forum_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle('Параметры раздела');
	}

	/** @return Df_Forum_Block_Adminhtml_Forum_Edit_Tabs */
	public static function i() {return df_block(__CLASS__);}
}