<?php
class Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Formmeta extends Mage_Adminhtml_Block_Widget_Form {
	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Formmeta
	 */
	protected function _prepareForm() {
		parent::_prepareForm();
		/** @var Df_Varien_Data_Form $form */
		$form = new Df_Varien_Data_Form();
		$this->setForm($form);
		$fieldset =
			$form
				->addFieldset(
					'post_form'
					,array(
						'legend' => 'Метаданные'
						,'class' => 'fieldset-wide'
					)
				)
		;
		/**
		 * Обратите внимание,
		 * что нельзя применять цепной вызов $fieldset->addField()->addField(),
		 * потому что addField() возвращает не $fieldset, а созданное поле.
		 */
		$fieldset
			->addField(
					'meta_keywords'
					,'editor'
					,array(
						'name' => 'meta_keywords'
						,'label' => 'Ключевые слова'
						,'title' => 'Ключевые слова'
						,'style' => 'width:98%; height:90px;'
					)
			)
		;
		$fieldset
			->addField(
				Df_Forum_Model_Topic::P__META_DESCRIPTION
				,'editor'
				,array(
					'name' => Df_Forum_Model_Topic::P__META_DESCRIPTION
					,'label' => 'Метаописание'
					,'title' => 'Метаописание'
					,'style' => 'width:98%; height:90px;'
				)
			)
		;
		if (Df_Forum_Model_Registry::s()->getForum()) {
			$form->setValues(Df_Forum_Model_Registry::s()->getForum()->getData());
		}
		else if (rm_session()->hasData('post_data')) {
			$form->setValues(rm_session()->getData('post_data'));
			rm_session()->unsetData('post_data');
		}
		return $this;
	}

	/** @return Df_Forum_Block_Adminhtml_Forum_Edit_Tab_Formmeta */
	public static function i() {return df_block(__CLASS__);}
}