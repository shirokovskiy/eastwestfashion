<?php
class Df_Forum_Block_Adminhtml_Admsettings_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {
	/**
	 * @override
	 * @return Mage_Adminhtml_Block_Widget_Form
	 */
	protected function _prepareForm() {
		parent::_prepareForm();
		/** @var Varien_Data_Form $form */
		$form = new Df_Varien_Data_Form();
		$this->setForm($form);
		$fieldset =
			$form
				->addFieldset(
					'post_form'
					,array(
						'legend' => 'Витринные настройки администратора'
					)
				)
		;
		/**
		 * Обратите внимание,
		 * что нельзя применять цепной вызов $fieldset->addField()->addField(),
		 * потому что addField() возвращает не $fieldset, а созданное поле.
		 */
		$fieldset
			->addField(
				'_dummy'
				,'text'
				,array(
					'label' => ''
					,'name' => '_dummy'
					,'id' => '_dummy'
				)
			)
		;
		$fieldset
			->addField(
				'nickname'
				,'text'
				,array(
					'label' => 'Имя на форуме'
					,'name' => 'nickname'
				)
			)
		;
		$fieldset
			->addField(
				'avatar'
				,'file'
				,array(
					'label' => 'Аватар'
					,'name' => 'avatar'
				)
			)
		;
		$fieldset
			->addField(
				'signature'
				,'textarea'
				,array(
					'label' => 'Подпись'
					,'name' => 'signature'
				)
			)
		;
		if (Df_Forum_Model_Registry::s()->getAdminSettings()) {
			$form->setValues(Df_Forum_Model_Registry::s()->getAdminSettings()->getData());
		}
		else if (rm_session()->hasData('post_data')) {
			$form->setValues(rm_session()->getData('post_data'));
			rm_session()->unsetData('post_data');
		}
		return $this;
	}

	/** @return Df_Forum_Block_Adminhtml_Admsettings_Edit_Tab_Form */
	public static function i() {return df_block(__CLASS__);}
}