<?php
class Df_Forum_Block_Adminhtml_Admsettings_Edit_Tabs extends Df_Adminhtml_Block_Widget_Tabs {
	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Admsettings_Edit_Tabs
	 */
	protected function _beforeToHtml() {
		$this
			->addTab(
				'form_section'
				,array(
					'label' => 'Витринные настройки администратора'
					,'title' => 'Витринные настройки администратора'
					,'content' => Df_Forum_Block_Adminhtml_Admsettings_Edit_Tab_Form::i()->toHtml()
				)
			)
		;
		parent::_beforeToHtml();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->setId('admsettings_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle('Витринные настройки администратора');
	}

	/** @return Df_Forum_Block_Adminhtml_Admsettings_Edit_Tabs */
	public static function i() {return df_block(__CLASS__);}
}