<?php
class Df_Forum_Block_Adminhtml_Admsettings_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {
	/**
	 * Перекрывать надо именно конструктор, а не метод _construct,
	 * потому что родительский класс пихает инициализацию именно в конструктор.
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Admsettings_Edit
	 */
	public function __construct() {
		parent::__construct();
		$this->_objectId = 'id';
		$this->_blockGroup = 'df_forum';
		$this->_controller = 'adminhtml_admsettings';
		$this
			->_updateButton(
				'save'
				,'label'
				,'Сохранить настройки'
			)
		;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getBackUrl() {
		return $this->getUrl('*/*/');
	}

	/**
	 * @override
	 * @return string
	 */
	public function getHeaderText() {
		return 'Витринные настройки администратора';
	}

	/** @return Df_Forum_Block_Adminhtml_Admsettings_Edit */
	public static function i() {return df_block(__CLASS__);}
}