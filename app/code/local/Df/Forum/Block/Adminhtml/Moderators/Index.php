<?php
class Df_Forum_Block_Adminhtml_Moderators_Index extends Mage_Adminhtml_Block_Widget_Container {
	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_controller = 'adminhtml_moderators';
	}

	/** @return Df_Forum_Block_Adminhtml_Moderators_Index */
	public static function i() {return df_block(__CLASS__);}
}