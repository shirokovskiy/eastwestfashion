<?php
class Df_Forum_Block_Adminhtml_Moderators_Grid extends Df_Adminhtml_Block_Widget_Grid {
	/**
	 * Перекрывать надо именно конструктор, а не метод _construct,
	 * потому что родительский класс пихает инициализацию именно в конструктор.
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Moderators_Grid
	 */
	public function __construct() {
		parent::__construct();
		$this->setId('moderatorGrid');
		$this->setDefaultSort('Moder_Id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
		$this->setUseAjax(true);
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Moderators_Grid
	 */
	protected function _prepareCollection() {
		/** @var Df_Customer_Model_Resource_Customer_Collection $collection */
		$collection = Df_Customer_Model_Resource_Customer_Collection::i();
		$collection
			->addNameToSelect()
			->addAttributeToSelect('email')
		;
		$collection->getSelect()
			->join(
				array(
					'table_moderators' =>
						$collection->getTable(
							Df_Forum_Model_Resource_Moderator::TABLE_NAME
						)
				)
				,'`e`.entity_id = table_moderators.system_user_id'
				,'table_moderators.moderator_id as Moder_Id'
			)
		;
		$this->setCollection($collection);
		parent::_prepareCollection();
		return $this;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Moderators_Grid
	 */
	protected function _prepareColumns() {
		$this
			->addColumn(
				'Moder_Id'
				,array(
					'header' => '№'
					,'align' => 'right'
					,'width' => '50px'
					,'index' => 'Moder_Id'
				)
			)
			->addColumn(
				'firstname'
				,array(
					'header' => 'Имя'
					,'align' => 'left','index' => 'firstname'
				)
			)
			->addColumn(
				'lastname'
				,array(
					'header' => 'Фамилия'
					,'align' => 'left'
					,'index' => 'lastname'
				)
			)
			->addColumn(
				'email'
				,array(
					'header' => 'Элктропочта'
					,'align' => 'left'
					,'index' => 'email'
				)
			)
		;
		if (!Mage::app()->isSingleStoreMode()) {
			$this
				->addColumn(
					'website_id'
					,array(
						'header' => 'Сайт'
						,'align' => 'center','width' => '180px'
						,'type' => 'options'
						,'options' =>
							df_mage()->adminhtml()->system()->storeSingleton()
								->getWebsiteOptionHash(true)
						,'index' => 'website_id'
					)
				)
			;
		}
		return parent::_prepareColumns();
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Moderators_Grid
	 */
	protected function _prepareMassaction() {
		parent::_prepareMassaction();
		$this->setMassactionIdField('entity_id');
		$this->getMassactionBlock()
			->setDataUsingMethod(
				Df_Adminhtml_Block_Widget_Grid_Massaction::P__FORM_FIELD_NAME
				,'moderators'
			)
		;
		$this
			->getMassactionBlock()
				->addItem(
					'delete'
					,array(
						'label' => 'удалить'
						,'url' => $this->getUrl('*/*/massDelete')
						,'confirm' => 'Удалить?'
					)
			)
		;
		return $this;
	}

	const _CLASS = __CLASS__;
}