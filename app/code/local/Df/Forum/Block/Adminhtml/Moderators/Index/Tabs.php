<?php
class Df_Forum_Block_Adminhtml_Moderators_Index_Tabs extends Df_Adminhtml_Block_Widget_Tabs {
	/** @return Df_Forum_Block_Adminhtml_Moderators_Index_Tabs */
	protected function _beforeToHtml() {
		$this
			->addTab(
				'moderators_section'
				,array(
					'label' => 'Модераторы'
					,'title' => 'Модераторы'
					,'content' => Df_Forum_Block_Adminhtml_Moderators_Index_Tab_Gridmoderators::i()->toHtml()
				)
			)
		;
		$this
			->addTab(
				'moderators_section_2'
				,array(
					'label' => 'Назначить покупателя модератором'
					,'title' => 'Назначить покупателя модератором'
					,'content' =>
						Df_Forum_Block_Adminhtml_Moderators_Index_Tab_Fromcustomermoder::i()->toHtml()
				)
			)
		;
		parent::_beforeToHtml();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->setId('moderators_tabs');
		$this->setDestElementId('content');
		$this->setTitle('Модераторы');
	}

	/** @return Df_Forum_Block_Adminhtml_Moderators_Index_Tabs */
	public static function i() {return df_block(__CLASS__);}
}