<?php
class Df_Forum_Block_Adminhtml_Moderators_Index_Tab_Fromcustomermoder
	extends Mage_Adminhtml_Block_Widget_Grid_Container {
	/**
	 * Перекрывать надо именно конструктор, а не метод _construct,
	 * потому что родительский класс пихает инициализацию именно в конструктор.
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Moderators_Index_Tab_Fromcustomermoder
	 */
	public function __construct() {
		parent::__construct();
		$this->_controller = 'adminhtml_customer';
		$this->_blockGroup = 'df_forum';
		$this->setTemplate('df/forum/customers.phtml');
	}

	/** @return Df_Forum_Block_Adminhtml_Moderators_Index_Tab_Fromcustomermoder */
	public static function i() {return df_block(__CLASS__);}
}