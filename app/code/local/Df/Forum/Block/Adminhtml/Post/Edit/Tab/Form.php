<?php
class Df_Forum_Block_Adminhtml_Post_Edit_Tab_Form
	extends Mage_Adminhtml_Block_Widget_Form
	implements Mage_Adminhtml_Block_Widget_Tab_Interface {
	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Post_Edit_Tab_Form
	 */
	protected function _prepareForm() {
		parent::_prepareForm();
		$form = new Df_Varien_Data_Form();
		$this->setForm($form);
		$fieldset =
			$form->addFieldset(
				'post_form'
				,array(
					'legend' => 'Параметры сообщения'
					,'class' => 'fieldset-wide'
				)
			)
		;
		/**
		 * Обратите внимание,
		 * что нельзя применять цепной вызов $fieldset->addField()->addField(),
		 * потому что addField() возвращает не $fieldset, а созданное поле.
		 */
		$fieldset
			->addField(
				Df_Forum_Model_Topic::P__PARENT_ID
				,'select'
				,array(
					'label' => 'Тема'
					,'name' => Df_Forum_Model_Topic::P__PARENT_ID
					,'values' =>
						df_h()->forum()->topic()
							->getOptionsTopics(
								$status_all = true
								,$skip_id = 0
								,$where = array()
								,$no_text = false
								,$skip_parent = false
								,$ret_as_it = false
								,$use_store = (0 < Df_Forum_Model_Registry::s()->getStoreId())
								,$storeId = Df_Forum_Model_Registry::s()->getStoreId()
							)
				)
			)
		;
		$fieldset
			->addField(
				Df_Forum_Model_Post::P__STATUS
				,'select'
				,array(
					'label' => 'опубликовано?'
					,'name' => Df_Forum_Model_Post::P__STATUS
					,'values' => Df_Admin_Model_Config_Source_YesNo::s()->toOptionArray()
				)
			)
		;
		$fieldset
			->addField(
				'is_sticky'
				,'select'
				,array(
					'label' => 'Отображать ли данное сообщение всегда выше других?'
					,'name' => 'is_sticky'
					,'values' => Df_Admin_Model_Config_Source_YesNo::s()->toOptionArray()
				)
			)
		;
		$fieldset
			->addField(
				'post'
				,'editor'
				,array(
					'name' => 'post'
					,'label' => 'Сообщение'
					,'title' => 'Сообщение'
					,'style' => 'width:98%; height:200px;'
				)
			)
		;
		if (Df_Forum_Model_Registry::s()->hasPost()) {
			$form->setValues(Df_Forum_Model_Registry::s()->getPost()->getData());
		}
		else if (rm_session()->hasData('post_data')) {
			$form->setValues(rm_session()->getData('post_data'));
			rm_session()->unsetData('post_data');
		}
		return $this;
	}

	/**
	 * @override
	 * @return bool
	 */
	public function canShowTab() {
		return true;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getTabLabel() {
		return 'Содержимое';
	}

	/**
	 * @override
	 * @return string
	 */
	public function getTabTitle() {
		return 'Содержимое';
	}

	/**
	 * @override
	 * @return bool
	 */
	public function isHidden() {
		return false;
	}

	/** @return Df_Forum_Block_Adminhtml_Post_Edit_Tab_Form */
	public static function i() {return df_block(__CLASS__);}
}