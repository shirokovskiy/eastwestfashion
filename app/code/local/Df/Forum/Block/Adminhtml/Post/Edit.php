<?php
class Df_Forum_Block_Adminhtml_Post_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {
	/**
	 * Перекрывать надо именно конструктор, а не метод _construct,
	 * потому что родительский класс пихает инициализацию именно в конструктор.
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Post_Edit
	 */
	public function __construct() {
		parent::__construct();
		$this->_objectId = 'id';
		$this->_blockGroup = 'df_forum';
		$this->_controller = 'adminhtml_post';
		$this
			->_updateButton(
				'save'
				,'label'
				,'сохранить сообщение'
			)
		;
		$this
			->_updateButton(
				'delete'
				,'label'
				,'удалить сообщение'
			)
		;
		$this
			->_addButton(
				'saveandcontinue'
				,array(
					'label' => 'сохранить и остаться'
					,'onclick' => 'saveAndContinueEdit()'
					,'class' => 'save'
				)
				,-100
			)
		;
		$this->_formScripts[]
			= "
				function saveAndContinueEdit(){
					editForm.submit($('edit_form').action+'back/edit/');
				}
			"
		;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getBackUrl() {
		return $this->getUrl('*/*/', array('store' => $this->getRequest()->getParam('store', 0)));
	}

	/**
	 * @override
	 * @return string
	 */
	public function getHeaderText() {
		return
			df_concat(
						Df_Forum_Model_Registry::s()->hasPost()
					&&
						Df_Forum_Model_Registry::s()->getPost()->getId()
				?
					rm_sprintf(
						'Редактирование сообщения №«%s»'
						,df_text()->escapeHtml(
							Df_Forum_Model_Registry::s()->getPost()->getId()
						)
					)
				: 'Добавить сообщение'
				,
				(0 === Df_Forum_Model_Registry::s()->getStoreId())
				? ''
				: df_concat(
					' '
					,'Магазин'
					,': '
					,df_text()->escapeHtml(
						Mage::app()->getStore(
							Df_Forum_Model_Registry::s()->getStoreId()
						)->getName()
					)
				)
			)
		;
	}

	/** @return Df_Forum_Block_Adminhtml_Post_Edit */
	public static function i() {return df_block(__CLASS__);}
}