<?php
class Df_Forum_Block_Adminhtml_Topic extends Mage_Adminhtml_Block_Widget_Grid_Container {
	/**
	 * Перекрывать надо именно конструктор, а не метод _construct,
	 * потому что родительский класс пихает инициализацию именно в конструктор.
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Topic
	 */
	public function __construct() {
		parent::__construct();
		$this->_controller = 'adminhtml_topic';
		$this->_blockGroup = 'df_forum';
		$this->_headerText = 'Темы';
		$this->_addButtonLabel = 'открыть новую тему';
		$this->setTemplate('df/forum/topics.phtml');
	}

	/** @return string */
	public function getAddNewButtonHtml() {
		return $this->getChildHtml('add_new_button');
	}

	/** @return int */
	public function getStoreId() {
		return Df_Forum_Model_Registry::s()->getStoreId();
	}

	/** @return bool */
	public function isSingleStoreMode() {
		return Mage::app()->isSingleStoreMode();
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Topic
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->setChild(
			'add_new_button'
			,Df_Adminhtml_Block_Widget_Button::i(
				'Открыть тему'
				, 'add'
				, $this->getUrl('*/*/new', array('store' => $this->getStoreId()))
			)
		);
		return $this;
	}
}