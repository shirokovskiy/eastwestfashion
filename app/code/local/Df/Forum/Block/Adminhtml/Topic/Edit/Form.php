<?php
class Df_Forum_Block_Adminhtml_Topic_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {
	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Topic_Edit_Form
	 */
	protected function _prepareForm() {
		parent::_prepareForm();
		$form =
			new Df_Varien_Data_Form(
				array(
					'id' => 'edit_form'
					,'action' =>
						$this->getUrl(
							'*/*/save'
							,array(
								'id' => $this->getRequest()->getParam('id')
								,'store' => Df_Forum_Model_Registry::s()->getStoreId()
							)
						)
						,'method' => 'post'
				)
			)
		;
		$form->setUseContainer(true);
		$this->setForm($form);
		return $this;
	}

	const _CLASS = __CLASS__;
}