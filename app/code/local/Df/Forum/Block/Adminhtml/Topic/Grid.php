<?php
class Df_Forum_Block_Adminhtml_Topic_Grid extends Df_Adminhtml_Block_Widget_Grid {
	/**
	 * Перекрывать надо именно конструктор, а не метод _construct,
	 * потому что родительский класс пихает инициализацию именно в конструктор.
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Topic_Grid
	 */
	public function __construct() {
		parent::__construct();
		$this->setId('forumTopicGrid');
		$this->setDefaultSort(Df_Forum_Model_Topic::P__ID);
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	/**
	 * @override
	 * @param Df_Forum_Model_Topic $row
	 * @return string
	 */
	public function getRowUrl($row) {
		return
			$this
				->getUrl(
					'*/*/edit'
					,array(
						'id' => $row->getId()
						,'store' => Df_Forum_Model_Registry::s()->getStoreId()
					)
				)
		;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Topic_Grid
	 */
	protected function _prepareCollection() {
		$collection = Df_Forum_Model_Topic::c();
		$collection->getTopicsOnly();
		$table_topics = $collection->getTable(Df_Forum_Model_Resource_Topic::TABLE_NAME);
		$collection->getSelect()
			->where('? = main_table.is_category', 0)
			->joinLeft(
				array('table_topics' => $table_topics)
				,'main_table.parent_id = table_topics.topic_id'
				,'table_topics.title as parent_title'
			)
		;
		$storeId = Df_Forum_Model_Registry::s()->getStoreId();
		if (0 !== $storeId) {
			$collection->addStoreFilter($storeId);
		}
		$this->setCollection($collection);
		parent::_prepareCollection();
		return $this;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Topic_Grid
	 */
	protected function _prepareColumns() {
		$this
			->addColumn(
				Df_Forum_Model_Topic::P__ID
				,array(
					'header' => '№'
					,'align' => 'right'
					,'width' => '50px'
					,'index' => Df_Forum_Model_Topic::P__ID
				)
			)
			->addColumn(
				'title'
				,array(
					'header' => 'заголовок'
					,'align' => 'left'
					,'index' => 'title'
					,'filter_index' => 'main_table.title'
				)
			)
			->addColumn(
				'user_name'
				,array(
					'header' => 'автор'
					,'align' => 'left'
					,'index' => 'user_name'
					,'width' => '100px'
					,'filter_index' => 'main_table.user_name'
				)
			)
			->addColumn(
				'parent_title'
				,array(
					'header' => 'раздел'
					,'align' => 'left'
					,'width' => '140px'
					,'index' => 'parent_title'
					,'filter_index' => 'table_topics.title'
				)
			)
			->addColumn(
				'url_text'
				,array(
					'header' => 'адрес'
					,'align' => 'left'
					,'width' => '140px'
					,'index' => 'url_text'
					,'filter_index' => 'main_table.url_text'
				)
			)
			->addColumn(
				'created_time'
				,array(
					'header' => 'дата создания'
					,'align' => 'left'
					,'width' => '140px'
					,'type' => 'datetime'
					,'default' => '--'
					,'index' => 'created_time'
					,'filter_index' => 'main_table.created_time'
				)
			)
			->addColumn(
				'update_time'
				,array(
					'header' => 'дата обновления'
					,'align' => 'left'
					,'width' => '140px'
					,'type' => 'datetime'
					,'default' => '--'
					,'index' => 'update_time'
					,'filter_index' => 'main_table.update_time'
				)
			)
			->addColumn(
				Df_Forum_Model_Topic::P__STATUS
				,array(
					'header' => 'опубликована?'
					,'align' => 'left'
					,'width' => '80px'
					,'index' => Df_Forum_Model_Topic::P__STATUS
					,'type' => 'options'
					,'options' => Df_Admin_Model_Config_Source_YesNo::s()->toOptionArrayAssoc()
					,'filter_index' => 'main_table.status'
				)
			)
			->addColumn(
				'action'
				,array(
					'header' => 'команда'
					,'width' => '50px'
					,'type' => 'action'
					,'getter' => 'getId'
					,'actions' =>
						array(
							array(
								'caption' => 'изменить'
								,'url' =>
									array(
										'base' => '*/*/edit'
										,'params' =>
											array(
												'store' => $this->getRequest()->getParam('store')
											)
									)
								,'field' => 'id'
							)
						)
						,'filter' => false
						,'sortable' => false
						,'index' => 'stores'
				)
			)
		;
		return parent::_prepareColumns();
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Topic_Grid
	 */
	protected function _prepareMassaction() {
		parent::_prepareMassaction();
		$this->setMassactionIdField('entity_id');
		$this->getMassactionBlock()
			->setDataUsingMethod(
				Df_Adminhtml_Block_Widget_Grid_Massaction::P__FORM_FIELD_NAME
				,
				'topic'
			)
		;
		$this->getMassactionBlock()
			->addItem(
				'delete'
				,array(
					'label' => 'удалить'
					,'url' =>
						$this->getUrl(
							'*/*/massDelete'
							,array(
								'store' => Df_Forum_Model_Registry::s()->getStoreId()
							)
						)
					,'confirm' => 'Удалить?'
				)
			)
		;
		$this->getMassactionBlock()
			->addItem(
				Df_Forum_Model_Topic::P__STATUS
				,array(
					'label' => 'скрыть / опубликовать'
					,'url' =>
						$this->getUrl(
							'*/*/massStatus'
							,array(
								'_current' => true
								,'store' => Df_Forum_Model_Registry::s()->getStoreId()
							)
						)
					,'additional' =>
						array(
							'visibility' =>
								array(
									'name' => Df_Forum_Model_Topic::P__STATUS
									,'type' => 'select'
									,'class' => 'required-entry'
									,'label' => 'опубликована?'
									,'values' =>
										Df_Admin_Model_Config_Source_YesNo::s()->toOptionArrayWithEmpty()
								)
						)
				)
			)
		;
		return $this;
	}

	const _CLASS = __CLASS__;
}