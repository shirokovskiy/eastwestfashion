<?php
class Df_Forum_Block_Adminhtml_Topic_Edit_Tab_Icons extends Mage_Adminhtml_Block_Widget_Form {
	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Topic_Edit_Tab_Icons
	 */
	protected function _prepareForm() {
		parent::_prepareForm();
		$form = new Df_Varien_Data_Form();
		$this->setForm($form);
		$fieldset =
			$form->addFieldset(
				'post_form'
				,array(
					'legend' => 'Пиктограммы темы'
					,'class' => 'fieldset-wide'
				)
			)
		;
		$icons = $this->_getIcons();
		/**
		 * Обратите внимание,
		 * что нельзя применять цепной вызов $fieldset->addField()->addField(),
		 * потому что addField() возвращает не $fieldset, а созданное поле.
		 */
		$fieldset
			->addField(
				Df_Forum_Model_Topic::P__ICON_ID
				,'radios'
				,array(
					'label' => 'Пиктограммы'
					,'name' => Df_Forum_Model_Topic::P__ICON_ID
					,'values' => $icons
					,'separator' => '<br>'
				)
			)
		;
		if (Df_Forum_Model_Registry::s()->hasTopic()) {
			$form->setValues(Df_Forum_Model_Registry::s()->getTopic()->getData());
		}
		else if (rm_session()->hasData('post_data')) {
			$form->setValues(rm_session()->getData('post_data'));
			rm_session()->unsetData('post_data');
		}
		return $this;
	}

	/** @return array[] */
	private function _getIcons() {
		return df_h()->forum()->getForumIconsRadios();
	}

	/** @return Df_Forum_Block_Adminhtml_Topic_Edit_Tab_Icons */
	public static function i() {return df_block(__CLASS__);}
}