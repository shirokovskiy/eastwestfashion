<?php
class Df_Forum_Block_Adminhtml_Topic_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {
	/**
	 * Перекрывать надо именно конструктор, а не метод _construct,
	 * потому что родительский класс пихает инициализацию именно в конструктор.
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Topic_Edit
	 */
	public function __construct() {
		parent::__construct();
		$this->_objectId = Df_Forum_Model_Topic::P__ID;
		$this->_blockGroup = 'df_forum';
		$this->_controller = 'adminhtml_topic';
		$this->_updateButton('save', 'label', 'Сохранить тему');
		$this->_updateButton('delete', 'label', 'Удалить тему');
		$this
			->_addButton(
				'saveandcontinue'
				,array(
					'label' => 'Сохранить и остаться'
					,'onclick' => 'saveAndContinueEdit()'
					,'class' => 'save'
				)
				,-100
			)
		;
		$this->_formScripts[]
			= "
				function saveAndContinueEdit(){
					editForm.submit($('edit_form').action+'back/edit/');
				}
			"
		;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getBackUrl() {
		return $this->getUrl('*/*/', array('store' => $this->getRequest()->getParam('store', 0)));
	}

	/**
	 * @override
	 * @return string
	 */
	public function getHeaderText() {
		return
			df_concat(
						Df_Forum_Model_Registry::s()->hasTopic()
					&&
						Df_Forum_Model_Registry::s()->getTopic()->getId()
					? rm_sprintf(
						'Изменить тему «%s»'
						,df_text()->escapeHtml(
							Df_Forum_Model_Registry::s()->getTopic()->getTitle()
						)
					)
					: 'Открыть тему'
				,
					(0 === Df_Forum_Model_Registry::s()->getStoreId())
					? ''
					: df_concat(
						' '
						,'Магазин'
						,': '
						,df_text()->escapeHtml(
							Mage::app()->getStore(
								Df_Forum_Model_Registry::s()->getStoreId()
							)->getName()
						)
					)
			)
		;
	}

	/** @return Df_Forum_Block_Adminhtml_Topic_Edit */
	public static function i() {return df_block(__CLASS__);}
}