<?php
class Df_Forum_Block_Adminhtml_Topic_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {
	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Topic_Edit_Tab_Form
	 */
	protected function _prepareForm() {
		parent::_prepareForm();
		$form = new Df_Varien_Data_Form();
		$this->setForm($form);
		$fieldset =
			$form->addFieldset(
				'post_form'
				,array(
					'legend' => 'Параметры темы'
				)
			)
		;
		/**
		 * Обратите внимание,
		 * что нельзя применять цепной вызов $fieldset->addField()->addField(),
		 * потому что addField() возвращает не $fieldset, а созданное поле.
		 */
		$fieldset
			->addField(
				'title'
				,'text'
				,array(
					'label' => 'Тема'
					,'class' => 'required-entry'
					,'required' => true
					,'name' => 'title'
				)
			)
		;
		$fieldset
			->addField(
				Df_Forum_Model_Topic::P__URL_TEXT_SHORT
				,'text'
				,array(
					'name' => Df_Forum_Model_Topic::P__URL_TEXT_SHORT
					,'label' => 'адрес'
					,'class' => 'rm.validate.urlKey'
				)
			)
		;
		$fieldset
			->addField(
				Df_Forum_Model_Topic::P__PARENT_ID
				,'select'
				,array(
					'label' => 'раздел'
					,'name' => Df_Forum_Model_Topic::P__PARENT_ID
					,'values' =>
						df_h()->forum()->topic()
							->getOptionsTopics(
								$status_all = true
								,$skip_id = Df_Forum_Model_Registry::s()->getTopic()->getId()
								,$where = array('1' => 'is_category=?')
								,$no_text = 'нет раздела'
								,$skip_parent = false
								,$ret_as_it = (0 < Df_Forum_Model_Registry::s()->getStoreId())
								,$use_store = (0 < Df_Forum_Model_Registry::s()->getStoreId())
								,$storeId = Df_Forum_Model_Registry::s()->getStoreId()
							)
				)
			)
		;
		$fieldset
			->addField(
				Df_Forum_Model_Topic::P__STATUS
				,'select'
				,array(
					'label' => 'опубликована?'
					,'name' => Df_Forum_Model_Topic::P__STATUS
					,'values' => Df_Admin_Model_Config_Source_YesNo::s()->toOptionArray()
				)
			)
		;
		$fieldset
			->addField(
				Df_Forum_Model_Topic::P__HAS_SUBTOPIC
				,'select'
				,array(
					'label' => 'Содержит подтемы?'
					,'name' => Df_Forum_Model_Topic::P__HAS_SUBTOPIC
					,'values' => Df_Admin_Model_Config_Source_YesNo::s()->toOptionArray()
				)
			)
		;
		$fieldset
			->addField(
				'description'
				,'editor'
				,array(
					'name' => 'description'
					,'label' => 'Описание'
				)
			)
		;
		if (Df_Forum_Model_Registry::s()->hasTopic()) {
			$form->setValues(Df_Forum_Model_Registry::s()->getTopic()->getData());
		}
		else if (rm_session()->hasData('post_data')) {
			$form->setValues(rm_session()->getData('post_data'));
			rm_session()->unsetData('post_data');
		}
		return $this;
	}

	/** @return Df_Forum_Block_Adminhtml_Topic_Edit_Tab_Form */
	public static function i() {return df_block(__CLASS__);}
}