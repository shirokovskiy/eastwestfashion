<?php
class Df_Forum_Block_Adminhtml_Topic_Edit_Tabs extends Df_Adminhtml_Block_Widget_Tabs {
	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Topic_Edit_Tabs
	 */
	protected function _beforeToHtml() {
		$this
			->addTab(
				'form_section'
				,array(
					'label' => 'Параметры темы'
					,'title' => 'Параметры темы'
					,'content' => Df_Forum_Block_Adminhtml_Topic_Edit_Tab_Form::i()->toHtml()
			)
		);
		$this
			->addTab(
				'form_section_icons'
				,array(
					'label' => 'Пиктограммы темы'
					,'title' => 'Пиктограммы темы'
					,'content' => Df_Forum_Block_Adminhtml_Topic_Edit_Tab_Icons::i()->toHtml()
			)
		);
		parent::_beforeToHtml();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->setId('forum_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle('Параметры темы');
	}

	/** @return Df_Forum_Block_Adminhtml_Topic_Edit_Tabs */
	public static function i() {return df_block(__CLASS__);}
}