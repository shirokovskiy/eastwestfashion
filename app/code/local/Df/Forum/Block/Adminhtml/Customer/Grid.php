<?php
class Df_Forum_Block_Adminhtml_Customer_Grid extends Df_Adminhtml_Block_Widget_Grid {
	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Customer_Grid
	 */
	public function __construct() {
		parent::__construct();
		$this->setId('moderatorCustomerGrid');
		$this->setDefaultSort('entity_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
		$this->setUseAjax(true);
	}

	/**
	 * @override
	 * @param array(string => mixed) $params[optional]
	 * @return string
	 */
	public function getCurrentUrl($params = array()) {
		$result = parent::getCurrentUrl($params);
		if (!$params) {
			$result .= 'is_customers/1/';
		}
		return $result;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Customer_Grid
	 */
	protected function _prepareCollection() {
		/** @var Df_Customer_Model_Resource_Customer_Collection $collection */
		$collection = Df_Customer_Model_Resource_Customer_Collection::i();
		$collection
			->addNameToSelect()
			->addAttributeToSelect('email')
		;
		/** @var Varien_Db_Select $select */
		$select = $collection->getSelect();
		$select->where('? <> website_id', 0);
		$select
			->joinLeft(
				array(
					'table_moderators' =>
						$collection->getTable(Df_Forum_Model_Resource_Moderator::TABLE_NAME)
				)
				,'`e`.entity_id = table_moderators.system_user_id'
				,'table_moderators.moderator_id as Moder_Id'
			)
			->where('table_moderators.moderator_id IS null')
		;
		$this->setCollection($collection);
		parent::_prepareCollection();
		return $this;
	}

	/**
	 * @override
	 * @return Mage_Adminhtml_Block_Widget_Grid
	 */
	protected function _prepareColumns() {
		$this
			->addColumn(
				'entity_id'
				,array(
					'header' => '№'
					,'align' => 'right'
					,'width' => '50px'
					,'index' => 'entity_id'
				)
			)
			->addColumn(
				'firstname'
				,array(
					'header' => 'Имя'
					,'align' => 'left'
					,'index' => 'firstname'
				)
			)
			->addColumn(
				'lastname'
				,array(
					'header' => 'Фамилия'
					,'align' => 'left'
					,'index' => 'lastname'
				)
			)
			->addColumn(
				'email',
				array(
					'header' => 'Эл.почта'
					,'align' => 'left'
					,'index' => 'email'
				)
			)
		;
		if (!Mage::app()->isSingleStoreMode()) {
			$this
				->addColumn(
					'website_id'
					,array(
						'header' => 'Сайт'
						,'align' => 'center'
						,'width' => '180px'
						,'type' => 'options'
						,'options' =>
							df_mage()->adminhtml()->system()->storeSingleton()
								->getWebsiteOptionHash(true)
						,'index' => 'website_id'
					)
			);
		}
		return parent::_prepareColumns();
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Adminhtml_Customer_Grid
	 */
	protected function _prepareMassaction() {
		parent::_prepareMassaction();
		$this->setMassactionIdField('entity_id');
		$this->getMassactionBlock()
			->setDataUsingMethod(
				Df_Adminhtml_Block_Widget_Grid_Massaction::P__FORM_FIELD_NAME
				,
				'moderators'
			)
		;
		$this
			->getMassactionBlock()->addItem(
				'assign_to_moder'
				,array(
					'label' => 'привязать к модератору'
					,'url' => $this->getUrl('*/*/massAssign')
				)
		);
		return $this;
	}

	const _CLASS = __CLASS__;
}
