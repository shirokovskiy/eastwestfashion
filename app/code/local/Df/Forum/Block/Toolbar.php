<?php
class Df_Forum_Block_Toolbar extends Df_Core_Block_Template {
	/** @return array(string => int) */
	public function getAvailableLimit() {
		return $this->_limit_avaiable;
	}

	/** @return Varien_Data_Collection|array */
	public function getCollection() {
		return $this->_collection;
	}

	/** @return int */
	public function getFirstNum() {
		return
				$this->getCollection()->getPageSize()
			*
				(
					$this->getCollection()->getCurPage() - 1
				)
			+ 1
		;
	}

	/** @return string */
	public function getHtmlLimiter() {
		return $this->getHtmlForSimpleChild('df/forum/toolbar/limiter.phtml');
	}

	/** @return string */
	public function getHtmlStatistics() {
		return $this->getHtmlForSimpleChild('df/forum/toolbar/statistics.phtml');
	}

	/** @return string */
	public function getItemsNameInGenetiveCase() {
		return 'элементов';
	}

	/** @return string */
	public function getItemsNameInNominativeCase() {
		return 'элементы';
	}

	/** @return int */
	public function getLastNum() {
		return
				$this->getCollection()->getPageSize()
			*
				(
					$this->getCollection()->getCurPage() - 1
				)
			+
				$this->getCollection()->count()
		;
	}

	/** @return int */
	public function getLastPageNum() {
		return $this->getCollection()->getLastPageNumber();
	}

	/**
	 * @param $limit
	 * @return string
	 */
	public function getLimitUrl($limit) {
		return
			$this->getPagerUrl(
				array(
					$this->limit_var_name => $limit
					,$this->sort_var_name => null
					,$this->page_var_name => 1
				)
			)
		;
	}

	/** @return string */
	public function getPagerHtml() {
		$this->initPager();
		$pagerBlock = $this->getChild('toolbar_pager');
		/** @var string $result */
		$result = '';
		if ($pagerBlock) {
			/* @var $pagerBlock Mage_Page_Block_Html_Pager */
			$pagerBlock->setAvailableLimit($this->getAvailableLimit());
			$collection = $this->getCollection();
			$collection->setCurPage($this->getCurPage());
			$pagerBlock->setUseContainer(false);
			$pagerBlock->setShowPerPage(false);
			$pagerBlock->setShowAmounts(false);
			$pagerBlock->setLimitVarName($this->limit_var_name);
			$pagerBlock->setPageVarName($this->page_var_name);
			$pagerBlock->setLimit($this->getLimit());
			/*$pagerBlock->setFrameLength(Mage::getStoreConfig('design/pagination/pagination_frame')); */
			$pagerBlock->setJump(Mage::getStoreConfig('design/pagination/pagination_frame_skip'));
			$pagerBlock->setLimit($this->getLimit());
			$pagerBlock->setCollection($collection);
			$result = $pagerBlock->toHtml();
		}
		return $result;
	}

	/**
	 * @param $sort
	 * @return string
	 */
	public function getSortUrl($sort) {
		return
			$this->getPagerUrl(
				array(
					$this->sort_var_name => $sort
					,$this->page_var_name => null
					,$this->limit_var_name => null
				)
			)
		;
	}

	/** @return int */
	public function getTotalNum() {
		return $this->getCollection()->getSize();
	}

	/**
	 * @param $is_limit
	 * @return bool
	 */
	public function isLimitCurrent($is_limit) {
		return $is_limit == $this->_limit;
	}

	/**
	 * @param $sort
	 * @return bool
	 */
	public function isSortCurrent($sort) {
		return $sort == $this->_sort;
	}

	/**
	 * @override
	 * @return Df_Forum_Block_Toolbar
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->initPager();
		return $this;
	}

	/**
	 * @override
	 * @return string|null
	 */
	protected function getDefaultTemplate() {
		return 'df/forum/toolbar.phtml';
	}

	/**
	 * @override
	 * @return bool
	 */
	protected function needToShow() {
		return parent::needToShow() && (0 < $this->getCollection()->getSize());
	}

	/**
	 * @param string $class
	 * @param string|null $name
	 * @return null|Mage_Core_Block_Abstract
	 */
	private function getBlock($class, $name) {
		/** @var null|Mage_Core_Block_Abstract $result */
		$result = null;
		if ($name) {
			$result = rm_layout()->getBlock($name);
		}
		return $result ? $result : df_block($class, microtime());
	}

	/**
	 * @param string $name
	 * @return bool|Mage_Core_Block_Abstract
	 */
	private function getForumBlock($name) {
		return $this->getBlock($name, $this->getForumBlockName());
	}

	/** @return string|null */
	private function getForumBlockName() {
		return $this->ForumBlockName;
	}

	/**
	 * @param $name
	 * @return Mage_Core_Block_Abstract|null
	 */
	public function getForumMyTopicsBlock($name) {
		return $this->getBlock($name, $this->getForumMyTopicsBlockName());
	}

	/** @return string|null */
	public function getForumMyTopicsBlockName() {
		return $this->MyForumTopicsBlockName;
	}

	/**
	 * @param string $template
	 * @return string
	 */
	private function getHtmlForSimpleChild($template) {
		return Df_Core_Block_Template::create(array('parent' => $this), $template)->toHtml();
	}

	/**
	 * @param string $name
	 * @return null|Mage_Core_Block_Abstract
	 */
	private function getObjectBlock($name) {
		return $this->getBlock($name, $this->getObjectBlockName());
	}

	/** @return string|null */
	private function getObjectBlockName() {
		return $this->ObjectBlockName;
	}

	/**
	 * @param array(string => mixed) $params
	 * @return string
	 */
	private function getPagerUrl($params = array()) {
		return
			$this->getUrl(
				'*/*/*'
				,array(
					'_current' => true
					,'_escape' => true
					,'_use_rewrite' => true
					,'_query' => $params
				)
			)
		;
	}

	/**
	 * @param string $name
	 * @return null|Mage_Core_Block_Abstract
	 */
	private function getSearchBlock($name) {
		return $this->getBlock($name, $this->getSearchBlockName());
	}

	/** @return string|null */
	private function getSearchBlockName() {
		return $this->ForumSearchName;
	}

	/** @return void */
	private function initPager() {
		if (!is_null(Df_Forum_Model_Action::s()->getCurrentTopic())) {
			$object = $this->getObjectBlock($this->ObjectBlockName);
		}
		else if (Mage::registry('search_value_page')) {
			$object = $this->getSearchBlock($this->ForumSearchName);
		}
		else if (
				Mage::registry('myprivatemessages_inbox')
			||
				Mage::registry('myprivatemessages_sent')
			||
				Mage::registry('myprivatemessages_trash')
		) {
            $object = $this->getMyprivateMessagesBlock($this->MyPrivateMessagesBlockName);
		}
		else if (Mage::registry('myforumtopics')) {
			$object = $this->getForumMyTopicsBlock($this->MyForumTopicsBlockName);
		}
		else if (Mage::registry('myforumposts')) {
			$object = $this->getForumMyPostsBlock($this->MyForumPostsBlockName);
		}
		else {
			$object = $this->getForumBlock($this->ForumBlockName);
		}
		if ($object) {
			$this->_collection = $object->initCollection();
			$this->_limit_avaiable = $object->limits;
			$this->page_var_name = $object->getPageVarName();
			$this->limit_var_name = $object->getLimitVarName();
			$this->sort_var_name = $object->getSortVarName();
			$this->_limit = $object->getLimit();
			$this->curPage = $object->getCurPage();
			$this->_sort = $object->getSort();
			$this->_collection->setCurPage($this->curPage);
		}
	}

	/**
	 * @param $name
	 * @return Mage_Core_Block_Abstract|null
	 */
	public function getMyprivateMessagesBlock($name) {
		return $this->getBlock($name, $this->getForumMyPrivateMessagesBlockName());
	}

	/** @return string|null */
	public function getForumMyPrivateMessagesBlockName() {
    	return $this->MyPrivateMessagesBlockName;
	}

	/**
	 * @param $name
	 * @return Mage_Core_Block_Abstract|null
	 */
	public function getForumMyPostsBlock($name) {
		return $this->getBlock($name, $this->getForumMyPostsBlockName());
	}

	/** @return string|null */
	public function getForumMyPostsBlockName() {
		return $this->MyForumPostsBlockName;
	}

	/** @return bool */
	public function isCategory() {
		return
				!is_null(Df_Forum_Model_Action::s()->getCurrentTopic())
			&&
				(
						Df_Forum_Model_Action::s()->getCurrentTopic()->isCategory()
				    ||
						Df_Forum_Model_Action::s()->getCurrentTopic()->hasSubTopics()
				)
		;
	}

	/** @var int */
	private $_limit = 0;
	/** @var int */
	private $_sort = 1;
	private $_collection = array();
	private $_limit_avaiable = array();
	/** @var string */
	private $ObjectBlockName = 'df_forum/view';
	/** @var string */
	private $ForumBlockName = 'df_forum/forum';
	/** @var string */
	private $ForumSearchName = 'df_forum/search';
	/** @var string */
	private $MyForumTopicsBlockName = 'df_forum/mytopics';
	/** @var string */
	private $MyForumPostsBlockName = 'df_forum/myposts';
	/** @var string */
	private $MyPrivateMessagesBlockName = 'df_forum/myprivatemessages';
	private $page_var_name = false;
	private $limit_var_name = false;
	private $sort_var_name = false;

	const _CLASS = __CLASS__;
}