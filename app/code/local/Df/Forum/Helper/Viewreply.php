<?php
class Df_Forum_Helper_Viewreply extends Mage_Core_Helper_Abstract {
	/** @return string */
	public function getReplyIdBegin() {
		return self::REPLY_ID_BEGIN;
	}

	/**
	 * @param int $id
	 * @return Varien_Object
	 */
	public function getViewReplyUrlObj($id) {
		df_param_integer($id, 0);
		/** @var Varien_Object $result */
		$result = $this->redirectToIndex();
		$this->REPLY_LIMIT = $this->getPagesLimits();
		$this->object_id = $id;
		$obj = $this->getReplyObject($id);
		if (!$this->validateNotShow($obj)) {
			$parentObj = $this->getReplyObjectParent($obj->getParentId());
			if (!$this->validateNotShow($parentObj)) {
				$collection = $this->getParentCollection($obj->getParentId());
				$page_number = intval($this->getPageNumber($collection, $id));
				if (0 !== $page_number) {
					$path = $this->getPath($parentObj, $id);
					$this->setSessions($page_number);
					$result =
						$this->getObject(
							$path
							,array(
								'_escape' => true
								,'_use_rewrite' => true
							)
						)
					;
				}
			}
		}
		return $result;
	}

	/**
	 * @param string $path
	 * @param array $arguments
	 * @return Varien_Object
	 */
	private function getObject($path = '*/', $arguments = array()) {
		$ret = new Varien_Object();
		$data =
			array(
				'path' => $path
				,'arguments' => $arguments
				,'url' => $this->_getUrl($path, $arguments)
			)
		;
		if ('*/' !== $path) {
			$data['identifier'] = df_concat('#', $this->getReplyIdBegin(), $this->object_id);
		}
		$ret->setData($data);
		return $ret;
	}

	/**
	 * @param $c
	 * @param $id
	 * @return bool|int
	 */
	private function getPageNumber($c, $id) {
		if (!$c->getSize()) {
			return false;
		}
		$num = 1;
		$count = 1;
		foreach ($c as $val) {
			if ($count > $this->REPLY_LIMIT) {
				$count = 1;
				$num++;
			}
			if ($val->getId() == $id) {
				return $num;
			}
			$count++;
		}
		return false;
	}

	/** @return mixed */
	private function getPagesLimits() {
		$l = df_cfg()->forum()->general()->getNumItemsPerPage();
		$l = $l ? $l : 0;
		$arr = $this->pages_limits[$l];
		return $arr[0];
	}

	/**
	 * @param int|null $id
	 * @return Df_Forum_Model_Resource_Post_Collection
	 */
	private function getParentCollection($id) {
		$c = Df_Forum_Model_Post::c();
		$c->setOrder('created_time', self::REPLY_ORDER);
		$c->getSelect()->where('? = status', 1);
		if (!$id) {
			$c->getSelect()->where('parent_id is null');
		}
		else {
			$c->getSelect()->where('? = parent_id', $id);
		}
		return $c;
	}

	/**
	 * @param $obj
	 * @param $id
	 * @return string
	 */
	private function getPath($obj, $id) {
		return '' . $obj->getUrlText();
	}

	/**
	 * @param $id
	 * @return Mage_Core_Model_Abstract
	 */
	private function getReplyObject($id) {
		return Df_Forum_Model_Post::i()->load($id);
	}

	/**
	 * @param $id
	 * @return Mage_Core_Model_Abstract
	 */
	private function getReplyObjectParent($id) {
		return Df_Forum_Model_Topic::i()->load($id);
	}

	/** @return Varien_Object */
	private function redirectToIndex() {
		return $this->getObject();
	}

	/**
	 * @param $page
	 */
	private function setSessions($page) {
		Df_Forum_Model_Session::s()->setPageLimitPost($this->REPLY_LIMIT);
		Df_Forum_Model_Session::s()->setPageSortPost(self::REPLY_SORT);
		Df_Forum_Model_Session::s()->setPageCurrentPost($page);
	}

	/**
	 * @param $obj
	 * @return bool
	 */
	private function validateNotShow($obj) {
		return (!$obj->getId()) || (0 === intval($obj->getStatus()));
	}
	private $pages_limits
		= array(
			array(
				5,10,15
			),array(
				10,15,30
			),array(
				20,30,50
			)
		);
	private $REPLY_LIMIT = 5;
	private $object_id = false;

	const _CLASS = __CLASS__;
	const REPLY_ID_BEGIN = 'rm-forum-post-';
	const REPLY_ORDER = 'asc';
	const REPLY_SORT = 2;

	/** @return Df_Forum_Helper_Viewreply */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}