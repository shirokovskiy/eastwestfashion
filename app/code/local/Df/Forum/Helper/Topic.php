<?php
class Df_Forum_Helper_Topic extends Mage_Core_Helper_Abstract {
	public function ___getStoreId($_id) {
		return Df_Forum_Model_Topic::i()->load($_id)->getStoreId();
	}

	/**
	 * @param int $_id
	 * @param int $storeId
	 * @return Df_Forum_Helper_Topic
	 */
	public function ___updateUrlStoreById($_id, $storeId) {
		$id_path = $this->buildIdPath($_id);
		/** @var Df_Core_Model_Url_Rewrite $rewrite */
		$rewrite = Df_Core_Model_Url_Rewrite::i();
		$rewrite->load($id_path, 'id_path');
		if ($rewrite->getId()) {
			$rewrite->setStoreId($storeId);
			$rewrite->save();
		}
		return $this;
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function buildIdForumPath($id) {
		return 'topic/view/' . $id;
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function buildIdPath($id) {
		return 'topic/view/' . $id;
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function buildRequestForumPath($id) {
		return 'forum/topic/view/id/' . $id . '/';
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function buildRequestPath($id) {
		return 'forum/topic/view/id/' . $id . '/';
	}

	/**
	 * @param $title
	 * @param $id
	 * @return mixed|string
	 */
	public function buildUrlKeyFromTitle($title, $id) {
		$title_to_check = $new_title = $this->getUrlKeyFromTitle(str_replace(' ', '-', mb_strtolower($title)));
		$t = 1;
		if (str_replace('.', '', str_replace('-', '', $title_to_check)) == '') {
			$addon = date('Y-m-d');
			$new_title = $title_to_check = 'rm-forum-' . $addon;
		}
		while ($this->validateUrlKey('forum/' . $title_to_check, $id)) {
			$t++;
			$title_to_check = $new_title . ($t ? '-' . $t : '');
		}
		return $title_to_check;
	}

	/**
	 * @param Df_Forum_Model_Topic $topic
	 * @return bool
	 */
	public function canShow(Df_Forum_Model_Topic $topic) {
		return $this->isModerator() || $topic->getStatus();
	}

	/**
	 * @param string $id_path
	 * @return Df_Forum_Helper_Topic
	 */
	public function deleteUrlRewrite($id_path) {
		if ($id_path && $id_path != '') {
			try {
				/** @var Df_Core_Model_Url_Rewrite $rewrite */
				$rewrite = Df_Core_Model_Url_Rewrite::i();
				$rewrite->load($id_path, 'id_path');
				if ($rewrite->getId()) {
					$rewrite->delete();
				}
			}
			catch(Exception $e) {
			}
		}
		return $this;
	}

	/**
	 * @param $_id
	 * @return mixed
	 */
	public function getAllowedModerateWebsites($_id) {
		$result = null;
		$obj =
			Df_Forum_Model_Moderator::i()->load(
				$_id
				,Df_Forum_Model_Moderator::P__SYSTEM_USER_ID
			)
		;
		if ($obj->getId()) {
			$result = $obj->getUserWebsiteId();
		}
		return $result;
	}

	/**
	 * @param $locale
	 * @return string
	 */
	public function getAvLocale($locale) {
		$arr_tmp = explode('_', $locale);
		return
			!df_a($arr_tmp, 0)
			? $this->editor_locales[0]
			: (
				in_array($arr_tmp[0], $this->editor_locales)
				? $arr_tmp[0]
				: $this->editor_locales[0]
			)
		;
	}

	/**
	 * @param int|null $id
	 * @return Df_Forum_Model_Resource_Topic_Collection
	 */
	public function getChildsTopics($id) {
		$isModerator = $this->isModerator();
		/** @var Df_Forum_Model_Resource_Topic_Collection $result */
		$result = Df_Forum_Model_Topic::c();
		if (!$isModerator) {
			$result->getSelect()->where('? = status', 1);
		}
		if (!$id) {
			$result->getSelect()->where('parent_id is null');
		}
		else {
			$result->getSelect()->where('? = parent_id', $id);
		}
		return $result;
	}

	/**
	 * @param $urlBegin
	 * @param bool $is_edit
	 * @param bool $_have_all_urls
	 * @return array
	 */
	public function getBreadcrumbPath(
		$urlBegin = ''
		,$is_edit = false
		,$_have_all_urls = false
	) {
		$result = array();
		$current_object = Df_Forum_Model_Action::s()->getCurrentTopic();
		$parent_object = Mage::registry('current_object_parent');
		$result['alltopics'] = array('label' => 'Форум');
		if ($_have_all_urls) {
			$result['alltopics']['link'] = $urlBegin . $this->url_all_topics;
		}
		if ($this->breadCrumbBlock) {
			$result['alltopics']['link'] = $urlBegin . $this->url_all_topics;
			$result['b_block'] = array('label' => $this->breadCrumbBlock);
		}
		if ($current_object || $is_edit) {
			if ($current_object->getId() && !$is_edit) {
				$result['alltopics']['link'] = $urlBegin . $this->url_all_topics;
				if ($current_object->getParentId() && $current_object->getParentId() != 0) {
					$result = $this->addTopicsBreadcrumbsParent($current_object->getParentId(), $result);
				}
				$result['topic_' . $current_object->getId()] =
					array('label' => $current_object->getTitle())
				;
			}
			else if ($current_object->getId() && $is_edit) {
				$result['alltopics']['link'] = $urlBegin . $this->url_all_topics;
				if ($current_object->getParentId() && $current_object->getParentId() != 0) {
					$result = $this->addTopicsBreadcrumbsParent($current_object->getParentId(), $result);
				}
				if (
					Df_Forum_Model_Action::s()->getCurrentTopic()->getSystemUserId()
					===
						rm_session_customer()->getCustomer()->getId()
					&&
						Mage::registry('current_object_post')->getSystemUserId()
					||
						$this->isModerator()
				) {
					$result['topic_' . $current_object->getId()] =
						array(
							'label' => rm_sprintf('Тема «%s»: ответ/редактирование.', $current_object->getTitle())
						)
					;
				}
				else if (
						Mage::registry('current_object_post')->getSystemUserId()
					===
					rm_session_customer()->getCustomer()->getId()
				) {
					$result['topic_' . $current_object->getId()] =
						array(
							'label' =>
								rm_sprintf(
									'Редактирование сообщения в теме «%s»'
									,$current_object->getTitle()
								)
						)
					;
				}
				else {
					$result['topic_' . $current_object->getId()]
						= array('label' => rm_sprintf('Вы отвечаете в теме «%s»', $current_object->getTitle()));
				}
			}
			else if ($is_edit) {
				$result['alltopics']['link'] = $urlBegin . $this->url_all_topics;
				if ($parent_object->getId() && $parent_object->getId() != 0) {
					$result = $this->addTopicsBreadcrumbsParent($parent_object->getId(), $result);
				}
				$result['topic_' . $current_object->getId()] =
					array('label' => 'открыть новую тему')
				;
			}
		}
		return $result;
	}

	/**
	 * @param Df_Forum_Model_Topic $obj
	 * @param bool $parent_obj
	 * @return Df_Forum_Model_Resource_Post_Collection|null
	 */
	public function getChildsPosts(Df_Forum_Model_Topic $obj, $parent_obj = false) {
		/** @var Df_Forum_Model_Resource_Post_Collection|null $result */
		$result = null;
		if (!$obj->hasSubTopics()) {
			$isModerator = $this->isModerator();
			$result = Df_Forum_Model_Post::c();
			if (!$isModerator) {
				$result->getSelect()
					->where('? = status', 1)
				;
			}
			/** @var int|null $id */
			$id = $obj->getId();
			if (!$id) {
				$result->getSelect()->where('parent_id is null');
			}
			else {
				$result->getSelect()->where('? = parent_id', $id);
			}
			if ($parent_obj) {
				$this->setLatestPosts($result, $obj, $parent_obj);
			}
		}
		return $result;
	}

	/**
	 * @param $id
	 * @return int
	 */
	public function getEntityId($id) {
		return intval(Df_Forum_Model_Entity::i()->load($id, 'entity_id')->getId());
	}

	/**
	 * @param string $base_url
	 * @return string
	 */
	public function getForumUrl($base_url = '/') {
		return $base_url . 'forum';
	}

	/**
	 * @param $url
	 * @param bool $status_all
	 * @return mixed
	 */
	public function getIdTopicByTitle($url, $status_all = false) {
		/** @var Df_Forum_Model_Resource_Topic_Collection $modelCollection */
		$modelCollection = Df_Forum_Model_Topic::c()->setPageSize(1);
		if ($status_all) {
			$modelCollection->getSelect()
				->where('? = status', 1)
			;
		}
		$modelCollection->getSelect()
			->where('? = url_text', $url)
		;
		$result = null;
		foreach ($modelCollection as $val) {
			$result = $val->getId();
			break;
		}
		return $result;
	}

	/** @return mixed */
	public function getObject() {
		return $this->obj;
	}

	/**
	 * @param bool $status_all [optional]
	 * @param int $skip_id [optional]
	 * @param array $where [optional]
	 * @param bool $no_text [optional]
	 * @param bool $skip_parent [optional]
	 * @param bool $ret_as_it [optional]
	 * @param bool $use_store [optional]
	 * @param int $storeId [optional]
	 * @param bool $no_empty [optional]
	 * @param bool $subtopics_parent_only [optional]
	 * @return array[]
	 */
	public function getOptionsTopics(
		$status_all = true
		,$skip_id = 0
		,$where = array()
		,$no_text = false
		,$skip_parent = false
		,$ret_as_it = false
		,$use_store = false
		,$storeId = 0
		,$no_empty = false
		,$subtopics_parent_only = false
	) {
		/** @var array[] $result */
		$result = array();
		if (!$no_empty) {
			$no_text = $no_text ? $no_text : 'без темы';
			$result[]= array ('value' => 0, 'label' => $no_text);
		}
		$this->subtopics_parent_only = $subtopics_parent_only;
		/** @var mixed[] $arr */
		$arr =
			$this->getTopicsTreeArr(
				0
				,$status_all
				,$where
				,$skip_parent
				,$use_store
				,$storeId
			)
		;
		$this->skip_id = $skip_id;
		if (!$ret_as_it) {
			$result = $this->addTopicsRecursive($arr, $result);
		}
		else {
			$result = $this->addTopicsRecursiveArr($arr, $result);
		}
		df_result_array($result);
		return $result;
	}

	/** @return mixed */
	public function getPagesLimits() {
		$l = df_cfg()->forum()->general()->getNumItemsPerPage();
		$l = $l ? $l : 0;
		return $this->pages_limits[$l];
	}

	/**
	 * @param Df_Forum_Model_Topic $obj
	 * @param int $quantity
	 * @param bool $obj_parent
	 * @return int
	 */
	public function getPostsQuantity(Df_Forum_Model_Topic $obj, $quantity = 0, $obj_parent = false) {
		if ($c = $this->getChildsPosts($obj, $obj_parent)) {
			$quantity += $c->getSize();
		}
		if ($obj->isCategory() || $obj->hasSubTopics()) {
			$modelData = $this->getChildsTopics($obj->getId());
			if ($modelData->getSize()) {
				foreach ($modelData as $val) {
					$quantity = $this->getPostsQuantity($val, $quantity, $obj_parent);
				}
			}
		}
		return $quantity;
	}

	/** @return int */
	public function getStoreId() {
		return Mage::app()->getStore()->getWebsiteId();
	}

	/**
	 * @param $id
	 * @param int $quantity
	 * @return int
	 */
	public function getTopicsQuantity($id, $quantity = 0) {
		$modelData = $this->getChildsTopics($id);
		if ($modelData->getSize()) {
			$quantity += $modelData->getSize();
			foreach ($modelData as $val) {
				/** @var Df_Forum_Model_Topic $val */
				if ($val->isCategory() || $val->hasSubTopics()) {
					$new_id = $val->getId();
					$quantity = $this->getTopicsQuantity($new_id, $quantity);
				}
			}
		}
		return $quantity;
	}

	/**
	 * @param int $parent_id
	 * @param bool $status_all
	 * @param array $where
	 * @param bool $skip_parent
	 * @param bool $use_store
	 * @param int $storeId
	 * @return mixed[]
	 */
	public function getTopicsTreeArr(
		$parent_id = 0
		,$status_all = true
		,$where = array()
		,$skip_parent = false
		,$use_store = false
		,$storeId = 0
	) {
		$model = Df_Forum_Model_Topic::c();
		if ($use_store) {
			$model->addStoreFilter($storeId);
		}
		if (!$status_all) {
			$model->getSelect()->where('? = status', 1);
		}
		if (count($where) > 0) {
			foreach ($where as $key => $val) {
				$model->getSelect()->where($val, $key);
			}
		}
		if (!$skip_parent) {
			if (!$parent_id) {
				$model->getSelect()->where('parent_id is null');
			}
			else {
				$model->getSelect()->where('? = parent_id', $parent_id);
			}
		}
		/** @var mixed[] $result */
		$result = $model->getData();
		if (count($result) > 0) {
			if ($this->isModerator()) {
				$status_all = true;
				$where = array();
			}
			foreach ($result as $key => $val) {
				$result[$key]['_childs']=
					$this->getTopicsTreeArr(
						$val[Df_Forum_Model_Topic::P__ID]
						,$status_all
						,$where
						,false
						,$use_store
						,$storeId
					)
				;
			}
		}
		return $result;
	}

	/**
	 * @param $obj
	 * @return int
	 */
	public function getTotalViews($obj) {
		$id = $obj->getId();
		return Df_Forum_Model_Entity::i()->load($id, 'entity_id')->getTotalViews() ? Df_Forum_Model_Entity::i()->load($id, 'entity_id')->getTotalViews() : 0;
	}

	/**
	 * @param $string
	 * @return mixed
	 */
	public function getUrlKeyFromTitle($string) {
		return preg_replace("/[^a-zA-Z0-9-\.\-\s]/", "", $string);
	}

	/**
	 * @param $userId
	 * @param bool $storeId
	 * @return mixed
	 */
	public function getUserBySystemId($userId, $storeId = false) {
		if ($storeId === false) {
			$storeId = Mage::app()->getStore()->getId();
		}
		$o = Df_Forum_Model_User::c();
		$o->getSelect()
			->where('? = system_user_id', $userId)
			->where('? = store_id', $storeId)
		;
		$result = null;
		if ($o->getSize()) {
			foreach ($o as $val) {
				$result = $val->getId();
				break;
			}
		}
		return $result;
	}

	/** @return int */
	public function getWebsiteId() {
		return Mage::app()->getStore()->getWebsiteId();
	}

	/**
	 * @param int $id
	 * @return bool
	 */
	public function hasSubTopics($id) {
		/** @var Df_Forum_Model_Topic $topic */
		$topic = Df_Forum_Model_Topic::i()->load($id);
		return $topic->hasSubTopics();
	}

	/** @return bool */
	public function isForumActive() {
		return $this->_isActive;
	}

	/** @return bool */
	public function isModerator() {
		/** @var bool $result */
		$result = false;
		if (rm_session_customer()->getCustomer()->getId()) {
			$result =
					($this->getWebsiteId() === rm_session_customer()->getCustomer()->getWebsiteId())
				&&
					(
						$this->getWebsiteId()
					 ===
					 	$this->getAllowedModerateWebsites(rm_session_customer()->getCustomer()->getId())
					)
			;
		}
		return $result;
	}

	/**
	 * @param $id
	 * @param $obj
	 * @param bool $not_set_user
	 */
	public function setEntity($id, $obj, $not_set_user = false) {
		/** @var Df_Forum_Model_Entity $entityModel */
		$entityModel = Df_Forum_Model_Entity::i();
		$entityModel
			->setId($this->getEntityId($id))
			->setEntityType($obj->getEntityType())
			->setEntityId($id)
		;
		$entityModel->save();
		if ($obj->getEntityUserId() && !$not_set_user) {
			$this
				->setUserForum(
					$obj->getEntityUserId()
					,$obj
				)
			;
		}
	}

	/**
	 * @param $model
	 * @param $obj
	 * @param $parent_obj
	 */
	public function setLatestPosts($model, $obj, $parent_obj) {
		if ($model->getSize()) {
			foreach ($model as $post) {
				if (empty($this->latestPosts[$parent_obj->getId()])) {
					$this->___setLatestPost($obj, $post, $parent_obj);
				}
				else if ($post->getCreatedTime()) {
					if (strtotime ($this->latestPosts[$parent_obj->getId()]['created_time'])
						< strtotime ($post->getCreatedTime())
					) {
						$this->___setLatestPost($obj, $post, $parent_obj);
					}
				}
			}
		}
	}

	/**
	 * @param $obj
	 */
	public function setObject($obj) {
		$this->obj = $obj;
	}

	/**
	 * @param $userId
	 * @param $obj
	 * @param bool $storeId
	 */
	public function setUserForum($userId, $obj, $storeId = false) {
		if (!$this->getUserBySystemId($userId, $storeId)) {
			if ($storeId === false) {
				$storeId = Mage::app()->getStore()->getId();
			}
			$userModel = Df_Forum_Model_User::i();
			$userModel->setId(null)->setSystemUserId($userId)->setFirst_post(now())->setSystem_user_name($obj->getUserName())->setStoreId($storeId);
			$userModel->save();
		}
	}

	/**
	 * @param $_id
	 * @param $storeId
	 */
	public function updateTopicStoreId($_id, $storeId) {
		$m = Df_Forum_Model_Topic::i()->load($_id);
		if ($m->getId()) {
			$m->setStoreId($storeId);
			$m->save();
		}
	}

	/**
	 * @param string $id_path
	 * @param int $storeId
	 * @param string $request_path
	 * @param string $target_path
	 * @return Df_Forum_Helper_Topic
	 */
	public function updateUrlRewrite($id_path, $storeId, $request_path, $target_path) {
		$this->deleteUrlRewrite($id_path);
		/** @var Df_Core_Model_Url_Rewrite $model */
		$model = Df_Core_Model_Url_Rewrite::i();
		$model->setId(null);
		$model->setRequestPath($request_path);
		$model->setStoreId($storeId);
		$model->setTargetPath($target_path);
		$model->setIdPath($id_path);
		$model->save();
		return $this;
	}

	/**
	 * @param $key
	 * @param $id
	 * @param bool $storeId
	 * @return bool
	 */
	public function validateUrlKey($key, $id, $storeId = false) {
		/** @var bool $result */
		$result = true;
		if (!in_array($key, $this->_NOT_VALID_KEYS)) {
			if (!$storeId) {
				$storeId = Mage::app()->getStore()->getId();
			}
			$topics = Df_Forum_Model_Topic::c();
			$topics->getSelect()
				->where('? = url_text', df_trim($key))
				->where('? <> topic_id', $id)
			;
			if ($storeId) {
				$topics->getSelect()->where('store_id IN(?)', array(0, $storeId));
			}
			$result = (0 < $topics->getSize());
		}
		return $result;
	}

	/**
	 * @param $obj
	 */
	public function updateDateJoined($obj) {
		$c = Df_Forum_Model_Post::c();
		$c->setOrder('created_time', 'desc')->setPageSize(1);
		$user_id = $obj->getSystemUserId() == 10000000 ? 0 : $obj->getSystemUserId();
		$c->getSelect()
			->where('? = system_user_id', $user_id)
		;
		if ($c->getSize()) {
			foreach ($c as $val) {
				$date = $val->getCreatedTime();
				$m = Df_Forum_Model_User::i()->load($obj->getId());
				$m->setFirst_post($date);
				$m->save();
				break;
			}
		}
	}

	/**
	 * @param $obj
	 */
	public function updateTotalPosts($obj) {
		$c = Df_Forum_Model_Post::c();
		$user_id = $obj->getSystemUserId() == 10000000 ? 0 : $obj->getSystemUserId();
		$c->getSelect()
			->where('? = system_user_id', $user_id)
		;
		$all_posts = $c->getSize();
		$m = Df_Forum_Model_User::i()->load($obj->getId());
		$m->setTotalPosts($all_posts);
		$m->save();
	}

	/**
	 * @param $system_user_id
	 * @param bool $storeId
	 * @return void
	 */
	public function updateTotalPostsUser($system_user_id, $storeId = false) {
		if ($system_user_id) {
			$id = $this->getUserBySystemId($system_user_id, $storeId);
			$o = Df_Forum_Model_User::i()->load($id);
			if ($o->getId()) {
				$o->setTotalPosts(intval($o->getTotalPosts() + 1));
				$o->save();
			}
		}
	}

	/**
	 * @param string $id_path
	 * @param string $request_path
	 * @return Df_Forum_Helper_Topic
	 */
	public function updateUrlRewriteRequestPath($id_path, $request_path) {
		/** @var Df_Core_Model_Url_Rewrite $rewrite */
		$rewrite = Df_Core_Model_Url_Rewrite::i();
		$rewrite->load($id_path, 'id_path');
		if ($rewrite->getId()) {
			$rewrite->setRequestPath($request_path);
			$rewrite->save();
		}
		return $this;
	}

	/**
	 * @param $id
	 */
	public function updateEnitityView($id) {
		$all = Df_Forum_Model_Entity::i()->load($id, 'entity_id')->getTotalViews();
		$all++;
		$entityModel = Df_Forum_Model_Entity::i();
		$entityModel->setId($this->getEntityId($id))->setTotalViews($all);
		$entityModel->save();
	}

	/**
	 * @param $id
	 * @param $result
	 * @return mixed
	 */
	private function addTopicsBreadcrumbsParent($id, $result) {
		$model = Df_Forum_Model_Topic::i();
		$data = $model->load($id);
		if ($data->getTitle() && $data->getId()) {
			if ($data->getParentId()) {
				$result = $this->addTopicsBreadcrumbsParent($data->getParentId(), $result);
			}
			$result['topic_' . $data->getId()] = array(
				'label' => $data->getTitle(),'link' => $this->getObject()->getViewUrl($data->getId(), $data)
			);
		}
		return $result;
	}

	/**
	 * @param $obj
	 * @param Df_Forum_Model_Post $post
	 * @param $parent_obj
	 * @return $this
	 */
	private function ___setLatestPost($obj, Df_Forum_Model_Post $post, $parent_obj) {
		/** @var int $parentId */
		$parentId = $parent_obj->getId();
		$this->latestPosts[$parentId] =
			array_merge(
				df_a($this->latestPosts, $parentId, array())
				,array(
					Df_Forum_Model_Post::P__CREATED_TIME => $post->getCreatedTime()
					,'topic_obj' => $obj
					,Df_Forum_Model_Post::P__USER_NAME => $post->getUserName()
					,Df_Forum_Model_Post::P__USER_NICK => $post->getUserNick()
					,Df_Forum_Model_Post::P__SYSTEM_USER_ID => $post->getSystemUserId()
					,Df_Forum_Model_Post::P__ID => $post->getId()
					,'obj' => $post
				)
			)
		;
		return $this;
	}

	/**
	 * @param $arr
	 * @param $result
	 * @param int $depth
	 * @return array
	 */
	private function addTopicsRecursive($arr, $result, $depth = 0) {
		if (is_array($arr) && count($arr) > 0) {
			foreach ($arr as $val) {
				if (
						intval($val[Df_Forum_Model_Topic::P__ID]) === intval($this->skip_id)
					||
						(
								$this->subtopics_parent_only
							&&
								!rm_bool(
									$val[Df_Forum_Model_Topic::P__IS_CATEGORY]
								)
							&&
								!rm_bool($val[Df_Forum_Model_Topic::P__HAS_SUBTOPIC])
						)
				) {
					continue;
				}
				$nbsp = str_pad ("", intval((3 * ($depth / 10)) * 4), " - ", STR_PAD_LEFT);
				$result[]=
					array(
						'value' => intval($val[Df_Forum_Model_Topic::P__ID])
						,'label' => $nbsp . $val['title']
						,'label_nbsp' => $nbsp . $val['title']
						,'style' =>
							'margin-left:'
							. intval($depth)
							. 'px;'
							. ($depth == 0 ? 'font-weight:bold;' : '')
						,'url_text' => $val['url_text']
					)
				;
				if (
						!empty($val['_childs'])
					&&
						(
								rm_bool(
									$val[Df_Forum_Model_Topic::P__IS_CATEGORY]
								)
							||
								rm_bool(
									$val[Df_Forum_Model_Topic::P__HAS_SUBTOPIC]
								)
						)
				) {
					if (is_array($val['_childs']) && count($val['_childs']) > 0) {
						$result = $this->addTopicsRecursive($val['_childs'], $result, $depth + 10);
					}
				}
			}
		}
		return $result;
	}

	/**
	 * @param mixed[] $arr
	 * @param array[] $result
	 * @return array[]
	 */
	private function addTopicsRecursiveArr(array $arr, $result = array()) {
		if (is_array($arr) && count($arr) > 0) {
			foreach ($arr as $val) {
				if (
							intval($val[Df_Forum_Model_Topic::P__ID])
						===
							intval($this->skip_id)
					||
						(
								$this->subtopics_parent_only
							&&
								!rm_bool(
									$val[Df_Forum_Model_Topic::P__IS_CATEGORY]
								)
							&&
								!rm_bool($val[Df_Forum_Model_Topic::P__HAS_SUBTOPIC])
						)
				) {
					continue;
				}
				if (
						rm_bool(
							$val[Df_Forum_Model_Topic::P__IS_CATEGORY]
						)
					&&
						is_array($val['_childs'])
					&&
						count($val['_childs']) > 0
				) {
					$result[]=
						array(
							'value' =>
								is_array(df_a($val, '_childs'))
								? $this->addTopicsRecursiveArr($val['_childs'])
								: array()
							,'label' => $val['title']
							,'style' => 'font-weight:bold;'
							,'url_text' => $val['url_text']
						)
					;
				}
				else if (
					!rm_bool(
						$val[Df_Forum_Model_Topic::P__IS_CATEGORY]
					)
				) {
					$result[]=
						array(
							'value' => intval($val[Df_Forum_Model_Topic::P__ID])
							,'label' => $val['title']
							,'url_text' => $val['url_text']
						)
					;
				}
			}
		}
		return $result;
	}

	/** @var bool */
	private $subtopics_parent_only;

	const _CLASS = __CLASS__;
	private $url_all_topics = 'forum/';
	private $skip_id = 0;
	private $_NOT_VALID_KEYS = array('df_forum/topic');
	private $pages_limits
		= array(
			array(
				5,10,15
			),array(
				10,15,30
			),array(
				20,30,50
			)
		);
	public $_isActive = false;
	public $breadCrumbBlock = false;
	public $obj;
	public $latestPosts = array();
	private $editor_locales
		= array(
			'en','de','fr','cz','nl','pl','it','ja','pt','sv'
		);

	/** @return Df_Forum_Helper_Topic */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}