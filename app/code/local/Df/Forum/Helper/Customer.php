<?php
class Df_Forum_Helper_Customer extends Mage_Core_Helper_Abstract {
	/**
	 * @param bool $customer
	 * @return string
	 */
	public function getCustomerUrl($customer = false) {
		/** @var string $result */
		$result = '';
		if (is_object($customer)) {
			$id = $customer->getId();
		}
		else {
			$id = $customer ? $customer : Df_Forum_Model_Action::ADMIN__ID;
		}
		if ($id) {
			if ($id == Df_Forum_Model_Action::ADMIN__ID) {
				$id = Df_Forum_Model_Action::ADMIN__NAME;
			}
			$result =
				$this->_getUrl(
					'df_forum/customer'
					,array(
						Df_Forum_Model_Action::REQUEST_PARAM__CUSTOMER_ID => $id
					)
				)
			;
		}
		return $result;
	}

	/**
	 * @param $id
	 * @param $nick
	 * @param bool $class
	 * @return string
	 */
	public function getCustomerLink($id, $nick, $class = false) {
		return
			Df_Core_Model_Output_Html_A::i(array(
				Df_Core_Model_Output_Html_A::P__HREF => $this->getCustomerUrl($id)
				,Df_Core_Model_Output_Html_A::P__CLASSES => array($class)
				,Df_Core_Model_Output_Html_A::P__ANCHOR => $nick
			))->__toString()
		;
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function getCustomerPMUrl($id) {
		return
			$this->_getUrl(
				'df_forum/myprivatemessages/add'
				,array(
					Df_Forum_Model_Action::REQUEST_PARAM__CUSTOMER_ID => $id
				)
			)
		;
	}

	/**
	 * @param int $userId
	 * @return string
	 */
	public function getEmail($userId) {
		return Df_Customer_Model_Customer::ld($userId)->getEmail();
	}

	/**
	 * @param $system_user_id
	 * @return string
	 */
	public function getCustomerNick($system_user_id) {
		/** @var string $result */
		$result = '';
		$system_user_id = $system_user_id ? $system_user_id : Df_Forum_Model_Action::ADMIN__ID;
		$usersettings =
			Df_Forum_Model_Usersettings::i()->load(
				$system_user_id
				,Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
			)
		;
		if ($usersettings->getId()) {
			$result = $usersettings->getNickname();
		}
		else {
			/** @var Df_Customer_Model_Customer $customer */
			$customer = Df_Customer_Model_Customer::ld($system_user_id);
			$result =
				implode(
					' '
					,array($customer->getNameFirst(), $customer->getNameLast())
				)
			;
		}
		return $result;
	}

	/**
	 * @param $nick
	 * @param $email_to
	 * @param $subject
	 */
	public function notifyCustomerPrivateMessage($nick, $email_to, $subject) {
		$obj = new Varien_Object();
		$data['url'] = $this->_getUrl('df_forum/myprivatemessages/index');
		$data['nickname'] = $nick;
		$data['subject'] = $subject;
		$name = 'Личное сообщение на форуме';
		$obj->setData($data);
		$title = 'Личное сообщение на форуме';
		$this
			->sendEmail(
				$obj
				,df_cfg()->forum()->notification()->moderator()->getEmailTemplate()
				,df_cfg()->forum()->notification()->moderator()->getEmailSenderName()
				,$email_to
				,$name
				,$title
			)
		;
	}

	/**
	 * @param $postObject
	 * @param $template_id
	 * @param $sender
	 * @param $email_to
	 * @param $name
	 * @param $title
	 */
	private function sendEmail(
		$postObject
		,$template_id
		,$sender
		,$email_to
		,$name = ''
		,$title = ''
	) {
		/** @var Df_Core_Model_Email_Template $mailTemplate */
		$mailTemplate = Df_Core_Model_Email_Template::i();
		if ($title != '') {
			$mailTemplate->setTemplateSubject($title);
		}
		$translate = Df_Core_Model_Translate::s();
		$translate->setTranslateInline(false);
		try {
			$mailTemplate->setDesignConfig(array(
					'area' => 'frontend','type' => 'html'
				)
			)->sendTransactional($template_id, $sender, $email_to, $name, array('data' => $postObject)
			);
			$translate->setTranslateInline(true);
		}
		catch(Exception $e) {
			$translate->setTranslateInline(true);
		}
	}

	/** @return Df_Forum_Helper_Customer */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}