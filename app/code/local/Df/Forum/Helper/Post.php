<?php
class Df_Forum_Helper_Post extends Mage_Core_Helper_Abstract {
	/**
	 * @param $_post
	 * @return mixed
	 */
	public function preparePostBySearchValue($_post) {
		$search_val = Df_Forum_Model_Session::s()->getSearchValue();
		$pattern = '#(?!<.*)(?<!\w)(\w*)(' . $search_val . ')(\w*)(?!\w|[^<>]*(?:</s(?:cript|tyle))?>)#is';
		return
			preg_replace(
				$pattern
				,'$1' . $this->search_block_begin . '$2' . $this->search_block_end . '$3'
				,$_post
			)
		;
	}
	private $search_block_begin = '<span class="forum-search-block-selected">';
	private $search_block_end = '</span>';

	/** @return Df_Forum_Helper_Post */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}