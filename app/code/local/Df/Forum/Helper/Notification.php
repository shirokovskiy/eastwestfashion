<?php
class Df_Forum_Helper_Notification extends Mage_Core_Helper_Abstract {
	/**
	 * @param $data
	 * @param $url
	 * @param $user
	 */
	public function notifyModerNewPost($data, $url, $user) {
		$obj = new Varien_Object();
		if (!empty($data['Post'])) {
			$data['post_orig'] = $data['Post'];
		}
		else if (!empty($data['post_id'])) {
			$data['post_orig'] = $this->loadPost($data['post_id']);
		}
		$data['url'] = Mage::getUrl('df_forum/topic/viewreply/id/' . $data['post_id'], array(
				'_current' => false,'_escape' => false,'_use_rewrite' => false
			)
		);
		$data['forum_name'] =
			$this->getObjectName(
				$data[Df_Forum_Model_Post::P__PARENT_ID]
			)
		;
		$data['topic_name'] = $this->getObjectName($data['id']);
		$data['now'] = date('Y-m-d H:i:s');
		$data['posted_by'] = $user;
		$name = 'Новый ответ на форуме';
		$obj->setData($data);
		$title = 'Новый ответ на форуме ' . df_mage()->core()->url()->getHomeUrl();
		$this
			->sendEmail(
				$obj
				,df_cfg()->forum()->notification()->moderator()->getEmailTemplate()
				,df_cfg()->forum()->notification()->moderator()->getEmailSenderName()
				,df_cfg()->forum()->notification()->moderator()->getEmail()
				,$name
				,$title
			)
		;
	}

	/**
	 * @param $data
	 * @param $url
	 * @param $user
	 */
	public function notifyModerNewTopic($data, $url, $user) {
		$obj = new Varien_Object();
		if (!empty($data['Post'])) {
			$data['post_orig'] = $data['Post'];
		}
		else if (!empty($data['post_id'])) {
			$data['post_orig'] = $this->loadPost($data['post_id']);
		}
		$data['url'] = Mage::getUrl($url, array(
				'_current' => false,'_escape' => false,'_use_rewrite' => false,'_query' => array(
					self::PAGE_VAR_NAME => 1,self::SORT_VAR_NAME => 1
				)
			)
		);
		$data['forum_name'] =
			$this->getObjectName(
				$data[Df_Forum_Model_Topic::P__PARENT_ID]
			)
		;
		$data['topic_name'] = $this->getObjectName($data['id']);
		$data['now'] = date('Y-m-d H:i:s');
		$data['posted_by'] = $user;
		$obj->setData($data);
		$title = 'Новая тема на форуме ' . df_mage()->core()->url()->getHomeUrl();
		$name = 'Новая тема на форуме';
		$this
			->sendEmail(
				$obj
				,df_cfg()->forum()->notification()->moderator()->getEmailTemplate()
				,df_cfg()->forum()->notification()->moderator()->getEmailSenderName()
				,df_cfg()->forum()->notification()->moderator()->getEmail()
				,$name
				,$title
			)
		;
	}

	/**
	 * @param $data
	 */
	public function notifyCustomersPost($data) {
		$topic_id = $data['id'];
		$c = $this->getByTopic($topic_id);
		if ($c) {
			try {
				if (!empty($data['Post'])) {
					$data['post_orig'] = $data['Post'];
				}
				else if (!empty($data['post_id'])) {
					$data['post_orig'] = $this->loadPost($data['post_id']);
				}
				$user = $this->loadUser($data['post_id']);
				$data['url'] = Mage::getUrl('df_forum/topic/viewreply/id/' . $data['post_id'], array(
						'_current' => false,'_escape' => false,'_use_rewrite' => false
					)
				);
				$data['forum_name'] =
					$this->getObjectName(
						$data[Df_Forum_Model_Post::P__PARENT_ID]
					)
				;
				$data['topic_name'] = $this->getObjectName($data['id']);
				$data['now'] = date('Y-m-d H:i:s');
				$data['posted_by'] = $user;
				$title = 'Новое сообщение на форуме ' . df_mage()->core()->urlSingleton()->getHomeUrl();
				$name = 'Новое сообщение на форуме';
				foreach ($c as $val) {
					if ($val->getSystemUserId() == $data[Df_Forum_Model_Post::P__SYSTEM_USER_ID]) {
						continue;
					}
					$email_to = $val->getSystemUserEmail();
					$data['unsubscribe_link'] = $this->getUnsubscribeLink($val);
					$obj = new Varien_Object();
					$obj->setData($data);
					$this
						->sendEmail(
							$obj
							,df_cfg()->forum()->notification()->moderator()->getEmailTemplate()
							,df_cfg()->forum()->notification()->moderator()->getEmailSenderName()
							,$email_to
							,$name
							,$title
						)
					;
				}
			}
			catch(Exception $e) {
			}
		}
	}

	/**
	 * @param $topic_id
	 */
	public function addToNotifyList($topic_id) {
		$email = rm_session_customer()->getCustomer()->getEmail();
		$id = rm_session_customer()->getCustomer()->getId();
		$hash = md5 ('notify_' . $email);
		if (!$this->checkIsNotified($topic_id, $id)) {
			$m = Df_Forum_Model_Notification::i();
			$m->setId(null)->setHash($hash)->setSystemUserId($id)->setSystemUserEmail($email)->setTopicId($topic_id);
			$m->save();
		}
	}

	/**
	 * @param $topic_id
	 * @return bool
	 */
	public function removeFromNotifyList($topic_id) {
		/** @var bool $result */
		$result = false;
		$email = rm_session_customer()->getCustomer()->getEmail();
		$id = rm_session_customer()->getCustomer()->getId();
		$hash = md5 ('notify_' . $email);
		if ($this->checkIsNotified($topic_id, $id)) {
			$c = Df_Forum_Model_Notification::c();
			$c->getSelect()
				->where('? = system_user_id', $id)
				->where('? = topic_id', $topic_id)
			;
			if ($c->getSize()) {
				foreach ($c as $obj) {
					$del = Df_Forum_Model_Notification::i()->load($obj->getId());
					$del->delete();
				}
				$result = true;
			}
		}
		return $result;
	}

	/**
	 * @param $topic_id
	 * @param $customer_id
	 * @return bool
	 */
	public function getIsNotified($topic_id, $customer_id) {
		return $this->checkIsNotified($topic_id, $customer_id);
	}

	/**
	 * @param $topic_id
	 * @param $hash
	 * @return bool
	 */
	public function deleteByHash($topic_id, $hash) {
		/** @var bool $result */
		$result = false;
		/** @var Df_Forum_Model_Resource_Notification_Collection $c */
		$c = Df_Forum_Model_Notification::c();
		$c->getSelect()
			->where('? = hash', $hash)
			->where('? = topic_id', $topic_id)
		;
		if ($c->getSize()) {
			foreach ($c as $val) {
				$c = Df_Forum_Model_Notification::i()->load($val->getId());
				$c->delete();
			}
			$result = $val;
		}
		return $result;
	}

	/**
	 * @param $id
	 * @return string
	 */
	private function loadUser($id) {
		$result = '';
		/** @var Df_Forum_Model_Post $post */
		$post = Df_Forum_Model_Post::i()->load($id);
		if ($post->getId()) {
			$result = $post->getUserNick() ? $post->getUserNick() : $post->getUserName();
		}
		return $result;
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	private function loadPost($id) {
		/** @var Df_Forum_Model_Post $post */
		$post = Df_Forum_Model_Post::i()->load($id);
		return
			$post->getId()
			? $post->getPostOrig()
			: ''
		;
	}

	/**
	 * @param $obj
	 * @return string
	 */
	private function getUnsubscribeLink($obj) {
		return
			Mage::getUrl(
				df_concat_url(
					'df_forum/topic/unsubscribe/topic_id'
					, $obj->getTopicId()
					, 'h'
					, $obj->getHash()
				)
			)
		;
	}

	/**
	 * @param $topic_id
	 * @return Df_Forum_Model_Resource_Notification_Collection|null
	 */
	private function getByTopic($topic_id) {
		$c = Df_Forum_Model_Notification::c();
		$c->getSelect()->where('? = topic_id', $topic_id);
		return
			$c->getSize()
			? $c
			: null
		;
	}

	/**
	 * @param $topic_id
	 * @param $customer_id
	 * @return bool
	 */
	private function checkIsNotified($topic_id, $customer_id) {
		$c = Df_Forum_Model_Notification::c();
		$c->getSelect()
			->where('? = system_user_id', $customer_id)
			->where('? = topic_id', $topic_id)
		;
		return 0 < $c->getSize();
	}

	/**
	 * @param $id
	 * @return string|null
	 */
	private function getObjectName($id) {
		$data = Df_Forum_Model_Topic::i()->load($id);
		return
			$data->getId()
			? $data->getTitle()
			: null
		;
	}

	/**
	 * @param $postObject
	 * @param $template_id
	 * @param $sender
	 * @param $email_to
	 * @param $name
	 * @param $title
	 */
	private function sendEmail(
		$postObject
		,$template_id
		,$sender
		,$email_to
		,$name = ''
		,$title = ''
	) {
		/** @var Df_Core_Model_Email_Template $mailTemplate */
		$mailTemplate = Df_Core_Model_Email_Template::i();
		if ($title != '') {
			$mailTemplate->setTemplateSubject($title);
		}
		$translate = Df_Core_Model_Translate::s();
		$postObject = $this->preparePostObject($postObject);
		$translate->setTranslateInline(false);
		try {
			$mailTemplate->setDesignConfig(array(
					'area' => 'frontend','type' => 'html'
				)
			)->sendTransactional($template_id, $sender, $email_to, $name, array('data' => $postObject)
			);
			$translate->setTranslateInline(true);
		}
		catch(Exception $e) {
			$translate->setTranslateInline(true);
		}
	}

	/**
	 * @param array $obj
	 * @return array
	 */
	private function preparePostObject($obj = array()) {
		if (!empty($obj['post_orig'])) {
			$obj['post_orig'] =
				str_replace(
					'src="/'
					,df_concat('src="', Mage::app()->getStore()->getBaseUrl())
					, $obj['post_orig']
				)
			;
		}
		return $obj;
	}

	const PAGE_VAR_NAME = 'p';
	const SORT_VAR_NAME = 'sort';

	/** @return Df_Forum_Helper_Notification */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}