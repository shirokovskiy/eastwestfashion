<?php
class Df_Forum_Helper_Sitemap extends Mage_Core_Helper_Abstract {
	/**
	 * @param $url_begin
	 * @param $store_code
	 * @param $o
	 * @param $limit
	 * @param $page
	 * @return string
	 */
	public function __getUrl($url_begin, $store_code, $o, $limit, $page) {
		return
			$url_begin
			. $o->getUrlText()
			. '?'
			. self::STORE_VAR_NAME
			. '=' . $store_code
			. '&'
			. self::PAGE_VAR_NAME
			. '='
			. $page
			. '&'
			. self::LIMIT_VAR_NAME
			. '='
			. $limit
			. '&'
			. self::SORT_VAR_NAME
			. '=1'
		;
	}

	const LIMIT_VAR_NAME = 'limit';
	const PAGE_VAR_NAME = 'p';
	const SORT_VAR_NAME = 'sort';
	const STORE_VAR_NAME = '___store';

	/** @return Df_Forum_Helper_Sitemap */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}