<?php
class Df_Forum_Helper_Data extends Mage_Core_Helper_Abstract {
	/** @return Df_Forum_Helper_Customer */
	public function customer() {
		return Df_Forum_Helper_Customer::s();
	}

	/** @return Df_Forum_Helper_Forum */
	public function forum() {
		return Df_Forum_Helper_Forum::s();
	}

	/** @return array */
	public function getForumIcons() {
		return array (
			'ids' => $this->forum_icons_id
			,'img' => $this->forum_icons_img
			,'key' => $this->forum_icons_key_lang
		);
	}

	/**
	 * @param bool $no_none[optional]
	 * @return array[]
	 */
	public function getForumIconsRadios($no_none = false) {
		$icons = $this->getForumIcons();
		$result = array();
		if (!$no_none) {
			$arr
				= array(
					'label' => '&nbsp;отсутствует'
					,'value' => ''
				)
			;
			$ret[]= $arr;
		}
		if (is_array($icons['ids']) && !empty($icons['ids'])) {
			foreach ($icons['ids'] as $id) {
				if (!empty($icons['img'][$id]) && !empty($icons['key'][$id])) {
					$arr =
						array(
							'label' =>
								'<nobr>&nbsp;&nbsp;<img src="'
								. $this->getIconImg($id) . '" class="'
								. self::FORUM_ICONS_SMALL_CLASS . '"> '
								. $icons['key'][$id]
								. '</nobr>'
							,'value' => '' . $id
						)
					;
					$result[]= $arr;
				}
			}
		}
		return $result;
	}

	/**
	 * @param null $id
	 * @return string
	 */
	public function getIconImg($id = null) {
		$result = '';
		if ($id) {
			if (!empty($this->forum_icons_img[$id])) {
				$result =
					Mage::getDesign()->getSkinUrl(
						self::FORUM_ICONS_PATH . $this->forum_icons_img[$id]
						,array('_area' => 'frontend')
					)
				;
			}
		}
		return $result;
	}

	/** @return string */
	public function getLayout() {
		return df_cfg()->forum()->design()->getLayout();
	}

	/**
	 * @param null $id
	 * @return string
	 */
	public function getIconImgFront($id = null) {
		$result = '';
		if ($id) {
			if (!empty($this->forum_icons_img[$id])) {
				$result = self::FORUM_ICONS_PATH . $this->forum_icons_img[$id];
			}
		}
		return $result;
	}

	/**
	 * @param null $id
	 * @return string
	 */
	public function getIconImgFrontMedium($id = null) {
		/** @var string $result */
		$result = '';
		if ($id) {
			if (!empty($this->forum_icons_img[$id])) {
				$result = self::FORUM_ICONS_PATH_MEDIUM . $this->forum_icons_img[$id];
			}
		}
		return $result;
	}

	/**
	 * @param null $id
	 * @return string
	 */
	public function getIconImgMedium($id = null) {
		/** @var string $result */
		$result = '';
		if ($id) {
			if (!empty($this->forum_icons_img[$id])) {
				$result = Mage::getDesign()->getSkinUrl(self::FORUM_ICONS_PATH_MEDIUM . $this->forum_icons_img[$id]);
			}
		}
		return $result;
	}

	/** @return int */
	public function getQuantityPostsHotTopic() {
		return $this->quantity_posts_hot_topic;
	}

	/** @return Df_Forum_Helper_Notification */
	public function notification() {
		return Df_Forum_Helper_Notification::s();
	}

	/** @return Df_Forum_Helper_Post */
	public function post() {
		return Df_Forum_Helper_Post::s();
	}

	/** @return Df_Forum_Helper_Sitemap */
	public function sitemap() {
		return Df_Forum_Helper_Sitemap::s();
	}

	/** @return Df_Forum_Helper_Topic */
	public function topic() {
		return Df_Forum_Helper_Topic::s();
	}

	/** @return Df_Forum_Helper_Viewreply */
	public function viewreply() {
		return Df_Forum_Helper_Viewreply::s();
	}

	/** @var int */
	private $quantity_posts_hot_topic = 5;

	/** @var string[] */
	private $forum_icons_id =
		array(
			'smile','wink','kiss','saint','evil','question','cry','devil','shame'
			,'disappointed','smart','laughter','monkey','sad','accept','cancel'
		)
	;

	/** @var string[] */
	private $forum_icons_img =
		array(
			'accept' => 'accept.png','cancel' => 'cancel.png','saint' => 'saint.png'
			,'evil' => 'evil.png','question' => 'question.png','cry' => 'cry.png'
			,'devil' => 'devil.png','shame' => 'shame.png','disappointed' => 'disappointed.png'
			,'smart' => 'smart.png','laughter' => 'laughter.png','smile' => 'smile.png'
			,'monkey' => 'monkey.png','sad' => 'sad.png','kiss' => 'kiss.png','wink' => 'wink.png'
		)
	;

	/** @var string[] */
	private $forum_icons_key_lang
		= array(
			'accept' => 'удовлетворённый'
			,'cancel' => 'неудовлетворённый'
			,'saint' => 'святой'
			,'evil' => 'злой'
			,'question' => 'вопрос'
			,'cry' => 'плачущий'
			,'devil' => 'дьявол'
			,'shame' => 'стыдливый'
			,'disappointed' => 'разочарованный'
			,'smart' => 'умный'
			,'laughter' => 'смеющийся'
			,'smile' => 'улыбающийся'
			,'monkey' => 'обезьяна'
			,'sad' => 'раздосаженный'
			,'kiss' => 'целующий'
			,'wink' => 'подмигивающий'
		)
	;
	const FORUM_ICONS_PATH = 'df/images/forum/icons/';
	const FORUM_ICONS_PATH_MEDIUM = 'df/images/forum/icons_medium/';
	const FORUM_ICONS_SMALL_CLASS = 'df-forum-icons-small';

	/** @return Df_Forum_Helper_Data */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}