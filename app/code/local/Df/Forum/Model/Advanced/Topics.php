<?php
class Df_Forum_Model_Advanced_Topics extends Df_Core_Model_Abstract {
	/** @return array */
	public function toOptionArray() {
		$result = array();
		if (is_array($this->_options)) {
			foreach ($this->_options as $val) {
				$result[]= array(
					'value' => (string)$val,'label' => (string)$val
				);
			}
		}
		return $result;
	}
	private $_options
		= array(
			1,2,3,4,5,6,7,8,9,10
		);
	const _CLASS = __CLASS__;
}