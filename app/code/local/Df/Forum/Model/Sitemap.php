<?php
class Df_Forum_Model_Sitemap extends Df_Core_Model_Abstract {
	/** @return Df_Forum_Model_Sitemap */
	public function ___generateXml() {
		if (df_cfg()->forum()->sitemap()->isEnabled() && $this->checkUpdate()) {
			try {
				$this->initCollection();
				$io = new Varien_Io_File();
				$io->setAllowCreateFolders(true);
				$io->open(array('path' => df_cfg()->forum()->sitemap()->getFilePath()));
				$io->streamOpen(df_cfg()->forum()->sitemap()->getFileName());
				$io->streamWrite('<?xml version="1.0" encoding="UTF-8"?>' . "\n");
				$io->streamWrite('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
				$date = Mage::getSingleton('core/date')->gmtDate('Y-m-d');
				/**
				 * Generate forum pages sitemap
				 */
				$changefreq = (string)Mage::getStoreConfig('sitemap/df_forum/changefreq');
				$priority = (string)Mage::getStoreConfig('sitemap/df_forum/priority');
				if (is_array($this->collection)) {
					foreach ($this->collection as $item) {
						$xml
							= rm_sprintf('<url><loc>%s</loc><lastmod>%s</lastmod><changefreq>%s</changefreq><priority>%.1f</priority></url>', htmlspecialchars ($item->getData('url')), $date, $changefreq, $priority
						);
						$io->streamWrite($xml);
					}
					unset($this->collection);
					$io->streamWrite('</urlset>');
					$io->streamClose();
					$this->___setSitemapTime();
				}
			}
			catch(Exception $e) {
				rm_exception_to_session($e);
			}
		}
		return $this;
	}

	/** @return void */
	public function ___initialize() {
		$this->___generateXml();
	}

	/** @return mixed */
	private function ___getSitemapTime() {
		return Mage::getStoreConfig('df_forum/sitemap/update');
	}

	/** @return void */
	private function ___setSitemapTime() {
		if (!Mage::getStoreConfig('df_forum/sitemap/update')) {
			/** @var Df_Core_Model_Config_Data $configData */
			$configData = Df_Core_Model_Config_Data::i();
			$configData
				->setScope('default')
				->setPath('df_forum/sitemap/update')
				->setValue(time())
				->save()
			;
		}
		else {
			/** @var Mage_Core_Model_Config $config */
			$config = df_model('core/config');
			$config
				->saveConfig(
					'df_forum/sitemap/update'
					,time()
				)
			;
		}
	}

	/** @return bool */
	private function checkUpdate() {
		return time() >= intval($this->___getSitemapTime() + $this->getPeriod());
	}

	/** @return array */
	private function getAvaiableStores() {
		return Mage::app()->getStores();
	}

	/**
	 * @param int|null $_id
	 * @param bool $storeId
	 * @return bool|Df_Forum_Model_Resource_Topic_Collection
	 */
	private function getObjectsByParentId($_id, $storeId = false) {
		$topics = Df_Forum_Model_Topic::c();
		$topics->getSelect()->where('? = status', 1);
		if (!$_id) {
			$topics->getSelect()->where('parent_id is null');
		}
		else {
			$topics->getSelect()->where('? = parent_id', $_id);
		}
		if ($storeId) {
			$topics->addStoreFilter($storeId);
		}
		return
			0 < $topics->getSize()
			? $topics
			: false
		;
	}

	/**
	 * @param int|null $topic_id
	 * @return int
	 */
	private function getPagesQuantity($topic_id) {
		/** @var int $result */
		$result = 0;
		$c = Df_Forum_Model_Post::c();
		$c->getSelect()->where('? = status', 1);
		if (!$topic_id) {
			$c->getSelect()->where('parent_id is null');
		}
		else {
			$c->getSelect()->where('? = parent_id', $topic_id);
		}
		if ($c->getSize()) {
			$size = $c->getSize();
			$result = intval(ceil($size / $this->limit));
		}
		return $result;
	}

	/** @return mixed */
	private function getPeriod() {
		return Mage::getStoreConfig('df_forum/sitemap/periodcreation') + 3600;
	}

	/** @return void */
	private function initCollection() {
		$stores = $this->getAvaiableStores();
		if ($stores && is_array($stores)) {
			foreach ($stores as $store) {
				$this->current_store = $store->getId();
				$forums = $this->getObjectsByParentId(0, $this->current_store);
				if ($forums) {
					foreach ($forums as $val) {
						$topics = $this->getObjectsByParentId($val->getId());
						$this->setTopics($topics);
					}
				}
			}
		}
	}

	/**
	 * @param $collection
	 */
	private function setTopics($collection) {
		if ($collection) {
			$store = Mage::app()->getStore($this->current_store);
			$url_begin = $store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
			$code = $store->getCode();
			foreach ($collection as $val) {
				if ($val->hasSubTopics() == 1) {
					$col = $this->getObjectsByParentId($val->getId(), $this->current_store);
					if ($col) {
						$this->setTopics($col);
					}
				}
				else {
					$all = $this->getPagesQuantity($val->getId());
					if ($all) {
						while ($all) {
							$array = $this->collection;
							$array[$val->getId() . '_' . $all . '_' . $this->current_store] = new Varien_Object;
							$data =
								array(
									'url' =>
										df_h()->forum()->sitemap()->__getUrl(
											$url_begin
											, $code
											, $val
											, $this->limit
											, $all
										)
									,'store_id' => $this->current_store
								)
							;
							$array[$val->getId() . '_' . $all . '_' . $this->current_store]->setData($data);
							$this->collection = $array;
							$all--;
						}
					}
				}
			}
		}
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		$this->_init('df_forum/topic');
		parent::_construct();
	}

	const _CLASS = __CLASS__;
	private $limit = 5;
	private $collection = false;
	private $current_store = false;
}