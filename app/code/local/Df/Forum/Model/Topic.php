<?php
class Df_Forum_Model_Topic extends Df_Core_Model_Abstract {
	/** @return string|null */
	public function getCreatedTime() {return $this->cfg(self::P__CREATED_TIME);}

	/** @return string|null */
	public function getDescription() {return $this->cfg(self::P__DESCRIPTION);}

	/** @return int */
	public function getEntityUserId() {return intval($this->cfg(self::P__ENTITY_USER_ID));}

	/** @return string|null */
	public function getIconId() {return $this->cfg(self::P__ICON_ID);}

	/** @return string|null */
	public function getMetaDescription() {return $this->cfg(self::P__META_DESCRIPTION);}

	/** @return string|null */
	public function getMetaKeywords() {return $this->cfg(self::P__META_KEYWORDS);}

	/** @return Df_Forum_Model_Topic|null */
	public function getParent() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_Forum_Model_Topic|null $result */
			$result = null;
			if ($this->getParentId()) {
				$result = self::i()->load($this->getParentId());
				if (!$result->getId()) {
					$result = null;
				}
			}
			$this->{__METHOD__} = rm_n_set($result);
		}
		return rm_n_get($this->{__METHOD__});
	}

	/** @return int|null */
	public function getParentId() {return rm_empty_to_null($this->cfg(self::P__PARENT_ID));}

	/** @return string|null */
	public function getPriority() {return $this->cfg(self::P__PRIORITY);}

	/** @return string|null */
	public function getProductId() {return $this->cfg(self::P__PRODUCT_ID);}
	
	/** @return Df_Forum_Model_Topic */
	public function getProgenitor() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_Forum_Model_Topic $result */
			$result = $this;
			while (!is_null($result->getParent())) {
				$result = $result->getParent();
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return bool */
	public function getStatus() {return rm_bool($this->cfg(self::P__STATUS));}

	/** @return string|null */
	public function getStoreId() {return $this->cfg(self::P__STORE_ID);}

	/** @return string|null */
	public function getSystemUserId() {return $this->cfg(self::P__SYSTEM_USER_ID);}

	/** @return string|null */
	public function getTitle() {return $this->cfg(self::P__TITLE);}

	/** @return string|null */
	public function getUpdateTime() {return $this->cfg(self::P__UPDATE_TIME);}

	/** @return string|null */
	public function getUrlText() {return $this->cfg(self::P__URL_TEXT);}

	/** @return string|null */
	public function getUrlTextShort() {return $this->cfg(self::P__URL_TEXT_SHORT);}

	/** @return string|null */
	public function getUserName() {return $this->cfg(self::P__USER_NAME);}

	/** @return string|null */
	public function getUserNick() {return $this->cfg(self::P__USER_NICK);}

	/** @return bool */
	public function hasSubTopics() {return rm_bool($this->cfg(self::P__HAS_SUBTOPIC));}

	/** @return bool */
	public function isCategory() {return rm_bool($this->cfg(self::P__IS_CATEGORY));}

	/** @return bool */
	public function isProgenitor() {return intval($this->getId()) !== intval($this->getProgenitor()->getId());}

	/** @return bool */
	public function isSubTopic() {return rm_bool($this->cfg(self::P__IS_SUBTOPIC));}

	/**
	 * @param bool $value
	 * @return Df_Forum_Model_Topic
	 */
	public function markAsCategory($value) {
		df_param_boolean($value, 0);
		$this->setData(self::P__IS_CATEGORY, $value);
		return $this;
	}

	/**
	 * @param bool $value
	 * @return Df_Forum_Model_Topic
	 */
	public function markAsHavingSubTopic($value) {
		df_param_boolean($value, 0);
		$this->setData(self::P__HAS_SUBTOPIC, $value);
		return $this;
	}

	/**
	 * @param bool $value
	 * @return Df_Forum_Model_Topic
	 */
	public function markAsSubTopic($value) {
		df_param_boolean($value, 0);
		$this->setData(self::P__IS_SUBTOPIC, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Topic
	 * @see now()
	 */
	public function setCreatedTime($value) {
		df_param_string($value, 0);
		$this->setData(self::P__CREATED_TIME, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setDescription($value) {
		df_param_string($value, 0);
		$this->setData(self::P__DESCRIPTION, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setEntityUserId($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__ENTITY_USER_ID, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setIconId($value) {
		df_param_string($value, 0);
		$this->setData(self::P__ICON_ID, $value);
		return $this;
	}

	/**
	 * @override
	 * @param int|null $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setId($value) {
		parent::setId($value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setMetaDescription($value) {
		df_param_string($value, 0);
		$this->setData(self::P__META_DESCRIPTION, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setMetaKeywords($value) {
		df_param_string($value, 0);
		$this->setData(self::P__META_KEYWORDS, $value);
		return $this;
	}

	/**
	 * @param int|null $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setParentId($value) {
		if (!is_null($value)) {
			df_param_integer($value, 0);
		}
		/**
		 * Вместо 0 устанавливаем null,
		 * для совместимости с ограничением:
				foreign key (`parent_id`)
					references `df_forum_topic`(topic_id)
					on delete set null
		 * Если устанавливать 0, то произойдёт сбой:
		 * Integrity constraint violation: 1452 Cannot add or update a child row:
		 * a foreign key constraint fails
		 * (`df_forum_topic`,
		 * CONSTRAINT `df_forum_topic_ibfk_1`
		 * FOREIGN KEY (`parent_id`)
		 * 	REFERENCES `df_forum_topic` (`topic_id`)
		 * 	ON DELETE SET NULL
		 * )
		 */
		$value = rm_empty_to_null($value);
		$this->setData(self::P__PARENT_ID, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setPriority($value) {
		df_param_string($value, 0);
		$this->setData(self::P__PRIORITY, $value);
		return $this;
	}

	/**
	 * @param bool $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setStatus($value) {
		df_param_boolean($value, 0);
		$this->setData(self::P__STATUS, $value);
		return $this;
	}

	/**
	 * @param int|null $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setStoreId($value) {
		if (!is_null($value)) {
			df_param_integer($value, 0);
		}
		$this->setData(self::P__STORE_ID, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Topic
	 * @see now()
	 */
	public function setUpdateTime($value) {
		df_param_string($value, 0);
		$this->setData(self::P__UPDATE_TIME, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setUserName($value) {
		df_param_string($value, 0);
		$this->setData(self::P__USER_NAME, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setTitle($value) {
		df_param_string($value, 0);
		$this->setData(self::P__TITLE, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setUrlText($value) {
		df_param_string($value, 0);
		$this->setData(self::P__URL_TEXT, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Topic
	 */
	public function setUrlTextShort($value) {
		df_param_string($value, 0);
		$this->setData(self::P__URL_TEXT_SHORT, $value);
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Resource_Topic::mf());
	}
	const _CLASS = __CLASS__;
	const P__CREATED_TIME = 'created_time';
	const P__DESCRIPTION = 'description';
	const P__ENTITY_USER_ID = 'entity_user_id';
	const P__HAS_SUBTOPIC = 'has_subtopics';
	const P__ICON_ID = 'icon_id';
	const P__ID = 'topic_id';
	const P__IS_CATEGORY = 'is_category';
	const P__IS_SUBTOPIC = 'is_subtopic';
	const P__META_DESCRIPTION = 'meta_description';
	const P__META_KEYWORDS = 'meta_keywords';
	const P__PARENT_ID = 'parent_id';
	const P__PRIORITY = 'priority';
	const P__PRODUCT_ID = 'product_id';
	const P__STATUS = 'status';
	const P__STORE_ID = 'store_id';
	const P__SYSTEM_USER_ID = 'system_user_id';
	const P__TITLE = 'title';
	const P__UPDATE_TIME = 'update_time';
	const P__URL_TEXT = 'url_text';
	const P__URL_TEXT_SHORT = 'url_text_short';
	const P__USER_NAME = 'user_name';
	const P__USER_NICK = 'user_nick';

	/** @return Df_Forum_Model_Resource_Topic_Collection */
	public static function c() {return self::s()->getCollection();}
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_Topic
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/**
	 * @see Df_Forum_Model_Resource_Topic_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf(__CLASS__);}
	/** @return Df_Forum_Model_Topic */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}