<?php
/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract для совместимости с Magento CE 1.4
 */
class Df_Forum_Model_Resource_Usersettings extends Mage_Core_Model_Mysql4_Abstract {
	/**
	 * @param Mage_Core_Model_Resource_Setup $setup
	 * @return Df_Forum_Model_Resource_Usersettings
	 */
	public function tableCreate(Mage_Core_Model_Resource_Setup $setup) {
		$f_AVATAR_NAME = Df_Forum_Model_Usersettings::P__AVATAR_NAME;
		$f_ID = Df_Forum_Model_Usersettings::P__ID;
		$f_NICKNAME = Df_Forum_Model_Usersettings::P__NICKNAME;
		$f_SIGNATURE = Df_Forum_Model_Usersettings::P__SIGNATURE;
		$f_SYSTEM_USER_ID = Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID;
		$f_WEBSITE_ID = Df_Forum_Model_Usersettings::P__WEBSITE_ID;
		$t_DF_FORUM_USERSETTINGS = $this->getTable(self::TABLE_NAME);
		/**
		 * Не используем $this->getConnection()->newTable()
		 * для совместимости с Magento CE 1.4
		 */
		$setup->run("
			create table if not exists `{$t_DF_FORUM_USERSETTINGS}` (
				`{$f_ID}` int(10) unsigned not null auto_increment
				,`{$f_SYSTEM_USER_ID}` int(10) not null
				,`{$f_NICKNAME}` varchar(255) not null
				,`{$f_SIGNATURE}` text not null
				,`{$f_AVATAR_NAME}` text not null
				,`{$f_WEBSITE_ID}` int(10) not null
				,primary key (`{$f_ID}`)
			) engine=InnoDB default charset=utf8;
		");
		/**
		 * После изменения структуры базы данных надо удалить кэш,
		 * потому что Magento кэширует структуру базы данных
		 */
		Mage::app()->getCache()->clean();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		$this->_init(self::TABLE_NAME, Df_Forum_Model_Usersettings::P__ID);
	}
	const _CLASS = __CLASS__;
	const TABLE_NAME = 'df_forum/usersettings';
	/**
	 * @see Df_Forum_Model_Usersettings::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Forum_Model_Resource_Usersettings */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}