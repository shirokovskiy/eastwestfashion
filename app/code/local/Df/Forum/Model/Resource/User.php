<?php
/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract для совместимости с Magento CE 1.4
 */
class Df_Forum_Model_Resource_User extends Mage_Core_Model_Mysql4_Abstract {
	/**
	 * @param Mage_Core_Model_Resource_Setup $setup
	 * @return Df_Forum_Model_Resource_User
	 */
	public function tableCreate(Mage_Core_Model_Resource_Setup $setup) {
		$f_FIRST_POST = Df_Forum_Model_User::P__FIRST_POST;
		$f_USER_ID = Df_Forum_Model_User::P__ID;
		$f_STORE_ID = Df_Forum_Model_User::P__STORE_ID;
		$f_SYSTEM_USER_ID = Df_Forum_Model_User::P__SYSTEM_USER_ID;
		$f_SYSTEM_USER_NAME = Df_Forum_Model_User::P__SYSTEM_USER_NAME;
		$f_TOTAL_POSTS = Df_Forum_Model_User::P__TOTAL_POSTS;
		$f_USER_NICK = Df_Forum_Model_User::P__USER_NICK;
		$t_DF_FORUM_USER = $this->getTable(self::TABLE_NAME);
		/**
		 * Не используем $this->getConnection()->newTable()
		 * для совместимости с Magento CE 1.4
		 */
		$setup->run("
			create table if not exists `{$t_DF_FORUM_USER}` (
				`{$f_USER_ID}` int(10) not null auto_increment
				,`{$f_SYSTEM_USER_ID}` int(10) not null default 0
				,`{$f_SYSTEM_USER_NAME}` varchar(255)
				,`{$f_USER_NICK}` varchar (255) not null
				,`{$f_FIRST_POST}` datetime
				,`{$f_STORE_ID}` int (10) not null
				,`{$f_TOTAL_POSTS}` int (10) not null
				,PRIMARY KEY (`{$f_USER_ID}`)
			) engine=InnoDB default charset=utf8;
		");
		/**
		 * После изменения структуры базы данных надо удалить кэш,
		 * потому что Magento кэширует структуру базы данных
		 */
		Mage::app()->getCache()->clean();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		$this->_init(self::TABLE_NAME, Df_Forum_Model_User::P__ID);
	}

	const _CLASS = __CLASS__;
	const TABLE_NAME = 'df_forum/user';
	/**
	 * @see Df_Forum_Model_User::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Forum_Model_Resource_User */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}