<?php
class Df_Forum_Model_Resource_PrivateMessage_Collection
	extends Mage_Core_Model_Mysql4_Collection_Abstract {

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_PrivateMessage::mf());
	}
	const _CLASS = __CLASS__;
	/** @return Df_Forum_Model_Resource_PrivateMessage_Collection */
	public static function i() {return new self;}
}