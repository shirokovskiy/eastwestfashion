<?php
class Df_Forum_Model_Resource_Visitor_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {
	/**
	 * @param Mage_Core_Model_Store|int $store
	 * @return Df_Forum_Model_Resource_Visitor_Collection
	 */
	public function addStoreFilter($store) {
		if (!Mage::app()->isSingleStoreMode()) {
			if ($store instanceof Mage_Core_Model_Store) {
				$store = array($store->getId());
			}
			$this->getSelect()->where('? = main_table.store_id', $store);
		}
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Visitor::mf());
	}
	const _CLASS = __CLASS__;

	/** @return Df_Forum_Model_Resource_Visitor_Collection */
	public static function i() {return new self;}
}