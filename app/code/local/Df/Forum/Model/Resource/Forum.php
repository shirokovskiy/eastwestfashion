<?php
/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract для совместимости с Magento CE 1.4
 */
class Df_Forum_Model_Resource_Forum extends Mage_Core_Model_Mysql4_Abstract {
	/**
	 * @override
	 * @param Mage_Core_Model_Abstract $object
	 * @return Df_Forum_Model_Resource_Forum
	 */
	protected function _beforeDelete(Mage_Core_Model_Abstract $object) {
		parent::_beforeDelete($object);
//		Df_Forum_Model_Entity::i()
//			->setId($this->getEntityId($this->getId()))
//			->delete()
//		;
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		$this->_init(self::TABLE_NAME, Df_Forum_Model_Forum::P__ID);
	}
	const _CLASS = __CLASS__;
	const TABLE_NAME = 'df_forum/forum';
	/**
	 * @see Df_Forum_Model_Forum::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Forum_Model_Resource_Forum */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}