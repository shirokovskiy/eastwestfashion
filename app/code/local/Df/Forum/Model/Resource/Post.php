<?php
/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract для совместимости с Magento CE 1.4
 */
class Df_Forum_Model_Resource_Post extends Mage_Core_Model_Mysql4_Abstract {
	/**
	 * @param Mage_Core_Model_Resource_Setup $setup
	 * @return Df_Forum_Model_Post
	 */
	public function tableCreate(Mage_Core_Model_Resource_Setup $setup) {
		$f_CREATED_TIME = Df_Forum_Model_Post::P__CREATED_TIME;
		$f_IS_STICKY = Df_Forum_Model_Post::P__IS_STICKY;
		$f_PARENT_ID = Df_Forum_Model_Post::P__PARENT_ID;
		$f_POST = Df_Forum_Model_Post::P__POST;
		$f_POST_ID = Df_Forum_Model_Post::P__ID;
		$f_POST_ORIG = Df_Forum_Model_Post::P__POST_ORIG;
		$f_PRODUCT_ID = Df_Forum_Model_Post::P__PRODUCT_ID;
		$f_STATUS = Df_Forum_Model_Post::P__STATUS;
		$f_SYSTEM_USER_ID = Df_Forum_Model_Post::P__SYSTEM_USER_ID;
		$f_UPDATE_TIME = Df_Forum_Model_Post::P__UPDATE_TIME;
		$f_USER_NAME = Df_Forum_Model_Post::P__USER_NAME;
		$f_USER_NICK = Df_Forum_Model_Post::P__USER_NICK;
		$ff_FORUM_TOPIC__TOPIC_ID = Df_Forum_Model_Topic::P__ID;
		$t_FORUM_POST = $this->getTable(self::TABLE_NAME);
		$t_FORUM_TOPIC = $this->getTable(Df_Forum_Model_Resource_Topic::TABLE_NAME);
		/**
		 * Не используем $this->getConnection()->newTable()
		 * для совместимости с Magento CE 1.4
		 */
		$setup->run("
			create table if not exists `{$t_FORUM_POST}` (
				`{$f_POST_ID}` int(10) unsigned not null auto_increment
				,`{$f_PARENT_ID}` int(10) unsigned null default null
				,`{$f_SYSTEM_USER_ID}` int(10) unsigned not null
				,`{$f_USER_NAME}` varchar(255) not null
				,`{$f_USER_NICK}` varchar(255) not null
				,`{$f_POST}` TEXT not null
				,`{$f_POST_ORIG}` TEXT not null default ''
				,`{$f_CREATED_TIME}` DATETIME not null
				,`{$f_UPDATE_TIME}` DATETIME not null
				,`{$f_STATUS}` smallint(6) not null default '0'
				,`{$f_PRODUCT_ID}` int(10) not null
				,`{$f_IS_STICKY}` TINYINT(2) not null
				,primary key (`{$f_POST_ID}`)
				,foreign key (`{$f_PARENT_ID}`)
					references `{$t_FORUM_TOPIC}`(`{$ff_FORUM_TOPIC__TOPIC_ID}`)
					on delete set null
			) engine=InnoDB default charset=utf8;
		");
		/**
		 * После изменения структуры базы данных надо удалить кэш,
		 * потому что Magento кэширует структуру базы данных
		 */
		Mage::app()->getCache()->clean();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		$this->_init(self::TABLE_NAME, Df_Forum_Model_Post::P__ID);
	}
	const _CLASS = __CLASS__;
	const TABLE_NAME = 'df_forum/post';
	/**
	 * @see Df_Forum_Model_Post::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Forum_Model_Resource_Post */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}