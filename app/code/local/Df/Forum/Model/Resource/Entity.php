<?php
/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract для совместимости с Magento CE 1.4
 */
class Df_Forum_Model_Resource_Entity extends Mage_Core_Model_Mysql4_Abstract {
	/**
	 * @param Mage_Core_Model_Resource_Setup $setup
	 * @return Df_Forum_Model_Resource_Entity
	 */
	public function tableCreate(Mage_Core_Model_Resource_Setup $setup) {
		$f_ENTITY_ID = Df_Forum_Model_Entity::P__ENTITY_ID;
		$f_ID = Df_Forum_Model_Entity::P__ID;
		$f_LAST_VIEW = Df_Forum_Model_Entity::P__LAST_VIEW;
		$f_TOTAL_VIEWS = Df_Forum_Model_Entity::P__TOTAL_VIEWS;
		$ff_FORUM_TOPIC__TOPIC_ID = Df_Forum_Model_Topic::P__ID;
		$t_FORUM_ENTITY = $this->getTable(self::TABLE_NAME);
		$t_FORUM_TOPIC = $this->getTable(Df_Forum_Model_Resource_Topic::TABLE_NAME);
		/**
		 * Не используем $this->getConnection()->newTable()
		 * для совместимости с Magento CE 1.4
		 */
		$setup->run("
			create table if not exists `{$t_FORUM_ENTITY}` (
				`{$f_ID}` int(10) unsigned not null auto_increment
				,`{$f_ENTITY_ID}` int(10) unsigned not null default 0
				,`{$f_LAST_VIEW}` datetime null
				,`{$f_TOTAL_VIEWS}` int(10) unsigned not null default 0
				,primary key (`{$f_ID}`)
				,foreign key (`{$f_ENTITY_ID}`)
					references `{$t_FORUM_TOPIC}`($ff_FORUM_TOPIC__TOPIC_ID)
					on delete cascade
			) engine=InnoDB default charset=utf8;
		");
		/**
		 * После изменения структуры базы данных надо удалить кэш,
		 * потому что Magento кэширует структуру базы данных
		 */
		Mage::app()->getCache()->clean();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		$this->_init(self::TABLE_NAME, Df_Forum_Model_Entity::P__ID);
	}
	const _CLASS = __CLASS__;
	const TABLE_NAME = 'df_forum/entity';
	/**
	 * @see Df_Forum_Model_Entity::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Forum_Model_Resource_Entity */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}