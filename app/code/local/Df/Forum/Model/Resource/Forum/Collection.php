<?php
class Df_Forum_Model_Resource_Forum_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {
	/**
	 * @param int $storeId
	 * @return Df_Forum_Model_Resource_Forum_Collection
	 */
	public function addStoreFilter($storeId) {
		df_param_integer($storeId, 0);
		if (!Mage::app()->isSingleStoreMode()) {
			$this->getSelect()
				->where('main_table.store_id IN(?)', array(0, $storeId))
				->orWhere('main_table.store_id IS null')
			;
			return $this;
		}
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Forum::mf());
	}
	const _CLASS = __CLASS__;

	/** @return Df_Forum_Model_Resource_Forum_Collection */
	public static function i() {return new self;}
}