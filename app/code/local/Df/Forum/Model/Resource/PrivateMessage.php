<?php
/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract для совместимости с Magento CE 1.4
 */
class Df_Forum_Model_Resource_PrivateMessage extends Mage_Core_Model_Mysql4_Abstract {
	/**
	 * @param Mage_Core_Model_Resource_Setup $setup
	 * @return Df_Forum_Model_Resource_PrivateMessage
	 */
	public function tableCreate(Mage_Core_Model_Resource_Setup $setup) {
		$f_DATE_SENT = Df_Forum_Model_PrivateMessage::P__DATE_SENT;
		$f_IS_DELETEINBOX = Df_Forum_Model_PrivateMessage::P__IS_DELETEINBOX;
		$f_IS_DELETESENT = Df_Forum_Model_PrivateMessage::P__IS_DELETESENT;
		$f_IS_PRIMARY = Df_Forum_Model_PrivateMessage::P__IS_PRIMARY;
		$f_IS_READ = Df_Forum_Model_PrivateMessage::P__IS_READ;
		$f_IS_TRASH = Df_Forum_Model_PrivateMessage::P__IS_TRASH;
		$f_MESSAGE = Df_Forum_Model_PrivateMessage::P__MESSAGE;
		$f_PARENT_ID = Df_Forum_Model_PrivateMessage::P__PARENT_ID;
		$f_PM_ID = Df_Forum_Model_PrivateMessage::P__ID;
		$f_SENT_FROM = Df_Forum_Model_PrivateMessage::P__SENT_FROM;
		$f_SENT_TO = Df_Forum_Model_PrivateMessage::P__SENT_TO;
		$f_SUBJECT = Df_Forum_Model_PrivateMessage::P__SUBJECT;
		$t_DF_FORUM_PRIVATE_MESSAGE = $this->getTable(self::TABLE_NAME);
		/**
		 * Не используем $this->getConnection()->newTable()
		 * для совместимости с Magento CE 1.4
		 */
		$setup->run("
			create table if not exists `{$t_DF_FORUM_PRIVATE_MESSAGE}` (
				`{$f_PM_ID}` int(10) unsigned not null auto_increment
				,`{$f_DATE_SENT}` datetime null
				,`{$f_MESSAGE}` text not null
				,`{$f_SUBJECT}` text not null
				,`{$f_PARENT_ID}` int(10) not null
				,`{$f_IS_PRIMARY}` smallint(6) not null default '0'
				,`{$f_SENT_FROM}` int(10) not null
				,`{$f_SENT_TO}` int(10) not null
				,`{$f_IS_TRASH}` smallint(6) not null default '0'
				,`{$f_IS_READ}` smallint(6) not null default '0'
				,`{$f_IS_DELETESENT}` smallint(6) not null default '0'
				,`{$f_IS_DELETEINBOX}` smallint(6) not null default '0'
				,primary key (`{$f_PM_ID}`)
			) engine=InnoDB default charset=utf8;
		");
		/**
		 * После изменения структуры базы данных надо удалить кэш,
		 * потому что Magento кэширует структуру базы данных
		 */
		Mage::app()->getCache()->clean();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		$this->_init(self::TABLE_NAME, Df_Forum_Model_PrivateMessage::P__ID);
	}
	const _CLASS = __CLASS__;
	const TABLE_NAME = 'df_forum/private_message';
	/**
	 * @see Df_Forum_Model_PrivateMessage::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Forum_Model_Resource_PrivateMessage */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}