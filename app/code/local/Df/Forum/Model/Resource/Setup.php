<?php
class Df_Forum_Model_Resource_Setup extends Df_Core_Model_Resource_Setup_Abstract {
	/** @return Df_Forum_Model_Resource_Setup */
	public function install_2_21_0() {
		Df_Forum_Model_Setup_2_21_0::i(
			array(
				Df_Forum_Model_Setup_2_21_0::P__SETUP => $this
			)
		)->process();
		return $this;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @return string
	 */
	public static function mf() {
		return 'df_forum/setup';
	}
}