<?php
/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract для совместимости с Magento CE 1.4
 */
class Df_Forum_Model_Resource_Topic extends Mage_Core_Model_Mysql4_Abstract {
	/**
	 * @param Mage_Core_Model_Resource_Setup $setup
	 * @return Df_Forum_Model_Resource_Topic
	 */
	public function tableCreate(Mage_Core_Model_Resource_Setup $setup) {
		$f_CREATED_TIME = Df_Forum_Model_Topic::P__CREATED_TIME;
		$f_DESCRIPTION = Df_Forum_Model_Topic::P__DESCRIPTION;
		$f_HAS_SUBTOPIC = Df_Forum_Model_Topic::P__HAS_SUBTOPIC;
		$f_ICON_ID = Df_Forum_Model_Topic::P__ICON_ID;
		$f_TOPIC_ID = Df_Forum_Model_Topic::P__ID;
		$f_IS_CATEGORY = Df_Forum_Model_Topic::P__IS_CATEGORY;
		$f_IS_SUBTOPIC = Df_Forum_Model_Topic::P__IS_SUBTOPIC;
		$f_META_DESCRIPTION = Df_Forum_Model_Topic::P__META_DESCRIPTION;
		$f_META_KEYWORDS = Df_Forum_Model_Topic::P__META_KEYWORDS;
		$f_PARENT_ID = Df_Forum_Model_Topic::P__PARENT_ID;
		$f_PRIORITY = Df_Forum_Model_Topic::P__PRIORITY;
		$f_PRODUCT_ID = Df_Forum_Model_Topic::P__PRODUCT_ID;
		$f_STATUS = Df_Forum_Model_Topic::P__STATUS;
		$f_STORE_ID = Df_Forum_Model_Topic::P__STORE_ID;
		$f_SYSTEM_USER_ID = Df_Forum_Model_Topic::P__SYSTEM_USER_ID;
		$f_TITLE = Df_Forum_Model_Topic::P__TITLE;
		$f_UPDATE_TIME = Df_Forum_Model_Topic::P__UPDATE_TIME;
		$f_URL_TEXT = Df_Forum_Model_Topic::P__URL_TEXT;
		$f_URL_TEXT_SHORT = Df_Forum_Model_Topic::P__URL_TEXT_SHORT;
		$f_USER_NAME = Df_Forum_Model_Topic::P__USER_NAME;
		$f_USER_NICK = Df_Forum_Model_Topic::P__USER_NICK;
		$t_FORUM_TOPIC = $this->getTable(self::TABLE_NAME);
		/**
		 * Не используем $this->getConnection()->newTable()
		 * для совместимости с Magento CE 1.4
		 */
		$setup->run("
			create table if not exists `{$t_FORUM_TOPIC}` (
				`{$f_TOPIC_ID}` int(10) unsigned not null auto_increment
				,`{$f_PARENT_ID}` int(10) unsigned null default null
				,`{$f_SYSTEM_USER_ID}` int(10) not null
				,`{$f_IS_CATEGORY}` smallint(1) not null default 0
				,`{$f_USER_NAME}` varchar(255) not null
				,`{$f_USER_NICK}` varchar(255) not null
				,`{$f_CREATED_TIME}` datetime null
				,`{$f_UPDATE_TIME}` datetime null
				,`{$f_TITLE}` text not null
				,`{$f_DESCRIPTION}` text not null
				,`{$f_URL_TEXT}` text not null
				,`{$f_URL_TEXT_SHORT}` text not null
				,`{$f_STATUS}` smallint(1) not null default 0
				,`{$f_META_DESCRIPTION}` text not null
				,`{$f_META_KEYWORDS}` text not null
				,`{$f_PRIORITY}` int(10) not null
				,`{$f_ICON_ID}` varchar (255) not null
				,`{$f_HAS_SUBTOPIC}` int (2) not null
				,`{$f_IS_SUBTOPIC}` int (10) not null
				,`{$f_PRODUCT_ID}` int(10) not null
				,`{$f_STORE_ID}` int(10) not null
				,primary key (`{$f_TOPIC_ID}`)
				,foreign key (`{$f_PARENT_ID}`)
					references `{$t_FORUM_TOPIC}`(`{$f_TOPIC_ID}`)
					on delete set null
			) engine=InnoDB default charset=utf8;
		");
		/**
		 * После изменения структуры базы данных надо удалить кэш,
		 * потому что Magento кэширует структуру базы данных
		 */
		Mage::app()->getCache()->clean();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		$this->_init(self::TABLE_NAME, Df_Forum_Model_Topic::P__ID);
	}
	const _CLASS = __CLASS__;
	const TABLE_NAME = 'df_forum/topic';
	/**
	 * @see Df_Forum_Model_Topic::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Forum_Model_Resource_Topic */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}