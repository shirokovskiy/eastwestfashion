<?php
/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract для совместимости с Magento CE 1.4
 */
class Df_Forum_Model_Resource_Notification extends Mage_Core_Model_Mysql4_Abstract {
	/**
	 * @param Mage_Core_Model_Resource_Setup $setup
	 * @return Df_Forum_Model_Resource_Notification
	 */
	public function tableCreate(Mage_Core_Model_Resource_Setup $setup) {
		$f_HASH = Df_Forum_Model_Notification::P__HASH;
		$f_NOTIFICATION_ID = Df_Forum_Model_Notification::P__ID;
		$f_SYSTEM_USER_ID = Df_Forum_Model_Notification::P__SYSTEM_USER_ID;
		$f_SYSTEM_USER_EMAIL = Df_Forum_Model_Notification::P__SYSTEM_USER_EMAIL;
		$f_TOPIC_ID = Df_Forum_Model_Notification::P__TOPIC_ID;
		$ff_FORUM_TOPIC__TOPIC_ID = Df_Forum_Model_Topic::P__ID;
		$t_FORUM_NOTIFICATION = $this->getTable(self::TABLE_NAME);
		$t_FORUM_TOPIC = $this->getTable(Df_Forum_Model_Resource_Topic::TABLE_NAME);
		/**
		 * Не используем $this->getConnection()->newTable()
		 * для совместимости с Magento CE 1.4
		 */
		$setup->run("
			create table if not exists `{$t_FORUM_NOTIFICATION}` (
				`{$f_NOTIFICATION_ID}` int(10) unsigned not null auto_increment
				,`{$f_SYSTEM_USER_ID}` int(10) unsigned not null default 0
				,`{$f_TOPIC_ID}` int(10) unsigned not null default 0
				,`{$f_SYSTEM_USER_EMAIL}` varchar(255) not null
				,`{$f_HASH}` varchar(255) not null
				,primary key (`{$f_NOTIFICATION_ID}`)
				,foreign key (`{$f_TOPIC_ID}`)
					references `{$t_FORUM_TOPIC}`(`{$ff_FORUM_TOPIC__TOPIC_ID}`)
					on delete cascade
			) engine=InnoDB default charset=utf8;
		");
		/**
		 * После изменения структуры базы данных надо удалить кэш,
		 * потому что Magento кэширует структуру базы данных
		 */
		Mage::app()->getCache()->clean();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		$this->_init(self::TABLE_NAME, Df_Forum_Model_Notification::P__ID);
	}
	const _CLASS = __CLASS__;
	const TABLE_NAME = 'df_forum/notification';
	/**
	 * @see Df_Forum_Model_Notification::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Forum_Model_Resource_Notification */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}