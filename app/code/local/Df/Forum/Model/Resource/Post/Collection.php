<?php
class Df_Forum_Model_Resource_Post_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {
	/**
	 * @param int $total
	 * @return Df_Forum_Model_Resource_Post_Collection
	 */
	public function _increaseSize($total) {
		$this->increase_size_count = $total;
	}

	/**
	 * @param Mage_Core_Model_Store|int $store
	 * @return Df_Forum_Model_Resource_Post_Collection
	 */
	public function addStoreFilter($store) {
		if (!Mage::app()->isSingleStoreMode()) {
			if ($store instanceof Mage_Core_Model_Store) {
				$store = array($store->getId());
			}
			$table_topics = $this->getTable(Df_Forum_Model_Resource_Topic::TABLE_NAME);
			$this->getSelect()
				->joinLeft(
					array('table_topics_store' => $table_topics)
					,'main_table.parent_id = table_topics_store.topic_id'
					,'table_topics_store.status as topic_status'
				)
			;
			$this->getSelect()
				->where('table_topics_store.store_id IN(?)', array(0, $store))
				->orWhere('table_topics_store.store_id IS null')
			;
			return $this;
		}
		return $this;
	}

	/** @return int */
	public function getSize() {
		return intval(parent::getSize() + $this->increase_size_count);
	}

	/** @return int */
	public function getLastPageNumberTMP() {
		return
			($this->last_page_number_tmp && $this->last_page_number_tmp != 0)
			? $this->last_page_number_tmp
			: 0
		;
	}

	/**
	 * @param int $page_number
	 * @return Df_Forum_Model_Resource_Post_Collection
	 */
	public function setLastPageNumberTMP($page_number) {
		$this->last_page_number_tmp = $page_number;
		return $this;
	}

	/** @var int */
	private $increase_size_count = 0;
	/** @var int */
	public $last_page_number_tmp = 0;

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Post::mf());
	}
	const _CLASS = __CLASS__;

	/** @return Df_Forum_Model_Resource_Post_Collection */
	public static function i() {return new self;}
}