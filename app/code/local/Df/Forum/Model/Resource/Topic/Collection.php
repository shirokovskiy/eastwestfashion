<?php
class Df_Forum_Model_Resource_Topic_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {
	/**
	 * @param Mage_Core_Model_Store|int $store
	 * @param bool $ret_if_null
	 * @param bool $get_null_stores
	 * @return Df_Forum_Model_Resource_Topic_Collection
	 */
	public function addStoreFilter(
		$store
		,$ret_if_null = false
		,$get_null_stores = false
	) {
		if (!Mage::app()->isSingleStoreMode()) {
			if ($store instanceof Mage_Core_Model_Store) {
				$store = $store->getId();
			}
			if ($store || $ret_if_null) {
				$storeIds = array(0);
				if (!$get_null_stores) {
					$storeIds []= $store;
				}
				$this->getSelect()
					->where('main_table.store_id IN(?)', $storeIds)
					->orWhere('main_table.store_id IS null')
				;
			}
		}
		return $this;
	}

	/** @return Df_Forum_Model_Resource_Topic_Collection */
	public function getSubTopicsOnly() {
		$this->getSelect()
			->where('? = main_table.is_subtopic', 1)
		;
		return $this;
	}

	/** @return Df_Forum_Model_Resource_Topic_Collection */
	public function getTopicsOnly() {
		$this->getSelect()
			->where('? = main_table.is_subtopic', 0)
		;
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Topic::mf());
	}
	const _CLASS = __CLASS__;
	/** @return Df_Forum_Model_Resource_Topic_Collection */
	public static function i() {return new self;}
}