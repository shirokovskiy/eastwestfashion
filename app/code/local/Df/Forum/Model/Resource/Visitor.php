<?php
/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract для совместимости с Magento CE 1.4
 */
class Df_Forum_Model_Resource_Visitor extends Mage_Core_Model_Mysql4_Abstract {
	/**
	 * @param Mage_Core_Model_Resource_Setup $setup
	 * @return Df_Forum_Model_Resource_Visitor
	 */
	public function tableCreate(Mage_Core_Model_Resource_Setup $setup) {
		$f_VISITOR_ID = Df_Forum_Model_Visitor::P__ID;
		$f_PARENT_ID = Df_Forum_Model_Visitor::P__PARENT_ID;
		$f_SESSION_ID = Df_Forum_Model_Visitor::P__SESSION_ID;
		$f_STORE_ID = Df_Forum_Model_Visitor::P__STORE_ID;
		$f_SYSTEM_USER_ID = Df_Forum_Model_Visitor::P__SYSTEM_USER_ID;
		$f_TIME_VISITED = Df_Forum_Model_Visitor::P__TIME_VISITED;
		$f_TOPIC_ID = Df_Forum_Model_Visitor::P__TOPIC_ID;
		$t_DF_FORUM_VISITOR = $this->getTable(self::TABLE_NAME);
		/**
		 * Не используем $this->getConnection()->newTable()
		 * для совместимости с Magento CE 1.4
		 */
		$setup->run("
			create table if not exists `{$t_DF_FORUM_VISITOR}` (
				`{$f_VISITOR_ID}` int(10) unsigned not null auto_increment
				,`{$f_TOPIC_ID}` int(10) not null default 0
				,`{$f_STORE_ID}` int (10) not null
				,`{$f_PARENT_ID}` int(10) not null
				,`{$f_SYSTEM_USER_ID}` int(10) not null default 0
				,`{$f_SESSION_ID}` varchar(100) not null default ''
				,`{$f_TIME_VISITED}` datetime null
				,PRIMARY KEY (`{$f_VISITOR_ID}`)
			) engine=InnoDB default charset=utf8;
		");
		/**
		 * После изменения структуры базы данных надо удалить кэш,
		 * потому что Magento кэширует структуру базы данных
		 */
		Mage::app()->getCache()->clean();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		$this->_init(self::TABLE_NAME, Df_Forum_Model_Visitor::P__ID);
	}
	const _CLASS = __CLASS__;
	const TABLE_NAME = 'df_forum/visitor';
	/**
	 * @see Df_Forum_Model_Visitor::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Forum_Model_Resource_Visitor */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}