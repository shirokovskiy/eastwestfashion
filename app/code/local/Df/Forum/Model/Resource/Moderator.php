<?php
/**
 * Наследуемся от Mage_Core_Model_Mysql4_Abstract для совместимости с Magento CE 1.4
 */
class Df_Forum_Model_Resource_Moderator extends Mage_Core_Model_Mysql4_Abstract {
	/**
	 * @param Mage_Core_Model_Resource_Setup $setup
	 * @return Df_Forum_Model_Resource_Moderator
	 */
	public function tableCreate(Mage_Core_Model_Resource_Setup $setup) {
		$f_MODERATOR_ID = Df_Forum_Model_Moderator::P__ID;
		$f_SYSTEM_USER_ID = Df_Forum_Model_Moderator::P__SYSTEM_USER_ID;
		$f_USER_WEBSITE_ID = Df_Forum_Model_Moderator::P__USER_WEBSITE_ID;
		$t_FORUM_MODERATOR = $this->getTable(self::TABLE_NAME);
		/**
		 * Не используем $this->getConnection()->newTable()
		 * для совместимости с Magento CE 1.4
		 */
		$setup->run("
			create table if not exists `{$t_FORUM_MODERATOR}` (
				`{$f_MODERATOR_ID}` int(10) unsigned not null auto_increment
				,`{$f_SYSTEM_USER_ID}` int(10) unsigned not null default 0
				,`{$f_USER_WEBSITE_ID}` int(10) unsigned not null default 0
				,primary key (`{$f_MODERATOR_ID}`)
			) engine=InnoDB default charset=utf8;
		");
		/**
		 * После изменения структуры базы данных надо удалить кэш,
		 * потому что Magento кэширует структуру базы данных
		 */
		Mage::app()->getCache()->clean();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		$this->_init(self::TABLE_NAME, Df_Forum_Model_Moderator::P__ID);
	}

	const _CLASS = __CLASS__;
	const TABLE_NAME = 'df_forum/moderator';
	/**
	 * @see Df_Forum_Model_Moderator::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Forum_Model_Resource_Moderator */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}