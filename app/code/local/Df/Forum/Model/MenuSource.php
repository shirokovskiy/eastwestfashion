<?php
class Df_Forum_Model_MenuSource extends Df_Page_Model_MenuSource {
	/**
	 * @override
	 * @return Varien_Data_Tree
	 */
	public function getTree() {
		if (!isset($this->{__METHOD__})) {
			/** @var Varien_Data_Tree $result */
			$result = new Varien_Data_Tree();
			$result->addNode(
				$node = new Varien_Data_Tree_Node(
					$data = array(
						'id' => rm_uniqid()
						/**
						 * Обратите внимание, что Magento 1.7 RC1 трактует флаг is_active иначе,
						 * чем предыдущие версии.
						 * В предыдущих версиях is_active означает, что рубрика подлежит публикации.
						 * В Magento 1.7 is_active означает, что рубрика является текущей
						 */
						,'is_active' => df_magento_version('1.7', '<')
						,'name' => 'Форум'
						,'url' => Mage::getUrl('forum')
					)
					,$idField = 'id'
					,$tree = $result
					,$parent = null
				)
				,$parent = null
			);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return bool
	 */
	public function isEnabled() {
		return
				df_cfg()->forum()->general()->isEnabled()
			&&
				df_cfg()->forum()->menu()->productMenuShow()
			&&
				df_enabled(Df_Core_Feature::FORUM)
		;
	}

	const _CLASS = __CLASS__;
}