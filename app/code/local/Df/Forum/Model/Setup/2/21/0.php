<?php
class Df_Forum_Model_Setup_2_21_0 extends Df_Core_Model_Setup {
	/** @return Df_Forum_Model_Setup_2_21_0 */
	public function process() {
		Df_Forum_Model_Resource_Topic::s()->tableCreate($this->getSetup());
		Df_Forum_Model_Resource_Post::s()->tableCreate($this->getSetup());
		Df_Forum_Model_Resource_Entity::s()->tableCreate($this->getSetup());
		Df_Forum_Model_Resource_Visitor::s()->tableCreate($this->getSetup());
		Df_Forum_Model_Resource_User::s()->tableCreate($this->getSetup());
		Df_Forum_Model_Resource_Moderator::s()->tableCreate($this->getSetup());
		Df_Forum_Model_Resource_Notification::s()->tableCreate($this->getSetup());
		Df_Forum_Model_Resource_Usersettings::s()->tableCreate($this->getSetup());
		Df_Forum_Model_Resource_PrivateMessage::s()->tableCreate($this->getSetup());
		Df_Core_Model_Url_Rewrite::i(
			array(
				Df_Core_Model_Url_Rewrite::P__ID_PATH => 'forum'
				,Df_Core_Model_Url_Rewrite::P__IS_SYSTEM => 1
				,Df_Core_Model_Url_Rewrite::P__OPTIONS => null
				,Df_Core_Model_Url_Rewrite::P__REQUEST_PATH => 'forum'
				,Df_Core_Model_Url_Rewrite::P__STORE_ID => 0
				,Df_Core_Model_Url_Rewrite::P__TARGET_PATH => 'df_forum'
			)
		)->save();
		return $this;
	}

	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_Setup_2_21_0
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}