<?php
class Df_Forum_Model_Registry extends Df_Core_Model_Abstract {
	/** @return Df_Forum_Model_Usersettings|null */
	public function getAdminSettings() {
		return $this->_adminSettings;
	}

	/** @return Df_Customer_Model_Customer|null */
	public function getCustomer() {
		return $this->_customer;
	}

	/** @return Df_Forum_Model_Forum|null */
	public function getForum() {
		return $this->_forum;
	}

	/** @return Df_Forum_Model_Post|null */
	public function getPost() {
		return $this->_post;
	}

	/** @return int */
	public function getStoreId() {
		return $this->_storeId;
	}

	/** @return Df_Forum_Model_Topic|null */
	public function getTopic() {
		return $this->_topic;
	}

	/** @return bool */
	public function hasPost() {
		return !is_null($this->getPost());
	}

	/** @return bool */
	public function hasTopic() {
		return !is_null($this->getTopic());
	}

	/**
	 * @param Df_Forum_Model_Usersettings|null $value
	 * @return Df_Forum_Model_Registry
	 */
	public function setAdminSettings(Df_Forum_Model_Usersettings $value) {
		$this->_adminSettings = $value;
		return $this;
	}
	/** @var Df_Forum_Model_Usersettings|null */
	private $_adminSettings = null;

	/**
	 * @param Df_Customer_Model_Customer $value
	 * @return Df_Forum_Model_Registry
	 */
	public function setCustomer(Df_Customer_Model_Customer $value) {
		$this->_customer = $value;
		return $this;
	}
	/** @var Df_Customer_Model_Customer|null */
	private $_customer = null;

	/**
	 * @param Df_Forum_Model_Forum $value
	 * @return Df_Forum_Model_Registry
	 */
	public function setForum(Df_Forum_Model_Forum $value) {
		$this->_forum = $value;
		return $this;
	}
	/** @var Df_Forum_Model_Forum|null */
	private $_forum = null;

	/**
	 * @param Df_Forum_Model_Post $value
	 * @return Df_Forum_Model_Registry
	 */
	public function setPost(Df_Forum_Model_Post $value) {
		$this->_post = $value;
		return $this;
	}
	/** @var Df_Forum_Model_Post|null */
	private $_post = null;

	/**
	 * @param int $storeId
	 * @return Df_Forum_Model_Registry
	 */
	public function setStoreId($storeId) {
		df_param_integer($storeId, 0);
		$this->_adminSettings = $storeId;
		return $this;
	}
	/** @var int */
	private $_storeId = 0;

	/**
	 * @param Df_Forum_Model_Topic $value
	 * @return Df_Forum_Model_Registry
	 */
	public function setTopic(Df_Forum_Model_Topic $value) {
		$this->_topic = $value;
		return $this;
	}
	/** @var Df_Forum_Model_Topic|null */
	private $_topic = null;

	const _CLASS = __CLASS__;
	/** @return Df_Forum_Model_Registry */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}