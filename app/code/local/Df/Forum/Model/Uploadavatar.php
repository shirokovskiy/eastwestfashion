<?php
class Df_Forum_Model_Uploadavatar {
	/**
	 * @param $directory
	 * @return bool
	 */
	public function check_dir($directory) {
		/** @var bool $result */
		$result = false;
		if (is_dir($directory)) {
			$result = true;
		}
		else {
			if ($this->create_directory) {
				umask(0);
				mkdir($directory, $this->dirperm);
				$result = true;
			}
		}
		return $result;
	}

	/**
	 * @param $the_name
	 * @return bool
	 */
	public function check_file_name($the_name) {
		/** @var bool $result */
		$result = false;
		if (!$the_name) {
			$this->message[]= $this->error_text(10);
		}
		else {
			if (mb_strlen($the_name) > $this->max_length_filename) {
				$this->message[]= $this->error_text(13);
			}
			else {
				if ('y' !== $this->do_filename_check) {
					$result = true;
				}
				else {
					if (1 === preg_match('/^[a-z0-9_]*\.(.){1,5}$/i', $the_name)) {
						$result = true;
					}
					else {
						$this->message[]= $this->error_text(12);
					}
				}
			}
		}
		return $result;
	}

	/**
	 * this function creates a file field
	 * and if $show_alternate is true it will show a text field if the given file already exists
	 * there is also a submit button to remove the text field value
	 * ver. 2.32
	 * Method create_file_field():
	 * Minor code clean up (better code formatting and replaced double with single quotes)
	 * @param $element
	 * @param $label
	 * @param int $length
	 * @param bool $show_replace
	 * @param string $replace_label
	 * @param $file_path
	 * @param $file_name
	 * @param bool $show_alternate
	 * @param int $alt_length
	 * @param string $alt_btn_label
	 * @return string
	 */
	public function create_file_field(
		$element
		,$label = ''
		,$length = 25
		,$show_replace = true
		,$replace_label = 'Replace old file?'
		,$file_path = ''
		,$file_name = ''
		,$show_alternate = false
		,$alt_length = 30
		,$alt_btn_label = 'Delete image'
	) {
		$result = '';
		if ($label != '') {
			$result
				= '
			<label>' . $label . '</label>';
		}
		$result
			= '
			<input type="file" name="' . $element . '" size="' . $length . '" />';
		if ($show_replace) {
			$result
				.= '
			<span>' . $replace_label . '</span>
			<input type="checkbox" name="replace" value="y" />';
		}
		if ($file_name != '' && $show_alternate) {
			$result
				.= '
			<input type="text" name="' . $element . '" size="' . $alt_length . '" value="' . $file_name
				. '" readonly="readonly"';
			$result .= (!@file_exists($file_path . $file_name)) ?
				' title="' . rm_sprintf($this->error_text(17), $file_name) . '" />' : ' />';
			$result
				.= '
			<input type="checkbox" name="del_img" value="y" />
			<span>' . $alt_btn_label . '</span>';
		}
		return $result;
	}

	/**
	 * this method was first located inside the foto_upload extension
	 * @param $file
	 * @return void
	 */
	public function del_temp_file($file) {
		$delete = @unlink ($file);
		clearstatcache();
		if (@file_exists($file)) {
			$filesys = eregi_replace ('/', '\\', $file);
			$delete = @system ('del $filesys');
			clearstatcache();
			if (@file_exists($file)) {
				$delete = @chmod ($file, 0644);
				$delete = @unlink ($file);
				$delete = @system ('del $filesys');
			}
		}
	}

	/**
	 * @param $result
	 * @return mixed
	 */
	public function error_text($result) {
		/** @var array(int => string) $error */
		$error = array();
		$error[0] = rm_sprintf('Файл <b>«%s»</b> успешно загружен.', $this->the_file);
		$error[1] = 'Размер загружаемого файла превышает предельно допустимый. Выберите файл меньшего размера.';
		$error[2] = 'Размер загружаемого файла превышает предельно допустимый. Выберите файл меньшего размера.';
		$error[3] = 'При загрузке файла произошёл сбой. Попробуйте загрузить этот файл повторно.';
		$error[4] = 'При загрузке файла произошёл сбой. Попробуйте загрузить этот файл повторно.';
		$error[6] = 'При загрузке файла произошёл сбой: система не нашла временную папку на сервере.';
		$error[7] = 'Произошёл сбой записи загружаемого файла на сервер.';
		$error[8] = 'При загрузке файла произошёл сбой.';
		// end  http errors
		$error[10] = 'Укажите файл для загрузки';
		$error[11] =
			rm_sprintf(
				'Возможна загрузка следующих типов файлов: <b>%s</b>'
				,$this->ext_string
			)
		;
		$error[12] =
			'Загрузка этого файла невозможна из-за наличия в его имени недопустимых символов.'
			. '<br/>Вместо русских букв используйте в имени файла соответствующие латинские буквы,'
			. ' а вместо пробела — символ подчёркивания: «_».'
			. '<br/>Удостоверьтесь, что имя файла заканчивается точкой и расширением (например: «.jpeg»).'
		;
		$error[13] =
			rm_sprintf(
				'Загрузка этого файла невозможна, потому что его имя длиннее допустимых %d символов.'
				. ' Укоротите имя файла и загрузите его повторно.'
				, $this->max_length_filename
			)
		;
		$error[14] =
			'Загрузка файлов невозможна, потому что система не может найти'
			.' предназначенную программистом для загрузки файлов папку на сервере.'
		;
		$error[15]
			= rm_sprintf(
				'Загрузка файла «%s» невозможна, потому что файл с таким именем уже присутствует на сервере'
				,$this->the_file
			)
		;
		$error[16] = rm_sprintf('Загруженному файлу было назначено имя «<b>%s</b>».', $this->file_copy);
		$error[17] = rm_sprintf('Файл «%s» не найден.', $this->the_file);
		return $result;
	}

	/**
	 * @param $file_name
	 * @return bool
	 */
	public function existing_file($file_name) {
		return ('y' === $this->replace) || !file_exists($this->upload_dir . $file_name);
	}

	/** @return void */
	public function file_upload() {
		$this->language = 'en'; // choice of en, nl, es
		$this->rename_file = false;
		$this->ext_string = '';
	}

	/**
	 * @param $from_file
	 * @return string
	 */
	public function get_extension($from_file) {
		return mb_strtolower(mb_strrchr($from_file, '.'));
	}

	/**
	 * 	ver. 2.32
	 * Method get_uploaded_file_info(): Replaced old \n line-ends
	 * with the PHP constant variable PHP_EOL
	 * @param $name
	 * @return string
	 */
	public function get_uploaded_file_info($name) {
		$result = 'File name: ' . basename ($name) . PHP_EOL;
		$result .= 'File size: ' . filesize ($name) . ' bytes' . PHP_EOL;
		if (function_exists ('mime_content_type')) {
			$result .= 'Mime type: ' . mime_content_type ($name) . PHP_EOL;
		}
		if ($img_dim = getimagesize ($name)) {
			$result .= 'Image dimensions: x = ' . $img_dim[0] . 'px, y = ' . $img_dim[1] . 'px' . PHP_EOL;
		}
		return $result;
	}

	/**
	 * @param $tmp_file
	 * @param $new_file
	 * @return bool
	 */
	public function move_upload($tmp_file, $new_file) {
		/** @var bool $result */
		$result = false;
		if (!$this->existing_file($new_file)) {
			$this->message[]= $this->error_text(15);
		}
		else {
			$newfile = $this->upload_dir . $new_file;
			if (!$this->check_dir($this->upload_dir)) {
				$this->message[]= $this->error_text(14);
			}
			else {
				if (move_uploaded_file ($tmp_file, $newfile)) {
					umask(0);
					chmod($newfile, $this->fileperm);
					$result = true;
				}
			}
		}
		return $result;
	}

	/**
	 * @param $new_name
	 * @return int|mixed|string
	 */
	public function set_file_name($new_name = '') {
		// this 'conversion' is used for unique/new filenames
		/** @var string $result */
		$result = '';
		if (!$this->rename_file) {
			$result = str_replace(' ', '_', $this->the_file);
			// space will result in problems on linux systems
		}
		else {
			if ($this->the_file) {
				$result = ($new_name == '') ? strtotime ('now') : $new_name;
				sleep(3);
				if (!strstr ($result, '.')) {
					$result = $result . $this->get_extension($this->the_file);
				}
			}
		}
		return $result;
	}

	/**
	 * @param string $br
	 * @return string
	 */
	public function show_error_string($br = '<br />') {
		$result = '';
		foreach ($this->message as $value) {
			$result .= $value . $br;
		}
		return $result;
	}

	/**
	 * this method is only used for detailed error reporting
	 * @return void
	 */
	public function show_extensions() {
		$this->ext_string = implode(' ', $this->extensions);
	}

	/**
	 * @param $to_name
	 * @return bool
	 */
	public function upload($to_name = '') {
		/** @var bool $result */
		$result = false;
		$new_name = $this->set_file_name($to_name);
		if ($this->check_file_name($new_name)) {
			if (!$this->validateExtension()) {
				$this->show_extensions();
				$this->message[]= $this->error_text(11);
			}
			else {
				if (!is_uploaded_file ($this->the_temp_file)) {
					$this->message[]= $this->error_text($this->http_error);
				}
				else {
					$this->file_copy = $new_name;
					if ($this->move_upload($this->the_temp_file, $this->file_copy)) {
						$this->message[]= $this->error_text($this->http_error);
						if ($this->rename_file) {
							$this->message[]= $this->error_text(16);
						}
						$result = true;
					}
				}
			}
		}
		return $result;
	}

	/** @return bool */
	public function validateExtension() {
		$extension = $this->get_extension($this->the_file);
		$ext_array = $this->extensions;
		return in_array($extension, $ext_array);
	}
	const _CLASS = __CLASS__;
	public $the_file;
	public $the_temp_file;
	public $upload_dir;
	public $replace;
	public $do_filename_check;
	public $max_length_filename = 100;
	public $extensions;
	public $ext_string;
	public $language;
	public $http_error;
	public $rename_file; // if this var is true the file copy get a new name
	public $file_copy; // the new name
	public $message = array();
	public $create_directory = true;
	/*
	ver. 2.32
	Added vars for file and directory permissions, check also the methods move_upload() and check_dir().
	*/
	public $fileperm = 0644;
	public $dirperm = 0777;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_Uploadavatar
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}