<?php
class Df_Forum_Model_Action extends Df_Core_Model_Abstract {
	/** @return int */
	public function getCurrentCustomerId() {
		/** @var string $resultAsString */
		$resultAsString = Mage::app()->getRequest()->getParam(self::REQUEST_PARAM__CUSTOMER_ID);
		/** @var int $result */
		$result =
			(self::ADMIN__NAME === $resultAsString)
			? self::ADMIN__ID
			: intval($resultAsString)
		;
		return $result;
	}
	
	/** @return Df_Forum_Model_Usersettings */
	public function getCurrentCustomerSettings() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				Df_Forum_Model_Usersettings::i()->load(
					Df_Forum_Model_Action::s()->getCurrentCustomerId()
					,Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
				)
			;
		}
		return $this->{__METHOD__};
	}
	
	/** @return Df_Forum_Model_Topic|null */
	public function getCurrentTopic() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_Forum_Model_Topic|null $result */
			$result = null;
			if ($this->getCurrentTopicId()) {
				$result = Df_Forum_Model_Topic::i()->load($this->getCurrentTopicId());
				if (!$result->getId()) {
					$result = null;
				}
			}
			$this->{__METHOD__} = rm_n_set($result);
		}
		return rm_n_get($this->{__METHOD__});
	}
	
	/** @return int */
	public function getCurrentTopicId() {
		return
			intval(
				$this->getCurrentTopic()
				? $this->getCurrentTopic()->getId()
				: Mage::app()->getRequest()->getParam(self::REQUEST_PARAM__TOPIC_ID)
			)
		;
	}

	/** @return int */
	public function getCurrentTopicParentId() {
		return intval(Mage::app()->getRequest()->getParam(Df_Forum_Model_Topic::P__PARENT_ID));
	}

	/**
	 * @param Df_Forum_Model_Topic $topic
	 * @return Df_Forum_Model_Action
	 */
	public function setCurrentTopic(Df_Forum_Model_Topic $topic) {
		$this->{__CLASS__ . '::getCurrentTopic'} = $topic;
		return $this;
	}

	const _CLASS = __CLASS__;
	const ADMIN__NAME = 'admin';
	const ADMIN__ID = 10000000;
	const REQUEST_PARAM__CUSTOMER_ID = 'cid';
	const REQUEST_PARAM__TOPIC_ID = 'id';
	/** @return Df_Forum_Model_Action */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}