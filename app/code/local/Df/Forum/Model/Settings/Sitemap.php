<?php
class Df_Forum_Model_Settings_Sitemap extends Df_Core_Model_Settings {
	/** @return string */
	public function getFileName() {return $this->getString('file_name');}
	/** @return string */
	public function getFilePath() {return $this->getString('file_path');}
	/** @return boolean */
	public function isEnabled() {return $this->getYesNo('enabled');}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/sitemap/';}
	/** @return Df_Forum_Model_Settings_Sitemap */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}