<?php
class Df_Forum_Model_Settings_General extends Df_Core_Model_Settings {
	/** @return boolean */
	public function allowDeleteOwnTopics() {return $this->getYesNo('allow_delete_own_topics');}
	/** @return boolean */
	public function allowFastReply() {return $this->getYesNo('allow_fast_reply');}
	/** @return boolean */
	public function allowNicks() {return $this->getYesNo('allow_nicks');}
	/** @return string */
	public function getForumTitle() {return $this->getString('forum_title');}
	/** @return int */
	public function getNumItemsPerPage() {return $this->getNatural('num_items_per_page');}
	/** @return boolean */
	public function getOrderingType() {return $this->getYesNo('ordering_type');}
	/** @return boolean */
	public function haveNewestPostsHigherPriority() {
		return $this->getYesNo('have_newest_posts_higher_priority');
	}
	/** @return boolean */
	public function haveNewestTopicsHigherPriority() {
		return $this->getYesNo('have_newest_topics_higher_priority');
	}
	/** @return boolean */
	public function isEnabled() {return $this->getYesNo('enabled');}
	/** @return boolean */
	public function showAddToBookmarks() {return $this->getYesNo('show_add_to_bookmarks');}
	/** @return boolean */
	public function showBreadcrumbs() {return $this->getYesNo('show_breadcrumbs');}
	/** @return boolean */
	public function showDescription() {return $this->getYesNo('show_description');}
	/** @return boolean */
	public function showJumpToTop() {return $this->getYesNo('show_jump_to_top');}
	/** @return boolean */
	public function showSearhBlock() {return $this->getYesNo('show_search_block');}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/general/';}
	/** @return Df_Forum_Model_Settings_General */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}