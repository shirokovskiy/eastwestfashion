<?php
class Df_Forum_Model_Settings_TinyMCE_Image extends Df_Core_Model_Settings {
	/** @return int */
	public function getMaximumWidth() {return $this->getInteger('maximum_width');}
	/** @return bool */
	public function needResize() {return $this->getYesNo('need_resize');}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/tinymce/image__';}
	/** @return Df_Forum_Model_Settings_TinyMCE_Image */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}