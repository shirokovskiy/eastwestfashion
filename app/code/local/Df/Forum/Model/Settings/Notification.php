<?php
class Df_Forum_Model_Settings_Notification extends Df_Core_Model_Settings {
	/** @return Df_Forum_Model_Settings_Notification_Customer */
	public function customer() {return Df_Forum_Model_Settings_Notification_Customer::s();}
	/** @return Df_Forum_Model_Settings_Notification_Moderator */
	public function moderator() {return Df_Forum_Model_Settings_Notification_Moderator::s();}
	/** @return Df_Forum_Model_Settings_Notification */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}