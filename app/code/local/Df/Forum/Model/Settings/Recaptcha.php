<?php
class Df_Forum_Model_Settings_Recaptcha extends Df_Core_Model_Settings {
	/** @return string */
	public function getKeyPrivate() {return $this->getString('key__private');}
	/** @return string */
	public function getKeyPublic() {return $this->getString('key__public');}
	/** @return boolean */
	public function isEnabledForPM() {return $this->getYesNo('enabled_for_pm');}
	/** @return boolean */
	public function isEnabledForPosts() {return $this->getYesNo('enabled_for_posts');}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/recaptcha/';}
	/** @return Df_Forum_Model_Settings_Recaptcha */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}