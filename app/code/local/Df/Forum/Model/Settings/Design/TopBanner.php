<?php
class Df_Forum_Model_Settings_Design_TopBanner extends Df_Core_Model_Settings {
	/** @return string */
	public function getImageUrl() {return $this->getString('image');}
	/** @return string */
	public function getTargetUrl() {return $this->getString('url');}
	/** @return boolean */
	public function isEnabled() {return $this->getYesNo('enabled');}
	/** @return boolean */
	public function needOpenLinkInNewWindow() {return $this->getYesNo('open_link_in_new_window');}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/design/top_banner__';}
	/** @return Df_Forum_Model_Settings_Design_TopBanner */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}