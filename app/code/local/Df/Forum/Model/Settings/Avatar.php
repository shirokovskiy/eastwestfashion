<?php
class Df_Forum_Model_Settings_Avatar extends Df_Core_Model_Settings {
	/** @return string */
	public function getDefaultImage() {return $this->getString('avatar__default');}
	/** @return string */
	public function getPath() {return $this->getString('avatar__path');}
	/** @return int */
	public function getWidth() {return $this->getNatural0('avatar__width');}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/advanced/';}
	/** @return Df_Forum_Model_Settings_Avatar */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}