<?php
class Df_Forum_Model_Settings_TinyMCE extends Df_Core_Model_Settings {
	/** @return Df_Forum_Model_Settings_TinyMCE_Image */
	public function image() {return Df_Forum_Model_Settings_TinyMCE_Image::s();}
	/** @return Df_Forum_Model_Settings_TinyMCE */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}