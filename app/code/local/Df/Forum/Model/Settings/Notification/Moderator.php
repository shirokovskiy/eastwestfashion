<?php
class Df_Forum_Model_Settings_Notification_Moderator extends Df_Core_Model_Settings {
	/** @return string */
	public function getEmail() {return $this->getString('email');}
	/** @return string */
	public function getEmailSenderName() {return $this->getString('email_sender_name');}
	/** @return string */
	public function getEmailTemplate() {return $this->getString('email_template');}
	/** @return boolean */
	public function isEnabled() {return $this->getYesNo('enable');}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/notification/moderator__';}
	/** @return Df_Forum_Model_Settings_Notification_Moderator */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}