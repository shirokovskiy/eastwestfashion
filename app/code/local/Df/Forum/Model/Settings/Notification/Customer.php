<?php
class Df_Forum_Model_Settings_Notification_Customer extends Df_Core_Model_Settings {
	/** @return boolean */
	public function allowSubscription() {return $this->getYesNo('allow_subscription');}
	/** @return string */
	public function getEmailTemplate() {return $this->getString('email_template');}
	/** @return string */
	public function getEmailTemplateForPrivateMessages() {
		return $this->getString('private_messages__email_template');
	}
	/** @return boolean */
	public function notifyAboutPrivateMessages() {
		return $this->getYesNo('private_messages__enable');
	}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/notification/customer__';}
	/** @return Df_Forum_Model_Settings_Notification_Customer */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}