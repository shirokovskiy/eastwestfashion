<?php
class Df_Forum_Model_Settings_Design extends Df_Core_Model_Settings {
	/** @return string */
	public function getLayout() {return $this->getString('df_forum/design/layout');}
	/** @return Df_Forum_Model_Settings_Design_TopBanner */
	public function topBanner() {return Df_Forum_Model_Settings_Design_TopBanner::s();}
	/** @return Df_Forum_Model_Settings_Design */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}