<?php
class Df_Forum_Model_Settings_Menu extends Df_Core_Model_Settings {
	/** @return int */
	public function bottomMenuPosition() {return $this->getInteger('bottom_menu__position');}
	/** @return boolean */
	public function bottomMenuShow() {return $this->getYesNo('bottom_menu__show');}
	/** @return int */
	public function customerTopMenuPosition() {return $this->getInteger('customer_top_menu__position');}
	/** @return boolean */
	public function customerTopMenuShow() {return $this->getYesNo('customer_top_menu__show');}
	/** @return boolean */
	public function productMenuShow() {return $this->getYesNo('product_menu__show');}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/menu/';}
	/** @return Df_Forum_Model_Settings_Menu */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}