<?php
class Df_Forum_Model_Settings_Advanced extends Df_Core_Model_Settings {
	/** @return string */
	public function getStickyPostBackground() {return $this->getString('sticky_post_background');}
	/** @return string */
	public function getStickyPostBorderColor() {return $this->getString('sticky_post_border_color');}
	/** @return boolean */
	public function showControls() {return $this->getYesNo('show_controls');}
	/** @return boolean */
	public function showIconsOnFront() {return $this->getYesNo('show_icons_on_front');}
	/** @return boolean */
	public function showQuoteLink() {return $this->getYesNo('show_quote_link');}
	/** @return boolean */
	public function showTopicStatus() {return $this->getYesNo('show_topic_status');}
	/** @return boolean */
	public function useStickyPosts() {return $this->getYesNo('use_sticky_posts');}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/advanced/';}
	/** @return Df_Forum_Model_Settings_Advanced */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}