<?php
class Df_Forum_Model_Settings_Moderation extends Df_Core_Model_Settings {
	/** @return boolean */
	public function needModeratePosts() {return $this->getYesNo('posts');}
	/** @return boolean */
	public function needModerateTopics() {return $this->getYesNo('topics');}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/moderation/';}
	/** @return Df_Forum_Model_Settings_Moderation */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}