<?php
class Df_Forum_Model_Settings_ProductView extends Df_Core_Model_Settings {
	/** @return boolean */
	public function needToShow() {return $this->getYesNo('show');}
	/** @return int */
	public function numberOfTopicsToShow() {return $this->getNatural('quantity');}
	/**
	 * @override
	 * @return string
	 */
	protected function getKeyPrefix() {return 'df_forum/advanced/product_view__';}
	/** @return Df_Forum_Model_Settings_ProductView */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}