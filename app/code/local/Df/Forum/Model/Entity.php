<?php
class Df_Forum_Model_Entity extends Df_Core_Model_Abstract {
	/** @return int */
	public function getEntityId() {
		return intval($this->cfg(self::P__ENTITY_ID));
	}

	/** @return string|null */
	public function getLastView() {
		return $this->cfg(self::P__LAST_VIEW);
	}

	/** @return string|null */
	public function getTotalViews() {
		return $this->cfg(self::P__TOTAL_VIEWS);
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Entity
	 */
	public function setEntityId($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__ENTITY_ID, $value);
		return $this;
	}

	/**
	 * @param mixed $value
	 * @return Df_Forum_Model_Entity
	 */
	public function setEntityType($value) {
		$this->setData(self::P__ENTITY_TYPE, $value);
		return $this;
	}

	/**
	 * @override
	 * @param int|null $value
	 * @return Df_Forum_Model_Entity
	 */
	public function setId($value) {
		parent::setId($value);
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Resource_Entity::mf());
	}

	const _CLASS = __CLASS__;
	const P__ENTITY_ID = 'entity_id';
	const P__ENTITY_TYPE = 'entity_type';
	const P__ID = 'id';
	const P__LAST_VIEW = 'last_view';
	const P__TOTAL_VIEWS = 'total_views';

	/** @return Df_Forum_Model_Resource_Entity_Collection */
	public static function c() {return self::s()->getCollection();}
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_Entity
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/**
	 * @see Df_Forum_Model_Resource_Entity_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf(__CLASS__);}
	/** @return Df_Forum_Model_Entity */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}