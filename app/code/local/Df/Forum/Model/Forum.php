<?php
class Df_Forum_Model_Forum extends Df_Forum_Model_Topic {
	/**
	 * @override
	 * @return Df_Forum_Model_Forum
	 */
	protected function _beforeDelete() {
		parent::_beforeDelete();
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Resource_Forum::mf());
	}
	const _CLASS = __CLASS__;

	/** @return Df_Forum_Model_Resource_Forum_Collection */
	public static function c() {return self::s()->getCollection();}
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_Forum
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/**
	 * @see Df_Forum_Model_Resource_Forum_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf(__CLASS__);}
	/** @return Df_Forum_Model_Forum */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}