<?php
class Df_Forum_Model_User extends Df_Core_Model_Abstract {
	/** @return string|null */
	public function getFirstPost() {
		return $this->cfg(self::P__FIRST_POST);
	}

	/** @return string|null */
	public function getStoreId() {
		return $this->cfg(self::P__STORE_ID);
	}

	/** @return string|null */
	public function getSystemUserId() {
		return $this->cfg(self::P__SYSTEM_USER_ID);
	}

	/** @return string|null */
	public function getSystemUserName() {
		return $this->cfg(self::P__SYSTEM_USER_NAME);
	}

	/** @return string|null */
	public function getTotalPosts() {
		return $this->cfg(self::P__TOTAL_POSTS);
	}

	/** @return string|null */
	public function getuUserNick() {
		return $this->cfg(self::P__USER_NICK);
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Resource_User::mf());
	}
	const _CLASS = __CLASS__;
	const P__FIRST_POST = 'first_post';
	const P__ID = 'user_id';
	const P__STORE_ID = 'store_id';
	const P__SYSTEM_USER_ID = 'system_user_id';
	const P__SYSTEM_USER_NAME = 'system_user_name';
	const P__TOTAL_POSTS = 'total_posts';
	const P__USER_NICK = 'user_nick';

	/** @return Df_Forum_Model_Resource_User_Collection */
	public static function c() {return self::s()->getCollection();}
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_User
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/**
	 * @see Df_Forum_Model_Resource_User_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf(__CLASS__);}
	/** @return Df_Forum_Model_User */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}