<?php
/**
 * @singleton
 * Система создаёт объект-одиночку для потомков этого класса.
 * Не забывайте об этом при реализации кеширования результатов вычислений внутри этого класса!
 */
class Df_Forum_Model_Config_Source_Forum_OrderingType extends Df_Admin_Model_Config_Source {
	/**
	 * @override
	 * @param bool $isMultiSelect
	 * @return array(array(string => string))
	 */
	protected function toOptionArrayInternal($isMultiSelect = false) {
		return
			array(
				array(
					self::OPTION_KEY__LABEL => 'хронологически'
					,self::OPTION_KEY__VALUE => self::VALUE__BY_DATE
				)
				,array(
					self::OPTION_KEY__LABEL => 'как указал администратор'
					,self::OPTION_KEY__VALUE => self::VALUE__BY_WEIGHTS
				)
			)
		;
	}
	const _CLASS = __CLASS__;
	const VALUE__BY_DATE = 'by-date';
	const VALUE__BY_WEIGHTS = 'by-weights';
}


