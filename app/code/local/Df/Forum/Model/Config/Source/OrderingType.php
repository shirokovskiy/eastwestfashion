<?php
/**
 * @singleton
 * Система создаёт объект-одиночку для потомков этого класса.
 * Не забывайте об этом при реализации кеширования результатов вычислений внутри этого класса!
 */
class Df_Forum_Model_Config_Source_OrderingType extends Df_Admin_Model_Config_Source {
	/**
	 * @override
	 * @param bool $isMultiSelect
	 * @return array(array(string => string))
	 */
	protected function toOptionArrayInternal($isMultiSelect = false) {
		return
			array(
				array(
					self::OPTION_KEY__LABEL => 'хронологический: старое — выше нового'
					,self::OPTION_KEY__VALUE => self::VALUE__ASC
				)
				,array(
					self::OPTION_KEY__LABEL => 'обратный хронологический: новое — выше старого'
					,self::OPTION_KEY__VALUE => self::VALUE__DESC
				)
			)
		;
	}
	const _CLASS = __CLASS__;
	const VALUE__ASC = 0;
	const VALUE__DESC = 1;
}


