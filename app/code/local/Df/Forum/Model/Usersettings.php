<?php
class Df_Forum_Model_Usersettings extends Df_Core_Model_Abstract {
	/** @return string|null */
	public function getAvatarName() {return $this->cfg(self::P__AVATAR_NAME);}
	
	/** @return string */
	public function getAvatarUrl() {
		if (!isset($this->{__METHOD__})) {
			/** @var string $imageName */
			$imageName =
					$this->getAvatarName()
				&&
					file_exists(
						df_concat_path(
							Mage::getBaseDir()
							,df_cfg()->forum()->avatar()->getPath()
							,$this->getAvatarName()
						)
					)
				? $this->getAvatarName()
				: df_cfg()->forum()->avatar()->getDefaultImage()
			;
			$this->{__METHOD__} =
				df_concat_url(
					Mage::getBaseUrl()
					,df_cfg()->forum()->avatar()->getPath()
					,$imageName
				)
			;
		}
		return $this->{__METHOD__};
	}

	/** @return string|null */
	public function getNickname() {return $this->cfg(self::P__NICKNAME);}

	/** @return string|null */
	public function getSignature() {return $this->cfg(self::P__SIGNATURE);}

	/** @return string|null */
	public function getSystemUserId() {return $this->cfg(self::P__SYSTEM_USER_ID);}

	/** @return string|null */
	public function getWebsiteId() {return $this->cfg(self::P__WEBSITE_ID);}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Resource_Usersettings::mf());
	}
	const _CLASS = __CLASS__;
	const P__AVATAR_NAME = 'avatar_name';
	const P__ID = 'id';
	const P__NICKNAME = 'nickname';
	const P__SIGNATURE = 'signature';
	const P__SYSTEM_USER_ID = 'system_user_id';
	const P__WEBSITE_ID = 'website_id';

	/** @return Df_Forum_Model_Resource_Usersettings_Collection */
	public static function c() {return self::s()->getCollection();}
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_Usersettings
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/**
	 * @see Df_Forum_Model_Resource_Usersettings_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf(__CLASS__);}
	/** @return Df_Forum_Model_Usersettings */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}