<?php
class Df_Forum_Model_PrivateMessage extends Df_Core_Model_Abstract {
	/** @return Df_Forum_Model_PrivateMessage[] */
	public function getAncestors() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_Forum_Model_PrivateMessage[] $result  */
			$result = array();
			if (!is_null($this->getParent())) {
				$result = $this->getParent()->getAncestors();
				$result[]= $this->getParent();
			}
			df_result_array($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}
	
	/** @return string|null */
	public function getDateSent() {return $this->cfg(self::P__DATE_SENT);}

	/** @return string|null */
	public function getIsDeleteinbox() {
		return rm_bool($this->cfg(self::P__IS_DELETEINBOX));
	}

	/** @return bool */
	public function getIsDeletesent() {return $this->cfg(self::P__IS_DELETESENT);}

	/** @return bool */
	public function getIsPrimary() {return rm_bool($this->cfg(self::P__IS_PRIMARY));}

	/** @return bool */
	public function getIsTrash() {return rm_bool($this->cfg(self::P__IS_TRASH));}

	/** @return string|null */
	public function getMessage() {return $this->cfg(self::P__MESSAGE);}
	
	/** @return Df_Forum_Model_PrivateMessage|null */
	public function getParent() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_Forum_Model_PrivateMessage|null $result */
			$result = null;
			if ($this->getParentId()) {
				$result = self::i()->load($this->getParentId());
				if (!$result->getId()) {
					$result = null;
				}
			}
			$this->{__METHOD__} = rm_n_set($result);
		}
		return rm_n_get($this->{__METHOD__});
	}

	/** @return int */
	public function getParentId() {return intval($this->cfg(self::P__PARENT_ID));}

	/** @return string|null */
	public function getSentFrom() {return $this->cfg(self::P__SENT_FROM);}

	/** @return string|null */
	public function getSentTo() {return $this->cfg(self::P__SENT_TO);}

	/** @return string|null */
	public function getSubject() {return $this->cfg(self::P__SUBJECT);}

	/** @return bool */
	public function isRead() {return rm_bool($this->cfg(self::P__IS_READ));}

	/**
	 * @param bool $value
	 * @return Df_Forum_Model_PrivateMessage
	 */
	public function setRead($value) {
		df_param_boolean($value, 0);
		$this->setData(self::P__IS_READ, $value);
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Resource_PrivateMessage::mf());
	}
	const _CLASS = __CLASS__;
	const P__DATE_SENT = 'date_sent';
	const P__ID = 'pm_id';
	const P__IS_DELETEINBOX = 'is_deleteinbox';
	const P__IS_DELETESENT = 'is_deletesent';
	const P__IS_PRIMARY = 'is_primary';
	const P__IS_READ = 'is_read';
	const P__IS_TRASH = 'is_trash';
	const P__MESSAGE = 'message';
	const P__PARENT_ID = 'parent_id';
	const P__SENT_FROM = 'sent_from';
	const P__SENT_TO = 'sent_to';
	const P__SUBJECT = 'subject';

	/** @return Df_Forum_Model_Resource_PrivateMessage_Collection */
	public static function c() {return self::s()->getCollection();}
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_PrivateMessage
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/**
	 * @see Df_Forum_Model_Resource_PrivateMessage_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf(__CLASS__);}
	/** @return Df_Forum_Model_PrivateMessage */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}