<?php
class Df_Forum_Model_Session extends Mage_Core_Model_Session_Abstract {
	/** @return string[]|null */
	public function getBackProductLinks() {
		return $this->getData(self::P__BACK_PRODUCT_LINKS);
	}

	public function getDisplayMode() {
		return $this->_getData('display_mode');
	}

	/** @return int|null */
	public function getLastViewedObjectId() {
		return $this->getData(self::LAST_VIEWED_OBJECT_ID);
	}

	/** @return int[]|null */
	public function getLastViewedObjectIds() {
		return $this->getData(self::LAST_VIEWED_OBJECT_IDS);
	}

	/** @return int|null */
	public function getPageCurrentForum() {
		return $this->getData(self::P__PAGE_CURRENT_FORUM);
	}

	/** @return int|null */
	public function getPageCurrentPost() {
		return $this->getData(self::P__PAGE_CURRENT_POST);
	}

	/** @return int|null */
	public function getPageForumCurrent() {
		return $this->getData(self::P__PAGE_FORUM_CURRENT);
	}

	/** @return int|null */
	public function getPageForumLimit() {
		return $this->getData(self::P__PAGE_FORUM_LIMIT);
	}

	/** @return int|null */
	public function getPageLimitForum() {
		return $this->getData(self::P__PAGE_LIMIT_FORUM);
	}

	/** @return int|null */
	public function getPageLimitPost() {
		return $this->getData(self::P__PAGE_LIMIT_POST);
	}

	/** @return int|null */
	public function getPageLimitPrivateMessages() {
		return $this->getData(self::P__PAGE_LIMIT_PRIVATE_MESSAGES);
	}

	/** @return int|null */
	public function getPageMessagesCurrent() {
		return $this->getData(self::P__PAGE_MESSAGES_CURRENT);
	}

	/** @return int|null */
	public function getPageMyPostsCurrent() {
		return $this->getData(self::P__PAGE_MY_POSTS_CURRENT);
	}

	/** @return int|null */
	public function getPageMyPostsLimit() {
		return $this->getData(self::P__PAGE_MY_POSTS_LIMIT);
	}

	/**
	 * Возвращает null или натуральное число из множества (1, 2, 3).
	 * @return int|null
	 */
	public function getPageMyPostsStatus() {
		return $this->getData(self::P__PAGE_MY_POSTS_STATUS);
	}

	/** @return int|null */
	public function getPageMyTopicsCurrent() {
		return $this->getData(self::P__PAGE_MY_TOPICS_CURRENT);
	}

	/** @return int|null */
	public function getPageMyTopicsLimit() {
		return $this->getData(self::P__PAGE_MY_TOPICS_LIMIT);
	}

	/**
	 * Возвращает null или натуральное число из множества (1, 2, 3).
	 * @return int|null
	 */
	public function getPageMyTopicStatus() {
		return $this->getData(self::P__PAGE_MY_TOPIC_STATUS);
	}

	/** @return int|null */
	public function getPageSearchCurrent() {
		return $this->getData(self::P__PAGE_SEARCH_CURRENT);
	}

	/** @return int|null */
	public function getPageSearchLimit() {
		return $this->getData(self::P__PAGE_SEARCH_LIMIT);
	}

	/** @return int|null */
	public function getPageSortMyPost() {
		return $this->getData(self::P__PAGE_SORT_MY_POST);
	}

	/** @return int|null */
	public function getPageSortMyTopic() {
		return $this->getData(self::P__PAGE_SORT_MY_TOPIC);
	}

	/** @return int|null */
	public function getPageSortForum() {
		return $this->getData(self::P__PAGE_SORT_FORUM);
	}

	/** @return int|null */
	public function getPageSortPost() {
		return $this->getData(self::P__PAGE_SORT_POST);
	}

	/** @return mixed[] */
	public function getPostForumData() {
		/** @var mixed[] $result */
		$result = $this->getData(self::P__POST_FORUM_DATA);
		if (is_null($result)) {
			$result = array();
		}
		return $result;
	}

	/** @return mixed[]|null */
	public function getPostPMData() {
		return $this->getData(self::P__POST_PM_DATA);
	}

	/** @return string|null */
	public function getSearchType() {
		return $this->getData(self::P__SEARCH_TYPE);
	}

	/** @return string|null */
	public function getSearchValue() {
		return $this->getData(self::P__SEARCH_VALUE);
	}

	public function getVisitorIdBySession($sessId) {
		$id = Df_Forum_Model_Visitor::i()->load($sessId, 'session_id')->getId();
		return $id;
	}

	/**
	 * @param string[] $value
	 * @return Df_Forum_Model_Session
	 */
	public function setBackProductLinks(array $value) {
		$this->setData(self::P__BACK_PRODUCT_LINKS, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setLastViewedObjectId($value) {
		df_param_integer($value, 0);
		$this->setData(self::LAST_VIEWED_OBJECT_ID, $value);
		return $this;
	}

	/**
	 * @param int[] $value
	 * @return Df_Forum_Model_Session
	 */
	public function setLastViewedObjectIds(array $value) {
		$this->setData(self::LAST_VIEWED_OBJECT_IDS, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageCurrentForum($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_CURRENT_FORUM, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageCurrentPost($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_CURRENT_POST, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageForumCurrent($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_FORUM_CURRENT, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageLimitForum($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_LIMIT_FORUM, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageLimitPost($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_LIMIT_POST, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageLimitPrivateMessages($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_LIMIT_PRIVATE_MESSAGES, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageMessagesCurrent($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_MESSAGES_CURRENT, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageMyPostsCurrent($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_MY_POSTS_CURRENT, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageMyPostsLimit($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_MY_POSTS_LIMIT, $value);
		return $this;
	}

	/**
	 * $value — натуральное число из множества (1, 2, 3).
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageMyPostsStatus($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_MY_POSTS_STATUS, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageMyTopicsCurrent($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_MY_TOPICS_CURRENT, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageMyTopicsLimit($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_MY_TOPICS_LIMIT, $value);
		return $this;
	}

	/**
	 * $value — натуральное число из множества (1, 2, 3).
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageMyTopicStatus($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_MY_TOPIC_STATUS, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageForumLimit($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_FORUM_LIMIT, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageSearchCurrent($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_SEARCH_CURRENT, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageSearchLimit($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_SEARCH_LIMIT, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageSortMyPost($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_SORT_MY_POST, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageSortMyTopic($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_SORT_MY_TOPIC, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageSortForum($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_SORT_FORUM, $value);
		return $this;
	}

	/**
	 * @param int $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPageSortPost($value) {
		df_param_integer($value, 0);
		$this->setData(self::P__PAGE_SORT_POST, $value);
		return $this;
	}

	/**
	 * @param mixed[] $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPostForumData(array $value) {
		$this->setData(self::P__POST_FORUM_DATA, $value);
		return $this;
	}

	/**
	 * @param mixed[] $value
	 * @return Df_Forum_Model_Session
	 */
	public function setPostPMData(array $value) {
		$this->setData(self::P__POST_PM_DATA, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Session
	 */
	public function setSearchType($value) {
		df_param_string($value, 0);
		$this->setData(self::P__SEARCH_TYPE, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Session
	 */
	public function setSearchValue($value) {
		df_param_string($value, 0);
		$this->setData(self::P__SEARCH_VALUE, $value);
		return $this;
	}

	/** @return Df_Forum_Model_Session */
	public function unsetOldVisitorEntries() {
		$_5min_back = date('Y-m-d H:i:s', strtotime (now()) - 300);
		$mc = Df_Forum_Model_Visitor::c();
		$mc->getSelect()
			->where('? > time_visited', $_5min_back)
		;
		foreach ($mc as $del) {
			Df_Forum_Model_Visitor::i()->setId($del->getId())->delete();
		}
		return $this;
	}

	/** @return Df_Forum_Model_Session */
	public function unsetPostForumData() {
		$this->unsetData(self::P__POST_FORUM_DATA);
		return $this;
	}

	/** @return Df_Forum_Model_Session */
	public function unsetSearchValue() {
		$this->unsetData(self::P__SEARCH_VALUE);
		return $this;
	}

	public function updateViews() {
		$current_object = Df_Forum_Model_Action::s()->getCurrentTopic();
		$storeId       = Mage::app()->getStore()->getId();
		if ($current_object) {
			$topicId = $current_object->getId();
			$parentId = $current_object->getParentId();
		}
		else {
			$topicId = null;
			$parentId = null;
		}
		if (!rm_session_customer()->getCustomer()) {
			return;
		}
		$id = session_id();
		$visitor_id = $this->getVisitorIdBySession($id);
		$m =
			Df_Forum_Model_Visitor::i()
				->setId($visitor_id)
				->setSystemUserId(
					rm_session_customer()->getCustomer()->getId()
				)
				->setSessionId($id)
				->setTopicId($topicId)
				->setTime_visited(now())
				->setParentId($parentId)
				->setStoreId($storeId)
			;
		$m->save();
		$this->unsetOldVisitorEntries();
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->init('forum');
		$this->updateViews();
	}

	const _CLASS = __CLASS__;
	const P__BACK_PRODUCT_LINKS = 'back_product_links';
	const LAST_VIEWED_OBJECT_ID = 'last_viewed_object_id';
	const LAST_VIEWED_OBJECT_IDS = 'last_viewed_object_ids';
	const P__PAGE_CURRENT_FORUM = 'page_current_forum';
	const P__PAGE_CURRENT_POST = 'page_current_post';
	const P__PAGE_FORUM_CURRENT = 'page_forum_current';
	const P__PAGE_FORUM_LIMIT = 'page_forum_limit';
	const P__PAGE_LIMIT_FORUM = 'page_limit_forum';
	const P__PAGE_LIMIT_POST = 'page_limit_post';
	const P__PAGE_LIMIT_PRIVATE_MESSAGES = 'page_limit_private_messages';
	const P__PAGE_MESSAGES_CURRENT = 'page_messages_current';
	const P__PAGE_MY_POSTS_CURRENT = 'page_my_posts_current';
	const P__PAGE_MY_POSTS_LIMIT = 'page_my_posts_limit';
	const P__PAGE_MY_POSTS_STATUS = 'page_my_posts_status';
	const P__PAGE_MY_TOPICS_CURRENT = 'page_my_topics_current';
	const P__PAGE_MY_TOPICS_LIMIT = 'page_my_topics_limit';
	const P__PAGE_MY_TOPIC_STATUS = 'page_my_topic_status';
	const P__PAGE_SEARCH_CURRENT = 'page_search_current';
	const P__PAGE_SEARCH_LIMIT = 'page_search_limit';
	const P__PAGE_SORT_MY_POST = 'page_sort_my_post';
	const P__PAGE_SORT_MY_TOPIC = 'page_sort_my_topic';
	const P__PAGE_SORT_FORUM = 'page_sort_forum';
	const P__PAGE_SORT_POST = 'page_sort_post';
	const P__POST_FORUM_DATA = 'post_forum_data';
	const P__POST_PM_DATA = 'post_pm_data';
	const P__SEARCH_TYPE = 'search_type';
	const P__SEARCH_VALUE = 'search_value';

	/** @return Df_Forum_Model_Session */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}