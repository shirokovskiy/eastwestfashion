<?php
class Df_Forum_Model_Action_PrivateMessages extends Df_Forum_Model_Action {
	/** @return string[] */
	public function getActions() {
		return array('index','sent','trash');
	}

	const _CLASS = __CLASS__;
	const REQUEST_PARAM__MESSAGE_ID = 'mid';
	const REQUEST_PARAM__REDIRECT_TARGET = 'r';
	/** @return Df_Forum_Model_Action_PrivateMessages */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}