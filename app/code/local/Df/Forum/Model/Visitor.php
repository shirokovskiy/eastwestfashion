<?php
class Df_Forum_Model_Visitor extends Df_Core_Model_Abstract {
	/** @return string|null */
	public function getParentId() {
		return $this->cfg(self::P__PARENT_ID);
	}

	/** @return string|null */
	public function getSessionId() {
		return $this->cfg(self::P__SESSION_ID);
	}

	/** @return string|null */
	public function getStoreId() {
		return $this->cfg(self::P__STORE_ID);
	}

	/** @return string|null */
	public function getSystemUserId() {
		return $this->cfg(self::P__SYSTEM_USER_ID);
	}

	/** @return string|null */
	public function getTimeVisited() {
		return $this->cfg(self::P__TIME_VISITED);
	}

	/** @return string|null */
	public function getTopicId() {
		return $this->cfg(self::P__TOPIC_ID);
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Resource_Visitor::mf());
	}
	const _CLASS = __CLASS__;
	const P__ID = 'visitor_id';
	const P__PARENT_ID = 'parent_id';
	const P__SESSION_ID = 'session_id';
	const P__STORE_ID = 'store_id';
	const P__SYSTEM_USER_ID = 'system_user_id';
	const P__TIME_VISITED = 'time_visited';
	const P__TOPIC_ID = 'topic_id';

	/** @return Df_Forum_Model_Resource_Visitor_Collection */
	public static function c() {return self::s()->getCollection();}
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_Visitor
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/**
	 * @see Df_Forum_Model_Resource_Visitor_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf(__CLASS__);}
	/** @return Df_Forum_Model_Visitor */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}