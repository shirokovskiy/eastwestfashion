<?php
class Df_Forum_Model_System_Source_Limits {
	/** @return array */
	public function toOptionArray() {
		return array(
			array(
				'value' => 0,'label' => '5, 10, 15'
			),array(
				'value' => 1,'label' => '10, 15, 30'
			),array(
				'value' => 2,'label' => '20, 30, 50'
			),);
	}

	const _CLASS = __CLASS__;
}