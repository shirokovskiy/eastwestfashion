<?php
class Df_Forum_Model_Settings extends Df_Core_Model_Settings {
	/** @return Df_Forum_Model_Settings_Advanced */
	public function advanced() {return Df_Forum_Model_Settings_Advanced::s();}
	/** @return Df_Forum_Model_Settings_Avatar */
	public function avatar() {return Df_Forum_Model_Settings_Avatar::s();}
	/** @return Df_Forum_Model_Settings_Design */
	public function design() {return Df_Forum_Model_Settings_Design::s();}
	/** @return Df_Forum_Model_Settings_General */
	public function general() {return Df_Forum_Model_Settings_General::s();}
	/** @return Df_Forum_Model_Settings_Menu */
	public function menu() {return Df_Forum_Model_Settings_Menu::s();}
	/** @return Df_Forum_Model_Settings_Moderation */
	public function moderation() {return Df_Forum_Model_Settings_Moderation::s();}
	/** @return Df_Forum_Model_Settings_Notification */
	public function notification() {return Df_Forum_Model_Settings_Notification::s();}
	/** @return Df_Forum_Model_Settings_ProductView */
	public function productView() {return Df_Forum_Model_Settings_ProductView::s();}
	/** @return Df_Forum_Model_Settings_Recaptcha */
	public function recaptcha() {return Df_Forum_Model_Settings_Recaptcha::s();}
	/** @return Df_Forum_Model_Settings_Sitemap */
	public function sitemap() {return Df_Forum_Model_Settings_Sitemap::s();}
	/** @return Df_Forum_Model_Settings_TinyMCE */
	public function tinyMCE() {return Df_Forum_Model_Settings_TinyMCE::s();}
	/** @return Df_Forum_Model_Settings */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}