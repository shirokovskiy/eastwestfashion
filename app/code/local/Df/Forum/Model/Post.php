<?php
class Df_Forum_Model_Post extends Df_Core_Model_Abstract {
	/** @return string */
	public function getContentShort() {
		return df_text()->chop($text = strip_tags($this->getPost()), $requiredLength = 500);
	}

	/** @return string|null */
	public function getCreatedTime() {return $this->cfg(self::P__CREATED_TIME);}
	
	/** @return Df_Forum_Model_Topic|null */
	public function getForum() {
		return is_null($this->getTopic()) ? null : $this->getTopic()->getParent();
	}
	
	/** @return Df_Forum_Model_Topic|null */
	public function getParent() {return $this->getTopic();}

	/** @return int|null */
	public function getParentId() {return $this->getTopicId();}

	/** @return string|null */
	public function getPost() {return $this->cfg(self::P__POST);}

	/** @return string|null */
	public function getPostOrig() {return $this->cfg(self::P__POST_ORIG);}

	/** @return int */
	public function getProductId() {return intval($this->cfg(self::P__PRODUCT_ID));}

	/** @return Df_Forum_Model_Topic */
	public function getProgenitor() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_Forum_Model_Topic $result */
			$result = $this;
			while (!is_null($result->getParent())) {
				$result = $result->getParent();
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return bool */
	public function getStatus() {return rm_bool($this->cfg(self::P__STATUS));}

	/** @return int */
	public function getSystemUserId() {return intval($this->cfg(self::P__SYSTEM_USER_ID));}
	
	/** @return Df_Forum_Model_Topic|null */
	public function getTopic() {
		if (!isset($this->{__METHOD__})) {
			/** @var Df_Forum_Model_Topic|null $result */
			$result = null;
			if ($this->getTopicId()) {
				$result = Df_Forum_Model_Topic::i()->load($this->getTopicId());
				if (!$result->getId()) {
					$result = null;
				}
			}
			$this->{__METHOD__} = rm_n_set($result);
		}
		return $this->{__METHOD__};
	}

	/** @return int|null */
	public function getTopicId() {return rm_empty_to_null($this->cfg(self::P__PARENT_ID));}

	/** @return string|null */
	public function getUpdateTime() {
		$result = $this->cfg(self::P__UPDATE_TIME);
		if ('0000-00-00 00:00:00' === $result) {
			$result = null;
		}
		return $result;
	}

	/** @return string|null */
	public function getUserName() {return $this->cfg(self::P__USER_NAME);}

	/** @return string|null */
	public function getUserNick() {return $this->cfg(self::P__USER_NICK);}

	/** @return bool */
	public function isProgenitor() {
		return intval($this->getId()) !== intval($this->getProgenitor()->getId());
	}

	/** @return bool */
	public function isSticky() {return rm_bool($this->cfg(self::P__IS_STICKY));}

	/**
	 * @param bool $value
	 * @return Df_Forum_Model_Post
	 */
	public function markAsSticky($value) {
		df_param_boolean($value, 0);
		$this->setData(self::P__IS_STICKY, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Post
	 * @see now()
	 */
	public function setCreatedTime($value) {
		df_param_string($value, 0);
		$this->setData(self::P__CREATED_TIME, $value);
		return $this;
	}

	/**
	 * @override
	 * @param int|null $value
	 * @return Df_Forum_Model_Post
	 */
	public function setId($value) {
		parent::setId($value);
		return $this;
	}

	/**
	 * @param int|null $value
	 * @return Df_Forum_Model_Post
	 */
	public function setParentId($value) {
		df_param_integer($value, 0);
		/**
		 * Вместо 0 устанавливаем null
		 * для совместимости с FOREIGN KEY ON DELETE SET NULL
		 */
		$value = rm_empty_to_null($value);
		$this->setData(self::P__PARENT_ID, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Post
	 */
	public function setPost($value) {
		df_param_string($value, 0);
		$this->setData(self::P__POST, $value);
		return $this;
	}

	/**
	 * @param bool $value
	 * @return Df_Forum_Model_Post
	 */
	public function setStatus($value) {
		df_param_boolean($value, 0);
		$this->setData(self::P__STATUS, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Post
	 * @see now()
	 */
	public function setUpdateTime($value) {
		df_param_string($value, 0);
		$this->setData(self::P__UPDATE_TIME, $value);
		return $this;
	}

	/**
	 * @param string $value
	 * @return Df_Forum_Model_Post
	 */
	public function setUserName($value) {
		df_param_string($value, 0);
		$this->setData(self::P__USER_NAME, $value);
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Resource_Post::mf());
	}
	const _CLASS = __CLASS__;
	const P__CREATED_TIME = 'created_time';
	const P__IS_STICKY = 'is_sticky';
	const P__ID = 'post_id';
	const P__PARENT_ID = 'parent_id';
	const P__POST = 'post';
	const P__POST_ORIG = 'post_orig';
	const P__PRODUCT_ID = 'product_id';
	const P__STATUS = 'status';
	const P__SYSTEM_USER_ID = 'system_user_id';
	const P__UPDATE_TIME = 'update_time';
	const P__USER_NAME = 'user_name';
	const P__USER_NICK = 'user_nick';

	/** @return Df_Forum_Model_Resource_Post_Collection */
	public static function c() {return self::s()->getCollection();}
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_Post
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/**
	 * @see Df_Forum_Model_Resource_Post_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf(__CLASS__);}
	/** @return Df_Forum_Model_Post */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}