<?php
class Df_Forum_Model_Design_Layout extends Df_Core_Model_Abstract {
	/** @return mixed */
	public function toOptionArray() {
		if (!$this->_options) {
			$layouts = array();
			$node = Mage::getConfig()->getNode('global/cms/layouts') ? Mage::getConfig()->getNode('global/cms/layouts') : Mage::getConfig()->getNode('global/page/layouts');
			foreach ($node->children() as $layoutConfig) {
				$this->_options[]= array(
					'value' => (string)$layoutConfig->template,'label' => (string)$layoutConfig->label
				);
			}
		}
		return $this->_options;
	}

	protected $_options;
	const _CLASS = __CLASS__;
}