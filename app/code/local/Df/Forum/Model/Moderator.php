<?php
class Df_Forum_Model_Moderator extends Df_Core_Model_Abstract {
	/** @return string|null */
	public function getSystemUserId() {
		return $this->cfg(self::P__SYSTEM_USER_ID);
	}

	/** @return string|null */
	public function getUserWebsiteId() {
		return $this->cfg(self::P__USER_WEBSITE_ID);
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Forum_Model_Resource_Moderator::mf());
	}

	const _CLASS = __CLASS__;
	const P__ID = 'moderator_id';
	const P__SYSTEM_USER_ID = 'system_user_id';
	const P__USER_WEBSITE_ID = 'user_website_id';

	/** @return Df_Forum_Model_Resource_Moderator_Collection */
	public static function c() {return self::s()->getCollection();}
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_Moderator
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/**
	 * @see Df_Forum_Model_Resource_Moderator_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf(__CLASS__);}
	/** @return Df_Forum_Model_Moderator */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}