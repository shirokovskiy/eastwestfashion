<?php
class Df_Forum_Model_Customer extends Df_Core_Model_Abstract {
	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this
			->_prop(self::P__ID, self::V_INT)
			->_prop(self::P__STATUS, self::V_STRING_NE)
		;
	}
	const _CLASS = __CLASS__;
	const P__ID = 'id';
	const P__STATUS = 'status';
	const P__SYSTEM_USER_DATA = 'system_user_data';
	const P__TOTAL_POSTS = 'total_posts';
	const P__TOTAL_TOPICS = 'total_topics';
	const P__USER_SETTINGS_DATA = 'user_settings_data';
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Forum_Model_Customer
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}