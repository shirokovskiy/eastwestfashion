<?php
class Df_Forum_CustomerController extends Mage_Core_Controller_Front_Action {
	/** @return void */
	public function indexAction() {
		if (0 === Df_Forum_Model_Action::s()->getCurrentCustomerId()) {
			$this->_redirect('forum');
		}
		if (
				Df_Forum_Model_Action::s()->getCurrentCustomerId()
			!==
				Df_Forum_Model_Action::ADMIN__ID
		) {
			/** @var Df_Customer_Model_Customer $customer */
			$customer =
				Df_Customer_Model_Customer::ld(
					Df_Forum_Model_Action::s()->getCurrentCustomerId()
				)
			;
			if (!rm_session_customer()->getCustomer()->getId()) {
				$this->_redirect('forum');
			}
			else {
				Df_Forum_Model_Registry::s()->setCustomer($customer);
			}
		}
		$this->loadLayout();
		$this->_initLayoutMessages('df_forum/session');
		$this->renderLayout();
	}
}