<?php
class Df_Forum_MytopicsController extends Mage_Core_Controller_Front_Action {
	/** @return void */
	public function indexAction() {
		Mage::register('myforumtopics', true);
		$this->loadLayout();
		$this->getLayout()->getBlock('head')->setTitle('Ваши темы');
		$this->_initLayoutMessages('df_forum/session');
		$this->renderLayout();
	}

	/**
	 * @override
	 * @return Df_Forum_MytopicsController
	 */
	public function preDispatch() {
		parent::preDispatch();
		if (!rm_session_customer()->authenticate($this)) {
			$this->setFlag('', 'no-dispatch', true);
		}
		return $this;
	}
}