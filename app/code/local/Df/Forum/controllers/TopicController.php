<?php
class Df_Forum_TopicController extends Mage_Core_Controller_Front_Action {
	/** @return void */
	public function deletePostAction() {
		$this->_init();
		if ($this->getRequest()->getParam('post_id') > 0) {
			/** @var Df_Forum_Model_Post $post */
			$post = Df_Forum_Model_Post::i()->load($this->getRequest()->getParam('post_id'));
			if (
					!df_h()->forum()->topic()->isModerator()
				&&
					(
							$post->getSystemUserId()
					 	!=
							rm_session_customer()->getCustomer()->getId()
					)
			) {
				$this->_forward('index');
			}
			else {
				$ret = $this->getRequest()->getParam('ret');
				$ret = urldecode ($ret);
				try {
					$postModel = Df_Forum_Model_Post::i();
					$postModel->setId($this->getRequest()->getParam('post_id')
					)->delete();
					Df_Forum_Model_Session::s()->addSuccess('Сообщение успешно удалено.');
					$this->_redirect($ret);
				}
				catch(Exception $e) {
					Df_Forum_Model_Session::s()
						->addError(
							'При удалении сообщения произошёл сбой.'
						)
					;
					$this->_redirect($ret);
				}
			}
		}
	}

	/** @return void */
	public function deleteTopicAction() {
		$this->_init();
		if (!df_cfg()->forum()->general()->allowDeleteOwnTopics()) {
			$this->_forward('index');
		}
		else {
			if (0 < intval($this->getRequest()->getParam(Df_Forum_Model_Topic::P__ID))) {
				$model =
					Df_Forum_Model_Topic::i()->load(
						intval(
							$this->getRequest()->getParam(
								Df_Forum_Model_Topic::P__ID
							)
						)
					)
				;
				if (
						!df_h()->forum()->topic()->isModerator()
					&&
						(
								$model->getSystemUserId()
							!=
								rm_session_customer()->getCustomer()->getId()
						)
				) {
					$this->_forward('index');
				}
				else {
					$ret = $this->getRequest()->getParam('ret');
					$ret = urldecode($ret);
					try {
						$this
							->deleteUrlRewrite(
								intval(
									$this->getRequest()->getParam(
										Df_Forum_Model_Topic::P__ID
									)
								)
							)
						;
						$topicModel = Df_Forum_Model_Topic::i();
						$topicModel
							->setId(
								intval(
									$this->getRequest()->getParam(
										Df_Forum_Model_Topic::P__ID
									)
								)
							)
							->delete()
						;
						Df_Forum_Model_Session::s()->addSuccess('Тема успешно удалена.');
						$this->_redirect($ret);
					}
					catch(Exception $e) {
						Df_Forum_Model_Session::s()->addError('При удалении темы произошёл сбой.');
						$this->_redirect($ret);
					}
				}
			}
		}
	}

	/** @return void */
	public function disablePostAction() {
		$ret = $this->getRequest()->getParam('ret');
		$isModerator = df_h()->forum()->topic()->isModerator();
		if (!$isModerator) {
			$this->_forward('index');
		}
		else {
			$postId = $this->getRequest()->getParam('post_id');
			if ($postId) {
				$this->_disablePost($postId);
			}
			Df_Forum_Model_Session::s()->addSuccess('Сообщение скрыто.');
			$ret = urldecode ($ret);
			$this->_redirect($ret);
		}
	}

	/** @return void */
	public function disableTopicAction() {
		$ret = $this->getRequest()->getParam('ret');
		$isModerator = df_h()->forum()->topic()->isModerator();
		if (!$isModerator) {
			$this->_forward('index');
		}
		else {
			/** @var int $topicId */
			$topicId =
				intval(
					$this->getRequest()->getParam(
						Df_Forum_Model_Topic::P__ID
					)
				)
			;
			if (0 !== $topicId) {
				Df_Forum_Model_Session::s()->addSuccess('Тема скрыта');
				$this->_disableTopic($topicId);
			}
			$ret = urldecode ($ret);
			$this->_redirect($ret);
		}
	}

	/** @return void */
	public function editAction() {
		/** @var bool $processed */
		$processed = false;
		$this->_init();
		/** @var mixed[] $data */
		$data = rm_session_customer()->getCustomer()->getData();
		if (empty($data['entity_id'])) {
			$this->_forward('index');
		}
		else {
			/** @var int $product_id */
			$product_id =
				intval(
					$this->getRequest()->getParam(
						Df_Forum_Model_Topic::P__PRODUCT_ID
					)
				)
			;
			if (0 !== $product_id) {
				$product = $this->registerProduct($product_id);
				if (!$this->current_product) {
					$this->_redirect('*/');
					$processed = true;
				}
			}
			if (!$processed) {
				$quoteId = (int)$this->getRequest()->getParam('quote', false);
				/** @var int $parentObjectId */
				$parentObjectId =
					intval(
						$this->getRequest()->getParam(Df_Forum_Model_Topic::P__PARENT_ID)
					)
				;
				$postId = (int)$this->getRequest()->getParam('post_id', false);
				$redirect = $this->getRequest()->getParam('ret', false);
				$sb = $this->getRequest()->getParam('sb', false);
				$parentObjectModel = Df_Forum_Model_Topic::i()->load($parentObjectId);
				if ($sb && !empty($product_id)) {
					$this->_setProductBackLink($sb);
				}
				if (
						Df_Forum_Model_Action::s()->getCurrentTopic()->getProductId()
					&&
						empty($product_id)
				) {
					$this->registerProduct(Df_Forum_Model_Action::s()->getCurrentTopic()->getProductId());
				}
				if ((!$parentObjectModel->getId() && $parentObjectId)) {
					$this->_redirect('*/');
					return;
				}
				if (($parentObjectModel->getStoreId() != Mage::app()->getStore()->getId() && $parentObjectModel->getStoreId())
					|| (
						$parentObjectModel->getStatus() == 0 && $parentObjectModel->getId())
					|| (
							Df_Forum_Model_Action::s()->getCurrentTopic()->getStatus() == 0
						&&
							Df_Forum_Model_Action::s()->getCurrentTopic()->getId()
					)
					||
						(
								Df_Forum_Model_Action::s()->getCurrentTopic()->getStoreId() != Mage::app()->getStore()->getId()
							&&
								Df_Forum_Model_Action::s()->getCurrentTopic()->getStoreId()
						)
				) {
					$this->_redirect('*/');
					return;
				}
				$postModel = Df_Forum_Model_Post::i()->load($postId);
				$isModerator = df_h()->forum()->topic()->isModerator();
				if (
						(
								(
										is_null(Df_Forum_Model_Action::s()->getCurrentTopic())
									&&
										(0 !== Df_Forum_Model_Action::s()->getCurrentTopicId())
								)
							||
								($parentObjectId && !$parentObjectModel->getId())
						||
							(
									$postId
								&&
										$postModel->getSystemUserId()
									!=
										rm_session_customer()->getCustomer()->getId()
							)
						)
					&&
						!$isModerator
				) {
					$this->_redirect('*/');
					return;
				}
				else {
					Mage::register('redirect', $redirect);
					Mage::register('quote', $quoteId);
					Mage::register('current_object_post', $postModel);
					Mage::register('current_object_parent', $parentObjectModel);
					$this->_loadLayout();
				}
			}
		}
	}

	/** @return void */
	public function enablePostAction() {
		$ret = $this->getRequest()->getParam('ret');
		$isModerator = df_h()->forum()->topic()->isModerator();
		if (!$isModerator) {
			$this->_forward('index');
		}
		else {
			$postId = $this->getRequest()->getParam('post_id');
			if ($postId) {
				$this->_enablePost($postId);
			}
			Df_Forum_Model_Session::s()->addSuccess('Сообщение опубликовано.');
			$ret = urldecode ($ret);
			if (Mage::getStoreConfig('df_forum/notification/customer__allow_subscription')) {
				$obj = Df_Forum_Model_Post::i()->load($postId);
				if ($obj->getId()) {
					$obj_topic = Df_Forum_Model_Topic::i()->load($obj->getParentId());
				}
				if ($obj->getId()) {
					df_h()->forum()->notification()->notifyCustomersPost(
						array(
							'id' => $obj->getParentId()
							,Df_Forum_Model_Topic::P__PARENT_ID => $obj_topic->getParentId()
							,Df_Forum_Model_Post::P__SYSTEM_USER_ID => $obj->getSystemUserId()
							,'post_id' => $postId
						)
					);
				}
			}
			$this->_redirect($ret);
		}
	}

	/** @return void */
	public function enableTopicAction() {
		$ret = $this->getRequest()->getParam('ret');
		$isModerator = df_h()->forum()->topic()->isModerator();
		if (!$isModerator) {
			$this->_forward('index');
		}
		else {
			/** @var int $topicId */
			$topicId =
				intval(
					$this->getRequest()->getParam(
						Df_Forum_Model_Topic::P__ID
					)
				)
			;
			if ($topicId) {
				$this->_enableTopic($topicId);
				Df_Forum_Model_Session::s()->addSuccess('Тема опубликована.');
			}
			$ret = urldecode ($ret);
			$this->_redirect($ret);
		}
	}

	/** @return void */
	public function indexAction() {
		$this->_init();
		Df_Forum_Model_Session::s()->setLastViewedObjectId(0);
		$this->_loadLayout();
	}

	/** @return void */
	public function newAction() {
		$this->_forward('edit');
	}

	/**
	 * @override
	 * @return Df_Forum_TopicController
	 */
	public function preDispatch() {
		parent::preDispatch();
		$action = $this->getRequest()->getActionName();
		if ($action == 'new' || $action == 'edit') {
			if (!rm_session_customer()->authenticate($this)
			) {
				$this->setFlag('', 'no-dispatch', true);
			}
		}
		return $this;
	}

	/** @return void */
	public function saveAction() {
		/** @var bool $processed */
		$processed = false;
		$this->_init();
		/** @var mixed[] $data */
		$data = rm_session_customer()->getCustomer()->getData();
		if (empty($data['entity_id'])) {
			$this->_forward('index');
		}
		else {
			$system_user_id = $data['entity_id'];
			if (!$this->getRequest()->getPost()) {
				$this->_forward('index');
			}
			else {
				try {
					$postData = $this->getRequest()->getPost();
					$topicId =
						!empty($postData[Df_Forum_Model_Topic::P__ID])
						? $postData[Df_Forum_Model_Topic::P__ID]
						: null
					;
					$postId = !empty($postData['post_id']) ? $postData['post_id'] : null;
					$redirect = !empty($postData['redirect']) ? urldecode ($postData['redirect']) : null;
					$userName = $data['firstname'] . ' ' . $data['lastname'];
					$topic = Df_Forum_Model_Topic::i()->load($topicId);
					$isModerator = df_h()->forum()->topic()->isModerator();
					if (!$isModerator && df_cfg()->forum()->recaptcha()->isEnabledForPM()) {
						$err = $this->validateRecaptcha();
						if ($err) {
							Df_Forum_Model_Session::s()->addError($err);
							$back = !empty($postData['back']) ? urldecode ($postData['back']) : 'index';
							Df_Forum_Model_Session::s()->setPostForumData($postData);
							$this->_redirect($back, array('ret' => $redirect));
							$processed = true;
						}
					}
					if (!$processed) {
						if (!empty($postData['quote'])) {
							$postData['Post'] = $postData['quote'] . $postData['Post'];
						}
						if (
								!$topicId
							||
								(
										$topic->getSystemUserId() == $system_user_id
									&&
										!empty($postData['Title'])
								)
							|| (
								$isModerator && !empty($postData['Title']))
						) {
							$topicModel = Df_Forum_Model_Topic::i();
							$topicModel
								->setId($topicId)
								->setStatus(
										!df_cfg()->forum()->moderation()->needModerateTopics()
									||
										$topicId
									||
										$isModerator
								)
								->setTitle($postData['Title'])
							;
							$topicModel->setIconId($postData[Df_Forum_Model_Topic::P__ICON_ID]);
							if (!empty($postData['description'])) {
								//$description    = strip_tags(ereg_replace("[\r\t\n\v]","",$postData['description']));
								$description = strip_tags(preg_replace("/[\r\t\n\v]/", '', $postData['description']));
								$topicModel->setDescription($description);
							}
							if (!empty($postData['product_id'])) {
								$topicModel->setProductId($postData['product_id']);
							}
							if (!$isModerator || !$topicId) {
								$topicModel->setUserName($data['firstname'] . ' ' . $data['lastname'])->setSystemUserId($system_user_id);
							}
							if (df_cfg()->forum()->general()->allowNicks()
								&& !empty($postData['use_nick'])
							) {
								if ($postData['use_nick'] == 'on') {
									$topicModel->setUserNick($postData['NickName']);
								}
							}
							else {
								$topicModel->setUserNick('');
							}
							if (!$topicId) {
								$url_text = df_h()->forum()->topic()->buildUrlKeyFromTitle($postData['Title'], $this->getRequest()->getParam('id')
								);
								$topicModel->setUrlText('df_forum/' . $url_text);
								$topicModel->setUrlTextShort($url_text);
								$topicModel->setCreatedTime(now());
							}
							else {
								$url_text = $topic->getUrlText();
								$topicModel->setUpdatedTime(now());
							}
							if (!empty($postData[Df_Forum_Model_Topic::P__PARENT_ID]) && !$topicId) {
								$topicModel->setParentId($postData[Df_Forum_Model_Topic::P__PARENT_ID]);
								$topicModel
									->setStoreId(
										df_h()->forum()->topic()->___getStoreId(
											$postData[Df_Forum_Model_Topic::P__PARENT_ID]
										)
									)
								;
								if (df_h()->forum()->topic()->hasSubTopics(
									intval(
										$postData[Df_Forum_Model_Topic::P__PARENT_ID])
									)
								) {
									$topicModel->markAsSubTopic(true);
								}
							}
							$topicModel->save();
							$topicModel
								->setEntityUserId(
									intval($data['entity_id'])
								)
							;
							df_h()->forum()->topic()->setEntity($topicModel->getId(), $topicModel, true);
							if (!$topicId && !empty($postData[Df_Forum_Model_Topic::P__PARENT_ID])) {
								$storeId =
									df_h()->forum()->topic()->___getStoreId(
										$postData[Df_Forum_Model_Topic::P__PARENT_ID]
									)
								;
								$id_path = $this->buildIdPath($topicModel->getId());
								$requestPath = $this->buildRequestPath($topicModel->getId());
								df_h()->forum()->topic()
									->updateUrlRewrite(
										$id_path
										,$storeId
										,'forum/' . $url_text
										,$requestPath
									)
								;
								Df_Forum_Model_Session::s()
									->addSuccess(
											df_cfg()->forum()->moderation()->needModerateTopics()
										&&
											!$topicId
										&&
											!$isModerator
										? 'Ваша тема появится на форуме после одобрения модератором'
										: 'Ваша тема успешно опубликована'
									)
								;
								$url_text = 'df_forum/' . $url_text;
							}
							else {
								Df_Forum_Model_Session::s()
									->addSuccess(
											df_cfg()->forum()->moderation()->needModerateTopics()
										&&
											!$topicId
										&&
											!$isModerator
										? 'Ваши правки темы будут опубликованы на форуме после одобрения модератором'
										: 'Ваши правки темы успешно опубликованы'
									)
								;
							}
							$postData['id'] = $topicModel->getId();
							if (
									!$isModerator
								&&
									df_cfg()->forum()->notification()->moderator()->isEnabled()
								&&
									!$topicId
							) {
								df_h()->forum()->notification()
									->notifyModerNewTopic(
										$postData
										,$topicModel->getUrlText()
										,$userName
									)
								;
							}
						}
						if (empty($url_text) && $topic) {
							$url_text = $this->getUrlRewrite($topic);
						}
						$topicId = $topicId ? $topicId : $topicModel->getId();
						$postData['id'] = $topicId;
						if ($postId) {
							$post = Df_Forum_Model_Post::i()->load($postId);
						}
						if ((!$postId || $post->getSystemUserId() == $system_user_id) || $isModerator) {
							$postModel = Df_Forum_Model_Post::i();
							$postModel
								->setPostId($postId)
								->setStatus(
										!df_cfg()->forum()->moderation()->needModeratePosts()
									||
										$postId
									||
										$isModerator
								)//active
								->setParentId($topicId)->setPost($postData['Post'])->setPostOrig(strip_tags($postData['Post']))
							;
							if (!empty($postData['product_id'])) {
								$postModel->setProductId($postData['product_id']);
							}
							if (!$isModerator || !$postId) {
								$postModel->setUserName($data['firstname'] . ' ' . $data['lastname'])->setSystemUserId($system_user_id);
							}
							if (df_cfg()->forum()->general()->allowNicks()
								&& !empty($postData['use_nick'])
							) {
								if ($postData['use_nick'] == 'on') {
									$postModel->setUserNick($postData['NickName']);
								}
							}
							else {
								$postModel->setUserNick('');
							}
							if (!$postId) {
								$postModel->setCreatedTime(now());
								df_h()->forum()->topic()
									->setUserForum(
										$postModel->getSystemUserId()
										,$postModel
										,df_h()->forum()->topic()->___getStoreId(
											$postData[Df_Forum_Model_Topic::P__PARENT_ID]
										)
									)
								;
								df_h()->forum()->topic()
									->updateTotalPostsUser(
										$system_user_id
										,df_h()->forum()->topic()->___getStoreId(
											$postData[Df_Forum_Model_Topic::P__PARENT_ID]
										)
									)
								;
							}
							else {
								$postModel->setUpdatedTime(now());
							}
							$postModel->save();
							if (!df_cfg()->forum()->notification()->moderator()->isEnabled()) {
								$postData['post_id'] = $postModel->getId();
								df_h()->forum()->notification()->notifyModerNewPost($postData, $url_text, $userName);
							}
							Df_Forum_Model_Session::s()
								->addSuccess(
										(
												!df_cfg()->forum()->moderation()->needModeratePosts()
											||
												$postId
											||
												$isModerator
											? 'Ваше сообщение успешно опубликовано'
											: 'Ваще сообщение будет опублкиовано после одобрения модератором'
										)
								)
							;
							Df_Forum_Model_Session::s()->unsetPostForumData();
							if (Mage::getStoreConfig('df_forum/notification/customer__allow_subscription')) {
								if (!empty($postData['notify_me'])) {
									if ($postData['notify_me'] == 'on') {
										df_h()->forum()->notification()->addToNotifyList($topicId);
										Df_Forum_Model_Session::s()
											->addSuccess('Вы успешно подписались на обновления этой темы.')
										;
									}
									else {
										$show_uns_message = df_h()->forum()->notification()->removeFromNotifyList($topicId);
										if ($show_uns_message) {
											Df_Forum_Model_Session::s()
												->addSuccess(
													'Вы успешно отписались от обновлений этой темы.'
												)
											;
										}
									}
								}
								else {
									$show_uns_message = df_h()->forum()->notification()->removeFromNotifyList($topicId);
									if ($show_uns_message) {
										Df_Forum_Model_Session::s()
											->addSuccess(
												'Вы успешно отписались от обновлений этой темы.'
											)
										;
									}
								}
								if (!df_cfg()->forum()->moderation()->needModeratePosts() || $isModerator) {
									$postData[Df_Forum_Model_Topic::P__SYSTEM_USER_ID] = $system_user_id;
									$postData['post_id'] = $postModel->getId();
									df_h()->forum()->notification()->notifyCustomersPost($postData);
								}
							}
						}
						if (df_cfg()->forum()->moderation()->needModerateTopics() && !empty($topicModel) && !$isModerator && !$topicId) {
							$url_text = Df_Forum_Model_Topic::i()->load($topicModel->getParentId())->getUrlText();
						}
						if (!empty($redirect)) {
							$this->_redirect($redirect);
						}
						else {
							$this->_redirect('*/*/viewreply/id/' . $postModel->getId());
						}
						//$this->_redirect($url_text, array( '_current'=>false, '_escape'=>false, '_use_rewrite'=>false, '_query'=>array(self::PAGE_VAR_NAME => 1, self::SORT_VAR_NAME => 1)));
					}
				}
				catch(Exception $e) {
					Df_Forum_Model_Session::s()
						->addError(
							rm_sprintf(
								'При выполнении операции произошёл сбой: «%s»', $e->getMessage()
							)
						)
					;
					$this
						->_redirect(
							'df_forum/'
							,array(
								'_current' => false
								,'_escape' => false
								,'_use_rewrite' => false
							)
						)
					;
				}
			}
		}
	}

	/** @return void */
	public function unsubscribeAction() {
		$hash = $this->getRequest()->getParam('h');
		/** @var int $topic_id */
		$topic_id = intval($this->getRequest()->getParam(Df_Forum_Model_Topic::P__ID));
		$res = df_h()->forum()->notification()->deleteByHash($topic_id, $hash);
		if ($res) {
			if ($res->getId()) {
				$o = Df_Forum_Model_Topic::i()->load($res->getTopicId());
				Df_Forum_Model_Session::s()
					->addSuccess(
						rm_sprintf(
							'Вы успешно отписались от обновлений темы «%s».', $o->getTitle()
						)
					)
				;
			}
		}
		$this->_redirect('*/');
	}

	/** @return void */
	public function viewAction() {
		$this->_init();
		$search = strip_tags(df_trim($this->getRequest()->getParam(self::SEARCH_VAR, false)));
		if ($search) {
			Df_Forum_Model_Session::s()->setSearchValue($search);
		}
		if (0 !== Df_Forum_Model_Action::s()->getCurrentTopicId()) {
			$this->_initObject();
			Df_Forum_Model_Session::s()->unsetPostForumData();
		}
	}

	/** @return void */
	public function viewreplyAction() {
		/** @var int $id */
		$id = intval($this->getRequest()->getParam('id'));
		$redirect = df_h()->forum()->viewreply()->getViewReplyUrlObj($id);
		$this->_redirectUrl($redirect->getData('url') . $redirect->getData('identifier'));
	}

	/** @return void */
	public function massDisableAction() {
		$post = $this->getRequest()->getPost();
		$ret = $this->getRequest()->getParam('ret');
		$ret = urldecode ($ret);
		$isModerator = df_h()->forum()->topic()->isModerator();
		if (!$isModerator || empty($post['df-forum-action-element'])) {
			$this->_forward('index');
		}
		else {
			$all = count($post['df-forum-action-element']);
			$type = $post['type'];
			foreach ($post['df-forum-action-element'] as $id) {
				if ($type == 'topic') {
					$this->_disableTopic($id);
				}
				else if ($type == 'post') {
					$this->_disablePost($id);
				}
			}
			if ($type == 'topic') {
				Df_Forum_Model_Session::s()->addSuccess('Указанные темы успешно скрыты с форума.');
			}
			else if ($type == 'post') {
				Df_Forum_Model_Session::s()->addSuccess('Указанные сообщеничя успешно скрыты с форума.');
			}
			$this->_redirect($ret);
		}
	}

	/** @return void */
	public function massEnableAction() {
		$post = $this->getRequest()->getPost();
		$ret = $this->getRequest()->getParam('ret');
		$ret = urldecode ($ret);
		$isModerator = df_h()->forum()->topic()->isModerator();
		if (!$isModerator || empty($post['df-forum-action-element'])) {
			$this->_forward('index');
		}
		else {
			$all = count($post['df-forum-action-element']);
			$type = $post['type'];
			foreach ($post['df-forum-action-element'] as $id) {
				if ($type == 'topic') {
					$this->_enableTopic($id);
				}
				else if ($type == 'post') {
					$notify = $this->_enablePost($id);
					if (Mage::getStoreConfig('df_forum/notification/customer__allow_subscription') && $notify) {
						$obj = Df_Forum_Model_Post::i()->load($id);
						if ($obj->getId()) {
							$obj_topic = Df_Forum_Model_Topic::i()->load($obj->getParentId());
						}
						if ($obj->getId()) {
							df_h()->forum()->notification()
								->notifyCustomersPost(
									array(
										'id' => $obj->getParentId()
										,Df_Forum_Model_Post::P__PARENT_ID => $obj_topic->getParentId()
										,Df_Forum_Model_Post::P__SYSTEM_USER_ID => $obj->getSystemUserId()
										,'post_id' => $id
								)
							);
						}
					}
				}
			}
			if ($type == 'topic') {
				Df_Forum_Model_Session::s()->addSuccess('Указанные темы успешно опубликованы на форуме.');
			}
			else if ($type == 'post') {
				Df_Forum_Model_Session::s()->addSuccess('Указанные сообщения успешно опубликованы на форуме.');
			}
			$this->_redirect($ret);
		}
	}

	/** @return void */
	public function massDeleteAction() {
		$post = $this->getRequest()->getPost();
		$ret = $this->getRequest()->getParam('ret');
		$isModerator = df_h()->forum()->topic()->isModerator();
		$ret = urldecode ($ret);
		if (!$isModerator || empty($post['df-forum-action-element'])) {
			$this->_forward('index');
		}
		else {
			$all = count($post['df-forum-action-element']);
			$type = $post['type'];
			foreach ($post['df-forum-action-element'] as $id) {
				if ($type == 'topic') {
					try {
						$this->deleteUrlRewrite($id);
						$topicModel = Df_Forum_Model_Topic::i();
						$topicModel->setId($id)->delete();
					}
					catch(Exception $e) {
						Df_Forum_Model_Session::s()->addError('При удалении темы произошёл сбой');
						$this->_redirect($ret);
					}
				}
				else if ($type == 'post') {
					try {
						$postModel = Df_Forum_Model_Post::i();
						$postModel->setId($id)->delete();
					}
					catch(Exception $e) {
						Df_Forum_Model_Session::s()->addError('При удалении сообщения произошёл сбой.');
						$this->_redirect($ret);
					}
				}
			}
			if ($type == 'topic') {
				Df_Forum_Model_Session::s()->addSuccess('Указанные темы успешно удалены.');
			}
			else if ($type == 'post') {
				Df_Forum_Model_Session::s()->addSuccess('Указанные сообщения успешно удалены.');
			}
			$this->_redirect($ret);
		}
	}

	/**
	 * @param $back
	 * @return void
	 */
	private function _setProductBackLink($back) {
		$links = Df_Forum_Model_Session::s()->getBackProductLinks();
		if (!$links) {
			$links = array();
		}
		$store = Mage::app()->getStore();
		$links[$store->getId() . '_' . Mage::registry('current_product')->getId()]
			= urldecode ($back);
		Df_Forum_Model_Session::s()->setBackProductLinks($links);
	}

	/**
	 * @param int|null $topic_id
	 * @return void
	 */
	private function _disableTopic($topic_id) {
		$posts = Df_Forum_Model_Post::c();
		if (!$topic_id) {
			$posts->getSelect()->where('parent_id is null');
		}
		else {
			$posts->getSelect()->where('? = parent_id', $topic_id);
		}
		foreach ($posts as $post) {
			if ($post->getId()) {
				$this->_disablePost($post->getId(), $post);
			}
		}
		$topic = Df_Forum_Model_Topic::i()->load($topic_id);
		if ($topic->getId()) {
			$topic->setStatus(false);
			$topic->save();
		}
	}

	/**
	 * @param $topic_id
	 * @return void
	 */
	private function _enableTopic($topic_id) {
		$posts = Df_Forum_Model_Post::c();
		if (!$topic_id) {
			$posts->getSelect()->where('parent_id is null');
		}
		else {
			$posts->getSelect()->where('? = parent_id', $topic_id);
		}
		foreach ($posts as $post) {
			if ($post->getId()) {
				$this->_enablePost($post->getId(), $post);
			}
		}
		$topic = Df_Forum_Model_Topic::i()->load($topic_id);
		if ($topic->getId()) {
			$topic->setStatus(true);
			$topic->save();
		}
	}

	/**
	 * @param $_postId
	 * @param Df_Forum_Model_Post|null $post [optional]
	 * @return bool
	 */
	private function _disablePost($_postId, $post = null) {
		if (!$post) {
			$post = Df_Forum_Model_Post::i()->load($_postId);
		}
		/** @var bool $result */
		$result = ($post->getId() && $post->getStatus() == 1);
		if ($result) {
			$post->setStatus(false);
			$post->save();
		}
		return $result;
	}

	/**
	 * @param $_postId
	 * @param Df_Forum_Model_Post|null $post [optional]
	 * @return bool
	 */
	private function _enablePost($_postId, $post = null) {
		if (!$post) {
			$post = Df_Forum_Model_Post::i()->load($_postId);
		}
		/** @var bool $result */
		$result = ($post->getId() && $post->getStatus() == 0);
		if ($result) {
			$post->setStatus(true);
			$post->save();
		}
		return $result;
	}

	/** @return void */
	private function _init() {
		$this->blockTopic = Mage::getBlockSingleton('df_forum/topic');
		df_h()->forum()->topic()->setObject($this->blockTopic);
	}

	/** @return void */
	private function _initObject() {
		if (!df_h()->forum()->topic()->canShow(Df_Forum_Model_Action::s()->getCurrentTopic())) {
			$this->_forward('index');
		}
		else {
			Df_Forum_Model_Session::s()
				->setLastViewedObjectId(
					Df_Forum_Model_Action::s()->getCurrentTopic()->getId()
				)
			;
			$objects_viewed =
				Df_Forum_Model_Session::s()->getLastViewedObjectIds()
				? Df_Forum_Model_Session::s()->getLastViewedObjectIds()
				: array()
			;
			if (!in_array(Df_Forum_Model_Action::s()->getCurrentTopicId(), $objects_viewed)) {
				array_push($objects_viewed, Df_Forum_Model_Action::s()->getCurrentTopicId());
				df_h()->forum()->topic()->updateEnitityView(Df_Forum_Model_Action::s()->getCurrentTopicId());
				Df_Forum_Model_Session::s()->setLastViewedObjectIds($objects_viewed);
			}
			$this->setMeta = true;
			if (Df_Forum_Model_Action::s()->getCurrentTopic()->getProductId()) {
				$this->registerProduct(Df_Forum_Model_Action::s()->getCurrentTopic()->getProductId());
				$sb = $this->getRequest()->getParam('sb', false);
				if ($sb && $this->current_product) {
					$this->_setProductBackLink($sb);
				}
			}
			$this->_loadLayout();
		}
	}

	/** @return void */
	private function _loadLayout() {
		$this->loadLayout();
		$this->_initLayoutMessages('df_forum/session');
		df_h()->forum()->topic()->_isActive = true;
		if ($this->setMeta) {
			$this->_setMetaData();
		}
		$this->renderLayout();
	}

	/** @return void */
	private function _setMetaData() {
		$meta_description = false;
		$meta_keywords = false;
		if (Df_Forum_Model_Action::s()->getCurrentTopic()->isCategory()) {
			$meta_description = Df_Forum_Model_Action::s()->getCurrentTopic()->getMetaDescription();
			$meta_keywords = Df_Forum_Model_Action::s()->getCurrentTopic()->getMetaKeywords();
		}
		else if (0 !== Df_Forum_Model_Action::s()->getCurrentTopic()->getParentId()) {
			/** @var Df_Forum_Model_Topic $parent_object */
			$parent_object =
				Df_Forum_Model_Topic::i()->load(
					Df_Forum_Model_Action::s()->getCurrentTopic()->getParentId()
				)
			;
			if ($parent_object->getId() && $parent_object->getStatus() == 1) {
				$meta_description = $parent_object->getMetaDescription();
				$meta_keywords = $parent_object->getMetaKeywords();
				Mage::register('parent_current_object', $parent_object);
			}
			else {
				$this->_redirect('*/');
			}
		}
		$head = $this->getLayout()->getBlock('head');
		if ($head && $meta_description && $meta_description != '' && $meta_keywords && $meta_keywords != '') {
			$head->setKeywords($meta_keywords);
			$head->setDescription($meta_description);
		}
	}

	/**
	 * @param $id
	 * @return string
	 */
	private function buildIdPath($id) {
		return df_h()->forum()->topic()->buildIdPath($id);
	}

	/**
	 * @param $id
	 * @return string
	 */
	private function buildRequestPath($id) {
		return df_h()->forum()->topic()->buildRequestPath($id);
	}

	/**
	 * @param $id
	 */
	private function deleteUrlRewrite($id) {
		$id_path = $this->buildIdPath($id);
		df_h()->forum()->topic()->deleteUrlRewrite($id_path);
	}

	/**
	 * @param $topicModel
	 * @return mixed
	 */
	private function getUrlRewrite($topicModel) {
		return Df_Forum_Model_Topic::i()->load($topicModel->getId())->getUrlText();
	}

	/** @return void */
	private function includeRecaptchLib() {
		require_once 'recaptchalib.php';
	}

	/**
	 * @param int $product_id
	 * @return Df_Catalog_Model_Product
	 */
	private function registerProduct($product_id) {
		df_param_integer($product_id, 0);
		/** @var Df_Catalog_Model_Product $result */
		$result = df_product();
		$result
			->setStoreId(Mage::app()->getStore()->getId())
			->load($product_id)
		;
		if ($result->getId()) {
			if (!Mage::helper('catalog/product')->canShow($result)) {
				$result = false;
			}
			if (!in_array(Mage::app()->getStore()->getWebsiteId(), $product->getWebsiteIds())) {
				$result = false;
			}
			if ($result) {
				Mage::register('current_product', $result);
				$this->current_product = $result;
			}
		}
		return $result;
	}

	/** @return bool|string */
	private function validateRecaptcha() {
		global $_SERVER;
		/** @var bool|string $result */
		$result = false;
		if (df_cfg()->forum()->recaptcha()->isEnabledForPM()) {
			$this->includeRecaptchLib();
			$privatekey = df_cfg()->forum()->recaptcha()->getKeyPrivate();
			$recaptcha_challenge_field = $this->getRequest()->getPost('recaptcha_challenge_field');
			$recaptcha_response_field = $this->getRequest()->getPost('recaptcha_response_field');
			$resp =
				recaptcha_check_answer(
					$privatekey
					,$_SERVER["REMOTE_ADDR"]
					,$recaptcha_challenge_field
					,$recaptcha_response_field
				)
			;
			if (!$resp->is_valid) {
				$result = 'Вы неправильно распознали слова с картинки.';
			}
		}
		return $result;
	}
	public $blockTopic;
	private $init = false;
	private $setMeta = false;
	const PAGE_VAR_NAME = 'p';
	const SEARCH_VAR = 'q_f';
	const SORT_VAR_NAME = 'sort';
	private $current_product;
}