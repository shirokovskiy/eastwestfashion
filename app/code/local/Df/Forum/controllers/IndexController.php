<?php
class Df_Forum_IndexController extends Mage_Core_Controller_Front_Action {
	/** @return void */
	public function indexAction() {
		/** @var bool $processed */
		$processed = false;
		$title = $this->getRequest()->getParam(self::TITLE_VAR);
		if ($title && $title != '') {
			$topic_id = df_h()->forum()->topic()->getIdTopicByTitle($title, true);
			if ($topic_id) {
				$this->_forward('view', 'topic', null, array('id' => $topic_id));
				$processed = true;
			}
		}
		if (!$processed) {
			$this->_forward('index', 'topic');
		}
	}
	const TITLE_VAR = 't';
}