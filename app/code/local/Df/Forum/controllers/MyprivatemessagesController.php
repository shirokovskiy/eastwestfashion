<?php
require_once 'Mage/Customer/controllers/AccountController.php';
class Df_Forum_MyprivatemessagesController extends Mage_Customer_AccountController {
	/** @return void */
	public function addAction() {
		if (
				(0 === Df_Forum_Model_Action::s()->getCurrentCustomerId())
			||
				(Df_Forum_Model_Action::ADMIN__ID === Df_Forum_Model_Action::s()->getCurrentCustomerId())
			||
				(rm_session_customer()->getCustomer()->getId() === Df_Forum_Model_Action::s()->getCurrentCustomerId())
		) {
			$this->_redirect('forum');
		}
		else {
			/** @var Df_Customer_Model_Customer $customer */
			$customer =
				Df_Customer_Model_Customer::ld(
					Df_Forum_Model_Action::s()->getCurrentCustomerId()
				)
			;
			if (!$customer->getId()) {
				$this->_redirect('forum');
			}
			else {
				Mage::register('customer_system', $customer);
				Mage::register('myprivatemessages_add', true);
				$this->_loadLayout();
			}
		}
	}

	/** @return void */
	public function cleartrashAction() {
	}

	/** @return void */
	public function deleteAction() {
		$mid =
			$this->getRequest()->getParam(
				Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__MESSAGE_ID
			)
		;
		$r = $this->getRequest()->getParam(Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__REDIRECT_TARGET);
		if (!$mid) {
			$this->_redirect('forum');
		}
		else {
			$this->_delete($mid);
			$this->_getSession()->addSuccess('Указанное личное сообщение успешно удалено.');
			$this->_doRedirect($r);
		}
	}

	/** @return void */
	public function indexAction() {
		Mage::register('myprivatemessages_inbox', true);
		$this->_loadLayout();
	}

	/** @return void */
	public function massdeleteAction() {
		$r = $this->getRequest()->getParam(Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__REDIRECT_TARGET);
		$post = $this->getRequest()->getPost();
		try {
			/** @var int $count */
			$count = 0;
			$ids = $post['df-forum-action-element'];
			if (is_array($ids)) {
				foreach ($ids as $id) {
					$this->_delete($id);
					$count++;
				}
			}
			$this->_getSession()
				->addSuccess(
					rm_sprintf('Указанные личные сообщения успешно удалены: %d.', $count)
				)
			;
			$this->_doRedirect($r);
		}
		catch(Exception $e) {
			$this->_getSession()
				->addError(
					rm_sprintf(
						'При удалении сообщения произошёл сбой: «%s».'
						,$e->getMessage()
					)
				)
			;
			$this->_redirect('*/*/index');
		}
	}

	/** @return void */
	public function massreadAction() {
		$r = $this->getRequest()->getParam(Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__REDIRECT_TARGET);
		$post = $this->getRequest()->getPost();
		try {
			/** @var int $count */
			$count = 0;
			$ids = $post['df-forum-action-element'];
			if (is_array($ids)) {
				foreach ($ids as $id) {
					$this->_read($id);
					$count++;
				}
			}
			$this->_getSession()
				->addSuccess(
					rm_sprintf('Указанные личные сообщения помечены как прочитанные: %d.', $count)
				)
			;
			$this->_doRedirect($r);
		}
		catch(Exception $e) {
			$this->_getSession()
				->addError(
					rm_sprintf(
						'При пометке сообщения прочитанным произошёл сбой: «%s».'
						,$e->getMessage()
					)
				)
			;
			$this->_redirect('*/*/index');
		}
	}

	/** @return void */
	public function masstrashAction() {
		$r = $this->getRequest()->getParam(Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__REDIRECT_TARGET);
		$post = $this->getRequest()->getPost();
		try {
			/** @var int $count */
			$count = 0;
			$ids = $post['df-forum-action-element'];
			if (is_array($ids)) {
				foreach ($ids as $id) {
					$this->_trash($id);
					$count++;
				}
			}
			$this->_getSession()
				->addSuccess(
					rm_sprintf('Указанные личные сообщения отправлены в мусорку: %d.', $count)
				)
			;
			$this->_doRedirect($r);
		}
		catch(Exception $e) {
			$this->_getSession()
				->addError(
					rm_sprintf(
						'При отправки сообщения в мусорку произошёл сбой: «%s».'
						,$e->getMessage()
					)
				)
			;
			$this->_redirect('*/*/index');
		}
	}

	/** @return void */
	public function massundoAction() {
		$r = $this->getRequest()->getParam(Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__REDIRECT_TARGET);
		$post = $this->getRequest()->getPost();
		try {
			/** @var int $count */
			$count = 0;
			$ids = $post['df-forum-action-element'];
			if (is_array($ids)) {
				foreach ($ids as $id) {
					$this->_undotrash($id);
					$count++;
				}
			}
			$this->_getSession()
				->addSuccess(
					rm_sprintf('Указанные личные сообщения возвращены из мусорки: %d.', $count)
				)
			;
			$this->_doRedirect($r);
		}
		catch(Exception $e) {
			$this->_getSession()
				->addError(
					rm_sprintf(
						'При возврате сообщения из мусорки произошёл сбой: «%s».'
						,$e->getMessage()
					)
				)
			;
			$this->_redirect('*/*/index');
		}
	}

	/** @return void */
	public function movetrashAction() {
		$mid =
			$this->getRequest()->getParam(
				Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__MESSAGE_ID
			)
		;
		$r = $this->getRequest()->getParam(Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__REDIRECT_TARGET);
		if (!$mid) {
			$this->_redirect('forum');
		}
		else {
			$this->_trash($mid);
			$this->_getSession()->addSuccess('Указанное сообщение отправлено в мусорку.');
			$this->_doRedirect($r);
		}
	}

	/**
	 * @override
	 * @return Df_Forum_MyprivatemessagesController
	 */
	public function preDispatch() {
		parent::preDispatch();
		if (!rm_session_customer()->authenticate($this)
		) {
			$this->setFlag('', 'no-dispatch', true);
		}
		return $this;
	}

	/** @return void */
	public function sentAction() {
		Mage::register('myprivatemessages_sent', true);
		$this->_loadLayout();
	}

	/** @return void */
	public function trashAction() {
		Mage::register('myprivatemessages_trash', true);
		$this->_loadLayout();
	}

	/** @return void */
	public function saveAction() {
		/** @var bool $processed */
		$processed = false;
		if ($this->getRequest()->getPost()) {
			$isModerator = df_h()->forum()->topic()->isModerator();
			$postData = $this->getRequest()->getPost();
			$valid = $this->validateData($postData);
			if ($valid) {
				try {
					if (!$isModerator && df_cfg()->forum()->recaptcha()->isEnabledForPM()) {
						$err = $this->validateRecaptcha();
						if ($err) {
							$this->_getSession()->addError($err);
							$back = !empty($postData['back']) ? urldecode ($postData['back']) : 'index';
							Df_Forum_Model_Session::s()->setPostPMData($postData);
							if (!empty($postData[Df_Forum_Model_PrivateMessage::P__PARENT_ID])) {
								if (!empty($postData['r'])) {
									$r = $postData['r'];
								}
								else {
									$r =
										df_array_first(
											Df_Forum_Model_Action_PrivateMessages::s()->getActions()
										)
									;
								}
								$this->_redirect(
									'df_forum/myprivatemessages/view'
									,array(
										Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__MESSAGE_ID =>
											$postData[Df_Forum_Model_PrivateMessage::P__PARENT_ID]
										,Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__REDIRECT_TARGET => $r
									)
								);
							}
							else {
								$this
									->_redirect(
										'df_forum/myprivatemessages/add'
										,array(
											Df_Forum_Model_Action::REQUEST_PARAM__CUSTOMER_ID =>
												$postData[Df_Forum_Model_Action::REQUEST_PARAM__CUSTOMER_ID]
										)
									)
								;
							}
							$processed = true;
						}
					}
					if (!$processed) {
						/** @var int|null $parent_id */
						$parent_id =
							rm_empty_to_null(
								intval(
									$postData[Df_Forum_Model_PrivateMessage::P__PARENT_ID]
								)
							)
						;
						$model = Df_Forum_Model_PrivateMessage::i()->load(null);
						$message = strip_tags($postData['privatemessage'], '<b><ul><em>');
						$subject = strip_tags($postData['subject'], '<b><ul><em>');
						$model->setMessage($message);
						$model->setSubject($subject);
						$model->setSentFrom(rm_session_customer()->getCustomer()->getId());
						$model->setSentTo($postData[Df_Forum_Model_Action::REQUEST_PARAM__CUSTOMER_ID]);
						$model->setParentId($parent_id);
						$model->setIsPrimary(1);
						$model->setDateSent(now());
						$this->_getSession()->addSuccess('Сообщение успешно отправлено.');
						$model->save();
						if ($parent_id) {
							$parentModel = Df_Forum_Model_PrivateMessage::i()->load($parent_id);
							$parentModel->setIs_primary(0);
							$parentModel->save();
						}
						if (Mage::getStoreConfig('df_forum/notification/customer__private_messages__enable')) {
							$nick =
								df_h()->forum()->customer()->getCustomerNick(
									rm_session_customer()->getCustomer()->getId()
								)
							;
							$email_to =
								df_h()->forum()->customer()->getEmail(
									$postData[Df_Forum_Model_Action::REQUEST_PARAM__CUSTOMER_ID]
								)
							;
							df_h()->forum()->customer()->notifyCustomerPrivateMessage($nick, $email_to, $subject);
						}
						$this->_redirect('df_forum/myprivatemessages/sent');
					}
				}
				catch(Exception $e) {
					$this->_getSession()
						->addError(
							rm_sprintf(
								'При сохраннии личного сообщения произошёл сбой: «%s».'
								,$e->getMessage()
							)
						)
					;
					$this
						->_redirect(
							'*/*/add'
							,array(
								Df_Forum_Model_Action::REQUEST_PARAM__CUSTOMER_ID =>
									$postData[Df_Forum_Model_Action::REQUEST_PARAM__CUSTOMER_ID]
							)
						)
					;
				}
			}
			else {
				$this->_getSession()
					->addError(
						rm_sprintf(
							'При пометке сообщения прочитанным произошёл сбой: «%s».'
							,'недопустимые данные'
						)
					)
				;
				$this
					->_redirect(
						'*/*/add'
						,array(
							Df_Forum_Model_Action::REQUEST_PARAM__CUSTOMER_ID =>
								$postData[Df_Forum_Model_Action::REQUEST_PARAM__CUSTOMER_ID]
						)
					)
				;
			}
		}
	}

	/** @return void */
	public function undotrashAction() {
		$mid =
			$this->getRequest()->getParam(
				Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__MESSAGE_ID
			)
		;
		$r = $this->getRequest()->getParam(Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__REDIRECT_TARGET);
		if (!$mid) {
			$this->_redirect('forum');
		}
		else {
			$this->_undotrash($mid);
			$this->_getSession()
				->addSuccess(
					'Указанное личное сообщение успешно возвращего из мусорки.'
				)
			;
			$this->_doRedirect($r);
		}
	}

	/** @return void */
	public function viewAction() {
		Mage::register('myprivatemessages_view', true);
		$mid =
			$this->getRequest()->getParam(
				Df_Forum_Model_Action_PrivateMessages::REQUEST_PARAM__MESSAGE_ID
			)
		;
		/** @var Df_Forum_Model_PrivateMessage $message */
		$message = Df_Forum_Model_PrivateMessage::i()->load($mid);
		if (!$message->getId()
			|| ($message->getSentFrom() != rm_session_customer()->getCustomer()->getId()
				&& $message->getSentTo() != rm_session_customer()->getCustomer()->getId())
		) {
			$this->_redirect('forum');
		}
		else {
			if ($message->isRead() && ($message->getSentTo() == rm_session_customer()->getCustomer()->getId())) {
				$this->_read($message->getId());
			}
			$this->_loadLayout();
		}
	}

	/** @return void */
	private function _clearTrash() {
		$collection = Df_Forum_Model_PrivateMessage::c();
		$collection->getSelect()
			->where('? = is_trash', 1)
			->where('? = sent_to', rm_session_customer()->getCustomer()->getId())
			->where('? = is_deleteinbox', 0)
			->where('? = is_primary', 1)
		;
		if ($collection->getSize()) {
			foreach ($collection as $object) {
				try {
					$o = Df_Forum_Model_PrivateMessage::i()->load($object->getId());
					$o->setIs_deleteinbox();
					$o->save();
				}
				catch(Exception $e) {
					//ERRORS DEBUG
				}
			}
		}
	}

	/**
	 * @param $mid
	 * @return void
	 */
	private function _delete($mid) {
		$model = Df_Forum_Model_PrivateMessage::i()->load($mid);
		if (!$model->getId()
			|| ($model->getSentFrom() != rm_session_customer()->getCustomer()->getId()
				&& $model->getSentTo() != rm_session_customer()->getCustomer()->getId())
		) {
			$this->_redirect('forum');
		}
		else {
			if ($model->getSentFrom() == rm_session_customer()->getCustomer()->getId()) {
				$model->setIs_deletesent(true);
			}
			if ($model->getSentTo() == rm_session_customer()->getCustomer()->getId()) {
				$model->setIs_deleteinbox(true);
			}
			$model->save();
		}
	}

	/**
	 * @param $r
	 * @return void
	 */
	private function _doRedirect($r) {
		if (in_array($r, Df_Forum_Model_Action_PrivateMessages::s()->getActions())) {
			if ($r == 'index') {
				$this->_redirect('*/*/index');
			}
			else if ($r == 'sent') {
				$this->_redirect('*/*/sent');
			}
			else if ($r == 'trash') {
				$this->_redirect('*/*/trash');
			}
		}
		else {
			$this->_redirect('*/*/index');
		}
	}

	/** @return void */
	private function _loadLayout() {
		$msg = $this->_getSession()->getMessages(true);
		$this->loadLayout();
		if (Mage::registry('myprivatemessages_trash') || Mage::registry('myprivatemessages_sent')
			|| Mage::registry('myprivatemessages_add')
		) {
			if ($my_account_block = $this->getLayout()->getBlock('customer_account_navigation')
			) {
				if (Mage::registry('myprivatemessages_trash')) {
					$my_account_block->setActive('df_forum/myprivatemessages/');
				}
				if (Mage::registry('myprivatemessages_sent')) {
					$my_account_block->setActive('df_forum/myprivatemessages/');
				}
			}
		}
		$this->getLayout()->getBlock('head')->setTitle('Ваши личные сообщения');
		$this->getLayout()->getMessagesBlock()->addMessages($msg);
		$this->_initLayoutMessages('df_forum/session');
		$this->renderLayout();
	}

	/**
	 * @param $mid
	 * @return void
	 */
	private function _read($mid) {
		/** @var Df_Forum_Model_PrivateMessage $model */
		$model = Df_Forum_Model_PrivateMessage::i()->load($mid);
		if (!$model->getId() || ($model->getSentTo() != rm_session_customer()->getCustomer()->getId())) {
			$this->_redirect('forum');
		}
		else {
			$model->setRead(true);
			$model->save();
		}
	}

	/**
	 * @param $mid
	 * @return void
	 */
	private function _trash($mid) {
		$model = Df_Forum_Model_PrivateMessage::i()->load($mid);
		if (!$model->getId() || $model->getSentTo() != rm_session_customer()->getCustomer()->getId()) {
			$this->_redirect('forum');
		}
		else {
			$model->setIsTrash(true);
			$model->save();
		}
	}

	/**
	 * @param $mid
	 * @return void
	 */
	private function _undotrash($mid) {
		$model = Df_Forum_Model_PrivateMessage::i()->load($mid);
		if (!$model->getId() || $model->getSentTo() != rm_session_customer()->getCustomer()->getId()) {
			$this->_redirect('forum');
		}
		else {
			$model->setIsTrash(0);
			$model->save();
		}
	}

	/** @return void */
	private function includeRecaptchLib() {
		require_once 'recaptchalib.php';
	}

	/**
	 * @param $post
	 * @return bool
	 */
	private function validateData($post) {
		// TODO add validation for magento registrated customers only
		return !empty($post[Df_Forum_Model_Action::REQUEST_PARAM__CUSTOMER_ID]);
	}

	/** @return bool|string */
	private function validateRecaptcha() {
		/** @var $result $result */
		$result = false;
		global $_SERVER;
		if (df_cfg()->forum()->recaptcha()->isEnabledForPM()) {
			$this->includeRecaptchLib();
			$privatekey = df_cfg()->forum()->recaptcha()->getKeyPrivate();
			$recaptcha_challenge_field = $this->getRequest()->getPost('recaptcha_challenge_field');
			$recaptcha_response_field = $this->getRequest()->getPost('recaptcha_response_field');
			$resp
				= recaptcha_check_answer ($privatekey, $_SERVER["REMOTE_ADDR"], $recaptcha_challenge_field, $recaptcha_response_field
			);
			if (!$resp->is_valid) {
				$result = 'Вы неправильно распознали слова на картинке.';
			}
		}
		return $result;
	}
}