<?php
class Df_Forum_SearchController extends Mage_Core_Controller_Front_Action {
	/** @return void */
	public function indexAction() {
		$this->search = df_trim($this->getRequest()->getParam(self::SEARCH_VAR)
		)
			&& df_trim($this->getRequest()->getParam(self::SEARCH_VAR)
			) != '' ? strip_tags(df_trim($this->getRequest()->getParam(self::SEARCH_VAR)
			)
		) : strip_tags(Df_Forum_Model_Session::s()->getSearchValue()
		);
		$this->reset = $this->getRequest()->getParam(self::RESET_VAR);
		if ($this->reset || $this->search == '') {
			$this->redirectSearch();
		}
		else {
			Mage::register('search_value_page', $this->search);
			Df_Forum_Model_Session::s()->setSearchValue($this->search);
			$this->loadLayout();
			$this->_initLayoutMessages('catalog/session');
			df_h()->forum()->topic()->_isActive = true;
			$this->renderLayout();
		}
	}

	/** @return void */
	private function eraseSearchValue() {
		Df_Forum_Model_Session::s()->unsetSearchValue();
	}

	/** @return void */
	private function redirectSearch() {
		$redirect_url = $this->getRequest()->getParam(self::REDIRECT_URL);
		$this->eraseSearchValue();
		if ($redirect_url) {
			$this->_redirect($redirect_url);
		}
		else {
			$this->_redirect(self::REDIRECT_DEFAULT);
		}
	}
	const SEARCH_VAR = 'q_f';
	const RESET_VAR = 'r';
	const REDIRECT_URL = 'r_url';
	const REDIRECT_DEFAULT = 'df_forum/';
	private $search;
	private $posts_id_not;
	private $reset;
}