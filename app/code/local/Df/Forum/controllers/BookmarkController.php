<?php
class Df_Forum_BookmarkController extends Mage_Core_Controller_Front_Action {
	/** @return void */
	public function indexAction() {
		/** @var bool $forward */
		$forward = false;
		$postData = $this->getRequest()->getPost();
		if (empty($postData['___action'])) {
			$this->_forward('index', 'topic');
			$forward = true;
		}
		else if ($postData['___action'] != 'view') {
			$this->_forward('index', 'topic');
			$forward = true;
		}
		else if (!empty($postData['redirect'])) {
			Mage::register('redirect', $postData['redirect']);
		}
		if (!$forward) {
			$this->loadLayout();
			df_h()->forum()->topic()->_isActive = true;
			$this->renderLayout();
		}
	}
}