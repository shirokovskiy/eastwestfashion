<?php
class Df_Forum_Adminhtml_PostController extends Mage_Adminhtml_Controller_Action {
	/** @return void */
	public function deleteAction() {
		if ($this->getRequest()->getParam('id') > 0
		) {
			try {
				$post = Df_Forum_Model_Post::i();
				$post->setId($this->getRequest()->getParam('id')
				)->delete();
				rm_session()->addSuccess('Сообщение успешно удалено.');
				$this->_redirect('*/*/', array(
						'store' => $this->getRequest()->getParam('store')
					)
				);
			}
			catch(Exception $e) {
				rm_exception_to_session($e);
				$this->_redirect('*/*/edit', array(
						'id' => $this->getRequest()->getParam('id'),'store' => $this->getRequest()->getParam('store')
					)
				);
			}
		}
		$this->_redirect('*/*/', array(
				'store' => $this->getRequest()->getParam('store')
			)
		);
	}


	/** @return void */
	public function editAction() {
		$postId = $this->getRequest()->getParam('id');
		/** @var Df_Forum_Model_Post $post */
		$post = Df_Forum_Model_Post::i()->load($postId);
		$storeId = $this->getRequest()->getParam('store', false);
		if ($storeId) {
			Df_Forum_Model_Registry::s()->setStoreId(intval($storeId));
		}
		if ($post->getId() || $postId == 0) {
			Df_Forum_Model_Registry::s()->setPost($post);
			$this->loadLayout();
			$this->_setActiveMenu('df_forum/post');
			$this->_addBreadcrumb('Сообщения', 'Сообщения');
			$this->_addBreadcrumb('Сообщение', 'Сообщение');
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this
				->_addContent(Df_Forum_Block_Adminhtml_Post_Edit::i())
				->_addLeft(Df_Forum_Block_Adminhtml_Post_Edit_Tabs::i())
			;
			$this->renderLayout();
		}
		else {
			rm_session()->addError('Указанное сообщение не найдено.');
			$this->_redirect('*/*/');
		}
	}

	/** @return void */
	public function indexAction() {
		$storeId = $this->getRequest()->getParam('store');
		Df_Forum_Model_Registry::s()->setStoreId(intval($storeId));
		$this->loadLayout();
		$this->_setActiveMenu('df_forum/post');
		$this->_addBreadcrumb('Сообщения', 'Сообщения');
		$this->renderLayout();
	}

	/** @return void */
	public function massDeleteAction() {
		$params = $this->getRequest()->getParams();
		$ids = $params['post'];
		if (is_array($ids)) {
			foreach ($ids as $id) {
				try {
					$post = Df_Forum_Model_Post::i();
					$post->setId($id)->delete();
				}
				catch(Exception $e) {
					rm_exception_to_session($e);
					$this
						->_redirect(
							'*/*/edit'
							,array(
								'id' => $this->getRequest()->getParam('id')
								,'store' => $this->getRequest()->getParam('store')
							)
						)
					;
				}
			}
		}
		rm_session()->addSuccess('Указанные сообщения были успешно удалены.');
		$this->_redirect('*/*/', array(
				'store' => $this->getRequest()->getParam('store')
			)
		);
	}

	/** @return void */
	public function massStatusAction() {
		$params = $this->getRequest()->getParams();
		/** @var int $status */
		$status = intval(df_a($params, Df_Forum_Model_Topic::P__STATUS));
		$ids = $params['post'];
		if (is_array($ids)) {
			foreach ($ids as $id) {
				/** @var Df_Forum_Model_Post $post */
				$post = Df_Forum_Model_Post::i()->load($id);
				$post
					->setStatus(
						(2 !== $status) && rm_bool($status)
					)
					->setUpdateTime(now())
					->save()
				;
			}
		}
		rm_session()
			->addSuccess(
				rm_sprintf(
					'Указанные сообщения были успешно %s.'
					,rm_bool($status) ? 'опубликованы' : 'скрыты'
				)
			)
		;
		$this->_redirect('*/*/', array(
				'store' => $this->getRequest()->getParam('store')
			)
		);
	}

	/** @return void */
	public function newAction() {
		$this->_forward('edit');
	}

	/** @return void */
	public function saveAction() {
		$set_last_post = false;
		$postData = $this->getRequest()->getPost();
		if (!$postData) {
			$this->_redirect('*/*/', array('store' => $this->getRequest()->getParam('store')));
		}
		else {
			if ($errors = $this->validatePost($postData)) {
				rm_session()->addError($errors);
				rm_session()->setPostData($this->getRequest()->getPost());
				$this->_redirect('*/*/edit', array(
						'id' => $this->getRequest()->getParam('id'),'store' => $this->getRequest()->getParam('store')
					)
				);
			}
			else {
				try {
					$postData = $this->getRequest()->getPost();
					/** @var Df_Forum_Model_Post $post */
					$post = Df_Forum_Model_Post::i();
					$post
						->setId($this->getRequest()->getParam('id'))
						->setStatus(rm_bool($postData[Df_Forum_Model_Post::P__STATUS]))
						->setParentId($postData[Df_Forum_Model_Post::P__PARENT_ID])
						->setPost($postData['post'])
						->markAsSticky(rm_bool($postData['is_sticky']))
					;
					$post->setPostOrig(strval(strip_tags($postData['post'])));
					if (!$this->getRequest()->getParam('id')) {
						$post->setCreatedTime(now());
						$post->setUserName(__ ('admin'));
						$post->setSystemUserId(Df_Forum_Model_Action::ADMIN__ID);
						df_h()->forum()->topic()
							->setUserForum(
								Df_Forum_Model_Action::ADMIN__ID
								,$post
								,df_h()->forum()->topic()->___getStoreId(
									$postData[Df_Forum_Model_Post::P__PARENT_ID]
								)
							)
						;
						df_h()->forum()->topic()
							->updateTotalPostsUser(
								Df_Forum_Model_Action::ADMIN__ID
								,df_h()->forum()->topic()->___getStoreId(
									$postData[Df_Forum_Model_Post::P__PARENT_ID]
								)
							)
						;
					}
					else {
						$post->setUpdateTime(now());
					}
					$post->save();
					rm_session()->addSuccess('Сообщение успешно сохранено.');
					rm_session()->unsetData('post_data');
					if ('edit' === $this->getRequest()->getParam('back')) {
						$this
							->_redirect(
								'*/*/edit'
								,array(
									'id' => $post->getId()
									,'store' => $this->getRequest()->getParam('store', 0)
								)
							)
						;
					}
					else {
						$this
							->_redirect(
								'*/*/'
								,array(
									'store' => $this->getRequest()->getParam('store', 0)
								)
							)
						;
					}
				}
				catch(Exception $e) {
					rm_exception_to_session($e);
					rm_session()->setPostData($this->getRequest()->getPost());
					$this
						->_redirect(
							'*/*/edit'
							,array(
								'id' => $this->getRequest()->getParam('id')
								,'store' => $this->getRequest()->getParam('store')
							)
						)
					;
				}
			}
		}
	}

	/**
	 * @param $parent_id
	 * @return bool|string
	 */
	private function validateParent($parent_id) {
		$errors = '';
		/** @var Df_Forum_Model_Topic $m */
		$m = Df_Forum_Model_Topic::i()->load($parent_id);
		if ($m->hasSubTopics() == 1) {
			$errors .= 'Указанный Вами раздел может содержать только подразделы, но не темы напрямую.<br/>';
		}
		if ($m->isCategory()) {
			$errors .=
				'Вы указали в качестве родителя форум.'
				.' Форум может содержать только темы, но не сообщения напрямую<br/>';
		}
		return !$errors ? false : $errors;
	}

	/**
	 * @param $post
	 * @return bool|string
	 */
	private function validatePost($post) {
		$parent_id = $post[Df_Forum_Model_Post::P__PARENT_ID];
		$errors = $this->validateParent($parent_id);
		return $errors ? $errors : null;
	}
}