<?php
class Df_Forum_Adminhtml_ModeratorsController extends Mage_Adminhtml_Controller_Action {
	/** @return void */
	public function indexAction() {
		if ($this->getRequest()->getQuery('isAjax')) {
			$this->loadLayout();
			$is_customers = $this->getRequest()->getParam('is_customers');
			if ($is_customers) {
				$l = Df_Forum_Block_Adminhtml_Moderators_Index_Tab_Fromcustomermoder::i();
			}
			else {
				$l = Df_Forum_Block_Adminhtml_Moderators_Index_Tab_Gridmoderators::i();
			}
			$this->getResponse()->setBody($l->getGridHtml());
		}
		else {
			$this->loadLayout()
				->_setActiveMenu('df_forum/topic')
				->_addBreadcrumb('Модераторы', 'Модераторы')
			;
			$this
				->_addContent(Df_Forum_Block_Adminhtml_Moderators_Index::i())
				->_addLeft(Df_Forum_Block_Adminhtml_Moderators_Index_Tabs::i())
			;
			$this->renderLayout();
		}
	}

	/** @return void */
	public function massAssignAction() {
		$post = $this->getRequest()->getPost();
		$moder = $post['moderators'];
		if (is_array($moder)) {
			foreach ($moder as $customer_id) {
				$model = Df_Forum_Model_Moderator::i();
				/** @var Mage_Customer_Model_Resource_Customer_Collection $cust */
				$cust = Df_Customer_Model_Resource_Customer_Collection::i();
				$cust
					->getSelect()
					->where('? = entity_id', $customer_id)
				;
				$website_id = $cust->getItemById($customer_id)->getWebsiteId();
				$model->setId(null)->setSystemUserId($customer_id)->setUserWebsiteId($website_id);
				$model->save();
			}
			$total = count($moder);
			rm_session()->addSuccess(
				'Указанные покупатели успешно назначены модераторами');
		}
		if (!$this->getRequest()->getQuery('isAjax')) {
			$this->_redirect('*/*/');
		}
		else {
			$this->loadLayout();
			$this->getResponse()->setBody(
				Df_Forum_Block_Adminhtml_Moderators_Index_Tab_Fromcustomermoder::i()->getGridHtml()
			);
		}
	}

	/** @return void */
	public function massDeleteAction() {
		$post = $this->getRequest()->getPost();
		$moder = $post['moderators'];
		if (is_array($moder)) {
			foreach ($moder as $customer_id) {
				$model =
					Df_Forum_Model_Moderator::i()->load(
						$customer_id
						,Df_Forum_Model_Moderator::P__SYSTEM_USER_ID
					)
				;
				$model->delete();
			}
			rm_session()->addSuccess('Указанные модераторы были уволены.');
		}
		if (!$this->getRequest()->getQuery('isAjax')) {
			$this->_redirect('*/*/');
		}
		else {
			$this->loadLayout();
			$this->getResponse()->setBody(
				Df_Forum_Block_Adminhtml_Moderators_Index_Tab_Fromcustomermoder::i()->getGridHtml()
			);
		}
	}
}