<?php
class Df_Forum_Adminhtml_ForumController extends Mage_Adminhtml_Controller_Action {
	/** @return void */
	public function deleteAction() {
		/** @var int $id */
		$id = intval($this->getRequest()->getParam('id'));
		if (0 < $id) {
			try {
				$this->deleteUrlRewrite($id, 1);
				$forum = Df_Forum_Model_Forum::i();
				$forum->setId($id)->delete();
				rm_session()->addSuccess('Раздел форума и все темы внутри этого раздела были успешно удалены.');
				$this
					->_redirect(
						'*/*/'
						,array('store' => $this->getRequest()->getParam('store', 0)
					)
				);
			}
			catch(Exception $e) {
				rm_exception_to_session($e);
				$this
					->_redirect(
						'*/*/edit'
						,array(
							'id' => $this->getRequest()->getParam('id')
							,'store' => $this->getRequest()->getParam('store', 0)
						)
					)
				;
			}
		}
		$this->_redirect('*/*/', array(
				'store' => $this->getRequest()->getParam('store', 0)
			)
		);
	}

	/** @return void */
	public function editAction() {
		$forumId = $this->getRequest()->getParam('id');
		/** @var Df_Forum_Model_Forum $forum */
		$forum = Df_Forum_Model_Forum::i()->load($forumId);
		$storeId = $this->getRequest()->getParam('store', false);
		if ($storeId) {
			Df_Forum_Model_Registry::s()->setStoreId(intval($storeId));
		}
		if ($forum->getId() || $forumId == 0) {
			Df_Forum_Model_Registry::s()->setForum($forum);
			$this->loadLayout();
			$this->_setActiveMenu('df_forum/forum');
			$this->_addBreadcrumb('Разделы', 'Разделы');
			$this->_addBreadcrumb('Раздел', 'Раздел');
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this
				->_addContent(Df_Forum_Block_Adminhtml_Forum_Edit::i())
				->_addLeft(Df_Forum_Block_Adminhtml_Forum_Edit_Tabs::i())
			;
			$this->renderLayout();
		}
		else {
			rm_session()->addError('Указанный раздел не найден');
			$this->_redirect('*/*/');
		}
	}

	/** @return void */
	public function indexAction() {
		$storeId = $this->getRequest()->getParam('store');
		if ($storeId) {
			Df_Forum_Model_Registry::s()->setStoreId(intval($storeId));
		}
		$this->loadLayout();
		$this->_setActiveMenu('df_forum/forum');
		$this->_addBreadcrumb('Разделы', 'Разделы');
		$this->renderLayout();
	}

	/** @return void */
	public function massDeleteAction() {
		/** @var string[] $ids */
		$ids = $this->getRequest()->getParam('forum');
		if (is_array($ids)) {
			foreach ($ids as $id) {
				/** @var int $id */
				$id = intval($id);
				try {
					$this->deleteUrlRewrite($id, 1);
					$forum = Df_Forum_Model_Forum::i();
					$forum->setId($id)->delete();
				}
				catch(Exception $e) {
					rm_exception_to_session($e);
					$this->_redirect('*/*/edit', array(
							'id' => $this->getRequest()->getParam('id'),'store' => $this->getRequest()->getParam('store', 0)
						)
					);
				}
			}
			rm_session()
				->addSuccess('Указанные разделы форума и все темы внутри этих разделов были успешно удалены.')
			;
		}
		$this->_redirect('*/*/', array('store' => $this->getRequest()->getParam('store', 0)));
	}

	/** @return void */
	public function massStatusAction() {
		$params = $this->getRequest()->getParams();
		$status =
			intval(
				df_a($params, Df_Forum_Model_Topic::P__STATUS)
			)
		;
		/** @var int[]|null $ids */
		$ids = df_a($params, 'forum');
		if (is_array($ids)) {
			foreach ($ids as $id) {
				/** @var int $id */
				$id = intval($id);
				/** @var Df_Forum_Model_Forum $forum */
				$forum = Df_Forum_Model_Forum::i()->load($id);
				$forum->setStatus((2 !== $status) && rm_bool($status));
				$forum->setUpdateTime(now());
				$forum->save();
			}
		}
		rm_session()
			->addSuccess(
				rm_sprintf(
					'Указанные разделы были успешно %s'
					,rm_bool($status) ? 'опубликованы' : 'скрыты'
				)
			)
		;
		$this->_redirect('*/*/', array(
				'store' => $this->getRequest()->getParam('store', 0)
			)
		);
	}

	/** @return void */
	public function newAction() {
		$this->_forward('edit');
	}

	/** @return void */
	public function saveAction() {
		/** @var bool $processed */
		$processed = false;
		$postData = $this->getRequest()->getPost();
		if (!$postData) {
			$this->_redirect('*/*/');
		}
		else {
			try {
				/** @var Df_Forum_Model_Forum $forum */
				$forum = Df_Forum_Model_Forum::i();
				//validate url key
				if (!df_a($postData, Df_Forum_Model_Topic::P__URL_TEXT_SHORT)) {
					$postData[Df_Forum_Model_Topic::P__URL_TEXT_SHORT] =
						df_h()->forum()->topic()->buildUrlKeyFromTitle(
							df_a($postData, 'title')
							,$this->getRequest()->getParam('id')
						)
					;					
				}
				else {
					$notValidUrlKey =
						df_h()->forum()->topic()->validateUrlKey(
							'forum/'
							. df_a($postData, Df_Forum_Model_Topic::P__URL_TEXT_SHORT)
							, $this->getRequest()->getParam('id')
							, df_a($postData, 'store_id')
						)
					;
					if ($notValidUrlKey) {
						rm_session()->addError('Указанный адрес уже занят другой страницей');
						$this
							->_redirect(
								'*/*/edit'
								,array(
									'id' => $this->getRequest()->getParam('id')
									,'store' => $this->getRequest()->getParam('store', 0)
								)
							)
						;
						rm_session()->setPostData($this->getRequest()->getPost());
						$processed = true;
					}
				}
				if (!$processed) {
					$description =
						strip_tags(
							preg_replace("/[\r\t\n\v]/"
								,''
								,df_a($postData, 'description')
							)
						)
					;
					$forum
						->setId(intval($this->getRequest()->getParam('id')))
						->markAsCategory(true)
						->setStatus(rm_bool(df_a($postData, Df_Forum_Model_Topic::P__STATUS)))
						->setUserName('admin')
						->setUrlTextShort(df_a($postData, Df_Forum_Model_Topic::P__URL_TEXT_SHORT))
						->setUrlText('forum/' . df_a($postData, Df_Forum_Model_Topic::P__URL_TEXT_SHORT))
						->setTitle(df_a($postData, 'title', ''))
						->setPriority(df_a($postData, 'priority', ''))
						->setIconId(df_a($postData, Df_Forum_Model_Topic::P__ICON_ID))
						->setDescription($description)
						->setStoreId(df_a($postData, 'store_id', Mage::app()->getStore()->getId()))
					;
					if (!$this->getRequest()->getParam('id')) {
						$forum->setCreatedTime(now());
					}
					else {
						$this->updateTopicsRewrites($forum->getId(), df_a($postData, 'store_id'));
						$forum->setUpdateTime(now());
					}
					$forum->setMetaDescription(df_a($postData, Df_Forum_Model_Topic::P__META_DESCRIPTION));
					$forum->setMetaKeywords(df_a($postData, 'meta_keywords'));
					$forum->save();
					df_assert_gt0($forum->getId());
					$forum->setEntityUserId(Df_Forum_Model_Action::ADMIN__ID);
					$id_path = $this->buildIdForumPath($forum->getId());
					$requestPath = $this->buildRequestPath($forum->getId());
					df_h()->forum()->topic()
						->updateUrlRewrite(
							$id_path
							,df_a($postData, 'store_id')
							,'forum/' . df_a($postData, Df_Forum_Model_Topic::P__URL_TEXT_SHORT)
							,$requestPath
						)
					;
					df_h()->forum()->topic()->setEntity($forum->getId(), $forum, true);
					rm_session()->addSuccess('Раздел был успешно обновлён.');
					rm_session()->unsetData('post_data');
					if ('edit' === $this->getRequest()->getParam('back')) {
						$this
							->_redirect(
								'*/*/edit'
								,array(
									'id' => $forum->getId()
									,'store' => $this->getRequest()->getParam('store', 0)
								)
							)
						;
					}
					else {
						$this
							->_redirect(
								'*/*/'
								,array(
									'store' => $this->getRequest()->getParam('store', 0)
								)
							)
						;
					}
				}
			}
			catch(Exception $e) {
				rm_exception_to_session($e);
				rm_session()->setPostData($this->getRequest()->getPost());
				$this
					->_redirect(
						'*/*/edit'
						,array(
							'id' => $this->getRequest()->getParam('id')
							,'store' => $this->getRequest()->getParam('store', 0)
						)
					)
				;
			}
		}
	}

	/**
	 * @param $id
	 * @return string
	 */
	private function buildIdForumPath($id) {
		return df_h()->forum()->topic()->buildIdForumPath($id);
	}


	/**
	 * @param $id
	 * @return string
	 */
	private function buildIdPath($id) {
		return df_h()->forum()->topic()->buildIdPath($id);
	}

	/**
	 * @param $id
	 * @return string
	 */
	private function buildRequestPath($id) {
		return df_h()->forum()->topic()->buildRequestForumPath($id);
	}

	/**
	 * @param $id
	 * @param bool $is_forum
	 */
	private function deleteUrlRewrite($id, $is_forum = false) {
		if (!$is_forum) {
			$id_path = $this->buildIdForumPath($id);
		}
		else {
			$id_path = $this->buildIdPath($id);
		}
		df_h()->forum()->topic()->deleteUrlRewrite($id_path);
	}

	/**
	 * @param int|null $_id
	 * @param $storeId
	 */
	private function updateTopicsRewrites($_id, $storeId) {
		$c = Df_Forum_Model_Topic::c();
		if (!$_id) {
			$c->getSelect()->where('parent_id is null');
		}
		else {
			$c->getSelect()->where('? = parent_id', $_id);
		}
		if ($c->getSize()) {
			foreach ($c as $val) {
				df_h()->forum()->topic()->___updateUrlStoreById($val->getId(), $storeId);
				df_h()->forum()->topic()->updateTopicStoreId($val->getId(), $storeId);
			}
		}
	}
}