<?php
class Df_Forum_Adminhtml_AdmsettingsController extends Mage_Adminhtml_Controller_Action {
	/** @return void */
	public function delavatarAction() {
		$m =
			Df_Forum_Model_Usersettings::i()->load(
				Df_Forum_Model_Action::ADMIN__ID
				,Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
			)
		;
		if ($m->getAvatarName() && $m->getAvatarName() != '') {
			$this->deleteAvatar($m->getAvatarName());
			$m->setAvatarName('');
			$m->save();
			$this->_getSession()->addSuccess('Аватар успешно удалён.');
		}
		$this->_redirect('*/*/index');
	}

	/**
	 * @param $fileNameShort
	 */
	private function deleteAvatar($fileNameShort) {
		/** @var string $fileName */
		$fileName =
			df_concat_path(
				Mage::getBaseDir()
				,df_cfg()->forum()->avatar()->getPath()
				,$fileNameShort
			)
		;
		if (file_exists($fileName)) {
			unlink($fileName);
		}
	}

	/** @return void */
	public function indexAction() {
		$this
			->loadLayout()
			->_setActiveMenu('df_forum/forum')
			->_addBreadcrumb(
				'Витринные настройки администратора'
				,'Витринные настройки администратора'
			)
		;
		/** @var Df_Forum_Model_Usersettings $adminSettings */
		$adminSettings =
			Df_Forum_Model_Usersettings::i()->load(
				Df_Forum_Model_Action::ADMIN__ID
				,Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
			)
		;
		Df_Forum_Model_Registry::s()->setAdminSettings($adminSettings);
		$this->_setActiveMenu('df_forum/forum');
		$this->_addBreadcrumb('Витринные настройки администратора' ,'Витринные настройки администратора');
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this
			->_addContent(Df_Forum_Block_Adminhtml_Admsettings_Edit::i())
			->_addLeft(Df_Forum_Block_Adminhtml_Admsettings_Edit_Tabs::i())
		;
		$this->renderLayout();
	}

	/** @return void */
	public function saveAction() {
		/** @var bool $processed */
		$processed = false;
		$post = $this->getRequest()->getPost();
		if (!$errors = $this->validateNickname($post['nickname'])) {
			try {
				$m =
					Df_Forum_Model_Usersettings::i()->load(
						Df_Forum_Model_Action::ADMIN__ID
						,Df_Forum_Model_Usersettings::P__SYSTEM_USER_ID
					)
				;
				$m->setNickname($post['nickname']);
				$m->setSignature($post['signature']);
				$m->setSystemUserId(Df_Forum_Model_Action::ADMIN__ID);
				$m->setWebsiteId(0);
				if (isset($_FILES['avatar']) && $_FILES['avatar']['name'] != '') {
					$newfileName = $this->getNewFileName($_FILES['avatar']['name']);
					if (($error = $this->uploadFile($_FILES['avatar'], $newfileName)) !== true) {
						$this->_getSession()->addError($error);
						$this->_redirect('*/*/index');
						$processed = true;
					}
					else {
						if ($m->getAvatarName() && $m->getAvatarName() != '') {
							$this->deleteAvatar($m->getAvatarName());
						}
						$m->setAvatarName($newfileName);
					}
				}
				if (!$processed) {
					$m->save();
					$this->_getSession()->addSuccess('Профиль успешно сохранён.');
					rm_session()->unsetPostData();
				}
			}
			catch(Exception $e) {
				rm_exception_to_session($e);
				rm_session()->setPostData($this->getRequest()->getPost());
			}
		}
		else {
			rm_session()->addError($errors);
			rm_session()->setPostData($this->getRequest()->getPost());
		}
		if (!$processed) {
			$this->_redirect('*/*/index');
		}
	}

	/**
	 * @param $filename
	 * @return string
	 */
	private function getNewFileName($filename) {
		$name = mb_strtolower($filename);
		$ext = mb_substr(mb_strrchr($name, '.'), 1);
		$new_name = time() . '_' . Df_Forum_Model_Action::ADMIN__ID . '.' . $ext;
		return $new_name;
	}

	/**
	 * @param $file
	 * @param $newfileName
	 * @return bool|string
	 */
	private function uploadFile($file, $newfileName) {
		/** @var bool $result */
		$result = true;
		$max_size_file = 3670016;
		$uploadavatar = Df_Forum_Model_Uploadavatar::i();
		$uploadavatar->upload_dir =
			df_concat_path(
				Mage::getBaseDir()
				,df_cfg()->forum()->avatar()->getPath()
			) . DS
		;
		$uploadavatar->extensions = array(
			".gif",".jpg",".jpeg",".png"
		);
		$uploadavatar->max_length_filename = 100;
		$uploadavatar->rename_file = true;
		$uploadavatar->filename = Df_Forum_Model_Action::ADMIN__ID;
		$uploadavatar->the_temp_file = $file['tmp_name'];
		$uploadavatar->the_file = $file['name'];
		$uploadavatar->http_error = $file['error'];
		$uploadavatar->replace = true;
		$uploadavatar->do_filename_check = "no";
		$new_name = $newfileName;
		if (!$uploadavatar->upload($new_name)) {
			/** @var array(int => string) $error */
			$error = array();
			$error[0] = rm_sprintf('Файл <b>«%s»</b> успешно загружен.', $file['name']);
			$error[1] = 'Размер загружаемого файла превышает предельно допустимый. Выберите файл меньшего размера.';
			$error[2] = 'Размер загружаемого файла превышает предельно допустимый. Выберите файл меньшего размера.';
			$error[3] = 'При загрузке файла произошёл сбой. Попробуйте загрузить этот файл повторно.';
			$error[4] = 'При загрузке файла произошёл сбой. Попробуйте загрузить этот файл повторно.';
			$error[6] = 'При загрузке файла произошёл сбой: система не нашла временную папку на сервере.';
			$error[7] = 'Произошёл сбой записи загружаемого файла на сервер.';
			$error[8] = 'При загрузке файла произошёл сбой.';
			// end  http errors
			$error[10] = 'Укажите файл для загрузки';
			$error[11] =
				rm_sprintf(
					'Возможна загрузка следующих типов файлов: <b>%s</b>'
					,implode(', ', $uploadavatar->extensions)
				)
			;
			$error[12] =
				'Загрузка этого файла невозможна из-за наличия в его имени недопустимых символов.'
				. '<br/>Вместо русских букв используйте в имени файла соответствующие латинские буквы,'
				. ' а вместо пробела — символ подчёркивания: «_».'
				. '<br/>Удостоверьтесь, что имя файла заканчивается точкой и расширением (например: «.jpeg»).'
			;
			$error[13] =
				rm_sprintf(
					'Загрузка этого файла невозможна, потому что его имя длиннее допустимых %d символов.'
					. ' Укоротите имя файла и загрузите его повторно.'
					, $uploadavatar->max_length_filename
				)
			;
			$error[14] =
				'Загрузка файлов невозможна, потому что система не может найти'
				.' предназначенную программистом для загрузки файлов папку на сервере.'
			;
			$error[15]
				= rm_sprintf(
					'Загрузка файла «%s» невозможна, потому что файл с таким именем уже присутствует на сервере'
					,$file['name']
				)
			;
			$error[16] = rm_sprintf('Загруженному файлу было назначено имя «<b>%s</b>».', $new_name);
			$error[17] = rm_sprintf('Файл «%s» не найден.', $new_name);
			$t = intval($uploadavatar->show_error_string());
			if (!empty($error[$t])) {
				$result = $error[$t];
			}
			else {
				$result = 'При загрузке файла произошёл неизвестный сбой.';
			}
		}
		return $result;
	}

	/**
	 * @param $nickname
	 * @return bool|string
	 */
	private function validateNickname($nickname = '') {
		$errors = '';
		if ($nickname == '') {
			$errors
				.= ($errors != '' ? '<br>' : '')
				. 'Укажите имя.'
			;
		}
		if (mb_strlen($nickname) < 5) {
			$errors
				.= ($errors != '' ? '<br>' : '')
				. 'Укажите имя не короче 5 символов.'
			;
		}
		$check_collection = Df_Forum_Model_Usersettings::c();
		$check_collection->getSelect()
			->where('? = nickname', $nickname)
			->where('? <> system_user_id', Df_Forum_Model_Action::ADMIN__ID)
		;
		$check_collection->setPageSize(1);
		if ($check_collection->getSize()) {
			$errors
				.= ($errors != '' ? '<br>' : '')
				. 'Указанное имя уже используется кем-то.'
			;
		}
		return $errors ? $errors : false;
	}
}