<?php
class Df_Forum_Adminhtml_SubTopicController extends Mage_Adminhtml_Controller_Action {
	/** @return void */
	public function deleteAction() {
		if ($this->getRequest()->getParam('id') > 0) {
			try {
				$this->deleteUrlRewrite($this->getRequest()->getParam('id'));
				$topic = Df_Forum_Model_Topic::i();
				$topic->setId($this->getRequest()->getParam('id'))->delete();
				rm_session()->addSuccess('Указанная тема успешно удалена.');
				$this->_redirect('*/*/', array(
						'store' => $this->getRequest()->getParam('store', 0)
					)
				);
			}
			catch(Exception $e) {
				rm_exception_to_session($e);
				$this->_redirect('*/*/edit', array(
						'id' => $this->getRequest()->getParam('id'),'store' => $this->getRequest()->getParam('store', 0)
					)
				);
			}
		}
		$this->_redirect('*/*/', array(
				'store' => $this->getRequest()->getParam('store', 0)
			)
		);
	}

	/** @return void */
	public function editAction() {
		$topicId = $this->getRequest()->getParam('id');
		$storeId = $this->getRequest()->getParam('store', false);
		if ($storeId) {
			Df_Forum_Model_Registry::s()->setStoreId(intval($storeId));
		}
		/** @var Df_Forum_Model_Topic $topic */
		$topic = Df_Forum_Model_Topic::i()->load($topicId);
		if ($topic->getId() || $topicId == 0) {
			Df_Forum_Model_Registry::s()->setTopic($topic);
			$this->loadLayout();
			$this->_setActiveMenu('df_forum/topic');
			$this->_addBreadcrumb('Подтемы', 'Подтемы');
			$this->_addBreadcrumb('Подтема', 'Подтема');
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this
				->_addContent(Df_Forum_Block_Adminhtml_Subtopic_Edit::i())
				->_addLeft(Df_Forum_Block_Adminhtml_Subtopic_Edit_Tabs::i())
			;
			$this->renderLayout();
		}
		else {
			rm_session()->addError('Указанная подтема не найдена.');
			$this->_redirect('*/*/');
		}
	}

	/** @return void */
	public function indexAction() {
		$storeId = $this->getRequest()->getParam('store');
		if ($storeId) {
			Df_Forum_Model_Registry::s()->setStoreId(intval($storeId));
		}
		$this->loadLayout();
		$this->_setActiveMenu('df_forum/topic');
		$this->_addBreadcrumb('Подтемы', 'Подтемы');
		$this->renderLayout();
	}

	/** @return void */
	public function massDeleteAction() {
		/** @var bool $gotError */
		$gotError = false;
		$params = $this->getRequest()->getParams();
		$ids = $params['topic'];
		if (is_array($ids)) {
			foreach ($ids as $id) {
				try {
					$this->deleteUrlRewrite($id);
					$topic = Df_Forum_Model_Topic::i();
					$topic->setId($id)->delete();
				}
				catch(Exception $e) {
					rm_exception_to_session($e);
					$this->_redirect('*/*/');
					$gotError = true;
					break;
				}
			}
		}
		if (!$gotError) {
			rm_session()->addSuccess('Указанные темы успешно удалены.');
			$this->_redirect('*/*/', array('store' => $this->getRequest()->getParam('store', 0)));
		}
	}

	/** @return void */
	public function massStatusAction() {
		$params = $this->getRequest()->getParams();
		/** @var int $status */
		$status = intval(df_a($params, Df_Forum_Model_Topic::P__STATUS));
		$ids = $params['topic'];
		if (is_array($ids)) {
			foreach ($ids as $id) {
				/** @var Df_Forum_Model_Topic $topic */
				$topic = Df_Forum_Model_Topic::i()->load($id);
				$topic
					->setStatus((2 !== $status) && rm_bool($status))
					->setUpdateTime(now())
					->save()
				;
			}
		}
		rm_session()
			->addSuccess(
				rm_sprintf(
					'Указанные темы были успешно %s'
					,rm_bool($status) ? 'опубликованы' : 'скрыты'
				)
			)
		;
		$this
			->_redirect(
				'*/*/'
				,array(
					'store' => $this->getRequest()->getParam('store', 0)
				)
			)
		;
	}

	/** @return void */
	public function newAction() {
		$this->_forward('edit');
	}

	/** @return void */
	public function saveAction() {
		/** @var bool $processed */
		$processed = false;
		if (!$this->getRequest()->getPost()) {
			$this->_redirect('*/*/', array('store' => $this->getRequest()->getParam('store', 0)));
		}
		else {
			try {
				$postData = $this->getRequest()->getPost();
				/** @var Df_Forum_Model_Topic $topic */
				$topic = Df_Forum_Model_Topic::i();
				//validate url key
				if (
						$postData[Df_Forum_Model_Topic::P__URL_TEXT_SHORT]
					!=
						''
				) {
					$notValidUrlKey =
						df_h()->forum()->topic()
							->validateUrlKey(
								'forum/' . $postData[Df_Forum_Model_Topic::P__URL_TEXT_SHORT]
								,$this->getRequest()->getParam('id')
							)
					;
					if ($notValidUrlKey) {
						rm_session()
							->addError('Указанный адрес уже занят другой страницей')
						;
						rm_session()->setTopicData($this->getRequest()->getPost());
						$this
							->_redirect(
								'*/*/edit'
								,array(
									'id' => $this->getRequest()->getParam('id')
									,'store' => $this->getRequest()->getParam('store', 0)
								)
							)
						;
						$processed = true;
					}
				}
				else {
					$postData[Df_Forum_Model_Topic::P__URL_TEXT_SHORT] =
						df_h()->forum()->topic()->buildUrlKeyFromTitle(
							$postData['title']
							,$this->getRequest()->getParam('id')
						)
					;
				}
				if (!$processed) {
					$storeId =
						df_h()->forum()->topic()->___getStoreId(
							$postData[Df_Forum_Model_Topic::P__PARENT_ID]
						)
					;
					//$description = strip_tags(ereg_replace("[\r\t\n\v]","",$postData['description']));
					$description =
						strip_tags(
							preg_replace(
								"/[\r\t\n\v]/"
								, '', $postData['description']
							)
						)
					;
					$topic
						->setId(intval($this->getRequest()->getParam('id')))
						->setStatus(rm_bool($postData[Df_Forum_Model_Topic::P__STATUS]))
						->setUrlTextShort($postData[Df_Forum_Model_Topic::P__URL_TEXT_SHORT])
						->setParentId($postData[Df_Forum_Model_Topic::P__PARENT_ID])
						->markAsHavingSubTopic(false)
						->setUrlText('forum/' . $postData[Df_Forum_Model_Topic::P__URL_TEXT_SHORT])
						->markAsSubTopic(true)
						->setDescription($description)
						->setIconId($postData[Df_Forum_Model_Topic::P__ICON_ID])
						->setTitle($postData['title'])
						->setStoreId($storeId)
					;
					if (!$this->getRequest()->getParam('id')) {
						$topic->setUserName('admin');
					}
					if (!$this->getRequest()->getParam('id')) {
						$topic->setCreatedTime(now());
					}
					else {
						$topic->setUpdateTime(now());
					}
					$topic->save();
					$topic->setEntityUserId(Df_Forum_Model_Action::ADMIN__ID);
					$id_path = $this->buildIdPath($topic->getId());
					$requestPath = $this->buildRequestPath($topic->getId());
					df_h()->forum()->topic()
						->updateUrlRewrite(
							$id_path
							,$storeId
							,'forum/' . $postData[Df_Forum_Model_Topic::P__URL_TEXT_SHORT]
							,$requestPath
						)
					;
					df_h()->forum()->topic()->setEntity($topic->getId(), $topic);
					rm_session()->addSuccess('Тема успешно сохранена.');
					rm_session()->unsetData('topic_data');
					if ('edit' === $this->getRequest()->getParam('back')) {
						$this
							->_redirect(
								'*/*/edit'
								,array(
									'id' => $topic->getId()
									,'store' => $this->getRequest()->getParam('store', 0)
								)
							)
						;
					}
					else {
						$this
							->_redirect(
								'*/*/'
								,array(
									'store' => $this->getRequest()->getParam('store', 0)
								)
							)
						;
					}
				}
			}
			catch(Exception $e) {
				rm_exception_to_session($e);
				rm_session()->setTopicData($this->getRequest()->getPost()
				);
				$this->_redirect('*/*/edit', array(
						'id' => $this->getRequest()->getParam('id')
					)
				);
			}
		}
	}

	/**
	 * @param $id
	 * @return string
	 */
	private function buildIdPath($id) {
		return df_h()->forum()->topic()->buildIdPath($id);
	}

	/**
	 * @param $id
	 * @return string
	 */
	private function buildRequestPath($id) {
		return df_h()->forum()->topic()->buildRequestPath($id);
	}

	/**
	 * @param $id
	 */
	private function deleteUrlRewrite($id) {
		$id_path = $this->buildIdPath($id);
		df_h()->forum()->topic()->deleteUrlRewrite($id_path);
	}
}