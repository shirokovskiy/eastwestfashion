<?php
class Df_Core_Block_Element_Style extends Df_Core_Block_Element {
	/** @return Df_Core_Model_Style_Selector_Collection */
	public function getSelectors() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->cfg(self::P__SELECTORS);
			if (!$this->{__METHOD__}) {
				$this->{__METHOD__} = Df_Core_Model_Style_Selector_Collection::i();
			}
		}
		return $this->{__METHOD__};
	}

	/**
	 * Если данный метод вернёт true, то система не будет рисовать данный блок.
	 * @override
	 * @return bool
	 */
	protected function isBlockEmpty() {return(0 === $this->getSelectors()->count());}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_prop(self::P__SELECTORS, Df_Core_Model_Style_Selector_Collection::_CLASS, false);
	}
	const _CLASS = __CLASS__;
	const P__SELECTORS = 'selectors';
	/**
	 * @param Df_Core_Model_Style_Selector_Collection|null $selectors [optional]
	 * @return Df_Core_Block_Element_Style
	 */
	public static function i($selectors = null) {
		return df_block(new self(array(self::P__SELECTORS => $selectors)));
	}
}