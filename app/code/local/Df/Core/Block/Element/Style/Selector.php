<?php
class Df_Core_Block_Element_Style_Selector extends Df_Core_Block_Element {
	/** @return Df_Core_Model_Output_Css_Rule_Set */
	public function getRuleSet() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->cfg(self::P__RULE_SET);
			if (!$this->{__METHOD__}) {
				$this->{__METHOD__} = Df_Core_Model_Output_Css_Rule_Set::i();
			}
			df_assert($this->{__METHOD__} instanceof Df_Core_Model_Output_Css_Rule_Set);
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	public function getSelector() {
		/** @var string $result */
		$result = $this->cfg(self::P__SELECTOR);
		if (!$result) {
			df_notify('Требуется селектор');
			/**
			 * Иначе df_result_string приведёт к сбою браузера:
			 *
			 * Content Encoding Error
			 * The page you are trying to view cannot be shown
			 * because it uses an invalid or unsupported form of compression.
			 * Please contact the website owners to inform them of this problem.
			 */
			$result = '';
		}
		return $result;
	}

	/**
	 * Если данный метод вернёт true, то система не будет рисовать данный блок.
	 * @override
	 * @return bool
	 */
	protected function isBlockEmpty() {return(0 === $this->getRuleSet()->count());}

	const P__RULE_SET = 'rule_set';
	const P__SELECTOR = 'selector';
	/**
	 * @param string $selector
	 * @param Df_Core_Model_Output_Css_Rule_Set $ruleSet
	 * @return Df_Core_Block_Element_Style_Selector
	 */
	public static function i($selector, Df_Core_Model_Output_Css_Rule_Set $ruleSet) {
		return df_block(__CLASS__, null, array(
			self::P__RULE_SET => $ruleSet, self::P__SELECTOR => $selector
		));
	}
}