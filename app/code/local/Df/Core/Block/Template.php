<?php
/**
 * Намеренно не делаем это класс абстрактным,
 * потому что его экземпляры имеют практический смысл
 * @see Df_Forum_Block_Toolbar::getHtmlStatistics
 */
class Df_Core_Block_Template extends Mage_Core_Block_Template {
	/**
	 * @param string $key
	 * @param mixed $default[optional]
	 * @return mixed
	 */
	public function cfg($key, $default = null) {
		/** @var mixed $result */
		/**
		 * Обратите внимание,
		 * что здесь нужно вызывать именно @see Df_Core_Block_Template::getData(),
		 * а не @see Varien_Object::_getData()
		 * чтобы работали валидаторы.
		 */
		$result = $this->getData($key);
		// Некоторые фильтры заменяют null на некоторое другое значение,
		// поэтому обязательно учитываем равенство null
		// значения свойства ДО применения фильтров.
		/** @var bool $valueWasNullBeforeFilters */
		$valueWasNullBeforeFilters = df_a($this->_valueWasNullBeforeFilters, $key, true);
		// Раньше вместо !is_null($result) стояло !$result.
		// !is_null выглядит логичней.
		return !is_null($result) && !$valueWasNullBeforeFilters ? $result : $default;
	}

	/**
	 * @override
	 * @throws Exception
	 * @param string $fileName
	 * @return string
	 */
	public function fetchView($fileName) {
		try {
			$result = parent::fetchView($fileName);
		}
		catch(Exception $e) {
			/**
			 * «Failed to delete buffer zlib output compression»
			 * @link http://www.mombu.com/php/php/t-output-buffering-and-zlib-compression-issue-3554315.html
			 */
			if (ob_get_level()) {
				while (@ob_end_clean());
			}
			throw $e;
		}
		if ($this->isItFirstRunForTemplate()) {
			Mage::register($this->getTemplateExecutionKey(), true);
		}
		return $result;
	}

	/**
	 * @override
	 * @return string
	 */
	public function getCacheKey() {
		/** @var string $result */
		$result =
			!$this->needToShow()
			? self::CACHE_KEY_EMPTY
			: parent::getCacheKey()
		;
		return $result;
	}

	/**
	 * @override
	 * @return int|bool
	 */
	public function getCacheLifetime() {
		/** @var int|bool $result */
		$result =
			!$this->needToShow()
			? // кэшируем пустое состояние блока
			  self::CACHE_LIFETIME_STANDARD
			: parent::getCacheLifetime()
		;
		return $result;
	}

	/** @return string */
	public function getCurrentClassNameInMagentoFormat() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				df_h()->core()->reflection()->getModelNameInMagentoFormat(get_class($this))
			;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @throws Mage_Core_Exception
	 * @param string $key
	 * @param null|string|int $index
	 * @return mixed
	 */
	public function getData($key = '', $index = null) {
		/** @var mixed $result */
		if (('' === $key) || array_key_exists($key, $this->_data)) {
			$result = parent::getData($key, $index);
		}
		else {
			// Обрабатываем здесь только те случаи,
			// когда запрашиваются значения неицициализированных свойств объекта
			$result = $this->_applyFilters($key, null);
			// Обратите внимание, что фильтры и валидаторы применяются только единократно,
			// потому что повторно мы в эту ветку кода не попадём
			// из-за срабатывания условия array_key_exists($key, $this->_data) выше
			// (даже если филтры для null вернут null, наличие ключа array('ключ' => null))
			// достаточно, чтобы не попадать в данную точку программы повторно.
			$this->_validate($key, $result);
			$this->_data[$key] = $result;
		}
		return $result;
	}

	/**
	 * @override
	 * @return string|int
	 */
	public function getId() {
		return
			empty($this->_idFieldName) && is_null($this->_getData('id'))
			? // для использования блоков в коллекциях
			  $this->getAutoGeneratedId()
			: parent::getId()
		;
	}

	/**
	 * Этот метод используется, как правило,
	 * в заимоствованных модулях при их рефакторинге.
	 * @return Df_Core_Block_Template
	 */
	public function getRmParent() {return $this->getData(self::P__RM_PARENT);}

	/**
	 * Не кешируем результат метода, чтобы соблюсти спецификации родительского метода.
	 * Например, при вызове сначала setTemplate (A)
	 * последующий вызов getTemplate должен вернуть A.
	 * @override
	 * @return string|null
	 */
	public function getTemplate() {
		return
			!$this->needToShow()
			? null
			: (parent::getTemplate() ? parent::getTemplate() : $this->getDefaultTemplate())
		;
	}

	/** @return bool */
	public function isItFirstRunForTemplate() {
		return is_null(Mage::registry($this->getTemplateExecutionKey()));
	}

	/**
	 * @override
	 * @param string|array(string => mixed) $key
	 * @param mixed $value
	 * @return Df_Core_Block_Template
	 */
	public function setData($key, $value = null) {
		/**
		 * Раньше мы проводили валидацию лишь при извлечении значения свойства,
		 * в методе @see Df_Core_Block_Template::getData().
		 * Однако затем мы сделали улучшение:
		 * перенести валидацию на более раннюю стадию — инициализацию свойства
		 * @see Df_Core_Block_Template::setData(),
		 * и инициализацию валидатора/фильтра
		 * @see Df_Core_Block_Template::_prop().
		 * Это улучшило диагностику случаев установки объекту некорректных значений свойств,
		 * потому что теперь мы возбуждаем исключительную ситуацию
		 * сразу при попытке установки некорректного значения.
		 * А раньше, когда мы проводили валидацию лишь при извлечении значения свойства,
		 * то при диагностике было не вполне понятно,
		 * когда конкретно объекту было присвоено некорректное значение свойства.
		 */
		if (is_array($key)) {
			$this->_checkForNullArray($key);
			$key = $this->_applyFiltersToArray($key);
			$this->_validateArray($key);
		}
		else {
			$this->_checkForNull($key, $value);
			$value = $this->_applyFilters($key, $value);
			$this->_validate($key, $value);
		}
		parent::setData($key, $value);
		return $this;
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		if ($this->_data) {
			$this->_checkForNullArray($this->_data);
			/**
			 * Фильтры мы здесь пока применять не можем,
			 * потому что они ещё не инициализированны
			 * (фильтры будут инициализированы потомками
			 * уже после вызова @see Df_Core_Block_Template::_construct()).
			 * Вместо этого применяем фильтры для начальных данных
			 * в методе @see Df_Core_Block_Template::_prop(),
			 * а для дополнительных данных — в методе @see Df_Core_Block_Template::setData().
			 */
		}
		parent::_construct();
	}

	/**
	 * @param string $name
	 * @return Df_Core_Block_Template
	 */
	protected function addGlobalCss($name) {
		if (!is_null($this->getBlockHead())) {
			$this->getBlockHead()
				->addItem(
					$type = 'js_css'
					,$name
				)
			;
		}
		return $this;
	}

	/**
	 * @param string $name
	 * @return Df_Core_Block_Template
	 */
	protected function addGlobalJs($name) {
		if (!is_null($this->getBlockHead())) {
			$this->getBlockHead()
				->addItem(
					$type = 'js'
					,$name
				)
			;
		}
		return $this;
	}

	/**
	 * @param string $name
	 * @return Df_Core_Block_Template
	 */
	protected function addSkinCss($name) {
		if (!is_null($this->getBlockHead())) {
			$this->getBlockHead()
				->addItem(
					$type = 'skin_css'
					,$name
				)
			;
		}
		return $this;
	}

	/**
	 * @param string $name
	 * @return Df_Core_Block_Template
	 */
	protected function addSkinJs($name) {
		if (!is_null($this->getBlockHead())) {
			$this->getBlockHead()
				->addItem(
					$type = 'skin_js'
					,$name
				)
			;
		}
		return $this;
	}

	/**
	 * @param string $key
	 * @param Zend_Validate_Interface|Df_Zf_Validate_Type|string|mixed[] $validator
	 * @param bool|null $isRequired [optional]
	 * @throws Df_Core_Exception_Internal
	 * @return Df_Core_Model_Abstract
	 */
	protected function _prop($key, $validator, $isRequired = null) {
		/** @var mixed[] $arguments */
		$arguments = func_get_args();
		if (2 < count($arguments)) {
			$isRequired = df_array_last($arguments);
			/** @var bool $hasRequiredFlag */
			$hasRequiredFlag = is_bool($isRequired) || is_null($isRequired);
			if ($hasRequiredFlag) {
				$validator = array_slice($arguments, 1, -1);
			}
			else {
				$isRequired = null;
				$validator = df_array_tail($arguments);
			}
		}
		/** @var Zend_Validate_Interface[] $additionalValidators */
		$additionalValidators = array();
		/** @var Zend_Filter_Interface[] $additionalFilters */
		$additionalFilters = array();
		if (!is_array($validator)) {
			$validator = $this->_resolveValidatorOrFilter($validator, $key);
			df_assert($validator instanceof Zend_Validate_Interface);
		}
		else {
			/** @var array(Zend_Validate_Interface|Df_Zf_Validate_Type|string) $additionalValidatorsRaw */
			$additionalValidatorsRaw = df_array_tail($validator);
			$validator = $this->_resolveValidatorOrFilter(df_array_first($validator), $key);
			df_assert($validator instanceof Zend_Validate_Interface);
			foreach ($additionalValidatorsRaw as $additionalValidatorRaw) {
				/** @var Zend_Validate_Interface|Zend_Filter_Interface|string $additionalValidatorsRaw */
				/** @var Zend_Validate_Interface|Zend_Filter_Interface $additionalValidator */
				$additionalValidator = $this->_resolveValidatorOrFilter($additionalValidatorRaw, $key);
				if ($additionalValidator instanceof Zend_Validate_Interface) {
					$additionalValidators[]= $additionalValidator;
				}
				if ($additionalValidator instanceof Zend_Filter_Interface) {
					$additionalFilters[]= $additionalValidator;
				}
			}
		}
		$this->_addValidator($key, $validator, $isRequired);
		if ($validator instanceof Zend_Filter_Interface) {
			/** @var Zend_Filter_Interface $filter */
			$filter = $validator;
			$this->_addFilter($key, $filter);
		}
		foreach ($additionalFilters as $additionalFilter) {
			/** @var Zend_Filter_Interface $additionalFilter */
			$this->_addFilter($key, $additionalFilter);
		}
		/**
		 * Раньше мы проводили валидацию лишь при извлечении значения свойства,
		 * в методе @see Df_Core_Model_Abstract::getData().
		 * Однако затем мы сделали улучшение:
		 * перенести валидацию на более раннюю стадию — инициализацию свойства
		 * @see Df_Core_Model_Abstract::setData(),
		 * и инициализацию валидатора/фильтра
		 * @see Df_Core_Model_Abstract::_prop().
		 * Это улучшило диагностику случаев установки объекту некорректных значений свойств,
		 * потому что теперь мы возбуждаем исключительную ситуацию
		 * сразу при попытке установки некорректного значения.
		 * А раньше, когда мы проводили валидацию лишь при извлечении значения свойства,
		 * то при диагностике было не вполне понятно,
		 * когда конкретно объекту было присвоено некорректное значение свойства.
		 */
		/** @var bool $hasValueVorTheKey */
		$hasValueVorTheKey = array_key_exists($key, $this->_data);
		if ($hasValueVorTheKey) {
			$this->_validateByConcreteValidator($key, $this->_data[$key], $validator);
		}
		foreach ($additionalValidators as $additionalValidator) {
			/** @var Zend_Validate_Interface $additionalValidator */
			$this->_addValidator($key, $additionalValidator);
			if ($hasValueVorTheKey) {
				$this->_validateByConcreteValidator($key, $this->_data[$key], $additionalValidator);
			}
		}
		return $this;
	}
	/** @var array(string => Zend_Filter_Interface[]) */
	private $_filters = array();
	/** @var array(string => Zend_Validate_Interface[]) */
	private $_validators = array();

	/** @return Mage_Page_Block_Html_Head|null */
	protected function getBlockHead() {
		return df()->layout()->getBlockHead();
	}

	/** @return string|null */
	protected function getDefaultTemplate() {return null;}

	/** @return bool */
	protected function needToShow() {return true;}

	/**
	 * @param string $key
	 * @param Zend_Filter_Interface|string $filter
	 * @return void
	 */
	private function _addFilter($key, $filter) {
		if (is_string($filter)) {
			$filter = self::_getValidatorByName($filter);
		}
		df_assert(is_object($filter));
		df_assert($filter instanceof Zend_Filter_Interface);
		if (!isset($this->_filters[$key])) {
			$this->_filters[$key] = array();
		}
		$this->_filters[$key][] = $filter;
		/**
		 * Не используем @see isset(), потому что для массива
		 * $array = array('a' => null)
		 * isset($array['a']) вернёт false,
		 * что не позволит нам фильтровать значения параметров,
		 * сознательно установленные в null при конструировании объекта.
		 */
		if (array_key_exists($key, $this->_data)) {
			$this->_data[$key] = $filter->filter($this->_data[$key]);
		}
	}

	/**
	 * @param string $key
	 * @param Zend_Validate_Interface $validator
	 * @param bool|null $isRequired [optional]
	 * @return void
	 */
	private function _addValidator($key, Zend_Validate_Interface $validator, $isRequired = null) {
		$isRequired = rm_bool($isRequired);
		/**
		 * Обратите внимание, что флаг $RM_VALIDATOR__REQUIRED надо устанавливать в любом случае,
		 * потому что у нас подавляющее большинство валидаторов является объектами-одиночками,
		 * и нам надо сбросить предыдущее значение $isRequired у этого объекта.
		 */
		$validator->{self::$RM_VALIDATOR__REQUIRED} = $isRequired;
		if (!isset($this->_validators[$key])) {
			$this->_validators[$key] = array();
		}
		$this->_validators[$key][] = $validator;
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 * @return mixed
	 */
	private function _applyFilters($key, $value) {
		/** @var Zend_Filter_Interface[] $filters */
		$filters = df_a($this->_filters, $key, array());
		foreach ($filters as $filter) {
			/** @var Zend_Filter_Interface $filter */
			$value = $filter->filter($value);
		}
		return $value;
	}

	/**
	 * @param array(string => mixed) $params
	 * @return array(string => mixed)
	 */
	private function _applyFiltersToArray(array $params) {
		foreach ($params as $key => $value) {
			/** @var string $key */
			/** @var mixed $value */
			$params[$key] = $this->_applyFilters($key, $value);
		}
		return $params;
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 * @return void
	 */
	private function _checkForNull($key, $value) {
		$this->_valueWasNullBeforeFilters[$key] = is_null($value);
	}
	/** @var array(string => bool) */
	private $_valueWasNullBeforeFilters = array();

	/**
	 * @param array(string => mixed) $params
	 * @return void
	 */
	private function _checkForNullArray(array $params) {
		foreach ($params as $key => $value) {
			/** @var string $key */
			/** @var mixed $value */
			$this->_checkForNull($key, $value);
		}
	}

	/**
	 * @param Zend_Validate_Interface|Zend_Filter_Interface|string $validator
	 * @param string $key
	 * @return Zend_Validate_Interface|Zend_Filter_Interface
	 */
	private function _resolveValidatorOrFilter($validator, $key) {
		/** @var Zend_Validate_Interface|Zend_Filter_Interface $result */
		if (is_object($validator)) {
			$result = $validator;
		}
		else if (is_string($validator)) {
			$result = self::_getValidatorByName($validator);
		}
		else {
			df_error_internal(
				'Валидатор/фильтр поля «%s» класса «%s» имеет недопустимый тип: «%s».'
				,$key, get_class($this), gettype($validator)
			);
		}
		if (
				!($result instanceof Zend_Validate_Interface)
			&&
				!($result instanceof Zend_Filter_Interface)
		) {
			df_error_internal(
				'Валидатор/фильтр поля «%s» класса «%s» имеет недопустимый класс «%s»,'
				. ' у которого отсутствуют требуемые интерфейсы'
				.' Zend_Validate_Interface и Zend_Filter_Interface.'
				,$key, get_class($this), get_class($result)
			);
		}
		return $result;
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 * @throws Df_Core_Exception_Internal
	 * @return void
	 */
	private function _validate($key, $value) {
		/** @var @var array(Zend_Validate_Interface|Df_Zf_Validate_Type) $validators */
		$validators = df_a($this->_validators, $key, array());
		foreach ($validators as $validator) {
			/** @var Zend_Validate_Interface|Df_Zf_Validate_Type $validator */
			$this->_validateByConcreteValidator($key, $value, $validator);
		}
	}

	/**
	 * @param array(string => mixed) $params
	 * @throws Df_Core_Exception_Internal
	 * @return void
	 */
	private function _validateArray(array $params) {
		foreach ($params as $key => $value) {
			/** @var string $key */
			/** @var mixed $value */
			$params[$key] = $this->_validate($key, $value);
		}
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 * @param Zend_Validate_Interface|Df_Zf_Validate_Type $validator
	 * @throws Df_Core_Exception_Internal
	 * @return void
	 */
	private function _validateByConcreteValidator($key, $value, Zend_Validate_Interface $validator) {
		if (!(
				is_null($value)
			&&
				isset($validator->{self::$RM_VALIDATOR__REQUIRED})
			&&
				!$validator->{self::$RM_VALIDATOR__REQUIRED}
		)) {
			if (!$validator->isValid($value)) {
				/** @var string $compositeMessage */
				$compositeMessage =
					rm_sprintf(
						"«%s»: значение %s недопустимо для свойства «%s»."
						. "\r\nСообщение проверяющего:\r\n%s"
						,get_class($this)
						,df_h()->qa()->convertValueToDebugString($value)
						,$key
						,implode(Df_Core_Const::T_NEW_LINE, $validator->getMessages())
					)
				;
				$exception = new Df_Core_Exception_Internal($compositeMessage);
				/** @var Mage_Core_Model_Message $coreMessage */
				$coreMessage = df_model('core/message');
				$exception->addMessage(
					$coreMessage->error($compositeMessage, __CLASS__,  __METHOD__)
				);
				throw $exception;
			}
		}
	}

	/** @return string */
	private function getAutoGeneratedId() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = rm_uniqid();
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	private function getTemplateExecutionKey() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				implode('_', array(get_class($this), md5($this->getTemplate())))
			;
		}
		return $this->{__METHOD__};
	}

	/** @var string */
	private static $RM_VALIDATOR__REQUIRED = 'rm__required';
	const _CLASS = __CLASS__;
	const CACHE_KEY_EMPTY = 'empty'; // ключ для кэширования пустых блоков
	const CACHE_LIFETIME_STANDARD = false; // кэшировать до посинения
	const F_TRIM = 'filter-trim';
	const P__NAME = 'name';
	const P__RM_PARENT = 'rm_parent';
	const P__TEMPLATE = 'template';
	const P__TYPE = 'type';
	const V_ARRAY = 'array';
	const V_BOOL = 'boolean';
	const V_FLOAT = 'float';
	const V_INT = 'int';
	const V_NAT = 'nat';
	const V_NAT0 = 'nat0';
	const V_STRING_NE = 'string';
	const V_STRING = 'string_empty';

	/**
	 * Не используем имя i(), потому что от данного класса много кто наследуется,
	 * и у них будет своя спецификация метода i().
	 * @param array(string => mixed) $parameters [optional]
	 * @param string|null $template [optional]
	 * @param string|null $name [optional]
	 * @return Df_Core_Block_Template
	 */
	public static function create(array $parameters = array(), $template = null, $name = null) {
		return df_block(__CLASS__, $name,
			array_merge(array(self::P__TEMPLATE => $template), $parameters)
		);
	}

	/**
	 * @param string $class
	 * @param string|string[] $functions
	 * @return string[]
	 */
	protected static function m($class, $functions) {
		df_assert($functions);
		/** @var string[] $result */
		$result = array();
		if (!is_array($functions)) {
			/** @var mixed[] $arguments */
			$arguments = func_get_args();
			$functions = df_array_tail($arguments);
		}
		foreach ($functions as $function) {
			/** @var string $function */
			$result[]= $class . '::' . $function;
		}
		return $result;
	}

	/**
	 * @static
	 * @param string $validatorName
	 * @return Zend_Validate_Interface
	 */
	private static function _getValidatorByName($validatorName) {
		/** @var array(string => Zend_Validate_Interface) $map */
		static $map;
		if (!isset($map)) {
			$map = array(
				self::F_TRIM => Df_Zf_Filter_String_Trim::s()
				,self::V_ARRAY => Df_Zf_Validate_Array::s()
				,self::V_BOOL => Df_Zf_Validate_Boolean::s()
				,self::V_FLOAT => Df_Zf_Validate_Float::s()
				,self::V_INT => Df_Zf_Validate_Int::s()
				,self::V_NAT => Df_Zf_Validate_Nat::s()
				,self::V_NAT0 => Df_Zf_Validate_Nat0::s()
				,self::V_STRING => Df_Zf_Validate_String::s()
				,self::V_STRING_NE => Df_Zf_Validate_String_NotEmpty::s()
			);
		}
		/** @var Zend_Validate_Interface $result */
		$result = df_a($map, $validatorName);
		if (!$result) {
			if (@class_exists($validatorName) || @interface_exists($validatorName)) {
				$result = Df_Zf_Validate_Class::s($validatorName);
			}
			else {
				df_error_internal('Система не смогла распознать валидатор «%s».', $validatorName);
			}
		}
		return $result;
	}
}