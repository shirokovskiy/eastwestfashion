<?php
class Df_Core_Model_Design_PackageM extends Mage_Core_Model_Design_Package {
	/**
	 * @override
	 * @param string $file
	 * @param array $params
	 * @return string
	 */
	public function getSkinUrl($file = null, array $params = array()) {
		/** @var string $result */
		$result = parent::getSkinUrl($file, $params);
		if (rm_contains($result, '/rm/')) {
			/**
			 * Обратите внимание, что для ресурсов из папки js мы добавляем параметр v по-другому:
			 * в методе Df_Page_Block_Html_Head::_prepareStaticAndSkinElements
			 */
			$result = df()->url()->addVersionStamp($result);
		}
		else {
			/** @var bool */
			static $isRunningCustomSolution;
			if (!isset($isRunningCustomSolution)) {
				$isRunningCustomSolution =
						Df_Core_Model_Design_Package::s()->isCustom()
					&&
						Df_Core_Model_Design_Package::s()->hasConfiguration()
				;
			}
			if ($isRunningCustomSolution) {
				/** @var string $packageUrlPart */
				static $packageUrlPart;
				if (!isset($packageUrlPart)) {
					$packageUrlPart = rm_sprintf('/%s/', Df_Core_Model_Design_Package::s()->getName());
				}
				if (rm_contains($result, $packageUrlPart)) {
					$result =
						df()->url()
							->addVersionStamp($result, Df_Core_Model_Design_Package::s()->getVersion())
					;
				}
			}
		}
		return $result;
	}

	/**
	 * @override
	 * @param string $file
	 * @param array &$params
	 * @param array $fallbackScheme
	 * @return string
	 */
	protected function _fallback($file, array &$params, array $fallbackScheme = array(array())) {
		/**
		 * Раньше здесь стояло:
		 *
			array_splice(
				$fallbackScheme
				,0
				,0
				,array(
					array(
						'_package' => 'rm'
						,'_theme' => 'priority'
					)
					,array(
						'_package' => df_a($params, '_package')
						,'_theme' => df_a($params, '_theme')
					)
				)
			)
			;
		 *
		 *
		 * array_unshift, видимо, работает быстрее
		 */
		array_unshift(
			$fallbackScheme
			,array(
				'_package' => 'rm'
				,'_theme' => 'priority'
			)
			,array(
				/**
				 * Сюда мы можем попасть при установке оформительской темы.
				 * В частности, сюда попадаем при установке темы EM Taobaus.
				 * Российская сборка Magento во время работы установочного скрипта
				 * еще не инициализирована, и работа установочного скрипта завершалась сбоем:
				 * «Call to undefined function df_a()»
				 * @link http://magento-forum.ru/topic/3779/
				 */
				'_package' => isset($params['_package']) ? $params['_package'] : null
				,'_theme' => isset($params['_theme']) ? $params['_theme'] : null
			)
		);
		$fallbackScheme[]=
			array(
				'_package' => 'rm'
				,'_theme' => self::DEFAULT_THEME
			)
		;
		return parent::_fallback($file, $params, $fallbackScheme);
	}
}