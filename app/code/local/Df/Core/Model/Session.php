<?php
class Df_Core_Model_Session extends Mage_Core_Model_Session_Abstract {
	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->init(get_class($this));
	}

	const _CLASS = __CLASS__;
	/** @return Df_Core_Model_Session */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}