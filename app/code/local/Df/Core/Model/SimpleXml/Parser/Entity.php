<?php
class Df_Core_Model_SimpleXml_Parser_Entity extends Df_Core_Model_Abstract {
	/**
	 * Я так понимаю, этот метод нужен для коллекций?
	 * В то же время, есть ситуации, когда данный класс вовсе не является абстрактным
	 * (@see Df_Kkb_Model_Response_Payment), просто методы
	 * @see Df_Core_Model_SimpleXml_Parser_Entity::getId() и
	 * @see Df_Core_Model_SimpleXml_Parser_Entity::getName() не используются.
	 * @abstract
	 * @override
	 * @return string
	 */
	public function getId() {
		df_abstract(__METHOD__);
		return parent::getId();
	}

	/**
	 * Я так понимаю, этот метод нужен для 1С:Управление торговлей?
	 * В то же время, есть ситуации, когда данный класс вовсе не является абстрактным
	 * (@see Df_Kkb_Model_Response_Payment), просто методы
	 * @see Df_Core_Model_SimpleXml_Parser_Entity::getId() и
	 * @see Df_Core_Model_SimpleXml_Parser_Entity::getName() не используются.
	 * @abstract
	 * @return string
	 */
	public function getName() {
		df_abstract(__METHOD__);
		return null;
	}

	/**
	 * @param string $path
	 * @param bool $throw [optional]
	 * @return int
	 */
	public function descendI($path, $throw = false) {
		/** @var string $resultAsText */
		$resultAsText = $this->descendS($path, $throw);
		if ($throw && df_empty_string($resultAsText)) {
			df_error('В документе XML по пути «%s» требуется целое число, однако там пусто.', $path);
		}
		return rm_int($resultAsText);
	}

	/**
	 * @param string $path
	 * @param bool $throw [optional]
	 * @return string|null
	 * @throws Df_Core_Exception_Client
	 */
	public function descendS($path, $throw = false) {
		if (!isset($this->{__METHOD__}[$path])) {
			/** @var Df_Varien_Simplexml_Element|bool $element */
			$element = $this->e()->descend($path);
			/** @var bool $found */
			$found = !is_null($element);
			if (!$found && $throw) {
				df_error('В документе XML отсутствует путь «%s».', $path);
			}
			$this->{__METHOD__}[$path] = rm_n_set($found ? (string)$element : null);
		}
		return rm_n_get($this->{__METHOD__}[$path]);
	}

	/** @return Df_Varien_Simplexml_Element */
	public function e() {return $this->getSimpleXmlElement();}

	/**
	 * @param string $attributeName
	 * @param string|int|array|float $defaultValue[optional]
	 * @return mixed
	 */
	public function getAttribute($attributeName, $defaultValue = null) {
		df_param_string($attributeName, 0);
		/** @var string|int|array|float $result */
		$result = $this->e()->getAttribute($attributeName);
		return !is_null($result) ? $result : $defaultValue;
	}

	/**
	 * @param string $paramName
	 * @param string|int|array|float|null $defaultValue[optional]
	 * @return mixed
	 */
	public function getEntityParam($paramName, $defaultValue = null) {
		df_param_string_not_empty($paramName, 0);
		return df_a($this->getAsCanonicalArray(), $paramName, $defaultValue);
	}

	/**
	 * От разультата этого метода зависит добавление данного объекта
	 * в коллекцию Df_Core_Model_SimpleXml_Parser_Collection
	 * @see Df_Core_Model_SimpleXml_Parser_Collection::getItems()
	 * @return bool
	 */
	public function isValid() {return true;}

	/** @return Df_Varien_Simplexml_Element */
	public function getSimpleXmlElement() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = $this->_getData(self::P__SIMPLE_XML);
			if (is_string($this->{__METHOD__})) {
				$this->{__METHOD__} = rm_xml($this->{__METHOD__});
			}
			df_assert($this->{__METHOD__} instanceof Df_Varien_Simplexml_Element);
		}
		return $this->{__METHOD__};
	}

	/** @return array(string => mixed) */
	protected function getAsCanonicalArray() {return $this->e()->asCanonicalArray();}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		// параметр PARAM__SIMPLE_XML может быть как объектом, так и строкой.
	}
	const _CLASS = __CLASS__;
	const P__SIMPLE_XML = 'simple_xml';
	/**
	 * Обратите внимание, что этот метод нельзя называть i(),
	 * потому что от класса Df_Core_Model_SimpleXml_Parser_Entity наследуются другие классы,
	 * и у наследников спецификация метода i() другая, что приводит к сбою интерпретатора PHP:
	 * «Strict Notice: Declaration of Df_Licensor_Model_File::i()
	 * should be compatible with that of Df_Core_Model_SimpleXml_Parser_Entity::i()»
	 * @static
	 * @param Df_Varien_Simplexml_Element|string $simpleXml
	 * @return Df_Core_Model_SimpleXml_Parser_Entity
	 */
	public static function simple($simpleXml) {
		return new self(array(self::P__SIMPLE_XML => $simpleXml));
	}
}