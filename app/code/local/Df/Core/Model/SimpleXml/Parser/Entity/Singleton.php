<?php
abstract class Df_Core_Model_SimpleXml_Parser_Entity_Singleton
	extends Df_Core_Model_SimpleXml_Parser_Entity {
	/**
	 * @override
	 * @return string
	 */
	public function getId() {return get_class($this);}

	/**
	 * @override
	 * @return string
	 */
	public function getName() {return get_class($this);}
	const _CLASS = __CLASS__;
}