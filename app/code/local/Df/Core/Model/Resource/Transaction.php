<?php
class Df_Core_Model_Resource_Transaction extends Mage_Core_Model_Resource_Transaction {
	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Core_Model_Resource_Transaction
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}