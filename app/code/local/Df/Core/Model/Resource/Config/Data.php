<?php
class Df_Core_Model_Resource_Config_Data extends Mage_Core_Model_Resource_Config_Data {
	const _CLASS = __CLASS__;
	/**
	 * @see Df_Core_Model_Config_Data::_construct()
	 * @see Df_Core_Model_Resource_Config_Data_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf_r(__CLASS__);}
	/** @return Df_Core_Model_Resource_Config_Data */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}