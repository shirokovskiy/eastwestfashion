<?php
abstract class Df_Core_Model_Resource_Setup_Abstract extends Mage_Core_Model_Resource_Setup {
	/**
	 * @override
	 * @return Df_Directory_Model_Resource_Setup
	 */
	public function startSetup() {
		parent::startSetup();
		Df_Core_Model_Lib::s()->init();
		Df_Zf_Model_Lib::s()->init();
		return $this;
	}
}