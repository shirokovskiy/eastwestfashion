<?php
abstract class Df_Core_Model_Setup extends Df_Core_Model_Abstract {
	/**
	 * @abstract
	 * @return Df_Core_Model_Setup
	 */
	abstract public function process();

	/** @return Mage_Core_Model_Resource_Setup|null */
	protected function getSetup() {
		return $this->cfg(self::P__SETUP);
	}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_prop(self::P__SETUP, 'Mage_Core_Model_Resource_Setup', false);
	}
	const _CLASS = __CLASS__;
	const P__SETUP = 'setup';
}