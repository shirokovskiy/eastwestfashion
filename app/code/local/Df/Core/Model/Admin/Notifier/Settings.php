<?php
abstract class Df_Core_Model_Admin_Notifier_Settings extends Df_Core_Model_Admin_Notifier {
	/**
	 * @param Mage_Core_Model_Store $store
	 * @return bool
	 */
	abstract protected function isStoreAffected(Mage_Core_Model_Store $store);

	/**
	 * @override
	 * @return bool
	 */
	public function needToShow() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
					(0 < $this->getStoresAffectedCount())
				&&
					!Mage::getStoreConfigFlag($this->getConfigPathSkip())
			;
		}
		return $this->{__METHOD__};
	}

	/**
	 * Если администратор изменил значение наблюдаемой опции,
	 * то предшествующую команду администратора о скрытии предупреждения
	 * о проблемном значении этой опции считаем недействительной.
	 * Точно так же поступает и ядро Magento в сценарии предупреждений о настройках налогов:
	 * @see Mage_Tax_Model_Config_Notification::_resetNotificationFlag()
	 * @return void
	 */
	public function resetSkipStatus() {
		/** @var Df_Core_Model_Config_Data $config */
		$config = Df_Core_Model_Config_Data::i();
		$config->load($this->getConfigPathSkip(), 'path');
		$config->setValue(0);
		$config->setPath($this->getConfigPathSkip());
		$config->save();
	}

	/**
	 * @override
	 * @return array(string => string)
	 */
	protected function getMessageVariables() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = array_merge(parent::getMessageVariables(), array(
				self::MESSAGE_VAR__STORES_AFFECTED =>
					Mage::app()->isSingleStoreMode()
					? ''
					: rm_concat_clean(' '
						, (1 === $this->getStoresAffectedCount()) ? ' для магазина ' : 'для магазинов'
						, Df_Core_Model_Resource_Store_Collection::getNamesStatic(
							$this->getStoresAffected()
						)
					)
			));
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	private function getConfigPathSkip() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = self::getConfigPathSkipByClass(get_class($this));
		}
		return $this->{__METHOD__};
	}

	/** @return Mage_Core_Model_Store[] */
	private function getStoresAffected() {
		if (!isset($this->{__METHOD__})) {
			/** @var Mage_Core_Model_Store[] $result */
			$result = array();
			foreach (Mage::app()->getStores() as $store) {
				/** @var Mage_Core_Model_Store $store */
				if ($this->isStoreAffected($store)) {
					$result[]= $store;
				}
			}
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/** @return int */
	private function getStoresAffectedCount() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = count($this->getStoresAffected());
		}
		return $this->{__METHOD__};
	}

	const MESSAGE_VAR__STORES_AFFECTED = '{перечисление магазинов}';

	/**
	 * @param string $class
	 * @return string
	 */
	public static function getConfigPathSkipByClass($class) {return 'df/admin/notifiers/skip/' . $class;}
}