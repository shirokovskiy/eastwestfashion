<?php
class Df_Core_Model_Admin_Notifier_Collection extends Df_Varien_Data_Collection_Singleton {
	/**
	 * @override
	 * @return string
	 */
	protected function getItemClass() {return Df_Core_Model_Admin_Notifier::_CLASS;}

	/**
	 * @override
	 * @return void
	 */
	protected function loadInternal() {
		foreach ($this->getClasses() as $className) {
			/** @var string $className */
			/** @var Df_Core_Model_Admin_Notifier $notifier */
			$notifier = df_model($className);
			df_assert($notifier instanceof Df_Core_Model_Admin_Notifier);
			if ($notifier->needToShow()) {
				$this->addItem($notifier);
			}
		}
	}

	/** @return string[] */
	private function getClasses() {
		if (!isset($this->{__METHOD__})) {
			/** @var array(string => string)|string $configNodes */
			$configNodes = Mage::getConfig()->getNode('df/admin/notifiers')->asArray();
			$this->{__METHOD__} = is_array($configNodes) ? array_values($configNodes) : array();
		}
		return $this->{__METHOD__};
	}

	/** @return Df_Core_Model_Admin_Notifier_Collection */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}