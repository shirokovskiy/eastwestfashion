<?php
abstract class Df_Core_Model_Admin_Notifier extends Df_Core_Model_Abstract {
	/** @return bool */
	abstract public function needToShow();
	/** @return string */
	abstract protected function getMessageTemplate();

	/** @return string */
	public function getMessage() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				df_output()->processLink(
					strtr($this->getMessageTemplate(), $this->getMessageVariables())
					,$this->getUrlHelp()
				)
			;
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	public function getUrlSkip() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				rm_url_admin(
					'df_core_admin/notification/skip'
					, array(Df_Core_Model_Action_Admin_Notification_Skip::RP__CLASS => get_class($this))
				)
			;
		}
		return $this->{__METHOD__};
	}

	/** @return array(string => string) */
	protected function getMessageVariables() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = array(
				self::$MESSAGE_VAR__URL_HELP => $this->getUrlHelp()
			);
		}
		return $this->{__METHOD__};
	}

	/** @return string */
	protected function getUrlHelp() {return '';}

	const _CLASS = __CLASS__;
	/** @var string */
	protected static $MESSAGE_VAR__URL_HELP = '{веб-адрес пояснений}';
}