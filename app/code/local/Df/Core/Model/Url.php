<?php
class Df_Core_Model_Url extends Mage_Core_Model_Url {
	const _CLASS = __CLASS__;
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Core_Model_Url
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
}