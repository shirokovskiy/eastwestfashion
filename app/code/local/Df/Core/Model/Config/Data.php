<?php
/**
 * @method Df_Core_Model_Resource_Config_Data getResource()
 */
class Df_Core_Model_Config_Data extends Mage_Core_Model_Config_Data {
	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init(Df_Core_Model_Resource_Config_Data::mf());
	}
	const _CLASS = __CLASS__;

	/** @return Mage_Core_Model_Resource_Config_Data_Collection */
	public static function c() {return self::s()->getCollection();}
	/**
	 * @static
	 * @param mixed[] $parameters [optional]
	 * @return Df_Core_Model_Config_Data
	 */
	public static function i(array $parameters = array()) {return new self($parameters);}
	/**
	 * @static
	 * @param int|string $id
	 * @param string|null $field [optional]
	 * @return Df_Core_Model_Config_Data
	 */
	public static function ld($id, $field = null) {return df_load(self::i(), $id, $field);}
	/**
	 * @see Df_Core_Model_Resource_Config_Data_Collection::_construct()
	 * @return string
	 */
	public static function mf() {static $r; return $r ? $r : $r = rm_class_mf(__CLASS__);}
	/** @return Df_Core_Model_Config_Data */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}