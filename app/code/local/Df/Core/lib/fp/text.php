<?php
/**
 * @param string[]|mixed[] $arguments
 * @return string
 */
function df_concat($arguments) {
	// Обратите внимание, что функция func_get_args() не может быть параметром другой функции.
	$arguments = is_array($arguments) ? $arguments : func_get_args();
	return implode('', $arguments);
}

/**
 * @param string[]|mixed[] $arguments
 * @return string
 */
function df_concat_names($arguments) {
	// Обратите внимание, что функция func_get_args() не может быть параметром другой функции.
	$arguments = is_array($arguments) ? $arguments : func_get_args();
	return implode(', ', df_quote_russian($arguments));
}

/**
 * @param string[]|mixed[] $arguments
 * @return string
 */
function df_concat_path($arguments) {
	// Обратите внимание, что функция func_get_args() не может быть параметром другой функции.
	$arguments = is_array($arguments) ? $arguments : func_get_args();
	return implode(DS, $arguments);
}

/**
 * @param string[] $arguments
 * @return string
 */
function df_concat_url($arguments) {
	// Обратите внимание, что функция func_get_args() не может быть параметром другой функции.
	$arguments = is_array($arguments) ? $arguments : func_get_args();
	return implode('/', $arguments);
}

/**
 * @param string[] $arguments
 * @return string
 */
function df_concat_xpath($arguments) {
	// Обратите внимание, что функция func_get_args() не может быть параметром другой функции.
	$arguments = is_array($arguments) ? $arguments : func_get_args();
	return implode(Df_Core_Const::T_XPATH_SEPARATOR, $arguments);
}

/**
 * @param string $text
 * @return string
 */
function df_escape($text) {return df_text()->htmlspecialchars($text);}

/**
 * @param mixed|false $value
 * @return mixed|null
 */
function df_ftn($value) {return (false === $value) ? null : $value;}

/**
 * @param string $text
 * @return string
 */
function df_no_escape($text) {
	return df_text()->noEscape($text);
}

/**
 * @param string $string
 * @return string
 */
function df_lcfirst($string) {
	/** @var string $result */
	$result =
		(string)
			(
					mb_strtolower(
						mb_substr($string,0,1)
					)
				.
					mb_substr($string,1)
			)
	;
	return $result;
}

/**
 * @param mixed|null $value
 * @return mixed
 */
function df_nts($value) {return !is_null($value) ? $value : '';}

/**
 * @param string $text
 * @return string
 */
function df_prepend_tab($text) {
	return "\t" . $text;
}

/**
 * @param string|string[] $text
 * @return string|string[]
 */
function df_quote_duoble($text) {
	return df_text()->quote($text, Df_Core_Helper_Text::QUOTE__DOUBLE);
}

/**
 * @param string|string[] $text
 * @return string|string[]
 */
function df_quote_russian($text) {
	return df_text()->quote($text, Df_Core_Helper_Text::QUOTE__RUSSIAN);
}

/**
 * @param string|string[] $text
 * @return string|string[]
 */
function df_quote_single($text) {
	return df_text()->quote($text, Df_Core_Helper_Text::QUOTE__SINGLE);
}

/**
 * @param string $text
 * @return string
 */
function df_tab($text) {
	return implode("\n", array_map('df_prepend_tab', explode("\n", $text)));
}

/**
 * @param string $string
 * @param string $delimiter[optional]
 * @return array
 */
function df_parse_csv($string, $delimiter = ',') {
	return df_output()->parseCsv($string, $delimiter);
}

/**
 * Иногда я для разработки использую заплатку ядра для xDebug —
 * отключаю set_error_handler для режима разработчика.
 *
 * Так вот, xDebug при обработке фатальных сбоев (в том числе и E_RECOVERABLE_ERROR),
 * выводит на экран диагностическое сообщение, и после этого останавливает работу интерпретатора.
 *
 * Конечно, если у нас сбой типов E_ERROR, E_PARSE, E_CORE_ERROR, E_CORE_WARNING,
 * E_COMPILE_ERROR, E_COMPILE_WARNING, то и set_error_handler не поможет
 * (не обрабатывает эти типы сбоев, согласно официальной документации PHP).
 *
 * Однако сбои типа E_RECOVERABLE_ERROR обработик сбоев Magento,
 * установленный посредством set_error_handler, переводит в исключительние ситуации.
 *
 * xDebug же при E_RECOVERABLE_ERROR останавивает работу интерпретатора, что нехорошо.
 *
 * Поэтому для функций, которые могут привести к E_RECOVERABLE_ERROR,
 * пишем обёртки, которые вместо E_RECOVERABLE_ERROR возбуждают исключительную ситуацию.
 * Одна из таких функций — df_string.
 *
 * @param mixed $value
 * @return string
 */
function df_string($value) {
	if (is_object($value)) {
		if (
			/**
			 * К сожалению, нельзя здесь для проверки публичности метода
			 * использовать is_callable,
			 * потому что наличие Varien_Object::__call
			 * приводит к тому, что is_callable всегда возвращает true.
			 */
			!method_exists ($value, '__toString')
		) {
			df_error(
				'Программист ошибочно пытается трактовать объект класса %s как строку.'
				,get_class($value)
			);
		}
	}
	else if (is_array($value)) {
		df_error('Программист ошибочно пытается трактовать массив как строку.');
	}
	return strval($value);
}

/**
 * @param mixed $value
 * @return string
 */
function df_string_debug($value) {
	/** @var string $result */
	$result = '';
	if (is_object($value)) {
		if (
			/**
			 * К сожалению, нельзя здесь для проверки публичности метода
			 * использовать is_callable,
			 * потому что наличие Varien_Object::__call
			 * приводит к тому, что is_callable всегда возвращает true.
			 */
			!method_exists ($value, '__toString')
		) {
			$result = get_class($value);
		}
	}
	else if (is_array($value)) {
		$result = rm_sprintf('<массив из %d элементов>', count($value));
	}
	else if (is_bool($value)) {
		$result = $value ? 'логическое <да>' : 'логическое <нет>';
	}
	else {
		$result = strval($value);
	}
	return $result;
}

/**
 * @param $string1
 * @param $string2
 * @return bool
 */
function df_strings_are_equal_ci($string1, $string2) {
	return
		(
				0
			===
				strcmp(
					mb_strtolower($string1)
					,mb_strtolower($string2)
				)
		)
	;
}

/** @return Df_Core_Helper_Text */
function df_text() {return Df_Core_Helper_Text::s();}

/**
 * Обратите внимание, что иногда вместо данной функции надо применять trim.
 * Например, df_trim не умеет отсекать нулевые байты,
 * которые могут образовываться на конце строки
 * в результате шифрации, передачи по сети прямо в двоичном формате, и затем обратной дешифрации
 * посредством Varien_Crypt_Mcrypt.
 *
 * @see Df_Core_Model_RemoteControl_Coder::decode
 * @see Df_Core_Model_RemoteControl_Coder::encode
 *
 * @param string|string[] $string
 * @param string $charlist [optional]
 * @return string|string[]
 */
function df_trim($string, $charlist = null) {
	return
		is_array($string)
		? df_map(array(df_text(), 'trim'), $string, $charlist)
		: df_text()->trim($string, $charlist)
	;
}

/**
 * @param string $string
 * @param string $charlist [optional]
 * @return string
 */
function df_trim_left($string, $charlist = null) {
	// Пусть пока будет так.
	// Потом, если потребуется, добавлю дополнительную обработку спецсимволов Unicode.
	return ltrim($string, $charlist);
}

/**
 * @param string $string
 * @param string $suffix
 * @return string
 */
function df_trim_suffix($string, $suffix) {
	df_param_string($string, 0);
	df_param_string($suffix, 1);
	return preg_replace(rm_sprintf('#%s$#mui', preg_quote($suffix, '#')), '', $string);
}

/**
 * @param boolean $value
 * @return string
 */
function rm_bts($value) {return df_output()->convertBooleanToString($value);}

/**
 * @param boolean $value
 * @return string
 */
function rm_bts_r($value) {return df_output()->convertBooleanToStringRussian($value);}

/**
 * @param string $text
 * @return string
 */
function rm_cdata($text) {return Df_Varien_Simplexml_Element::markAsCData($text);}

/**
 * @param string[] $keyParts
 * @return string
 */
function rm_config_key($keyParts) {
	if (!is_array($keyParts)) {
		$keyParts = func_get_args();
	}
	return implode(Df_Core_Helper_Config::PATH_SEPARATOR, $keyParts);
}

/**
 * @param string $haystack
 * @param string $needle
 * @return bool
 * Я так понимаю, здесь безопысно использовать @see strpos вместо mb_strpos даже для UTF-8.
 * @link http://stackoverflow.com/questions/13913411/mb-strpos-vs-strpos-whats-the-difference
 */
function rm_contains($haystack, $needle) {return false !== strpos($haystack, $needle);}

/**
 * @param string $haystack
 * @param string $needle
 * @return bool
 * @link http://stackoverflow.com/a/10473026/254475
 * @link http://stackoverflow.com/a/834355/254475
 * @see rm_starts_with()
 */
function rm_ends_with($haystack, $needle) {
	/** @var int $length */
	$length = mb_strlen($needle);
	return (0 === $length) || ($needle === mb_substr($haystack, -$length));
}

/**
 * @param string $pattern
 * @param string $subject
 * @param bool $needThrow [optional]
 * @param bool $needLog [optional]
 * @return string|null
 */
function rm_preg_match($pattern, $subject, $needThrow = true, $needLog = true) {
	df_param_string_not_empty($pattern, 0);
	if (!df_check_string_not_empty($subject)) {
		df_error_internal(
			'%s пытается применить регулярное выражение «%s» к пустой строке.'
			, rm_caller()->getName()
			, $pattern
		);
	}
	/** @var string[] $matches */
	$matches = array();
	/** @var int $r */
	$r = preg_match($pattern, $subject, $matches);
	/** @var string|null $result */
	if (1 === $r) {
		/**
		 * Раньше тут стояло:
		 * $result = df_a($matches, 1);
		 * что не совсем правильно, потому что если регулярное выражение не содержит круглые скобки,
		 * то результирующий массив будет содержать всего один элемент.
		 * ПРИМЕР
		 * регулярное выражение: #[А-Яа-яЁё]#mu
		 * исходный текст: Категория Яндекс.Маркета
		 * результат: Array([0] => К)
		 */
		$result = df_array_last($matches);
	}
	else {
		/** @var string $fileName */
		$fileName = 'rm_preg_match.txt';
		if ($needLog) {
			df()->debug()->report($fileName, $subject);
		}
		if ($needThrow) {
			/** @var $fileBasePath */
			$fileBasePath = Mage::getBaseDir('var') . DS . 'log'. DS;
			df_error(
				'Текст (смотрите файл «%s»)'
				.' не соответствует регулярному выражению «%s».'
				, $fileBasePath . $fileName
				, $pattern
			);
		}
		$result = null;
	}
	return $result;
}

/**
 * @param string $pattern
 * @param string $subject
 * @param bool $needThrow [optional]
 * @param bool $needLog [optional]
 * @return int|null
 */
function rm_preg_match_int($pattern, $subject, $needThrow = true, $needLog = true) {
	df_param_string_not_empty($pattern, 0);
	df_param_string_not_empty($subject, 1);
	/** @var int|null $result */
	$result = rm_preg_match($pattern, $subject, $needThrow , $needLog);
	if (!is_null($result))  {
		df_assert(ctype_digit($result));
		$result = rm_int($result);
	}
	return $result;
}

/**
 * @param string|mixed[] $pattern
 * @return string
 * @throws Exception
 */
function rm_sprintf($pattern) {
	/** @var string $result */
	/** @var mixed[] $arguments */
	if (is_array($pattern)) {
		$arguments = $pattern;
		$pattern = df_array_first($arguments);
	}
	else {
		$arguments = func_get_args();
	}
	try {
		$result = rm_sprintf_strict($arguments);
	}
	catch (Df_Core_Exception_Internal $e) {
		/** @var bool $inProcess */
		static $inProcess = false;
		if (!$inProcess) {
			$inProcess = true;
			df_notify_me($e->getMessage());
			$inProcess = false;
		}
		$result = $pattern;
	}
	return $result;
}

/**
 * @param string|mixed[] $pattern
 * @return string
 * @throws Df_Core_Exception_Internal
 */
function rm_sprintf_strict($pattern) {
	/** @var mixed[] $arguments */
	if (is_array($pattern)) {
		$arguments = $pattern;
		$pattern = df_array_first($arguments);
	}
	else {
		$arguments = func_get_args();
	}
	/** @var string $result */
	if (1 === count($arguments)) {
		$result = $pattern;
	}
	else {
		try {
			$result = vsprintf($pattern, df_array_tail($arguments));
		}
		catch (Exception $e) {
			/** @var bool $inProcess */
			static $inProcess = false;
			if (!$inProcess) {
				$inProcess = true;
				df_error_internal(strtr(
					'При выполнении sprintf произошёл сбой «{message}».'
					. "\r\nШаблон: {pattern}."
					. "\r\nПараметры:\r\n{params}."
					,array(
						'{message}' => $e->getMessage()
						,'{pattern}' => $pattern
						,'{params}' => print_r(df_array_tail($arguments), true)
					)
				));
				$inProcess = false;
			}
		}
	}
	return $result;
}

/**
 * @param string $haystack
 * @param string $needle
 * @return bool
 * @link http://stackoverflow.com/a/10473026/254475
 * @link http://stackoverflow.com/a/834355/254475
 * @see rm_ends_with()
 */
function rm_starts_with($haystack, $needle) {
	/**
	 * Утверждают, что код ниже работает быстрее, чем
	 * return 0 === mb_strpos($haystack, $needle);
	 * @link http://stackoverflow.com/a/10473026/254475
	 */
	/** @var int $length */
	$length = mb_strlen($needle);
	return ($needle === mb_substr($haystack, 0, $length));
}

/**
 * @param string $string
 * @param string|string[] $charactersToRemove
 * @return string
 */
function rm_string_clean($string, $charactersToRemove) {
	if (!is_array($charactersToRemove)) {
		$charactersToRemove = rm_string_split($charactersToRemove);
	}
	/** @var string $result */
	$result =
		strtr(
			$string
			,array_combine(
				$charactersToRemove
				,array_fill(0, count($charactersToRemove), '')
			)
		)
	;
	return $result;
}

/**
 * @param string $string
 * @return array
 * @link http://us3.php.net/manual/en/function.str-split.php#107658
 */
function rm_string_split($string) {return preg_split("//u", $string, -1, PREG_SPLIT_NO_EMPTY);}

/**
 * @param string $xml
 * @return Df_Varien_Simplexml_Element
 * @throws Df_Core_Exception_Client
 */
function rm_xml($xml) {
	df_param_string_not_empty($xml, 0);
	/** @var Df_Varien_Simplexml_Element $result */
	$result = null;
	try {
		$result = new Df_Varien_Simplexml_Element($xml);
	}
	catch (Exception $e) {
		df_error(
			"При синтаксическом разборе документа XML произошёл сбой:\r\n"
			. "«%s»\r\n"
			. "********************\r\n"
			. "%s\r\n"
			. "********************\r\n"
			, $e->getMessage()
			, df_trim($xml)
		);
	}
	return $result;
}

/**
 * @param int|null $length [optional]
 * @return string
 */
function rm_uniqid($length = null) {
	/** @var string $result */
	$result =
		uniqid(
			$prefix = ''
			/**
			 * Важно использовать $more_entropy = true,
			 * потому что иначе на быстрых серверах
			 * (я заметил такое поведение при использовании Zend Server Enterprise и PHP 5.4)
			 * uniqid будет иногда возвращать одинаковые значения
			 * при некоторых двух последовательных вызовах.
			 */,$more_entropy = true
		)
	;
	if (!is_null($length)) {
		$result =
			substr(
				$result
				/**
				 * Обратите внимание, что уникальным является именно окончание uniqid, а не начало.
				 * Два последовательных вызова uniqid могу вернуть:
				 * 5233061890334
				 * 52330618915dd
				 * Начало у этих значений — одинаковое, а вот окончание — различное.
				 */
				, -$length
			)
		;
	}
	return $result;
}