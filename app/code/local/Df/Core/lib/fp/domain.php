<?php
/**
 * @param int|array(string => mixed) $id [optional]
 * @param int|Mage_Core_Model_Store|string|null $storeId [optional]
 * @return Df_Catalog_Model_Product
 */
function df_product($id = 0, $storeId = null) {
	/** @var Df_Catalog_Model_Product $result */
	$result = null;
	if (!$id) {
		$result = new Df_Catalog_Model_Product();
	}
	else if (is_array($id)) {
		$result = new Df_Catalog_Model_Product($id);
	}
	else {
		if (!is_null($storeId) && !df_check_integer($storeId)) {
			/** @var Mage_Core_Model_Store $store */
			$storeId = Mage::app()->getStore($storeId)->getId();
			df_assert_integer($storeId);
		}
		$result = Df_Catalog_Model_Product::ld($id, $storeId);
	}
	return $result;
}

/* @return Mage_Core_Model_Session */
function rm_session_checkout() {return Mage::getSingleton('checkout/session');}

/* @return Mage_Core_Model_Session */
function rm_session_core() {return Mage::getSingleton('core/session');}

/* @return Mage_Customer_Model_Session */
function rm_session_customer() {return Mage::getSingleton('customer/session');}


