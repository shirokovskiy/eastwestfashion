<?php
class Df_Core_Helper_Mage_Core_Design extends Mage_Core_Helper_Abstract {
	/** @return string */
	public function getThemeFrontend() {
		/**
		 * getTheme('template') работает в том случае, когда оформительская тема задана
		 * посредством административного меню «Система» → «Оформление витрины»
		 */
		/** @var string $result */
		$result = $this->packageSingleton()->getTheme('template');
		return $result ? $result : $this->packageSingleton()->getTheme('frontend');
	}

	/** @return Mage_Core_Model_Design_Package */
	public function packageSingleton() {return Mage::getSingleton('core/design_package');}

	/** @return Df_Core_Helper_Mage_Core_Design */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}