<?php
class Df_Core_Helper_Mage_Catalog extends Mage_Core_Helper_Abstract {
	/** @return Df_Core_Helper_Mage_Catalog_Product */
	public function product() {;return Df_Core_Helper_Mage_Catalog_Product::s();}
	/** @return Mage_Catalog_Model_Product_Media_Config */
	public function productMediaConfig() {return Mage::getSingleton('catalog/product_media_config');}
	/** @return Mage_Catalog_Model_Session */
	public function sessionSingleton() {return Mage::getSingleton('catalog/session');}
	/** @return Mage_Catalog_Model_Url */
	public function urlSingleton() {return Mage::getSingleton('catalog/url');}
	/** @return Df_Core_Helper_Mage_Catalog */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}