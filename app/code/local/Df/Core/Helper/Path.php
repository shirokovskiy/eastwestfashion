<?php
class Df_Core_Helper_Path extends Mage_Core_Helper_Abstract {
	/**
	 * @param string $path
	 * @return string
	 */
	public function adjustSlashes($path) {return str_replace('\\', DS, $path);}

	/**
	 * @param string $path
	 * @return mixed
	 */
	public function makeRelative($path) {
		/** @var string $cleaned */
		$cleaned = $this->clean($path);
		/** @var string $base */
		$base = BP . DS;
		return rm_starts_with($cleaned, $base) ? str_replace($base, '', $cleaned) : $cleaned;
	}

	/**
	 * @param string $path
	 * @return string
	 */
	public function removeTrailingSlash($path) {
		return
			!in_array(mb_substr($path, -1), array('/', DS))
			? $path
			: mb_substr($path, 0, -1)
		;
	}

	/**
	 * Function to strip additional / or \ in a path name
	 * @param string $path
	 * @param string $ds
	 * @return string
	 */
	private function clean($path, $ds = DS) {
		df_param_string($path, 0);
		df_param_string($path, 1);
		/** @var string $result */
		$result = df_trim($path);
		// Remove double slashes and backslahses
		// and convert all slashes and backslashes to DS
		$result = !$result ? BP : $this->adjustSlashes(preg_replace('#[/\\\\]+#u', $ds, $result));
		if (!df_check_string($result)) {
			df_error(
				strtr(
					"[{method}]:\tНе могу обработать путь {path}"
					,array('{method}%' => __METHOD__, '{path}' => $path)
				)
			);
		}
		return $result;
	}

	/** @return Df_Core_Helper_Path */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}