<?php
class Df_Core_Helper_Db extends Mage_Core_Helper_Abstract {
	/** @return Varien_Db_Adapter_Pdo_Mysql|Varien_Db_Adapter_Interface */
	public function conn() {
		return df_mage()->core()->resource()->getConnection('write');
	}

	/**
	 * @param Varien_Db_Adapter_Pdo_Mysql|Varien_Db_Adapter_Interface $adapter
	 * @param string $table
	 * @return Df_Core_Helper_Db
	 */
	public function truncate($adapter, $table) {
		/** @var bool $truncated */
		$truncated = false;
		/** @var string $method */
		$method = '';
		/**
		 * Метод Varien_Db_Adapter_Pdo_Mysql::truncateTable
		 * появился только в Magento CE 1.6.0.0,
		 * и при этом метод Varien_Db_Adapter_Pdo_Mysql::truncate стал устаревшим.
		 */
		/** @var string[] $methods */
		$methods = array('truncateTable', 'truncate');
		foreach ($methods as $currentMethod) {
			/** @var string $currentMethod */
			if (
				/**
				 * К сожалению, нельзя здесь для проверки публичности метода
				 * использовать is_callable,
				 * потому что наличие Varien_Object::__call
				 * приводит к тому, что is_callable всегда возвращает true.
				 */
				method_exists($adapter, $currentMethod)
			) {
				$method = $currentMethod;
				break;
			}
		}
		if ($method) {
			try {
				call_user_func(array($adapter, $method), $table);
				$truncated = true;
			}
			catch(Exception $e) {
				/**
				 * При выполнении профилей импорта-экспорта одним из клиентов
				 * произошёл сбой «DDL statements are not allowed in transactions»
				 */
			}
		}
		if (!$truncated) {
			$adapter->delete($table);
		}
		return $this;
	}

	/** @return Df_Core_Helper_Db */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}