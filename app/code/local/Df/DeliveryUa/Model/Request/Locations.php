<?php
class Df_DeliveryUa_Model_Request_Locations extends Df_DeliveryUa_Model_Request {
	/** @return array(string => int) */
	public function getLocations() {
		if (!isset($this->{__METHOD__})) {
			/** @var array(string => int) $result */
			$result = null;
			/**
			 * Обратите внимание, что не используем в качестве ключа __METHOD__,
			 * потому что данный метод может находиться
			 * в родительском по отношени к другим классе.
			 * @var string $cacheKey
			 */
			$cacheKey = implode('::', array(get_class($this), __FUNCTION__));
			/** @var string|bool $resultSerialized */
			$resultSerialized = $this->getCache()->load($cacheKey);
			if (false !== $resultSerialized) {
				$result = @unserialize($resultSerialized);
			}
			if (!is_array($result)) {
				$result = $this->parseLocations();
				df_assert_array($result);
				$resultSerialized = serialize($result);
				$this->getCache()->save($resultSerialized, $cacheKey);
			}
			df_result_array($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getQueryPath() {return '/ru/index.php';}

	/**
	 * @override
	 * @return string
	 */
	protected function getQueryParams() {return array('id' => 7068, 'show' => 29952);}

	/** @return array(string => int) */
	private function parseLocations() {
		/** @var array(string => int) $result */
		$result = array();
		/** @var array(string => int) $options */
		$options = $this->response()->options('select[name="city"] option');
		foreach ($options as $locationName => $locationId) {
			/** @var string $locationName */
			/** @var int $locationId */
			$locationName = preg_replace("#\-(\d)+#ui", '', $locationName);
			df_assert_string($locationName);
			if (!isset($result[$locationName])) {
				$result[$locationName] = $locationId;
			}
		}
		return $result;
	}

	const _CLASS = __CLASS__;
	/** @return Df_DeliveryUa_Model_Request_Locations */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}