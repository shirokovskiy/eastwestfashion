<?php
/**
 * @method Df_Parser_Model_Resource_Setup getSetup()
 */
class Df_Parser_Model_Setup_2_22_8 extends Df_Core_Model_Setup {
	/**
	 * @override
	 * @return Df_Parser_Model_Setup_2_22_8
	 */
	public function process() {
		if (Df_Catalog_Model_Resource_Installer_Attribute::s()->getAttributeId('catalog_category', 'lamoda__path')) {
			Df_Catalog_Model_Resource_Installer_Attribute::s()->removeAttribute('catalog_category', 'lamoda__path');
		}
		Df_Catalog_Model_Resource_Installer_Attribute::s()->addAdministrativeAttribute(
			$entityType = 'catalog_category'
			,$attributeId = Df_Catalog_Model_Category::P__EXTERNAL_URL
			,$attributeLabel = 'Веб-адрес на сайте-доноре'
			,$groupName = 'General Information'
		);
		/**
		 * Вот в таких ситуациях, когда у нас меняется структура прикладного типа товаров,
		 * нам нужно сбросить глобальный кэш EAV.
		 */
		rm_eav_reset();
		return $this;
	}

	const _CLASS = __CLASS__;
	/** @return Df_Parser_Model_Setup_2_22_8 */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}