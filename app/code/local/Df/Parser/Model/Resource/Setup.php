<?php
class Df_Parser_Model_Resource_Setup extends Df_Core_Model_Resource_Setup_Abstract {
	/** @return Df_Parser_Model_Resource_Setup */
	public function install_2_22_8() {
		Df_Parser_Model_Setup_2_22_8::s()->process();
		return $this;
	}
	const _CLASS = __CLASS__;
}