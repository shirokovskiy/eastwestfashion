<?php
class Df_Pec_Model_Request_Locations extends Df_Shipping_Model_Request {
	/** @return array(string => string) */
	public function getResponseAsArray() {
		if (!isset($this->{__METHOD__})) {
			/** @var array(string => string) $result */
			$result = null;
			/**
			 * Обратите внимание, что не используем в качестве ключа __METHOD__,
			 * потому что данный метод может находиться
			 * в родительском по отношени к другим классе.
			 * @var string $cacheKey
			 */
			$cacheKey = implode('::', array(get_class($this), __FUNCTION__));
			/** @var string|bool $resultSerialized */
			$resultSerialized = $this->getCache()->load($cacheKey);
			if (false !== $resultSerialized) {
				$result = @unserialize($resultSerialized);
			}
			if (!is_array($result)) {
				/** @var array(string => int) $locationsFlatten */
				$locationsFlatten =
					call_user_func_array(
						'array_merge'
						,array_map('array_flip', array_values($this->response()->json()))
					)
				;
				df_assert_array($locationsFlatten);
				/** @var string[] $locationNames */
				$locationNames = array_keys($locationsFlatten);
				df_assert_array($locationNames);
				/** @var string[] $locationNamesProcessed */
				$locationNamesProcessed =
					array_map(array($this, 'processLocationName'), $locationNames)
				;
				df_assert_array($locationNamesProcessed);
				$result =
					df_array_combine(
						$locationNamesProcessed
						,array_map('strval', array_values($locationsFlatten))
					)
				;
				$resultSerialized = serialize($result);
				$this->getCache()->save($resultSerialized, $cacheKey);
			}
			df_result_array($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param string $locationName
	 * @return string
	 */
	public function processLocationName($locationName) {
		df_param_string($locationName, 0);
		return mb_strtoupper(df_trim(rm_preg_match('#([^\(]+)#u', $locationName)));
	}

	/**
	 * @override
	 * @return string
	 */
	protected function getQueryHost() {return 'pecom.ru';}

	/**
	 * @override
	 * @return string
	 */
	protected function getQueryPath() {return '/ru/calc/towns.php';}

	/**
	 * @override
	 * @return bool
	 */
	protected function needConvertResponseFrom1251ToUtf8() {return true;}

	const _CLASS = __CLASS__;
	/** @return Df_Pec_Model_Request_Locations */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}