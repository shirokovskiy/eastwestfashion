<?php
class Df_Spsr_Model_Validator_Origin
	extends Df_Shipping_Model_Config_Backend_Validator_Strategy_Origin {
	/**
	 * @override
	 * @return bool
	 */
	public function validate() {
		/** @var bool $result */
		$result = false;
		try {
			Df_Spsr_Model_Locator::i(
				array(
					Df_Spsr_Model_Locator::P__CITY => $this->getOrigin()->getCity()
					,Df_Spsr_Model_Locator::P__COUNTRY_ID => $this->getOrigin()->getCountryId()
					,Df_Spsr_Model_Locator::P__REGION_ID => $this->getOrigin()->getRegionId()
					,Df_Spsr_Model_Locator::P__REQUEST => null
					,Df_Spsr_Model_Locator::P__IS_DESTINATION => false
				)
			)->getResult();
			$result = true;
		}
		catch(Exception $e) {
			$this->getBackend()->getMessages()
				->addMessage(
					new Mage_Core_Model_Message_Error(
						"Служба доставки СПСР"
						. " не может забрать груз"
						. " из указанного администратором в настройках магазина склада,"
						. " потому что не работает с этим населённым пунктом"
						. " или не понимает указанный администратором адрес."
					)
				)
			;
		}
		return $result;
	}

	const _CLASS = __CLASS__;
}