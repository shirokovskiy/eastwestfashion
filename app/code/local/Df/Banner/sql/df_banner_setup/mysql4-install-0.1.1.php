<?php
$installer = $this;
$installer->startSetup();
$installer->run("

DROP TABLE IF EXISTS {$this->getTable('df_banner')};
CREATE TABLE {$this->getTable('df_banner')} (
  `banner_id` int(11) unsigned NOT null auto_increment,
  `identifier` varchar(255) NOT null default '',
  `title` varchar(255) NOT null default '',
  `show_title` smallint(6) NOT null default '0',
  `content` text null default '',
  `width` int(11) unsigned null,
  `height` int(11) unsigned null,
  `delay` int(11) unsigned null,
  `status` smallint(6) NOT null default '0',
  `active_from` datetime null,
  `active_to` datetime null,
  `created_time` datetime null,
  `update_time` datetime null,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS {$this->getTable('df_banner_item')};
CREATE TABLE {$this->getTable('df_banner_item')} (
  `banner_item_id` int(11) unsigned NOT null auto_increment,
  `banner_id` int(11) unsigned NOT null,
  `title` varchar(255) NOT null default '',
  `image` varchar(255) NOT null default '',
  `image_url` varchar(512) NOT null default '',
  `thumb_image` varchar(255) NOT null default '',
  `thumb_image_url` varchar(512) NOT null default '',
  `content` text null default '',
  `link_url` varchar(512) NOT null default '#',
  `status` smallint(6) NOT null default '0',
  `created_time` datetime null,
  `update_time` datetime null,
  PRIMARY KEY (`banner_item_id`),
  CONSTRAINT `FK_DF_BANNER_ITEM` FOREIGN KEY (`banner_id`) REFERENCES `{$this->getTable('df_banner')}` (`banner_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	");
$installer->endSetup();