<?php
class Df_Directory_Model_Setup_Processor_UpgradeTo201 extends Df_Core_Model_Abstract {
	/** @return Df_Directory_Model_Setup_Processor_UpgradeTo201 */
	public function process() {
		foreach ($this->getInstaller()->getRussianRegionsFromLegacyModules() as $region) {
			/** @var Mage_Directory_Model_Region $region */
			/** @var string $oldCode */
			$oldCode = $region->getData(Df_Directory_Model_Region::P__ORIGINAL_NAME);
			if (is_null($oldCode)) {
				$oldCode = $region->getData(Df_Directory_Model_Region::P__DEFAULT_NAME);
			}
			df_assert_string($oldCode);
			/** @var string|null $newCode */
			$newCode = df_a($this->getMap(), $oldCode);
			if (is_null($newCode)) {
				Mage::log(rm_sprintf('Не могу найти новый код для старого кода %s', $oldCode));
			}
			else {
				$region->setData('code', $newCode);
				$region->save();
			}
		}
	}

	/** @return array(string => string) */
	private function getMap() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} =
				array(
					'Алтайский' => '22'
					,'Амурская' => '28'
					,'Архангельская' => '29'
					,'Астраханская' => '30'
					,'Белгородская' => '31'
					,'Брянская' => '32'
					,'Владимирская' => '33'
					,'Волгоградская' => '34'
					,'Вологодская' => '35'
					,'Воронежская' => '36'
					,'Еврейская' => '79'
					,'Забайкальский' => '75'
					,'Ивановская' => '37'
					,'Иркутская' => '38'
					,'Кабардино-Балкарская' => '07'
					,'Калининградская' => '39'
					,'Калужская' => '40'
					,'Камчатский' => '41'
					,'Карачаево-Черкесская' => '09'
					,'Кемеровская' => '42'
					,'Кировская' => '43'
					,'Костромская' => '44'
					,'Краснодарский' => '23'
					,'Красноярский' => '24'
					,'Курганская' => '45'
					,'Курская' => '46'
					,'Ленинградская' => '47'
					,'Липецкая' => '48'
					,'Магаданская' => '49'
					,'Москва' => '77'
					,'Московская' => '50'
					,'Мурманская' => '51'
					,'Ненецкий' => '83'
					,'Нижегородская' => '52'
					,'Новгородская' => '53'
					,'Новосибирская' => '54'
					,'Омская' => '55'
					,'Оренбургская' => '56'
					,'Орловская' => '57'
					,'Пензенская' => '58'
					,'Пермский' => '59'
					,'Приморский' => '25'
					,'Псковская' => '60'
					,'Адыгея' => '01'
					,'Алтай' => '04'
					,'Башкортостан' => '02'
					,'Бурятия' => '03'
					,'Дагестан' => '05'
					,'Ингушетия' => '06'
					,'Калмыкия' => '08'
					,'Карелия' => '10'
					,'Коми' => '11'
					,'Марий Эл' => '12'
					,'Мордовия' => '13'
					,'Саха (Якутия)' => '14'
					,'Северная Осетия — Алания' => '15'
					,'Татарстан' => '16'
					,'Тыва (Тува)' => '17'
					,'Хакасия' => '19'
					,'Ростовская' => '61'
					,'Рязанская' => '62'
					,'Самарская' => '63'
					,'Санкт-Петербург' => '78'
					,'Саратовская' => '64'
					,'Сахалинская' => '65'
					,'Свердловская' => '66'
					,'Смоленская' => '67'
					,'Ставропольский' => '26'
					,'Тамбовская' => '68'
					,'Тверская' => '69'
					,'Томская' => '70'
					,'Тульская' => '71'
					,'Тюменская' => '72'
					,'Удмуртская' => '18'
					,'Ульяновская' => '73'
					,'Хабаровский' => '27'
					,'Ханты-Мансийский' => '86'
					,'Челябинская' => '74'
					,'Чеченская' => '20'
					,'Чувашская' => '21'
					,'Чукотский' => '87'
					,'Ямало-Ненецкий' => '89'
					,'Ярославская' => '76'
				)
			;
		}
		return $this->{__METHOD__};
	}

	/** @return Df_Directory_Model_Resource_Setup */
	private function getInstaller() {return $this->cfg(self::P__INSTALLER);}

	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_prop(self::P__INSTALLER, Df_Directory_Model_Resource_Setup::_CLASS);
	}
	const _CLASS = __CLASS__;
	const P__INSTALLER = 'installer';
	/**
	 * @static
	 * @param Df_Directory_Model_Resource_Setup $setup
	 * @return Df_Directory_Model_Setup_Processor_UpgradeTo201
	 */
	public static function i(Df_Directory_Model_Resource_Setup $setup) {
		return new self(array(self::P__INSTALLER => $setup));
	}

}