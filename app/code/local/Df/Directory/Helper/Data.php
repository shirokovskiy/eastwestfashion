<?php
class Df_Directory_Helper_Data extends Mage_Directory_Helper_Data {
	/**
	 * @override
	 * @return string
	 */
	public function __() {
		/**
		 * Обратите внимание, что этот метод нельзя записать в одну строку,
		 * потому что функция func_get_args() не может быть параметром другой функции.
		 */
		/** @var mixed[] $args */
		$args = func_get_args();
		return df_h()->localization()->translation()->translateByParent($args, $this);
	}

	/** @return Df_Directory_Helper_Assert */
	public function assert() {
		return Df_Directory_Helper_Assert::s();
	}

	/** @return Df_Directory_Helper_Check */
	public function check() {
		return Df_Directory_Helper_Check::s();
	}

	/** @return Df_Directory_Helper_Country */
	public function country() {
		return Df_Directory_Helper_Country::s();
	}

	/** @return Df_Directory_Helper_Currency */
	public function currency() {
		return Df_Directory_Helper_Currency::s();
	}

	/** @return Df_Directory_Helper_Finder */
	public function finder() {
		return Df_Directory_Helper_Finder::s();
	}

	/** @return Zend_Locale */
	public function getLocaleEnglish() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = new Zend_Locale(Mage_Core_Model_Locale::DEFAULT_LOCALE);
		}
		return $this->{__METHOD__};
	}

	/**
	 * Retrieve regions data json
	 * @return string
	 */
	public function getRegionJson()
	{
		Varien_Profiler::start('TEST: '.__METHOD__);
		if (!$this->_regionJson) {
			$cacheKey = 'DIRECTORY_REGIONS_JSON_STORE'.Mage::app()->getStore()->getId();
			if (Mage::app()->useCache('config')) {
				$json = Mage::app()->loadCache($cacheKey);
			}
			if (empty($json)) {
				$countryIds = array();
				foreach ($this->getCountryCollection() as $country) {
					$countryIds[]= $country->getCountryId();
				}
				/** @var Df_Directory_Model_Resource_Region_Collection $collection */
				$collection = Df_Directory_Model_Region::c();
				$collection
					->addCountryFilter($countryIds)
					->load();
				$regions = array();
				foreach ($collection as $region) {
					/** @var Df_Directory_Model_Region $region */
					if (!$region->getRegionId()) {
						continue;
					}


					/**
					 * BEGIN PATCH
					 */
					$regions[$region->getCountryId()][]= array(
						'code' => $region->getCode(),'name' => $this->__($region->getName())
						,'id' => $region->getRegionId()
					);
					/**
					 * END PATCH
					 */
				}
				$json = df_mage()->coreHelper()->jsonEncode($regions);
				if (Mage::app()->useCache('config')) {
					Mage::app()->saveCache($json, $cacheKey, array('config'));
				}
			}
			$this->_regionJson = $json;
		}

		Varien_Profiler::stop('TEST: '.__METHOD__);
		return $this->_regionJson;
	}

	/**
	 * @param int $regionId
	 * @return string
	 */
	public function getRegionFullNameById($regionId) {
		df_param_integer($regionId, 0);
		/** @var Mage_Directory_Model_Region $region */
		$region = $this->getRussianRegions()->getItemById($regionId);
		df_assert($region);
		return $region->getName();
	}

	/** @return Df_Directory_Model_Resource_Region_Collection */
	public function getRegions() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = Df_Directory_Model_Region::c();
			$this->normalizeRegions($this->{__METHOD__});
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param int $regionId
	 * @return string|null
	 */
	public function getRegionNameById($regionId) {
		if (
				!isset($this->_regionNameById[$regionId])
			&&
				!df_a($this->_regionNameByIdIsNull, $regionId, false)
		) {
			/** @var Mage_Directory_Model_Region|null $region */
			$region = $this->getRegions()->getItemById($regionId);
			/** @var string|null $result */
			$result =
				is_null($region)
				? null
				: df_a(
					$region->getData()
					,Df_Directory_Model_Region::P__ORIGINAL_NAME
					,$region->getName()
				)
			;
			if (!is_null($result)) {
				df_result_string($result);
			}
			else {
				$this->_regionNameByIdIsNull[$regionId] = true;
			}
			$this->_regionNameById[$regionId] = $result;
		}
		return $this->_regionNameById[$regionId];
	}
	/** @var string[] */
	private $_regionNameById = array();
	/** @var bool[] */
	private $_regionNameByIdIsNull = array();

	/** @return Df_Directory_Model_Resource_Region_Collection */
	public function getRussianRegions() {
		if (!isset($this->{__METHOD__})) {
			$this->{__METHOD__} = df_h()->directory()->country()->getRussia()->getRegions();
			$this->normalizeRegions($this->{__METHOD__});
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param string $locationName
	 * @return string
	 */
	public function normalizeLocationName($locationName) {
		return df_trim(str_replace('Ё', 'Е', mb_strtoupper($locationName)));
	}

	/**
	 * @param Varien_Data_Collection_Db $regions
	 * @return Mage_Directory_Model_Resource_Region_Collection|Mage_Directory_Model_Mysql4_Region_Collection
	 */
	public function normalizeRegions(Varien_Data_Collection_Db $regions) {
		df_h()->directory()->assert()->regionCollection($regions);
		/** @var bool $needNormalize */
		static $needNormalize;
		if (!isset($needNormalize)) {
			$needNormalize =
					!df_cfg()->directory()->regionsRu()->getEnabled()
				||
					!df_enabled(Df_Core_Feature::DIRECTORY)
			;
		}
		if ($needNormalize) {
			Df_Directory_Model_Handler_ProcessRegionsAfterLoading::addTypeToNameStatic($regions);
		}
		return $this;
	}

	/** @return Df_Directory_Helper_Data */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}