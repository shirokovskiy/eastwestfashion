<?php
abstract class Df_Garantpost_Model_Request_Locations extends Df_Garantpost_Model_Request {
	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getOptionsSelector();

	/**
	 * @abstract
	 * @param string $locationName
	 * @return string
	 */
	abstract protected function normalizeLocationName($locationName);

	/** @return array(string => int) */
	public function getResponseAsArray() {
		if (!isset($this->{__METHOD__})) {
			/** @var array(string => int) $result */
			$result = null;
			/**
			 * Обратите внимание, что не используем в качестве ключа __METHOD__,
			 * потому что данный метод может находиться
			 * в родительском по отношени к другим классе.
			 * @var string $cacheKey
			 */
			$cacheKey = implode('::', array(get_class($this), __FUNCTION__));
			/** @var string|bool $resultSerialized */
			$resultSerialized = $this->getCache()->load($cacheKey);
			if (false !== $resultSerialized) {
				$result = @unserialize($resultSerialized);
			}
			if (!is_array($result)) {
				/** @var array(string => string) $options */
				$options = $this->response()->options($this->getOptionsSelector());
				/** @var array(string => int) $locations */
				$locations = array();
				foreach ($options as $locationName => $locationId) {
					/** @var string $locationName */
					/** @var int $locationId */
					$locationName = $this->normalizeLocationName($locationName);
					$locations[$locationName]= $locationId;
				}
				$result = $this->postProcessLocations($locations);
				$resultSerialized = serialize($result);
				$this->getCache()->save($resultSerialized, $cacheKey);
			}
			df_result_array($result);
			$this->{__METHOD__} = $result;
		}
		return $this->{__METHOD__};
	}

	/**
	 * @param array(string => int) $locations
	 * @return array(string => int)
	 */
	protected function postProcessLocations($locations) {
		// У Чеченской республики отсутствует код
		return df_clean($locations);
	}

	const _CLASS = __CLASS__;
}