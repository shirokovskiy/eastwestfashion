<?php
/**
 * @method Df_Catalog_Model_Event_AttributeSet_AddDefaultAttributes getEvent()
 */
class Df_Shipping_Model_Handler_AddDimensionsToNewProductAttributeSet extends Df_Core_Model_Handler {
	/**
	 * Метод-обработчик события
	 * @override
	 * @return void
	 */
	public function handle() {
		Df_Shipping_Model_Processor_AddDimensionsToProductAttributeSet::processStatic(
			$this->getEvent()->getAttributeSet(), $needReindex = true
		);
	}

	/**
	 * Класс события (для валидации события)
	 * @override
	 * @return string
	 */
	protected function getEventClass() {
		return Df_Catalog_Model_Event_AttributeSet_AddDefaultAttributes::_CLASS;
	}

	const _CLASS = __CLASS__;
}