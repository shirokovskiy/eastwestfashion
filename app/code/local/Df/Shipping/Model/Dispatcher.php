<?php
class Df_Shipping_Model_Dispatcher {
	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function df_catalog__attribute_set__add_default_attributes(Varien_Event_Observer $observer) {
		try {
			df_handle_event(
				Df_Shipping_Model_Handler_AddDimensionsToNewProductAttributeSet::_CLASS
				,Df_Catalog_Model_Event_AttributeSet_AddDefaultAttributes::_CLASS
				,$observer
			);
		}
		catch(Exception $e) {
			df_handle_entry_point_exception($e);
		}
	}
}