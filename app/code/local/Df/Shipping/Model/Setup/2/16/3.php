<?php
class Df_Shipping_Model_Setup_2_16_3 extends Df_Core_Model_Setup {
	/**
	 * @override
	 * @return Df_Shipping_Model_Setup_2_16_3
	 */
	public function process() {
		/** @var string[] $attributeCodes */
		$attributeCodes =
			array(
				Df_Catalog_Model_Product::P__WIDTH
				,Df_Catalog_Model_Product::P__HEIGHT
				,Df_Catalog_Model_Product::P__LENGTH
			)
		;
		foreach ($attributeCodes as $attributeCode) {
			/** @var string $attributeCode */
			Df_Catalog_Model_Resource_Installer_Attribute::s()
				->updateAttribute(
					$entityTypeId = Mage_Catalog_Model_Product::ENTITY
					,$id = $attributeCode
					,$field = 'is_user_defined'
					,$value = 0
				)
			;
		}
		rm_eav_reset();
		df_h()->catalog()->product()->reindexFlat();
		return $this;
	}
	/** @return Df_Shipping_Model_Setup_2_16_3 */
	public static function s() {static $r; return $r ? $r : $r = new self;}
}