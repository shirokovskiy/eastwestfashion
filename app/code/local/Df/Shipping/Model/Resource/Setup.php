<?php
class Df_Shipping_Model_Resource_Setup extends Df_Core_Model_Resource_Setup_Abstract {
	/** @return void */
	public function install_2_15_4() {Df_Shipping_Model_Setup_2_15_4::s()->process();}
	/** @return void */
	public function upgrade_2_16_2() {Df_Shipping_Model_Setup_2_16_2::s()->process();}
	/** @return void */
	public function upgrade_2_16_3() {Df_Shipping_Model_Setup_2_16_3::s()->process();}
	/** @return void */
	public function upgrade_2_30_0() {Df_Shipping_Model_Setup_2_30_0::s()->process();}
}