<?php
class Df_Pickup_Model_Resource_Setup extends Df_Core_Model_Resource_Setup_Abstract {
	/** @return Df_Pickup_Model_Resource_Setup */
	public function install_1_0_0() {
		Df_Pickup_Model_Resource_Point::s()->tableCreate($this);
		return $this;
	}
}