<?php
class Starik_Catalog_Model_Observer 
{
    public function addToTopmenu(Varien_Event_Observer $observer)
	{
		$menu = $observer->getMenu();
		$tree = $menu->getTree();
		$node = new Varien_Data_Tree_Node(array(
				'name'   => Mage::helper('catalog')->__('New'),
				'id'     => 'new',
				'url'    => Mage::getUrl('starikcatalog/index/view', array('id' => 7)), // point somewhere
				'sort'	 => '1'
		), 'id', $tree, $menu);
		$menu->addChild($node);
		// Children menu items
		$todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		
		$collection = Mage::getResourceModel('catalog/category_collection')
				->setStore(Mage::app()->getStore())
				->addAttributeToFilter('entity_id', array('in' => array(/*2, */7, 11, 18, 74)))
				->addIsActiveFilter()
				->addNameToResult();
				
		foreach ($collection as $category) {
			$category->setIsAnchor(true);

			$sub_collection = Mage::getResourceModel('catalog/product_collection')
				->addStoreFilter()
				->addCategoryFilter($category)
				->addAttributeToFilter('news_from_date', array('date' => true, 'to' => $todayDate))
				->addAttributeToFilter('news_to_date', array('or'=> array(
					0 => array('date' => true, 'from' => $todayDate),
					1 => array('is' => new Zend_Db_Expr('null')))
				), 'left');

			if (count($sub_collection)) {
				$tree = $node->getTree();
				$name = $category->getName();
				$id = $category->getId();
				if ($category->getId()==2) {
					$name = Mage::helper('catalog')->__('All Products');
				}
				$data = array(
					'name'   => $name,
					'id'     => 'category-node-'.$category->getId(),
					'url'    => Mage::getUrl('starikcatalog/index/view', array('id' => $id)),
				);
				$subNode = new Varien_Data_Tree_Node($data, 'id', $tree, $node);
				$node->addChild($subNode);
			}
		}
	}
}
