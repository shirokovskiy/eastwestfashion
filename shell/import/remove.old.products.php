<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 9/29/14
 * Time         : 12:52 PM
 * Description  : Removes Disabled products by Price & Type
 * Usage        : php remove.old.products.php simple lteq 1000
 */

set_time_limit(0);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
umask(0);
ini_set('memory_limit', '1024M');

$logfile = 'remove.old.products.'.date('Y.m.d').'.log';
$scriptStartTime = microtime(true);
$store_id = 1;
$website_ids = array(1);
$store_cat_depends = array(1=>2); // dependencies of store to the root category
$store_default_category_id = $store_cat_depends[$store_id];
$arrExistCategories = array();
$skip_images = false;

require_once dirname(__FILE__)."/../../app/Mage.php";
$app = Mage::app();
$app->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

if (preg_match("/htroot/im", __FILE__)) {
    Mage::setIsDeveloperMode(true);
    ini_set('display_errors', 1);
}



$prd_type = 'simple';

if (isset($argv[1]) && !empty($argv[1])) {
    $prd_type = (string)$argv[1];
}

$prd_cmp_price = 'lteq';

if (isset($argv[2]) && !empty($argv[2])) {
    $prd_cmp_price = (string)$argv[2];
}

if (!in_array($prd_cmp_price, array('lteq','gteq','eq','lt','gt','neq'))) {
    $prd_cmp_price = 'lteq';
}

$price_to = 10000; /// limit by default

if (isset($argv[3]) && floatval($argv[3]) > 0) {
    $price_to = (float)$argv[3];
}

/**
 * Сначала сбросим все существующие товары
 * Возможно какие-то товары уже не актуальны
 */
Mage::log("Prepare all Store products for clearing with price $prd_cmp_price $price_to !", null, $logfile);

$_ProductCollection = Mage::getModel('catalog/product')
    ->setStoreId($store_id)
    ->getResourceCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('status', array('eq' => 2)) // 2 = Disabled || 1 = Enabled
    ->addAttributeToFilter('type_id', array('eq' => $prd_type)) // simple || configurable
    ->addAttributeToFilter('price', array($prd_cmp_price => $price_to))
    ->addStoreFilter($store_id);

Mage::log('Founded products: '.$_ProductCollection->getSize(), null, $logfile);

Mage::register('isSecureArea', true); // Simulate CMS: all products has to be removed in CMS

if ($_ProductCollection->getSize() > 0) {
    foreach($_ProductCollection as $_product)
    {
        #
        try {
            $entity_id = $_product->getEntityId();
            $product = Mage::getModel('catalog/product')->load($entity_id);
            $price = $product->getPrice();

            if ($product->getId()) {
                $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
                $items = $mediaApi->items($product->getId());
                foreach($items as $item) {
                    if (file_exists(BP.DS.'media'.DS.'catalog'.DS.'product'.DS.$item['file'])) {
                        if (unlink(BP.DS.'media'.DS.'catalog'.DS.'product'.DS.$item['file'])) {
                            Mage::log('Image '.$item['file'].' removed', null, $logfile);
                        }
                    }
                }
            }

            $product->delete();
            Mage::log('Product ID '.$entity_id.' removed ('.$price.')', null, $logfile);

        } catch(Exception $e) {
            Mage::log($e->getTraceAsString(),null, $logfile);
        }
    }
}

$scriptStopTime = microtime(true);
$totalWorkTime = $scriptStopTime - $scriptStartTime;
Mage::log('End work time: '.$totalWorkTime,null,$logfile);
