<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 9/8/14
 * Time         : 12:55 PM
 * Description  : to get products from ShopBop through GoldenFeeds service
 *
 * TODO         : prices rates from Magento, configurable, images,
 */
set_time_limit(0);
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
umask(0);
ini_set('display_errors', 1);
ini_set('memory_limit', '1024M');

$scriptStartTime = microtime(true);
require_once dirname(__FILE__) . "/../app/Mage.php";

if (preg_match("/htroot/im", __FILE__)) {
    Mage::setIsDeveloperMode(true);
}

$import_log_filename = 'goldenfeeds.log';
$store_id = 1;
$website_ids = array(1);
$store_cat_depends = array(1=>2); // dependencies of store to the root category
$store_default_category_id = $store_cat_depends[$store_id];
$arrExistCategories = array();
$skip_images = false;

$app = Mage::app();
$app->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$baseCode = $app->getBaseCurrencyCode(); // should be RUB
$allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
$rates = Mage::getModel('directory/currency')->getCurrencyRates(/*$baseCode*/'USD', array_values($allowedCurrencies));
if (isset($rates[$baseCode])) {
    $rate_RUB_USD = (float) $rates[$baseCode];
}
//$translator = Mage::getSingleton('core/translate')->setLocale($locale)->init('frontend', true);

//$csvFile = new Varien_File_Csv();
$fileName = 'custom-feed-commission-junction-shopbop439-ewf-partners-goods-in-csv'; // read it on GoldenFeeds
$local_file_path = BP.DS.'var'.DS.'goldenfeed'.DS; // base folder for archive
$local_file_zip = $local_file_path.$fileName.'.csv.zip'; // comes to this location from GoldenFeeds
$local_file_csv = $local_file_path.$fileName.'.csv';
$local_file_zip_bkp = $local_file_csv.'.'.date('YmdHis').'.zip'; // comes to this location from GoldenFeeds
$local_file_md5_path = $local_file_path.$fileName.'.md5'; // этот файл появляется если выгружает GoldenFeeds

// Proper titles
$arrTitles = array(
    'sku'
    , 'product_number'
    , 'product_name'
    , 'brand'
    , 'category'
    , 'colors'
    , 'sizes'
    , 'materials'
    , 'price'
    , 'retail_price'
    , 'sale_price'
    , 'currency'
    , 'delivery_cost'
    , 'delivery_details'
    , 'promotion_details'
    , 'url'
    , 'description'
    , 'keywords'
    , 'gender'
    , 'availability'
    , 'thumbnail_image'
    , 'large_image'
    , 'thumbnail_page_url'
    , 'additional_image1'
    , 'additional_image2'
    , 'additional_image3'
    , 'additional_image4'
    , 'additional_image5'
    , 'condition'
    , 'isbn'
    , 'ean'
    , 'star_rating'
    , 'number_of _reviews'
    , 'country_of_origin'
    , 'dimensions'
    , 'original_url'
    , 'labels'
    , 'url_m'
    , 'url_ru'
    , 'short_product_name'
    , 'upc'
);

// awk '{ if (NR==1) sub(/^\xef\xbb\xbf/,""); print }' INFILE > OUTFILE

$scriptStopTime = microtime(true);
$totalWorkTime = $scriptStopTime - $scriptStartTime;
Mage::log('Line:'.__LINE__.', work time: '.$totalWorkTime,null,$import_log_filename);

if (Mage::getIsDeveloperMode() === false) {
    if (file_exists($local_file_zip)) {
        /**
         * Back-up file
         */
        if (!copy($local_file_zip, $local_file_zip_bkp)) {
            Mage::log("Could not backup zip $local_file_zip", null, $import_log_filename);
        } else {
            Mage::log("File has been backed-up from $local_file_zip to $local_file_zip_bkp", null, $import_log_filename);
            // Unzip file
            $zip = new ZipArchive;
            $res = $zip->open($local_file_zip);
            if ($res === TRUE) {
                $zip->extractTo($local_file_path);
                Mage::log("File has been unziped to $local_file_csv", null, $import_log_filename);
                $zip->close();
            } else {
                Mage::log("Could not unzip $local_file_zip", null, $import_log_filename);
            }
        }

        /**
         * Let's check unzipped file
         */
        if (file_exists($local_file_csv)) {
            /**
             * Work with MD5
             */
            $md5_previous = null;
            if (file_exists($local_file_md5_path)) {
                $md5_previous = file_get_contents($local_file_md5_path);
            }

            $md5_current = md5_file($local_file_csv);

            if ($md5_current == $md5_previous) {
                Mage::log("File has not updated", null, $import_log_filename);
                die("\nStop execution\n\n");
            } else {
                if (file_put_contents($local_file_md5_path, $md5_current)) {
                    Mage::log("MD5 of File saved", null, $import_log_filename);
                }
            }
        }
    } else {
        Mage::log("File isn't exist: $local_file_zip", null, $import_log_filename);
        die("\nStop execution\n\n");
    }
} else {
    // for quick testing
    $skip_images = false;
}

$firstRowCount = 0;
$tc = 0;
$firstRowChecked = false;
$arrProductNumbers = array();

if (file_exists($local_file_csv)){
    if (($handle = fopen($local_file_csv, "r")) !== FALSE) {
        /**
         * Перед тем как что-то обрабатывать
         * убедимся что файл из которого мы сейчас будем заливать данные
         * хороший (валидный) как минимум по количеству полей и их заголовкам
         */
        if (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
            if (empty($data)) {
                Mage::log('No data in row! '.$tc, null, $import_log_filename);
                die("Error at line:".__LINE__."\n");
            }

            # Lets check titles to be sure we have necessary order of titles
            if (is_array($data)) {
                if (count($data) == count($arrTitles)) {
                    foreach ($arrTitles as $key => $title) {
                        if ($arrTitles[$key] != $data[$key]) {
                            Mage::log('Bad CSV file by columns', null, $import_log_filename);
                            die;
                        }/* else {
                            Mage::log($arrTitles[$key].' equal '.$data[$key], null, $import_log_filename);
                        }*/
                    }
                }
            } else {
                Mage::log('Count of title`s row not equal to arrTitles', null, $import_log_filename);
                die;
            }
        }

        $scriptStopTime = microtime(true);
        $totalWorkTime = $scriptStopTime - $scriptStartTime;
        Mage::log('BEFORE SQL Line:'.__LINE__.', work time: '.$totalWorkTime,null,$import_log_filename);
//        die("\n" . __FILE__ . ":" . __LINE__ . "\n\n");

        /**
         * Сначала сбросим все существующие товары
         * Возможно какие-то товары уже не актуальны
         */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $sql = "UPDATE catalog_product_entity_int SET `value` = 2 WHERE attribute_id = 96 AND `value` = 1";
        try {
            $connection->query($sql);
            Mage::log('All Store products is cleared!', null, $import_log_filename);
        } catch (Exception $e){
            Mage::log( $e->getMessage(), null, $import_log_filename);
        }

        $scriptStopTime = microtime(true);
        $totalWorkTime = $scriptStopTime - $scriptStartTime;
        Mage::log('AFTER SQL Line:'.__LINE__.', work time: '.$totalWorkTime,null,$import_log_filename);

        $scriptStopTime = microtime(true);
        $totalWorkTime = $scriptStopTime - $scriptStartTime;
        Mage::log('Line:'.__LINE__.', work time: '.$totalWorkTime,null,$import_log_filename);

        $product_qty = 1; // todo: if we get Inventory data from ShopBop, will set it inside WHILE

        /**
         * Let's check every row from CSV file
         * and insert data into Magento
         */
        while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
            $tc++;
            $id = $article = $sku = null;

            $scriptStopTime = microtime(true);
            $totalWorkTime = $scriptStopTime - $scriptStartTime;
            Mage::log('READ CSV PRODUCT, tc = '.$tc.', work time: '. $totalWorkTime,null,$import_log_filename);

            if (empty($data)) {
                Mage::log('No data in row! '.$tc, null, $import_log_filename);
                continue; // let's check next row if exist
            }

            $Product = Mage::getModel('catalog/product');

            $sku = (string) $data[array_search('sku', $arrTitles)];
            Mage::log('SKU '.$sku,null,$import_log_filename);

            // Check if product exist
            $id = (int)$Product->getIdBySku($sku);

            if ($id > 0) {
                $Product = $Product->load($id);

                Mage::log('Product exist with ID: '.$id." (let`s update)", null, $import_log_filename);
            } else {
                /**
                 * Set default product info
                 */
                $Product->setTypeId('simple');
                $Product->setWeight(1);
                $Product->setAttributeSetId(4); // "Default" in Magento
                $Product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);
                $Product->setTaxClassId(0);
                $Product->setCreatedAt(strtotime('now'));
                $Product->setSku($sku);
                //$Product->setArticle($article);

                Mage::log('Set New Product', null, $import_log_filename);
            }

            $Product->setWebsiteIDs($website_ids);
            $Product->setStoreId($store_id);
            $Product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
            $Product->setData('is_salable', '1');

//            ->setMetaTitle('test meta title 2')
//            ->setMetaKeyword('test meta keyword 2')
//            ->setMetaDescription('test meta description 2')
//            ->setMediaGallery(array('images' => array(), 'values' => array())) //media gallery initialization

            //TODO: $product_qty = (int) $data[array_search('qty', $arrTitles)]; // ShopBop don't give us info =(

            $Product->setStockData(array(
                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                'manage_stock' => 1, //manage stock
                'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                'is_in_stock' => 1,
                'qty' => $product_qty
            ));

            // ***

            $product_name = (string) $data[array_search('product_name', $arrTitles)];

            if (empty($product_name)) {
                Mage::log("No Product Name for $tc", null, $import_log_filename);
                continue; // we can't proceed without this data
            }

            $Product->setName($product_name);

            // ***

            $short_product_desc = (string) $data[array_search('short_product_name', $arrTitles)];

            if (empty($short_product_desc)) {
                $short_product_desc = '&nbsp;';
            }

            $Product->setShortDescription($short_product_desc);

            // ***

            $description = (string) $data[array_search('description', $arrTitles)];

            if (empty($description)) {
                if (!empty($short_product_desc)) $description = $short_product_desc;
                else $description = $product_name;
            }

            $Product->setDescription($description);

            // ***

            $price = (float) $data[array_search('price', $arrTitles)];

            if (empty($price) || floatval($price) <= 0) {
                Mage::log("No Product Price for $tc", null, $import_log_filename);
                continue; // we can't proceed without this data
            }

            $price = $rate_RUB_USD * 1.05 * $price * 1.25;
//            $price = ($rate_RUB_USD * 1.05 * $price);
//            $price = $rate_RUB_USD * $price;

            $Product->setPrice($price);

            // ***

            $color = (string) $data[array_search('colors', $arrTitles)];

            $Product->setData('color',$color);

            $size = (string) $data[array_search('sizes', $arrTitles)];

            $Product->setData('size',$size);

            $materials = (string) $data[array_search('materials', $arrTitles)];

            $keywords = (string) $data[array_search('keywords', $arrTitles)];

            $dimensions = (string) $data[array_search('dimensions', $arrTitles)];

            $original_url = (string) $data[array_search('original_url', $arrTitles)];

            $product_number = (string) $data[array_search('product_number', $arrTitles)];

            if (!empty($product_number)) {
                if (isset($arrProductNumbers[$product_number])) {
                    if (array_key_exists($sku, $arrProductNumbers[$product_number]['childrens'])) {
                        Mage::log("SKU $sku already in ProductNumbers collection", null, $import_log_filename);
                    }
                }

                $arrProductNumbers[$product_number]['childrens'][$sku]['color'] = $color;
                $arrProductNumbers[$product_number]['childrens'][$sku]['size'] = $size;
                $arrProductNumbers[$product_number]['name'] = $product_name;
                $arrProductNumbers[$product_number]['price'] = isset($arrProductNumbers[$product_number]['price']) ? ($arrProductNumbers[$product_number]['price'] > $price ? $price : $arrProductNumbers[$product_number]['price']) : $price;
            }

            // ***

            $scriptStopTime = microtime(true);
            $totalWorkTime = $scriptStopTime - $scriptStartTime;
            Mage::log('Line before IMAGES:'.__LINE__.', work time: '.$totalWorkTime,null,$import_log_filename);

            /**
             * To download image for product
             */
            if ($skip_images != true)
            for ($i = 1; $i <=5; $i++) {
                $imgSize = $imgLocalPath = null;

                $image_url = (string) $data[array_search('additional_image'.$i , $arrTitles)];

                Mage::log($image_url,null,$import_log_filename);

                if (!empty($image_url)) {
                    Mage::log('Get remote image from: '.$image_url, null, $import_log_filename);

                    try {
                        $imgSize = getimagesize($image_url);
                    } catch (Exception $ex) {
                        Mage::log($ex->getMessage(), null, $import_log_filename);
                        Mage::log("Maybe bad Internet connection", null, $import_log_filename);
                    }

                    if (is_array($imgSize) && !empty($imgSize)) {
                        // картинка есть
                        try {
                            $imgLocalPath = Mage::getBaseDir('media').DS.'import'.DS.'shopbop'.DS.$sku.'.imported.jpg';
                            Mage::log('Local image '.$imgLocalPath,null,$import_log_filename);

                            if (file_exists($imgLocalPath)) {
                                # Проверим а стоит ли его перезаписывать
                                Mage::log('Image exist locally',null,$import_log_filename);
//                                $md5_remote_image = md5_file($image_url);
//                                $md5_local_image = md5_file($imgLocalPath);

//                                if ($md5_remote_image !== false && $md5_remote_image != $md5_local_image) {
//                                    if (copy($image_url, $imgLocalPath)) {
//                                        Mage::log('Image is rewrited OK. Path:'.$imgLocalPath, null, $import_log_filename);
//                                    }
//                                }
                            } else {
                                Mage::log('Copy remote image', null, $import_log_filename);
                                if (copy($image_url, $imgLocalPath)) {
                                    Mage::log('Image is copied OK. Path:'.$imgLocalPath, null, $import_log_filename);
                                }
                            }
                        } catch (Exception $ex) {
                            Mage::log($ex->getMessage(), null, $import_log_filename);
                        }

                        if (!empty($imgLocalPath) && file_exists($imgLocalPath)) {

                            // TODO: check if Image already attached to product

                            $Product->addImageToMediaGallery($imgLocalPath, array('image', 'small_image', 'thumbnail'), false, false); // 3rd boolean - means move (if true) image or copy (if false)
                        }
                    } else {
                        Mage::log('*** No IMAGE at: '.$image_url, null, $import_log_filename);
                    }
                } else {
                    Mage::log("*** No URL for image # $i of product ".$sku, null, $import_log_filename);
                }
            }
            // *** The end of image import

            $scriptStopTime = microtime(true);
            $totalWorkTime = $scriptStopTime - $scriptStartTime;
            Mage::log('Line after IMAGES:'.__LINE__.', work time: '.$totalWorkTime,null,$import_log_filename);

            /**
             * Let's create categories
             */
            $arrCategoriesIDs = array();
            $arrCategoriesIDs[] = $parentCategoryId = $store_default_category_id;
            $category_path = (string) $data[array_search('category', $arrTitles)];

            if (!empty($category_path)) {
                $md5catPath = md5($category_path);
                if (!array_key_exists($md5catPath, $arrExistCategories)) {
                    $arrExistCategories[$md5catPath] = array();
                    Mage::log("Detected Category Path: ". $category_path , null, $import_log_filename);
                    if (preg_match('/>/', $category_path)) {
                        // categories tree
                        $category_path = explode('>',$category_path);

                        if (is_array($category_path) && !empty($category_path)) {
                            /**
                             * Подготовим коллекцию для поиска по категориям
                             */
                            $collection = Mage::getModel('catalog/category')
                                ->setStoreId($store_id)
                                ->getCollection()
                                ->addFieldToFilter('parent_id', array('eq'=>$parentCategoryId))
                                ->addAttributeToSelect('name')
                                ->addAttributeToSelect('is_active');
                            // массив
                            foreach($category_path as $category_name) {
                                $isCatExist = false;
                                $category_name = trim($category_name); // could be as in Russian as in English
                                Mage::log('Let`s check Category "'.$category_name.'"', null, $import_log_filename);

                                /**
                                 * Я ищу категорию по названию, и если нахожу, то беру её ID
                                 * а если не нахожу, то создаю её
                                 */
                                foreach ($collection as $cat) {
                                    if ($cat->getName() == $category_name) {
                                        $parentCategoryId = $cat->getId();
                                        $arrExistCategories[$md5catPath][] = $arrCategoriesIDs[] = (int)$parentCategoryId;
                                        $isCatExist = true;

                                        Mage::log('Category exist "'.$cat->getName().'". Parent ID now: '.$parentCategoryId, null, $import_log_filename);
                                        break;
                                    }
                                }

                                /**
                                 * There is NO category by name
                                 */
                                if (!$isCatExist) {
                                    /**
                                     * Let's create it
                                     */
                                    #$urlKey = strtolower( preg_replace('/[^\da-z]/i', '', $category_name) ); // if non-English chars -> remove it
                                    $urlKey = strtolower( Slugger::slug($category_name) );
                                    Mage::log('URL key of non existent category: '.$urlKey, null, $import_log_filename);

                                    /**
                                     * Let's check if same URL_KEY exists
                                     */
                                    if (!empty($urlKey)) {
                                        $currentCategory = Mage::getModel('catalog/category')
                                            ->setStoreId($store_id)
                                            ->getCollection()
                                            ->addFieldToFilter('url_key', $urlKey)
                                            ->addFieldToFilter('parent_id', array('eq'=>$parentCategoryId))
                                            ->setCurPage(1)
                                            ->setPageSize(1)
                                            ->getFirstItem();

                                        /**
                                         * Если такого urlkey действительно нет,
                                         * то создадим категорию
                                         */
                                        if (!($currentCategory && $currentCategory->getId())) {

                                            Mage::log('No such URL key, let`s create category', null, $import_log_filename);

                                            $category = Mage::getModel('catalog/category');
                                            $category->setName($category_name)
                                                ->setUrlKey($urlKey)
                                                ->setIsActive(1)
                                                ->setDisplayMode('PRODUCTS')
                                                ->setIsAnchor(true)
                                                //->setDescription('This is a '.$category_name.' subcategory')
                                                //->setCustomDesignApply(1)
                                                //->setCustomDesign('storybloks/new-category-style') // create this template and layout in your design directory
                                                ->setAttributeSetId($category->getDefaultAttributeSetId())
                                            ;

                                            $parentCategory = Mage::getModel('catalog/category')->load($parentCategoryId);
                                            $category->setPath($parentCategory->getPath());

                                            Mage::log("parentCategory->getPath(): ".$parentCategory->getPath(), null, $import_log_filename);
                                            $category->save();

                                            $arrExistCategories[$md5catPath][] = $arrCategoriesIDs[] = $parentCategoryId = $category->getId();

                                            Mage::log("Created category: ".$category_name.' with ID = '.$category->getId(), null, $import_log_filename);

                                            unset($category, $parentCategory);
                                        } else {
                                            /**
                                             * А если такой urlKey существует...
                                             */
                                            $arrExistCategories[$md5catPath][] = $arrCategoriesIDs[] = $parentCategoryId = $currentCategory->getId();
                                            Mage::log('urlKey exists '.$urlKey.', $currentCategory->getId() = '.$currentCategory->getId(), null, $import_log_filename);
                                        }
                                    } else {
                                        Mage::log('*** urlKey empty for category '.$category_name, null, $import_log_filename);
                                    }
                                }
                            }

                            unset($collection);
                        }
                    } else {
                        // one category
                        Mage::log('There is only one category: '.$category_path.'.', null, $import_log_filename);
                    }
                } else {
                    foreach($arrExistCategories[$md5catPath] as $catId) {
                        $arrCategoriesIDs[] = $catId;
                    }
                }

                $Product->setCategoryIds($arrCategoriesIDs);
            }

            $Product->save();
            unset($Product);

            Mage::log("Product saved", null, $import_log_filename);

            // Lets parse and insert data into Products
            $scriptStopTime = microtime(true);
            $totalWorkTime = $scriptStopTime - $scriptStartTime;
            Mage::log('The End. Line:'.__LINE__.', work time: '.$totalWorkTime,null,$import_log_filename);

//            if ($tc >= 3) {
//                print_r($arrProductNumbers);
//                die("\nSTOP on $tc PRODUCTS: " . __FILE__ . ":" . __LINE__ . "\n\n");
//            }
        }

        Mage::log($arrProductNumbers, null, 'configurable.products.collection.txt');

        Mage::log('START CREATE CONFIGURABLE PRODUCTS',null,$import_log_filename);

        foreach ($arrProductNumbers as $product_number => $arrConfigProducts) {
            $productConfigurable = Mage::getModel('catalog/product');
            $productConfigurable
                ->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)
                ->setTaxClassId(0)
                ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->setStoreId($store_id)
                ->setWebsiteIDs($website_ids)
                ->setAttributeSetId(4)
                ->setSku("C" . $product_number)
                ->setName($arrConfigProducts['name'])
                ->setPrice(sprintf("%0.2f", floatval($arrConfigProducts['price'])))
                ->setCreatedAt(strtotime('now'))
                ->setStockData(
                    array(
                        'is_in_stock' => 1,
                        'qty' => 9999
                    )
                )
            ;

            $newAttributes = array();

            foreach(array("color", "size") as $attrCode){
                $super_attribute= Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product',$attrCode);
                $configurableAtt = Mage::getModel('catalog/product_type_configurable_attribute')->setProductAttribute($super_attribute);

                $newAttributes[] = array(
                    'id'             => $configurableAtt->getId(),
                    'label'          => $configurableAtt->getLabel(),
                    'position'       => $super_attribute->getPosition(),
                    'values'         => $configurableAtt->getPrices() ? $configurableAtt->getPrices() : array(),
                    'attribute_id'   => $super_attribute->getId(),
                    'attribute_code' => $super_attribute->getAttributeCode(),
                    'frontend_label' => $super_attribute->getFrontend()->getLabel(),
                );
            }

            Mage::log($newAttributes,null,$import_log_filename);

            $configurableData = array();

            foreach ($arrConfigProducts['childrens'] as $child_sku => $child_attr) {


                $childProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $child_sku);
                $childID = $childProduct->getId();

                if ($childID > 0) {
                    $sizeCode = $child_attr['size'];
                    $colourCode = $child_attr['color'];

                    $configurableData[$childID][] = array(
                        'attribute_id' => 160,                                     // HARD CODE for Color
                        'label' => $colourCode,
                        'value_index'=> getAttributeId($colourCode, 'color'),
                    );
                    $configurableData[$childID][] = array(
                        'attribute_id' => 143,                                     // HARD CODE for Size
                        'label' => $sizeCode,
                        'value_index'=> getAttributeId($sizeCode, 'size'),
                    );
                }
            }

            Mage::log($configurableData,null,$import_log_filename);

            $productConfigurable->setConfigurableProductsData($configurableData);
            $productConfigurable->setConfigurableAttributesData($newAttributes);
            try{
                $productConfigurable->save();
                Mage::log('Configurable product added: '.$productConfigurable->getId()."::".$productConfigurable->getSku(), null, $import_log_filename);
            }
            catch (Exception $e){
                Mage::log($e->getMessage() ,null,$import_log_filename);
            }
        }
    }
} else {
    Mage::log('Файл не существует: '.$local_file_csv, null, $import_log_filename);
}

$scriptStopTime = microtime(true);
$totalWorkTime = $scriptStopTime - $scriptStartTime;
Mage::log('STOP Line:'.__LINE__.', work time: '.$totalWorkTime,null,$import_log_filename);


function getAttributeId($option, $type) {
    $attributeId      = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', $type);
    $attribute        = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    $attributeOptions = $attribute ->getSource()->getAllOptions();
    foreach ($attributeOptions as $opts_arr) {
        if ($opts_arr['label'] == $option) {
            return $opts_arr['value'];
        }
    }
    return FALSE;
}

class Slugger {
    static protected $_translit=array(
        'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
        'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
        'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
        'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
        'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
        'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
        'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',

        "а" => 'a',
        "б" => 'b',
        "в" => 'v',
        "г" => 'g',
        "д" => 'd',
        "е" => 'e',
        "ё" => 'yo',
        "ж" => 'zh',
        "з" => "z",
        "и" => 'i',
        "й" => 'y',
        "к" => 'k',
        "л" => 'l',
        "м" => 'm',
        "н" => 'n',
        "о" => 'o',
        "п" => 'p',
        "р" => 'r',
        "с" => 's',
        "т" => 't',
        "у" => 'u',
        "ф" => 'f',
        "х" => 'h',
        "ц" => 'c',
        "ч" => 'ch',
        "ш" => 'sh',
        "щ" => 'th',
        "ъ" => '_',
        "ь" => "_",
        "ы" => 'y',
        "э" => 'e',
        "ю" => 'yu',
        "я" => 'ya',

        "А" => "A",
        "Б" => "B",
        "В" => "V",
        "Г" => "G",
        "Д" => "D",
        "Е" => "E",
        "Ё" => "Yo",
        "Ж" => "Zh",
        "З" => "Z",
        "И" => 'I',
        "Й" => "J",
        "К" => "K",
        "Л" => "L",
        "М" => "M",
        "Н" => "N",
        "О" => "O",
        "П" => "P",
        "Р" => "R",
        "С" => "S",
        "Т" => "T",
        "У" => "U",
        "Ф" => "F",
        "Х" => "H",
        "Ц" => "C",
        "Ч" => "Ch",
        "Ш" => "Sh",
        "Щ" => "Th",
        "Ъ" => "_",
        "Ь" => "_",
        "Ы" => "Y",
        "Э" => "E",
        "Ю" => "Yu",
        "Я" => "Ya"
    );

    public static function stripAccents($text){

        return strtr($text,self::$_translit);
    }

    public static function slug($str,$allowslash=false)
    {
        $str = strtolower(self::stripAccents(trim($str)));
        $rerep=$allowslash?'[^a-z0-9-/]':'[^a-z0-9-]';
        $str = preg_replace("|$rerep|", '-', $str);
        $str = preg_replace('|-+|', "-", $str);
        $str = preg_replace('|-$|', "", $str);
        return $str;
    }
}
