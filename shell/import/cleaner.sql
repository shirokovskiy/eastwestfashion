-- Выставить нули для количества всех продуктов
-- UPDATE cataloginventory_stock_status_idx SET `qty` = 0 WHERE website_id IN (1,4); -- Only Russian Websites
-- UPDATE cataloginventory_stock_status SET `qty` = 0 WHERE website_id IN (1,4); -- Only Russian Websites
-- UPDATE cataloginventory_stock_status SET `stock_status` = 0 WHERE website_id IN (1,4); -- Only Russian Websites
-- UPDATE catalog_product_entity_int SET `value` = 3 WHERE store_id IN (1,4) AND attribute_id = 102; -- Only Russian Websites

-- Удалить проиндексированные продукты из категорий как видимые
-- DELETE FROM catalog_category_product_index WHERE store_id IN (1,4) AND `visibility` = 4;


-- EWF
-- Disable all products in store
UPDATE catalog_product_entity_int SET `value` = 2 WHERE attribute_id = 96 AND `value` = 1; -- Special for EWF: 2 = Disable, 1 - Enable, 96 = status
-- Disable categories
-- UPDATE catalog_category_entity_int SET `value` = 0 WHERE attribute_id = 67 AND `value` = 1 AND entity_id > 2;


-- TOTAL CLEANER
-- TRUNCATE cataloginventory_stock_item;
-- TRUNCATE cataloginventory_stock_status;
-- TRUNCATE cataloginventory_stock_status_idx;
-- TRUNCATE catalogsearch_fulltext;
-- TRUNCATE catalog_category_product;
-- TRUNCATE catalog_category_product_index;
-- TRUNCATE catalog_product_index_price_idx;
-- DELETE FROM catalog_product_entity; 									-- необходимо зайти и удалить нужные записи
-- ALTER TABLE catalog_product_entity AUTO_INCREMENT=1; 				-- затем выставить новый AUTO_INCREMENT
-- TRUNCATE catalog_product_entity_datetime; 							-- таблица очиститься сама, выставить новый AUTO_INCREMENT
-- TRUNCATE catalog_product_entity_decimal; 							-- таблица очиститься сама, выставить новый AUTO_INCREMENT
-- TRUNCATE catalog_product_entity_int; 								-- таблица очиститься сама, выставить новый AUTO_INCREMENT
-- ALTER TABLE catalog_product_entity_media_gallery AUTO_INCREMENT=1;	-- выставить новый AUTO_INCREMENT
-- TRUNCATE catalog_product_entity_text; 								-- таблица очиститься сама, выставить новый AUTO_INCREMENT
-- TRUNCATE catalog_product_entity_varchar; 							-- таблица очиститься сама, выставить новый AUTO_INCREMENT
-- TRUNCATE catalog_product_super_link; 								-- таблица очиститься сама, выставить новый AUTO_INCREMENT
-- TRUNCATE importexport_importdata; 									-- таблица очиститься сама, выставить новый AUTO_INCREMENT. Данная таблица вообще видимо только при импорте нужна.
TRUNCATE core_url_rewrite;
