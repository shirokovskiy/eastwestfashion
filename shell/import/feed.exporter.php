<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 9/15/14
 * Time         : 17:32 PM
 * Description  : to get products from ShopBop through GoldenFeeds service for MAGMI
 * and download remote images
 */
set_time_limit(0);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
umask(0);
ini_set('display_errors', 1);
ini_set('memory_limit', '1024M');
$ilf = 'feed.exporter.'.date('Y-m-d.H-i').'.log';
$skip_images = false; // exclude any info of images if true
$skip_images_download = false; // exclude only download process if true. If image exist locally, it works + info
$skip_images_limit = 3000;
$skip_items_limit = 100; // amount of products to process in developer mode

$scriptStartTime = microtime(true);

require_once dirname(__FILE__) . "/../../app/Mage.php";

// Proper titles
$arrTitles = array(
    'sku'
    , 'product_number'
    , 'product_name'
    , 'brand'
    , 'category'
    , 'colors'
    , 'sizes'
    , 'materials'
    , 'price'               // current price on ShopBop (generally equal to sale_price)
    , 'retail_price'        // first price of item
    , 'sale_price'
    , 'currency'
    , 'delivery_cost'
    , 'delivery_details'
    , 'promotion_details'
    , 'url'
    , 'description'
    , 'keywords'
    , 'gender'
    , 'availability'
    , 'thumbnail_image'
    , 'large_image'
    , 'thumbnail_page_url'
    , 'additional_image1'
    , 'additional_image2'
    , 'additional_image3'
    , 'additional_image4'
    , 'additional_image5'
    , 'condition'
    , 'isbn'
    , 'ean'
    , 'star_rating'
    , 'number_of _reviews'
    , 'country_of_origin'
    , 'dimensions'
    , 'original_url'
    , 'labels'
    , 'url_m'
    , 'url_ru'
    , 'short_product_name'
    , 'upc'
);

$arrDestTitles = array(
    "store"
    , "store_id"
    , "attribute_set"
    , "type"
    , "categories"
    , "sku"
    , "product_number"
    , "product_name"
    , "price"
    , "sale_price"
    , "special_price"
    , "cost"
    , "name"
    , "has_options"
    , "size"
    , "color"
//    , "color_icon"
    , "country_of_origin"
    , "material"
    , "dimensions"
    , "image"
    , "small_image"
    , "thumbnail"
    , "image_label"
    , "small_image_label"
    , "thumbnail_label"
    , "options_container"
    , "url_key"
    , "url_path"
    , "weight"
    , "configurable_attributes"
    , "is_in_stock"
    , "stock_status_changed_automatically"
    , "status"
    , "visibility"
    , "enable_googlecheckout"
    , "tax_class_id"
    , "description"
    , "short_description"
    , "meta_title"
    , "meta_keyword"
    , "meta_description"
    , "media_gallery"
    , "simples_skus"
    , "qty"
    , "manufacturer"
    , "original_url"
    , "delivery_details"
);

/**
 * Store data
 */
$store_id = 1;
$website_ids = array(1);
$store_cat_depends = array(1=>2); // dependencies of store to the root category
$store_default_category_id = $store_cat_depends[$store_id];
$arrExistCategories = array();

$app = Mage::app(); // TODO: JIMMY: Разобраться с UTC timezone
$app->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

if (preg_match("/data/im", __FILE__)) {
    Mage::setIsDeveloperMode(true); // don't unzip, limit by  $skip_items_limit
    $skip_images_download = true;
    Mage::log('=== DEVELOPER MODE ==='."\n".'SKIP IMAGE DOWNLOADS',null,$ilf);
}

/**
 * Currency
 */
$baseCurrencyCode = $app->getBaseCurrencyCode(); // should be RUB
$allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();

/**
 * Get currency data from CBR.ru
 */
$url_daily_currency = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req='.date('d/m/Y');
$currency_xml = simplexml_load_file($url_daily_currency);
$str_RUB_USD = (string) $currency_xml->xpath("//Valute[@ID='R01235']/Value")[0];

if (empty($str_RUB_USD)) {
    Mage::log('Currency is not available at '.$url_daily_currency,null,$ilf);
    // контрольный выстрел
    $url_daily_currency = 'http://www.cbr.ru/scripts/XML_daily.asp';
    $currency_xml = simplexml_load_file($url_daily_currency);
    $str_RUB_USD = (string) $currency_xml->xpath("//Valute[@ID='R01235']/Value")[0];
}

if (empty($str_RUB_USD)) {
    Mage::log('Currency is not available at '.$url_daily_currency,null,$ilf);
    die();
} else {
    $str_RUB_USD = (float) preg_replace('/,/i', '.', $str_RUB_USD);
    $rate_RUB_USD = round( 1/$str_RUB_USD, 7);
    Mage::log('Currency rate = '.$rate_RUB_USD.' from CBR.ru',null,$ilf);
}

/**
 * If no data from CBR.ru
 * get it from shop storage, but it could be old or incorrect
 */
if ($rate_RUB_USD <= 0 || empty($rate_RUB_USD)) {
    Mage::log('Try to get currency data from Magento',null,$ilf);
    $rates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));
    if (isset($rates['USD'])) {
        $rate_RUB_USD = (float) $rates['USD'];
        Mage::log('Currency rate = '.$rate_RUB_USD.' from CMS',null,$ilf);
    }
}

/**
 * Filenames
 */

$fileName = 'custom-feed-commission-junction-shopbop439-ewf-partners-goods-in-csv'; // read it on GoldenFeeds
$fileNameMagmi = 'import_products'; // read it on GoldenFeeds
$local_file_path = BP.DS.'var'.DS.'goldenfeed'.DS; // base folder for archive
$local_file_zip = $local_file_path.$fileName.'.csv.zip'; // comes to this location from GoldenFeeds
$local_file_csv = $local_file_path.$fileName.'.csv';
$local_file_magmi = $local_file_path.$fileNameMagmi.'.csv';
$local_file_zip_bkp = $local_file_csv.'.'.date('YmdHis').'.zip'; // comes to this location from GoldenFeeds
$local_file_md5_path = $local_file_path.$fileName.'.md5'; // этот файл появляется если выгружает GoldenFeeds

Mage::log("START WORK", null, $ilf);

/**
 * Let's prepare zip file, csv file, etc.
 */
if (file_exists($local_file_zip)) {
    if (Mage::getIsDeveloperMode() === false) {
        /**
         * Back-up file
         */
        if (!copy($local_file_zip, $local_file_zip_bkp)) {
            Mage::log("Could not backup zip $local_file_zip", null, $ilf);
        } else {
            Mage::log("File has been backed-up from $local_file_zip to $local_file_zip_bkp", null, $ilf);
            // Unzip file
            $zip = new ZipArchive;
            $res = $zip->open($local_file_zip);
            if ($res === TRUE) {
                $zip->extractTo($local_file_path);
                Mage::log("File has been unziped to $local_file_csv", null, $ilf);
                $zip->close();
            } else {
                Mage::log("Could not unzip $local_file_zip", null, $ilf);
            }
        }
    }

    /**
     * Let's check unzipped file
     */
    if (file_exists($local_file_csv)) {
        /**
         * Work with MD5
         */
        $md5_previous = null;
        if (file_exists($local_file_md5_path)) {
            $md5_previous = file_get_contents($local_file_md5_path);
        }

        $md5_current = md5_file($local_file_csv);

        if ($md5_current == $md5_previous) {
            Mage::log("File had not updated", null, $ilf);
            die();
        } else {
            if (file_put_contents($local_file_md5_path, $md5_current)) {
                Mage::log("MD5 File saved", null, $ilf);
            }
        }
    }
}

$firstRowCount = 0;
$tc = 0;
$firstRowChecked = false;
$arrProductNumbers = array();

if (file_exists($local_file_csv)){
    if (($handle = fopen($local_file_csv, "r")) !== FALSE){
        /**
         * Перед тем как что-то обрабатывать
         * убедимся что файл из которого мы сейчас будем заливать данные
         * хороший (валидный) как минимум по количеству полей и их заголовкам
         */
        if (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
            if (empty($data)) {
                Mage::log('No data in row! '.$tc, null, $ilf);
                die("Error at line:".__LINE__."\n");
            }

            # Lets check titles to be sure we have necessary order of titles
            if (is_array($data)) {
                if (count($data) == count($arrTitles)) {
                    foreach ($arrTitles as $key => $title) {
                        if ($arrTitles[$key] != $data[$key]) {
                            Mage::log('Bad CSV file by columns', null, $ilf);
                            die("Error at line:".__LINE__."\n");
                        }
                    }
                }
            } else {
                Mage::log('Count of title`s row not equal to arrTitles', null, $ilf);
                die;
            }
        }

        $product_qty = 1; // todo: if we get Inventory data from ShopBop, will set it inside WHILE

        $prevPrdNum = null;

        /**
         * Write first row
         */
        $newCSV = fopen($local_file_magmi, 'w');
        fputcsv($newCSV, $arrDestTitles, ',', '"');

        $arrSimpleSKUs = array();
        $conf_product_qty = rand(100,999); // чтобы каждая заливка была идентична (нам всё равно этот параметр пока не важен)

        $image_copied = 0; // how many remote images checked or downloaded

        /**
         * Let's check every row from CSV file
         * and insert data into Magento
         */
        while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
            $tc++;
            $id = $article = $sku = null;
            $arrRowCSV = array();

            $scriptStopTime = microtime(true);
            $totalWorkTime = round($scriptStopTime - $scriptStartTime, 4);
            Mage::log('READ CSV PRODUCT, tc = '.$tc.',time: '. $totalWorkTime,null,$ilf);

            /**
             * Let's be sure we have correct data
             */
            if (empty($data)) {
                Mage::log('No data, row#'.$tc, null, $ilf);
                continue; // let's check next row if exist
            } else
                # Lets check titles to be sure we have necessary order of titles
                if (is_array($data)) {
                    if (count($data) != count($arrTitles)) {
                        Mage::log('BAD data row#'.$tc, null, $ilf);

                        die("\n" . __FILE__ . ":" . __LINE__ . "\n\n");

                        continue; // let's check next row if exist
                    }
                } else {
                    Mage::log('DATA-row doesn`t has cells', null, $ilf);
                    continue;
                }

            $arrRowCSV[/* store */] = "admin";
            $arrRowCSV[/* store_id */] = $store_id;
            $arrRowCSV[/* attribute_set */] = "Default";
            $arrRowCSV[/* type */] = "simple";

            $category_path = (string) $data[array_search('category', $arrTitles)];
            $category_path = str_replace("/","\/", $category_path);

            if (preg_match('/>/', $category_path)) {
                // categories tree
                $category_path = explode('>',$category_path);
                if (is_array($category_path) && !empty($category_path)) {
                    $category_path = array_map('trim',$category_path);
                    $category_path = implode('/',$category_path);
                }
            }

            $flag_sale = false;

            // Проверим, не Навинка ли?
            $labels = (string) $data[array_search('labels', $arrTitles)];
            if (preg_match("/НОВИНКИ/im", $labels)) {
                if (!empty($category_path)) {
                    $category_path .= ';;Новое';
                } else {
                    $category_path = 'Новое';
                }
            } elseif (preg_match("/Распродажа/im", $labels)) {
                if (!empty($category_path)) {
                    $category_path .= ';;Распродажа';
                } else {
                    $category_path = 'Распродажа';
                }
                $flag_sale = true;
            }

            $arrRowCSV[/* categories */] = $category_path;

            $sku = (string) $data[array_search('sku', $arrTitles)];
            $arrRowCSV[/* sku */] = $arrSimpleSKUs[] = $sku;

            Mage::log('SKU '.$sku,null,$ilf);

            $product_number = (string) $data[array_search('product_number', $arrTitles)];
            if ($tc == 1) $prevPrdNum = $product_number;
            $arrRowCSV[/* product_number */] = $product_number;

            $product_name = (string) $data[array_search('product_name', $arrTitles)];
            if (empty($product_name)) {
                Mage::log("No Product Name for $tc", null, $ilf);
                continue; // we can't proceed without this data
            }
            $arrRowCSV[/* product_name */] = $product_name;

            $price = $cost = (float) $data[array_search('retail_price', $arrTitles)];

            if (empty($price) || floatval($price) > 1000) {  // Exception from 29.09.2014
                Mage::log("Product Price not allowed for $sku - $product_name - $price", null, $ilf);
                continue; // we can't proceed without this data
            }

            $sale_price = (float) $data[array_search('sale_price', $arrTitles)];
//            $delivery_details = (string) $data[array_search('delivery_details', $arrTitles)];
            $delivery_details = ''; // reset

            /**
             * Вычисления разницы в цене
             * Если разница больше или равна 70%
             * такой товар не возвратный в США
             */
            $special_price = '';

            if ($sale_price > 0 && $price > 0) {
                $percent_sale = (100 - ($sale_price * 100/$price));

                if ($percent_sale >= 70) {
                    $delivery_details = "Этот товар возврату не подлежит!";
                    $special_price = $price * 0.6;
                    $special_price = convertUSDtoRUB($special_price);

                    if ($special_price > 50000) {
                        Mage::log('Special price more then 50K not allowed',null,$ilf);
                        continue;
                    }
                }
            }

            $price = convertUSDtoRUB($price);
            if ($price > 50000) {
                Mage::log('Final price more then 50K not allowed',null,$ilf);
                continue;
            }

            $sale_price = convertUSDtoRUB($sale_price);

            $arrRowCSV[/* price */] = sprintf("%0.2f", floatval($price));
            $arrRowCSV[/* sale_price */] = sprintf("%0.2f", floatval($sale_price));
            $arrRowCSV[/* special_price */] = $special_price > 0 ? sprintf("%0.2f", floatval($special_price)) : '';
            $arrRowCSV[/* cost */] = $cost; // Цена в USD
            $arrRowCSV[/* name */] = $product_name;
            $arrRowCSV[/* has_options */] = 0; // only for Simple

            $size = (string) $data[array_search('sizes', $arrTitles)];

            /**
             * Some sizes from ShopBop is inverted
             * i.e.:  37,5 => 5,37
             */
            if (preg_match('/5,[0-9]{1,2}/i',$size)) {
                $size = preg_replace('/5,([0-9]{1,2})/i', '$1,5', $size);
            }

            $color = (string) $data[array_search('colors', $arrTitles)];

            $arrRowCSV[/* size */] = $size;
            $arrRowCSV[/* color */] = $color;

            $country_of_origin = (string) $data[array_search('country_of_origin', $arrTitles)];

            if (preg_match("/Китая/i", $country_of_origin)) {
                $country_of_origin = "Китай";
            } elseif (preg_match("/Австрали/i", $country_of_origin)) {
                $country_of_origin = "Австралия";
            } elseif (preg_match("/Австрии/i", $country_of_origin)) {
                $country_of_origin = "Австрия";
            } elseif (preg_match("/Албани/i", $country_of_origin)) {
                $country_of_origin = "Албания";
            } elseif (preg_match("/Англии/i", $country_of_origin)) {
                $country_of_origin = "Англия";
            } elseif (preg_match("/Андорре/i", $country_of_origin)) {
                $country_of_origin = "Андорра";
            } elseif (preg_match("/Бали/i", $country_of_origin)) {
                $country_of_origin = "Бали";
            } elseif (preg_match("/Бразили/i", $country_of_origin)) {
                $country_of_origin = "Бразилия";
            } elseif (preg_match("/Бангладеш/i", $country_of_origin)) {
                $country_of_origin = "Бангладеш";
            } elseif (preg_match("/Боливии/i", $country_of_origin)) {
                $country_of_origin = "Боливия";
            } elseif (preg_match("/Бельгии/i", $country_of_origin)) {
                $country_of_origin = "Бельгия";
            } elseif (preg_match("/Великобритан/i", $country_of_origin)) {
                $country_of_origin = "Великобритания";
            } elseif (preg_match("/Вьетнам/i", $country_of_origin)) {
                $country_of_origin = "Вьетнам";
            } elseif (preg_match("/Гонконга/i", $country_of_origin)) {
                $country_of_origin = "Гонконг";
            } elseif (preg_match("/Гватемал/i", $country_of_origin)) {
                $country_of_origin = "Гватемала";
            } elseif (preg_match("/Греции/i", $country_of_origin)) {
                $country_of_origin = "Греция";
            } elseif (preg_match("/Гондурас/i", $country_of_origin)) {
                $country_of_origin = "Гондурас";
            } elseif (preg_match("/Египт/i", $country_of_origin)) {
                $country_of_origin = "Египет";
            } elseif (preg_match("/Индии/i", $country_of_origin)) {
                $country_of_origin = "Индия";
            } elseif (preg_match("/Испании/i", $country_of_origin)) {
                $country_of_origin = "Испания";
            } elseif (preg_match("/Итали/i", $country_of_origin)) {
                $country_of_origin = "Италия";
            } elseif (preg_match("/Иордани/i", $country_of_origin)) {
                $country_of_origin = "Иордания";
            } elseif (preg_match("/Канад/i", $country_of_origin)) {
                $country_of_origin = "Канада";
            } elseif (preg_match("/Кореи/i", $country_of_origin)) {
                $country_of_origin = "Корея";
            } elseif (preg_match("/Литв/i", $country_of_origin)) {
                $country_of_origin = "Литва";
            } elseif (preg_match("/Латвии/i", $country_of_origin)) {
                $country_of_origin = "Латвия";
            } elseif (preg_match("/Мехико/i", $country_of_origin)) {
                $country_of_origin = "Мехико";
            } elseif (preg_match("/Маврики/i", $country_of_origin)) {
                $country_of_origin = "Маврикия";
            } elseif (preg_match("/Молдовы/i", $country_of_origin)) {
                $country_of_origin = "Молдова";
            } elseif (preg_match("/Мексик/i", $country_of_origin)) {
                $country_of_origin = "Мексика";
            } elseif (preg_match("/Макао/i", $country_of_origin)) {
                $country_of_origin = "Макао";
            } elseif (preg_match("/Марокко/i", $country_of_origin)) {
                $country_of_origin = "Марокко";
            } elseif (preg_match("/Мадагаскар/i", $country_of_origin)) {
                $country_of_origin = "Мадагаскар";
            } elseif (preg_match("/Малайзии/i", $country_of_origin)) {
                $country_of_origin = "Малайзия";
            } elseif (preg_match("/Непала/i", $country_of_origin)) {
                $country_of_origin = "Непал";
            } elseif (preg_match("/ОАЭ/i", $country_of_origin)) {
                $country_of_origin = "ОАЭ";
            } elseif (preg_match("/Пакистана/i", $country_of_origin)) {
                $country_of_origin = "Пакистан";
            } elseif (preg_match("/Португали/i", $country_of_origin)) {
                $country_of_origin = "Португалия";
            } elseif (preg_match("/Польши/i", $country_of_origin)) {
                $country_of_origin = "Польша";
            } elseif (preg_match("/Румыни/i", $country_of_origin)) {
                $country_of_origin = "Румыния";
            } elseif (preg_match("/США/i", $country_of_origin) || preg_match("/Нью-Йорке/i", $country_of_origin) || preg_match("/Лос-Анджелесе/i", $country_of_origin)) {
                $country_of_origin = "США";
            } elseif (preg_match("/Словени/i", $country_of_origin)) {
                $country_of_origin = "Словения";
            } elseif (preg_match("/Словакии/i", $country_of_origin)) {
                $country_of_origin = "Словакия";
            } elseif (preg_match("/Сингапур/i", $country_of_origin)) {
                $country_of_origin = "Сингапур";
            } elseif (preg_match("/Таиланд/i", $country_of_origin) || preg_match("/Тайланд/i", $country_of_origin)) {
                $country_of_origin = "Тайланд";
            } elseif (preg_match("/Тайван/i", $country_of_origin)) {
                $country_of_origin = "Тайвань";
            } elseif (preg_match("/Турци/i", $country_of_origin)) {
                $country_of_origin = "Турция";
            } elseif (preg_match("/Чили/i", $country_of_origin)) {
                $country_of_origin = "Чили";
            } elseif (preg_match("/Уругва/i", $country_of_origin)) {
                $country_of_origin = "Уругвай";
            } elseif (preg_match("/Финляндии/i", $country_of_origin)) {
                $country_of_origin = "Финляндия";
            } elseif (preg_match("/Хорватии/i", $country_of_origin)) {
                $country_of_origin = "Хорватия";
            } elseif (preg_match("/Швеции/i", $country_of_origin)) {
                $country_of_origin = "Швеция";
            } elseif (preg_match("/Эквадор/i", $country_of_origin)) {
                $country_of_origin = "Эквадор";
            } elseif (preg_match("/Япони/i", $country_of_origin)) {
                $country_of_origin = "Япония";
            }

            $arrRowCSV[/* country_of_origin */] = $country_of_origin;
            $arrRowCSV[/* material */] = (string) $data[array_search('materials', $arrTitles)];
            $arrRowCSV[/* dimensions */] = (string) $data[array_search('dimensions', $arrTitles)];

            /**
             * Let's prepare description BEFORE images
             * do not spend time if skip
             */
            $short_product_desc = (string) $data[array_search('short_product_name', $arrTitles)];
            $description = (string) $data[array_search('description', $arrTitles)];

            if (empty($description)) {
                if (!empty($short_product_desc)) $description = $short_product_desc;
                else $description = $product_name;
            }

            // ЭТОТ ТОВАР НЕ МОЖЕТ БЫТЬ ДОСТАВЛЕН ЗА ПРЕДЕЛЫ США
            if (preg_match('/ЭТОТ ТОВАР НЕ МОЖЕТ БЫТЬ ДОСТАВЛЕН ЗА ПРЕДЕЛЫ США/i', $description)) {
                Mage::log("No Shipping from USA",null,$ilf);
                continue;
            }

            $large_image_url = (string) $data[array_search('large_image', $arrTitles)];
            $fileColor = strtolower( str_replace(' ','_',Slugger::slug($color)));
            $pathLargeImg = BP. DS .'media'.DS.'import'.DS.'shopbop'.DS.'LI_'.$product_number.'.'.$fileColor.'.imported.jpg';
            $arrMediaGallery = array();

            if ($skip_images != true) { // не пропускаем учёт и запись картинок в итоговый файл
                if (file_exists($pathLargeImg)) {
                    # Проверим а стоит ли его перезаписывать
                    Mage::log('Image exist',null,$ilf);
                    if (!touch($pathLargeImg)) {
                        Mage::log('Couldn`t change file timestamp',null,$ilf);
                    }
//                                $md5_remote_image = md5_file($image_url);
//                                $md5_local_image = md5_file($pathLargeImg);

//                                if ($md5_remote_image !== false && $md5_remote_image != $md5_local_image) {
//                                    if (copy($image_url, $pathLargeImg)) {
//                                        Mage::log('Image is rewrited OK. Path:'.$pathLargeImg, null, $ilf);
//                                    }
//                                }
                } else {
                    if (!$skip_images_download && $image_copied <= $skip_images_limit) {
                        Mage::log('large_image = '.$large_image_url,null,$ilf);

                        if (!empty($large_image_url) && strcmp('.jpg', substr($large_image_url, -4))===0) {
                            try {
                                if (copy($large_image_url, $pathLargeImg)) {
                                    Mage::log('Image is copied OK. Path:'.$pathLargeImg, null, $ilf);
                                    $image_copied++;
                                }
                            } catch (Exception $ex) {
                                Mage::log($ex->getMessage(), null, $ilf);
                                Mage::log("ERROR: Maybe bad Internet connection", null, $ilf);
                            }
                        } else {
                            Mage::log('Large image URL doesn`t looks like IMAGE`s URL : '.$large_image_url, null, $ilf);
                        }
                    }
                }
            }

            if (file_exists($pathLargeImg)) {
                $arrRowCSV[/* image */] = '/LI_'.$product_number.'.'.$fileColor.'.imported.jpg';
                $arrRowCSV[/* small_image */] = '/LI_'.$product_number.'.'.$fileColor.'.imported.jpg';
                $arrRowCSV[/* thumbnail */] = '/LI_'.$product_number.'.'.$fileColor.'.imported.jpg';

                $arrMediaGallery[] = '/LI_'.$product_number.'.'.$fileColor.'.imported.jpg::'.$product_name;
            } else {
                $arrRowCSV[/* image */] = '';
                $arrRowCSV[/* small_image */] = '';
                $arrRowCSV[/* thumbnail */] = '';
            }

            $arrRowCSV[/* image_label */] = $product_name;
            $arrRowCSV[/* small_image_label */] = $product_name;
            $arrRowCSV[/* thumbnail_label */] = $product_name;
            $arrRowCSV[/* options_container */] = "";

            $url_key = strtolower( str_replace(' ','_',Slugger::slug($product_name)));

            $arrRowCSV[/* url_key */] = $url_key;
            $arrRowCSV[/* url_path */] = $url_key.'.html';
            $arrRowCSV[/* weight */] = 1;
            $arrRowCSV[/* configurable_attributes */] = ""; // only for Simple
            $arrRowCSV[/* is_in_stock */] = 1;
            $arrRowCSV[/* stock_status_changed_automatically */] = 1;
            $arrRowCSV[/* status */] = "Enabled";
            $arrRowCSV[/* visibility */] = "Not Visible Individually"; // only for Simple - BUT!!!! Use plugin: Generic mapper for using accociations: "Not Visible Individually",1 "Catalog",2 "Search",3 "Catalog, Search",4. Or use digits instead
            $arrRowCSV[/* enable_googlecheckout */] = "No";
            $arrRowCSV[/* tax_class_id */] = "None";



            $description = preg_replace('/ShopBop/im', 'EastWestFashion', $description);
            $arrRowCSV[/* description */] = $description;

            if (empty($short_product_desc)) {
                $short_product_desc = '&nbsp;';
            } else {
                $short_product_desc = preg_replace('/ShopBop/im', 'EastWestFashion', $short_product_desc);
            }

            $arrRowCSV[/* short_description */] = $short_product_desc;
            $arrRowCSV[/* meta_title */] = 'Купить '.$product_name.' в интернет магазине EastWestFashion';

            $keywords = (string) $data[array_search('keywords', $arrTitles)];

            $arrRowCSV[/* meta_keyword */] = $keywords;
            $arrRowCSV[/* meta_description */] = "Купить $product_name. Оплата при получении. 100% настоящие европейские бренды. ".$description;

            /**
             * To download image for product
             */
            if ($skip_images != true) {
                $scriptStopTime = microtime(true);
                $totalWorkTime = round($scriptStopTime - $scriptStartTime, 4);
                Mage::log('Start get gallery IMAGES,time: '. $totalWorkTime,null,$ilf);

                for ($i = 1; $i <=5; $i++) {
                    if (!empty($large_image_url) && $i == 1) continue; // первая картинка из галереи и основная - обычно одинаковы.

                    $imgLocalPath = Mage::getBaseDir('media').DS.'import'.DS.'shopbop'.DS.$sku.'.'.$i.'.imported.jpg';

                    if (file_exists($imgLocalPath)) {
                        # Проверим а стоит ли его перезаписывать
                        Mage::log('Image exist locally '.$imgLocalPath,null,$ilf);
                        $arrMediaGallery[] = '/'.$sku.'.'.$i.'.imported.jpg::'.$product_name;
                        if (!touch($imgLocalPath)) {
                            Mage::log('Couldn`t change file timestamp',null,$ilf);
                        }

//                                $md5_remote_image = md5_file($image_url);
//                                $md5_local_image = md5_file($imgLocalPath);

//                                if ($md5_remote_image !== false && $md5_remote_image != $md5_local_image) {
//                                    if (copy($image_url, $imgLocalPath)) {
//                                        Mage::log('Image is rewrited OK. Path:'.$imgLocalPath, null, $ilf);
//                                    }
//                                }
                    } else {
                        $imgSize = null;

                        $image_url = (string) $data[array_search('additional_image'.$i , $arrTitles)];

                        if (!empty($image_url)) {
                            if (!$skip_images_download && $image_copied <= $skip_images_limit) {
                                try {
                                    Mage::log('Copy remote image', null, $ilf);

                                    if (copy($image_url, $imgLocalPath)) {
                                        Mage::log('Image has been copied OK. Path:'.$imgLocalPath, null, $ilf);
                                        $arrMediaGallery[] = '/'.$sku.'.'.$i.'.imported.jpg::'.$product_name;
                                        $image_copied++;
                                    }
                                } catch (Exception $ex) {
                                    Mage::log($ex->getMessage(), null, $ilf);
                                    Mage::log("Maybe bad Internet connection", null, $ilf);
                                }
                            }
                        } else {
                            Mage::log("> No URL image#$i sku ".$sku, null, $ilf);
                        }
                    }
                }

                $scriptStopTime = microtime(true);
                $totalWorkTime = round($scriptStopTime - $scriptStartTime, 4);
                Mage::log('Stop getting IMAGES,time: '. $totalWorkTime,null,$ilf);
            }
            // *** The end of image import

            $arrRowCSV[/* media_gallery */] = is_array($arrMediaGallery) ? implode(';', $arrMediaGallery) : '';
            $arrRowCSV[/* simples_skus */] = ""; // only for Configurable
            $arrRowCSV[/* qty */] = 1;
            $arrRowCSV[/* manufacturer */] = (string) $data[array_search('brand', $arrTitles)];
            $arrRowCSV[/* original_url */] = (string) $data[array_search('original_url', $arrTitles)];
            $arrRowCSV[/* delivery_details */] = $delivery_details;

            /**
             *
             *      CONFIGURABLE addition
             *      =====================
             */
            if (is_array($data_prev) && !empty($data_prev)) {
                if ($arrRowCSV[array_search('product_number', $arrDestTitles)] != $data_prev[array_search('product_number', $arrDestTitles)]) {
                    /**
                     * Записать $data_prev как Configurable
                     */
                    $arrConfCSV = array();
                    $bkp_sku = array_pop($arrSimpleSKUs);

                    /**
                     * Write data into file about Configurable product
                     */
                    $arrConfCSV[/* store */] = "admin";
                    $arrConfCSV[/* store_id */] = $store_id;
                    $arrConfCSV[/* attribute_set */] = "Default";
                    $arrConfCSV[/* type */] = "configurable";
                    $arrConfCSV[/* categories */] = $data_prev[array_search('categories', $arrDestTitles)];
                    $arrConfCSV[/* sku */] = 'CP-'.$data_prev[array_search('product_number', $arrDestTitles)];
                    $arrConfCSV[/* product_number */] = $data_prev[array_search('product_number', $arrDestTitles)];
                    $arrConfCSV[/* product_name */] = $data_prev[array_search('product_name', $arrDestTitles)];
                    $arrConfCSV[/* price */] = sprintf("%0.2f", floatval($data_prev[array_search('price', $arrDestTitles)]));
                    $arrConfCSV[/* sale_price */] = sprintf("%0.2f", floatval($data_prev[array_search('sale_price', $arrDestTitles)]));
                    $arrConfCSV[/* special_price */] = $data_prev[array_search('special_price', $arrDestTitles)] > 0 ? sprintf("%0.2f", floatval($data_prev[array_search('special_price', $arrDestTitles)])) : '';
                    $arrConfCSV[/* cost */] = $data_prev[array_search('cost', $arrDestTitles)];
                    $arrConfCSV[/* name */] = $data_prev[array_search('product_name', $arrDestTitles)];
                    $arrConfCSV[/* has_options */] = 1; // only for Configurable
                    $arrConfCSV[/* size */] = '';
                    $arrConfCSV[/* color */] = $data_prev[array_search('color', $arrDestTitles)];
                    $arrConfCSV[/* country_of_origin */] = $data_prev[array_search('country_of_origin', $arrDestTitles)];
                    $arrConfCSV[/* material */] = $data_prev[array_search('material', $arrDestTitles)];
                    $arrConfCSV[/* dimensions */] = $data_prev[array_search('dimensions', $arrDestTitles)];

                    if (file_exists($pathLargeImg)) {
                        $arrConfCSV[/* image */] = '/LI_'.$data_prev[array_search('product_number', $arrDestTitles)].'.'.$fileColor.'.imported.jpg';
                        $arrConfCSV[/* small_image */] = '/LI_'.$data_prev[array_search('product_number', $arrDestTitles)].'.'.$fileColor.'.imported.jpg';
                        $arrConfCSV[/* thumbnail */] = '/LI_'.$data_prev[array_search('product_number', $arrDestTitles)].'.'.$fileColor.'.imported.jpg';
                    } else {
                        $arrConfCSV[/* image */] = '';
                        $arrConfCSV[/* small_image */] = '';
                        $arrConfCSV[/* thumbnail */] = '';
                    }

                    $arrConfCSV[/* image_label */] = $data_prev[array_search('image_label', $arrDestTitles)];
                    $arrConfCSV[/* small_image_label */] = $data_prev[array_search('small_image_label', $arrDestTitles)];
                    $arrConfCSV[/* thumbnail_label */] = $data_prev[array_search('thumbnail_label', $arrDestTitles)];
                    $arrConfCSV[/* options_container */] = "container1";
                    $arrConfCSV[/* url_key */] = $data_prev[array_search('url_key', $arrDestTitles)];
                    $arrConfCSV[/* url_path */] = $data_prev[array_search('url_path', $arrDestTitles)];
                    $arrConfCSV[/* weight */] = "";
                    $arrConfCSV[/* configurable_attributes */] = "size"; // only for Configurable
                    $arrConfCSV[/* is_in_stock */] = 1;
                    $arrConfCSV[/* stock_status_changed_automatically */] = 1;
                    $arrConfCSV[/* status */] = "Enabled";
                    $arrConfCSV[/* visibility */] = "Catalog, Search"; // only for Configurable
                    $arrConfCSV[/* enable_googlecheckout */] = "No";
                    $arrConfCSV[/* tax_class_id */] = "None";
                    $arrConfCSV[/* description */] = $data_prev[array_search('description', $arrDestTitles)];
                    $arrConfCSV[/* short_description */] = $data_prev[array_search('short_description', $arrDestTitles)];
                    $arrConfCSV[/* meta_title */] = $data_prev[array_search('meta_title', $arrDestTitles)];
                    $arrConfCSV[/* meta_keyword */] = $data_prev[array_search('meta_keyword', $arrDestTitles)];
                    $arrConfCSV[/* meta_description */] = $data_prev[array_search('meta_description', $arrDestTitles)];
                    $arrConfCSV[/* media_gallery */] = $data_prev[array_search('media_gallery', $arrDestTitles)];
                    $arrConfCSV[/* simples_skus */] = is_array($arrSimpleSKUs) ? implode(',', $arrSimpleSKUs) : ''; // only for Configurable
                    $arrConfCSV[/* qty */] = $conf_product_qty;
                    $arrConfCSV[/* manufacturer */] = $data_prev[array_search('manufacturer', $arrDestTitles)];
                    $arrConfCSV[/* original_url */] = '';
                    $arrConfCSV[/* delivery_details */] = $data_prev[array_search('delivery_details', $arrDestTitles)];

                    fputcsv($newCSV, $arrConfCSV);

                    $arrSimpleSKUs = array($bkp_sku);
                }
            }

            fputcsv($newCSV, $arrRowCSV);

            $data_prev = $arrRowCSV;

            // Lets parse and insert data into Products
            $scriptStopTime = microtime(true);
            $totalWorkTime = round($scriptStopTime - $scriptStartTime, 4);
            Mage::log('The End of loop. Time: '. $totalWorkTime,null,$ilf);

            if (Mage::getIsDeveloperMode() === true && $skip_items_limit > 0) {
                if ($tc >= $skip_items_limit) {
                    break;
                }
            }

            if ($image_copied == $skip_images_limit) {
                Mage::log('~ Images limit to download reached ~ '.$image_copied,null,$ilf);
                $image_copied++; // to exclude second time log
            }
        }

        fclose($newCSV);

        $scriptStopTime = microtime(true);
        $totalWorkTime = round($scriptStopTime - $scriptStartTime, 4);
        Mage::log('Work time: '. $totalWorkTime,null,$ilf);
    }

    fclose($handle);
} else {
    Mage::log('Файл не существует: '.$local_file_csv, null, $ilf);
}

$scriptStopTime = microtime(true);
$totalWorkTime = round($scriptStopTime - $scriptStartTime, 4);
Mage::log('++++++++++++++++ STOP WORK,time: '. $totalWorkTime,null,$ilf);

class Slugger {
    static protected $_translit=array(
        'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
        'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
        'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
        'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
        'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
        'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
        'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',

        "а" => 'a', "б" => 'b', "в" => 'v', "г" => 'g', "д" => 'd', "е" => 'e', "ё" => 'yo', "ж" => 'zh', "з" => "z",
        "и" => 'i', "й" => 'y', "к" => 'k', "л" => 'l', "м" => 'm', "н" => 'n', "о" => 'o', "п" => 'p', "р" => 'r',
        "с" => 's', "т" => 't', "у" => 'u', "ф" => 'f', "х" => 'h', "ц" => 'c', "ч" => 'ch', "ш" => 'sh', "щ" => 'th',
        "ъ" => '_', "ь" => "_", "ы" => 'y', "э" => 'e', "ю" => 'yu',"я" => 'ya',

        "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D", "Е" => "E", "Ё" => "Yo", "Ж" => "Zh", "З" => "Z",
        "И" => 'I', "Й" => "J", "К" => "K", "Л" => "L", "М" => "M", "Н" => "N", "О" => "O", "П" => "P", "Р" => "R",
        "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F", "Х" => "H", "Ц" => "C", "Ч" => "Ch", "Ш" => "Sh", "Щ" => "Th",
        "Ъ" => "_", "Ь" => "_", "Ы" => "Y", "Э" => "E", "Ю" => "Yu","Я" => "Ya"
    );

    public static function stripAccents($text){

        return strtr($text,self::$_translit);
    }

    public static function slug($str,$allowslash=false)
    {
        $str = strtolower(self::stripAccents(trim($str)));
        $rerep=$allowslash?'[^a-z0-9-/]':'[^a-z0-9-]';
        $str = preg_replace("|$rerep|", '-', $str);
        $str = preg_replace('|-+|', "-", $str);
        $str = preg_replace('|-$|', "", $str);
        return $str;
    }
}


function stream_copy($src, $dest) {
    $fsrc = fopen($src,'r');
    $fdest = fopen($dest,'w+');
    $len = stream_copy_to_stream($fsrc,$fdest);
    fclose($fsrc);
    fclose($fdest);
    return $len;
}

function convertUSDtoRUB($price) {
    global $rate_RUB_USD;
    if ($price <= 0) return ''; // be aware: empty string, not digit
    $final_price = floor((($price / ($rate_RUB_USD / 1.05)) * 1.30) * 1.015); // вычисления + округление по копейкам
    $final_price = round($final_price, -1); // огругление по рублям (десятки)
    return $final_price;
}
