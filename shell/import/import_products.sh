#!/bin/bash

# EWF bash
#
# This file will start MAGMI import process
# mode=...
# update : will skip non existing skus & update existing ones
# create : will create new products for non exisiting skus, will update existing ones
# xcreate : will create new products for non exisiting skus, will skip existing ones
### SOURCE="${BASH_SOURCE[0]}"
### BDIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
### DR=$BDIR/htroot
date
DR=/var/www/data/eastwestfashion
if [ ! -d $DR ]; then
    DR=/var/www/eastwestfashion
fi

MSHELL=$DR/shell/import
FILE=import_products.csv
FILEPATH=$DR/var/goldenfeed
HOLDONMAGENTO=$DR/maintenance.flag

echo "DOCUMENT_ROOT : $DR"
cd $DR

date
echo "Start prepare remote products"
php ./shell/import/feed.exporter.php
find ./media/catalog/product/ -type f -size 0 -print0 | xargs -0 rm

if [ -f $FILEPATH/$FILE ];
then
    date
    echo "Turn OFF web-site"
    touch $HOLDONMAGENTO

    mysql --defaults-extra-file="$DR/var/backups/mydump.cnf" eastwestfashion < $MSHELL/cleaner.sql

    echo "Start import products"
    php ./magmi/cli/magmi.cli.php -profile=EWF -mode=create
    date

    mv -f $FILEPATH/$FILE $FILEPATH/import_products.$(date +%Y-%m-%d__%H.%M).csv
    cp -f $DR/magmi/state/progress.txt $DR/var/log/magmi.progress.$(date +%Y-%m-%d__%H.%M).txt

    echo "Clear cache"
	rm -rf var/cache/*
else
    echo "No "$FILE" on time "$(date "+%F %H:%M")
fi

if [ -f $HOLDONMAGENTO ];
then
	echo "Turn ON web-site"
    rm -f $HOLDONMAGENTO
fi

echo "The End"
date
