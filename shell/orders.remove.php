<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 10/3/14
 * Time         : 10:44 AM
 * Description  : remove some orders
 * $ php -f ./orders.remove.php 100000015
 */
set_time_limit(0);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
umask(0);
ini_set('memory_limit', '1024M');

$logfile = 'remove.old.orders.'.date('Y.m.d').'.log';
$scriptStartTime = microtime(true);
$store_id = 1;
$website_ids = array(1);
$store_cat_depends = array(1=>2); // dependencies of store to the root category
$store_default_category_id = $store_cat_depends[$store_id];
$arrExistCategories = array();
$skip_images = false;

require_once dirname(__FILE__)."/../app/Mage.php";
require_once BP.DS.'app/code/local/Df/Core/lib/fp/other.php';
require_once BP.DS.'app/code/local/Df/Core/lib/fp/validation.php';
require_once BP.DS.'app/code/local/Df/Core/lib/fp/array.php';
require_once BP.DS.'app/code/local/Df/Core/lib/fp/text.php';
$app = Mage::app('admin');
$app->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID)->setUseSessionInUrl(false);

if (preg_match("/htroot/im", __FILE__)) {
    Mage::setIsDeveloperMode(true);
    ini_set('display_errors', 1);
}

$order_id = null; // id of order to delete by default

if (isset($argv[1]) && floatval($argv[1]) > 0) {
    $order_id = (int)$argv[1];
}

if (!is_null($order_id)) {
    $test_order_ids=array(
        $order_id
    );
} else {
    //replace your own orders numbers here:
    /*$test_order_ids=array(
        '100000003'
        , '100000004'
    );*/

    // fill automatically
    for ($o = 1; $o <= 74; $o++) {
        $test_order_ids[] = '1'.sprintf('%08d', $o);
    }

    $test_order_ids[] = '100000003-1';
    $test_order_ids[] = '100000014-1';
    $test_order_ids[] = '100000017-1';
    $test_order_ids[] = '100000030-1';
    $test_order_ids[] = '100000031-1';
    $test_order_ids[] = '100000031-2';
    $test_order_ids[] = '100000032-1';
    $test_order_ids[] = '100000041-1';
    $test_order_ids[] = '100000045-1';
    $test_order_ids[] = '100000048-1';
    $test_order_ids[] = '100000051-1';
    $test_order_ids[] = '100000062-1';
    $test_order_ids[] = '100000072-1';
}

foreach($test_order_ids as $id){
    try{
        Mage::getModel('sales/order')->loadByIncrementId($id)->delete();
        Mage::log("order #".$id." is removed",null,$logfile);
    }catch(Exception $e){
        Mage::log("order #".$id." could not be removed: ".$e->getMessage(),null,$logfile);
    }
}
echo "\nComplete.\n\n";
