#!/bin/bash

# EWF bash
#
# 
### SOURCE="${BASH_SOURCE[0]}"
### BDIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
### DR=$BDIR/htroot

DR=/var/www/data/eastwestfashion/htroot
if [ ! -d $DR ]; then
    DR=/var/www/eastwestfashion
fi

date
echo "DOCUMENT_ROOT : $DR"
cd $DR
echo "Clear cache"
rm -rf var/cache/*
echo "Clear sessions"
rm -rf var/sessions/*
echo "Clear logs"
rm -rf var/log/*
echo "The End"
date