#!/bin/bash

DB_NAME=akeneo

SOURCE="${BASH_SOURCE[0]}"
cd -P "$( dirname "$SOURCE" )"
THIS_SCRIPT_DIR="$(pwd)"
DUMP_DIR=$THIS_SCRIPT_DIR/db
now="$(date +'%Y_%m_%d')"
logfile=$THIS_SCRIPT_DIR"/log/log_"$now".txt"
FN="db_"$DB_NAME"_"$now
SQL_FILE=$DUMP_DIR/$FN.sql
cd $THIS_SCRIPT_DIR
echo "mysqldump of $DB_NAME started at $(date +'%d-%m-%Y %H:%M:%S')" >> $logfile
mysqldump --defaults-extra-file=dump.$DB_NAME.cnf --single-transaction --opt $DB_NAME | 7z a -si $SQL_FILE.7z
echo "mysqldump of $DB_NAME finished at $(date +'%d-%m-%Y %H:%M:%S')" >> $logfile

find $DUMP_DIR -name "db_"$DB_NAME"*" -mtime +8 -exec rm {} \;
echo "Old files deleted at $(date +'%d-%m-%Y %H:%M:%S')" >> $logfile
echo "*****************" >> $logfile

