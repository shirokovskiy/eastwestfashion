if (rm.defined(window.RegionUpdater)) {
	RegionUpdater.prototype.update =
		function () {
			if (this.regions[this.countryEl.value]) {
	//            if (!this.regionSelectEl) {
	//                Element.insert(this.regionTextEl, {after : this.tpl.evaluate(this._regionSelectEl)});
	//                this.regionSelectEl = $(this._regionSelectEl.id);
	//            }
				if (this.lastCountryId!=this.countryEl.value) {
					var i, option, region, def;

					if (this.regionTextEl) {
						def = this.regionTextEl.value.toLowerCase();
						this.regionTextEl.value = '';
					}
					if (!def) {
						def = this.regionSelectEl.getAttribute('defaultValue');
					}

					this.regionSelectEl.options.length = 1;
					for (regionId in this.regions[this.countryEl.value]) {
						region = this.regions[this.countryEl.value][regionId];


						/**
						 * BEGIN PATCH
						 */
						regionId = region.id;

						if (rm.undefined(regionId)) {
							continue;
						}
						/**
						 * END PATCH
						 */

						option = document.createElement('OPTION');
						option.value = regionId;
						option.text = region.name;

						if (this.regionSelectEl.options.add) {
							this.regionSelectEl.options.add(option);
						} else {
							this.regionSelectEl.appendChild(option);
						}

						if (regionId==def || region.name.toLowerCase()==def || region.code.toLowerCase()==def) {
							this.regionSelectEl.value = regionId;
						}
					}
				}

				if (this.disableAction=='hide') {
					if (this.regionTextEl) {
						this.regionTextEl.style.display = 'none';
						this.regionTextEl.style.disabled = true;
					}
					this.regionSelectEl.style.display = '';
					this.regionSelectEl.disabled = false;
				} else if (this.disableAction=='disable') {
					if (this.regionTextEl) {
						this.regionTextEl.disabled = true;
					}
					this.regionSelectEl.disabled = false;
				}
				this.setMarkDisplay(this.regionSelectEl, true);

				this.lastCountryId = this.countryEl.value;
			} else {
				if (this.disableAction=='hide') {
					if (this.regionTextEl) {
						this.regionTextEl.style.display = '';
						this.regionTextEl.style.disabled = false;
					}
					this.regionSelectEl.style.display = 'none';
					this.regionSelectEl.disabled = true;
				} else if (this.disableAction=='disable') {
					if (this.regionTextEl) {
						this.regionTextEl.disabled = false;
					}
					this.regionSelectEl.disabled = true;
					if (this.clearRegionValueOnDisable) {
						this.regionSelectEl.value = '';
					}
				} else if (this.disableAction=='nullify') {
					this.regionSelectEl.options.length = 1;
					this.regionSelectEl.value = '';
					this.regionSelectEl.selectedIndex = 0;
					this.lastCountryId = '';
				}
				this.setMarkDisplay(this.regionSelectEl, false);

	//            // clone required stuff from select element and then remove it
	//            this._regionSelectEl.className = this.regionSelectEl.className;
	//            this._regionSelectEl.name      = this.regionSelectEl.name;
	//            this._regionSelectEl.id        = this.regionSelectEl.id;
	//            this._regionSelectEl.innerHTML = this.regionSelectEl.innerHTML;
	//            Element.remove(this.regionSelectEl);
	//            this.regionSelectEl = null;
			}
			varienGlobalEvents.fireEvent("address_country_changed", this.countryEl);
		}
	;
}